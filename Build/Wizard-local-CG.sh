#!/bin/bash
cd /Applications/Automation/mob_app/Build
TIMESTAMP=$(date +'%Y-%m-%d_%H.%M.%S')
echo "TIMESTAMP: $TIMESTAMP"

PC_NAME=$(hostname -s)
echo "PC_NAME: $PC_NAME"

echo "*******  USING CG LOCAL BUILD JAR FILE ********"

java -Djava.util.logging.config.file=sandbox/logging.properties -Xmx1g -cp jars/SeleniumEngine-CG-LocalBuild.jar com.bcbssc.webdriver.wizard.ExecutionWizard /Applications/Automation/mob_app 2> logs/log-$PC_NAME-$TIMESTAMP.log