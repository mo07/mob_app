#!/bin/bash
cd /Applications/Automation/mob_app/Build
TIMESTAMP=$(date +'%Y-%m-%d_%H.%M.%S')
echo "TIMESTAMP: $TIMESTAMP"

PC_NAME=$(hostname -s)
echo "PC_NAME: $PC_NAME"

echo "*******  USING SeleniumEngine-localbuild-22.01.10.jar JAR FILE ********"

java -Djava.util.logging.config.file=sandbox/logging.properties -Xmx1g -cp jars/SeleniumEngine-localbuild-22.01.10.jar com.bcbssc.webdriver.wizard.ExecutionWizard /Applications/Automation/mob_app 2> logs/log-$PC_NAME-$TIMESTAMP.log