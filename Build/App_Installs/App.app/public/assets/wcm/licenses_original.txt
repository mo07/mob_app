<hr>
This application utilizes the following software libraries under the license below:<br><br>
<div style=\"font-weight:bold;\">elementtree<\/div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-geolocation<\/div><BR>
<div style=\"font-weight:bold;\">reflect-metadata<\/div><BR>
<div style=\"font-weight:bold;\">cordova-android<\/div>
<div>Copyright 2015 Apache Cordova<\/div><BR>
<div style=\"font-weight:bold;\">cordova-common<\/div><BR>
<div style=\"font-weight:bold;\">cordova-ios<\/div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-add-swift-support<\/div><BR>
<div style=\"font-weight:bold;\">tslib<\/div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-compat<\/div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-device<\/div><BR>
<div style=\"font-weight:bold;\">rxjs<\/div>
<div>Copyright (c) 2015-2017 Google, Inc., Netflix, Inc., Microsoft Corp. and contributors</div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-inappbrowser<\/div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-ionic-webview<\/div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-keychain-touch-id-ios<\/div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-splashscreen<\/div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-statusbar<\/div>
<div>Copyright (c) 2017 jcesarmobile</div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-whitelist<\/div><BR>
<div style=\"font-weight:bold;\">localforage<\/div>
<div>Copyright 2014 Mozilla</div><BR>
<div style=\"font-weight:bold;\">typescript<\/div><BR>
<div style=\"font-weight:bold;\">sw-toolbox<\/div><BR>
<div style=\"font-weight:bold;\">jstransform<\/div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-local-notification<\/div>
<div>Copyright 2013 appPlant GmbH</div><BR>
<div style=\"font-weight:bold;\">localforage-cordovasqlitedriver<\/div>
<div>Copyright 2015 Thodoris Greasidis</div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-ionic-keyboard<\/div>
<div>Copyright 2014 Drifty Co.</div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-badge<\/div>
<div>Copyright 2013 appPlant GmbH</div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-android-fingerprint-auth<\/div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-passbook<\/div><BR>
<div style=\"font-weight:bold;\">serviceworker-cache-polyfill<\/div>
<div>Copyright 2015 Google, Inc.</div>
<BR>
<div>
Licensed under the Apache License, Version 2.0 (the \"License\");you may not use this file except in compliance with the License.You may obtain a copy of the License at<BR><BR>
http://www.apache.org/licenses/LICENSE-2.0<BR><BR>
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an \"AS IS\" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
<\/div>
<hr>
This application utilizes the following software libraries under the license below:<br><br>
<div style=\"font-weight:bold;\">source-map<\/div>
<div>Copyright (c) 2009-2011, Mozilla Foundation and contributors<\/div><BR>
<div style=\"font-weight:bold;\">esprima-fb<\/div><BR>
<div style=\"font-weight:bold;\">shelljs<\/div>
<div>Copyright (c) 2012, Artur Adib &lt;aadib@mozilla.com&gt;<\/div><BR>
<BR>
<div>
Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met:<BR>
<BR>
* Redistributions of source code must retain the above copyright notice, this 
  list of conditions and the following disclaimer.<BR>
<BR>
* Redistributions in binary form must reproduce the above copyright notice, 
  this list of conditions and the following disclaimer in the documentation 
  and/or other materials provided with the distribution.<BR>
<BR>
* Neither the names of the Mozilla Foundation nor the names of project 
  contributors may be used to endorse or promote products derived from this 
  software without specific prior written permission.<BR>
<BR>
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\" AND 
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE 
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.<BR>
<\/div>
<hr>
This application utilizes the following software libraries under the license below:<br><br>
<div style=\"font-weight:bold;\">glob<\/div>
<div>Copyright (c) Isaac Z. Schlueter and Contributors<\/div><BR>
<div style=\"font-weight:bold;\">wrappy<\/div>
<div>Copyright (c) Isaac Z. Schlueter and Contributors<\/div><BR>
<div style=\"font-weight:bold;\">minimatch<\/div>
<div>Copyright (c) Isaac Z. Schlueter and Contributors<\/div><BR>
<div style=\"font-weight:bold;\">nopt<\/div>
<div>Copyright (c) Isaac Z. Schlueter and Contributors<\/div><BR>
<div style=\"font-weight:bold;\">once<\/div>
<div>Copyright (c) Isaac Z. Schlueter and Contributors<\/div><BR>
<div style=\"font-weight:bold;\">inflight<\/div>
<div>Copyright (c) Isaac Z. Schlueter<\/div><BR>
<div style=\"font-weight:bold;\">osenv<\/div>
<div>Copyright (c) Isaac Z. Schlueter and Contributors<\/div><BR>
<div style=\"font-weight:bold;\">inherits<\/div>
<div>Copyright (c) Isaac Z. Schlueter<\/div><BR>
<div style=\"font-weight:bold;\">abbrev<\/div>
<div>Copyright (c) Isaac Z. Schlueter and Contributors<\/div><BR>
<div style=\"font-weight:bold;\">sax<\/div>
<div>Copyright (c) Isaac Z. Schlueter and Contributors<\/div><BR>
<div style=\"font-weight:bold;\">semver<\/div>
<div>Copyright (c) Isaac Z. Schlueter and Contributors<\/div>
<BR>
<div>
Permission to use, copy, modify, and/or distribute this software for any 
purpose with or without fee is hereby granted, provided that the above 
copyright notice and this permission notice appear in all copies.<BR>
<BR>
THE SOFTWARE IS PROVIDED \"AS IS\" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR 
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES 
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN 
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR 
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.<BR>
<\/div>
<hr>
This application utilizes the following software libraries under the license below:<br><br>
<div style=\"font-weight:bold;\">bplist-parser<\/div>
<div>Copyright (c) 2012 Near Infinity Corporation<\/div><BR>
<div style=\"font-weight:bold;\">brace-expansion<\/div>
<div>Copyright (c) 2013 Julian Gruber &lt;julian@juliangruber.com&gt;<\/div><BR>
<div style=\"font-weight:bold;\">com-sarriaroman-photoviewer<\/div><BR>
<div style=\"font-weight:bold;\">concat-map<\/div><BR>
<div style=\"font-weight:bold;\">angular\/forms<\/div>
<div>Copyright Google Inc. All Rights Reserved.</div><BR>
<div style=\"font-weight:bold;\">angular\/http<\/div>
<div>Copyright Google Inc. All Rights Reserved.</div><BR>
<div style=\"font-weight:bold;\">angular\/platform-browser-dynamic<\/div>
<div>Copyright Google Inc. All Rights Reserved.</div><BR>
<div style=\"font-weight:bold;\">angular\/platform-browser<\/div>
<div>Copyright Google Inc. All Rights Reserved.</div><BR>
<div style=\"font-weight:bold;\">angular\/tsc-wrapped<\/div>
<div>Copyright Google Inc. All Rights Reserved.</div><BR>
<div style=\"font-weight:bold;\">xml2js<\/div>
<div>Copyright 2010, 2011, 2012, 2013. All rights reserved.</div><BR>
<div style=\"font-weight:bold;\">ionic-native\/android-fingerprint-auth<\/div><BR>
<div style=\"font-weight:bold;\">ionic-native\/core<\/div><BR>
<div style=\"font-weight:bold;\">ionic-native\/firebase<\/div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-disable-ios11-statusbar<\/div>
<div>Copyright (c) 2017 jcesarmobile<\/div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-firebase<\/div>
<div>Copyright (c) 2016 Robert Arnesson AB<\/div><BR>
<div style=\"font-weight:bold;\">ionic-native\/geolocation<\/div><BR>
<div style=\"font-weight:bold;\">ionic-native\/in-app-browser<\/div><BR>
<div style=\"font-weight:bold;\">ionic-native\/keychain-touch-id<\/div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-ionic<\/div>
<div>Copyright 2016-present Drifty Co.</div><BR>
<div style=\"font-weight:bold;\">ionic-native\/local-notifications<\/div><BR>
<div style=\"font-weight:bold;\">util-deprecate<\/div>
<div>Copyright (c) 2014 Nathan Rajlich <nathan@tootallnate.net><\/div><BR>
<div style=\"font-weight:bold;\">unreachable-branch-transform<\/div>
<div>Copyright (c) 2015 Andres Suarez<\/div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-secure-storage<\/div>
<div>Copyright 2015 Crypho AS</div><BR>
<div style=\"font-weight:bold;\">ionic-native\/photo-viewer<\/div><BR>
<div style=\"font-weight:bold;\">ionic-native\/secure-storage<\/div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-streaming-media<\/div>
<div>Copyright (c) 2010-2014 Google, Inc. http:\/\/angularjs.org<\/div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-video-player<\/div>
<div>Copyright (c) 2014 Quentin Aupetit<\/div><BR>
<div style=\"font-weight:bold;\">ionic-native\/social-sharing<\/div><BR>
<div style=\"font-weight:bold;\">cordova-plugin-x-socialsharing<\/div><BR>
<div style=\"font-weight:bold;\">ionic-native\/splash-screen<\/div><BR>
<div style=\"font-weight:bold;\">ionic-native\/status-bar<\/div><BR>
<div style=\"font-weight:bold;\">cordova-sqlite-plugin<\/div><BR>
<div style=\"font-weight:bold;\">cordova-support-google-services<\/div>
<div>Copyright (c) 2017 Maksim Chemerisuk<\/div><BR>
<div style=\"font-weight:bold;\">core-util-is<\/div>
<div>Copyright Node.js contributors. All rights reserved.</div><BR>
<div style=\"font-weight:bold;\">ionic-native\/streaming-media<\/div><BR>
<div style=\"font-weight:bold;\">es3ify<\/div>
<div>Copyright (c) 2014 Ben Alpert<\/div><BR>
<div style=\"font-weight:bold;\">es6-promise-plugin<\/div>
<div>Copyright (c) 2014-15 Vlad Stirbu<\/div><BR>
<div style=\"font-weight:bold;\">esmangle-evaluator<\/div><BR>
<div style=\"font-weight:bold;\">angular\/common<\/div>
<div>Copyright Google Inc. All Rights Reserved.</div><BR>
<div style=\"font-weight:bold;\">ionic-native\/touch-id<\/div><BR>
<div style=\"font-weight:bold;\">falafel<\/div><BR>
<div style=\"font-weight:bold;\">foreach<\/div>
<div>Copyright (c) 2013 Manuel Stofer<\/div><BR>
<div style=\"font-weight:bold;\">ionic-native\/video-player<\/div><BR>
<div style=\"font-weight:bold;\">immediate<\/div>
<div>Copyright (c) 2012 Barnesandnoble.com, llc, Donavon West, Domenic Denicola, Brian Cavalier<\/div><BR>
<div style=\"font-weight:bold;\">xtend<\/div>
<div>Copyright (c) 2012-2014 Raynos.<\/div><BR>
<div style=\"font-weight:bold;\">inline-process-browser<\/div><BR>
<div style=\"font-weight:bold;\">intl<\/div>
<div>Copyright (c) 1991-2013 Unicode, Inc. All rights reserved. Distributed under<\/div><BR>
<div style=\"font-weight:bold;\">ionic-angular<\/div><BR>
<div style=\"font-weight:bold;\">underscore<\/div>
<div>Copyright (c) 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative<\/div><BR>
<div style=\"font-weight:bold;\">ionicons<\/div>
<div>Copyright (c) 2015-present Ionic (http:\/\/ionic.io\/)<\/div><BR>
<div style=\"font-weight:bold;\">ios-sim<\/div>
<div>Copyright (c) 2014 Shazron Abdullah<\/div><BR>
<div style=\"font-weight:bold;\">isarray<\/div>
<div>Copyright (c) 2013 Julian Gruber &lt;julian@juliangruber.com&gt;<\/div><BR>
<div style=\"font-weight:bold;\">ionic\/storage<\/div>
<div>Copyright (c) 2016 Ionic<\/div><BR>
<div style=\"font-weight:bold;\">lie<\/div>
<div>#Copyright (c) 2014 Calvin Metcalf</div><BR>
<div style=\"font-weight:bold;\">types\/localforage<\/div><BR>
<div style=\"font-weight:bold;\">angular\/animations<\/div>
<div>Copyright Google Inc. All Rights Reserved.</div><BR>
<div style=\"font-weight:bold;\">lodash<\/div>
<div>Copyright jQuery Foundation and other contributors &lt;https://jquery.org/&gt;</div><BR>
<div style=\"font-weight:bold;\">angular\/compiler-cli<\/div>
<div>Copyright Google Inc. All Rights Reserved.</div><BR>
<div style=\"font-weight:bold;\">acorn<\/div>
<div>Copyright (C) 2012-2014 by various contributors (see AUTHORS)<\/div><BR>
<div style=\"font-weight:bold;\">angular\/compiler<\/div>
<div>Copyright Google Inc. All Rights Reserved.</div><BR>
<div style=\"font-weight:bold;\">minimist<\/div><BR>
<div style=\"font-weight:bold;\">mkdirp<\/div>
<div>Copyright 2010 James Halliday (mail@substack.net)</div><BR>
<div style=\"font-weight:bold;\">mkpath<\/div>
<div>Copyright (C) 2012 Jonathan Rajavuori<\/div><BR>
<div style=\"font-weight:bold;\">node-uuid<\/div>
<div>Copyright (c)  2010-2012 Robert Kieffer <\/div><BR>
<div style=\"font-weight:bold;\">node-version-compare<\/div><BR>
<div style=\"font-weight:bold;\">android-versions<\/div><BR>
<div style=\"font-weight:bold;\">object-keys<\/div>
<div>Copyright (C) 2013 Jordan Harband<\/div><BR>
<div style=\"font-weight:bold;\">os-homedir<\/div>
<div>Copyright (c) Sindre Sorhus &lt;sindresorhus@gmail.com&gt; (sindresorhus.com)<\/div><BR>
<div style=\"font-weight:bold;\">os-tmpdir<\/div>
<div>Copyright (c) Sindre Sorhus &lt;sindresorhus@gmail.com&gt; (sindresorhus.com)<\/div><BR>
<div style=\"font-weight:bold;\">ansi<\/div>
<div>Copyright (c) 2012 Nathan Rajlich &lt;nathan@tootallnate.net&gt;<\/div><BR>
<div style=\"font-weight:bold;\">ast-types<\/div>
<div>Copyright (c) 2013 Ben Newman &lt;bn@cs.stanford.edu&gt;<\/div><BR>
<div style=\"font-weight:bold;\">path-is-absolute<\/div>
<div>Copyright (c) Sindre Sorhus &lt;sindresorhus@gmail.com&gt; (sindresorhus.com)<\/div><BR>
<div style=\"font-weight:bold;\">path-to-regexp<\/div>
<div>Copyright (c) 2014 Blake Embrey (hello@blakeembrey.com)<\/div><BR>
<div style=\"font-weight:bold;\">pegjs<\/div>
<div>Copyright (c) 2010-2015 David Majda<\/div><BR>
<div style=\"font-weight:bold;\">plist<\/div>
<div>Copyright (c) 2010-2014 Nathan Rajlich &lt;nathan@tootallnate.net&gt;<\/div><BR>
<div style=\"font-weight:bold;\">private<\/div>
<div>Copyright (c) 2014 Ben Newman &lt;bn@cs.stanford.edu&gt;<\/div><BR>
<div style=\"font-weight:bold;\">tsickle<\/div>
<div>Copyright (c) 2014-2016 Google, Inc.<\/div><BR>
<div style=\"font-weight:bold;\">q<\/div>
<div>Copyright 2009–2017 Kristopher Michael Kowal. All rights reserved.</div><BR>
<div style=\"font-weight:bold;\">readable-stream<\/div>
<div>Copyright Joyent, Inc. and other Node contributors. All rights reserved.</div><BR>
<div style=\"font-weight:bold;\">recast<\/div>
<div>Copyright (c) 2012 Ben Newman &lt;;bn@cs.stanford.edu&gt;<\/div><BR>
<div style=\"font-weight:bold;\">balanced-match<\/div>
<div>Copyright (c) 2013 Julian Gruber &lt;julian@juliangruber.com&gt;<\/div><BR>
<div style=\"font-weight:bold;\">angular\/core<\/div>
<div>Copyright Google Inc. All Rights Reserved.</div><BR>
<div style=\"font-weight:bold;\">base64-js<\/div>
<div>Copyright (c) 2014<\/div><BR>
<div style=\"font-weight:bold;\">xmlbuilder<\/div>
<div>Copyright (c) 2013 Ozgur Ozcitak<\/div><BR>
<div style=\"font-weight:bold;\">through<\/div>
<div>Copyright (c) 2011 Dominic Tarr<\/div><BR>
<div style=\"font-weight:bold;\">through2<\/div>
<div>Copyright 2013, Rod Vagg (the \"Original Author\") All rights reserved.</div><BR>
<div style=\"font-weight:bold;\">simctl<\/div>
<div>Copyright (c) 2014 Shazron Abdullah<\/div><BR>
<div style=\"font-weight:bold;\">simple-plist<\/div>
<div>Copyright (c) 2013 Joe Wollard<\/div><BR>
<div style=\"font-weight:bold;\">source-map-support<\/div>
<div>Copyright (c) 2014 Evan Wallace<\/div><BR>
<div style=\"font-weight:bold;\">bplist-creator<\/div>
<div>Copyright (c) 2012 Near Infinity Corporation<\/div><BR>
<div style=\"font-weight:bold;\">symbol-observable<\/div>
<div>Copyright (c) Sindre Sorhus &lt;sindresorhus@gmail.com&gt; (sindresorhus.com)</div>
<div>Copyright (c) Ben Lesh &lt;ben@benlesh.com&gt;<\/div><BR>
<div style=\"font-weight:bold;\">string_decoder<\/div>
<div>Copyright Joyent, Inc. and other Node contributors.</div><BR>
<div style=\"font-weight:bold;\">zone.js<\/div>
<div>Copyright (c) 2016 Google, Inc.<\/div><BR>
<div style=\"font-weight:bold;\">amdefine<\/div>
<div>Copyright (c) 2011-2016, The Dojo Foundation<\/div><BR>
<div style=\"font-weight:bold;\">xmldom<\/div><BR>
<div style=\"font-weight:bold;\">unorm<\/div>
<div>Copyright (c) 2008-2013 Matsuza &lt;matsuza@gmail.com&gt;, Bjarke Walling &lt;bwp@bwp.dk&gt;</div><BR>
<div style=\"font-weight:bold;\">base62<\/div>
<div>Copyright (c) 2012 Andrew Nesbitt<\/div><BR>
<BR>
<div>
Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the \"Software\"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:<BR>
<BR>
The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software.<BR>
<BR>
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
THE SOFTWARE.<BR>
<\/div>
<hr>
This application utilizes the following software libraries under the license below:<br><br>
<div style=\"font-weight:bold;\">stream-buffers<\/div><BR>
<div style=\"font-weight:bold;\">big-integer<\/div>
<BR>
<div>
This is free and unencumbered software released into the public domain.<BR>
<BR>  
Anyone is free to copy, modify, publish, use, compile, sell, or distribute this software, either in source code form or as a compiled binary, for any purpose, commercial or non-commercial, and by any means.<BR>
<BR>
In jurisdictions that recognize copyright laws, the author or authors of this software dedicate any and all copyright interest in the software to the public domain. We make this dedication for the benefit of the public at large and to the detriment of our heirs and successors. We intend this dedication to be an overt act of relinquishment in perpetuity of all present and future rights to this software under copyright law.<BR>
<BR>
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.<BR>
<BR>
For more information, please refer to &lt;http://unlicense.org\/&gt;
<\/div>
<hr>
This application utilizes the following software libraries:<br><br>
<div style=\"font-weight:bold;\">properties-parser<\/div><BR>
<div style=\"font-weight:bold;\">ionic\/pro<\/div><BR>
<div style=\"font-weight:bold;\">tail<\/div><BR>
<div style=\"font-weight:bold;\">xcode<\/div>
