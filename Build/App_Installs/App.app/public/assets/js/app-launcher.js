/**
 * Code to consume plugin 'cordova-plugin-app-launcher'
 * Solution below adapted from response found here:
 * https://forum.ionicframework.com/t/open-other-app-using-package-id-name/117089/2
 *
 * lr51 - Matt Bowen
 */

module.exports = {
    packageLaunch: packageName => {
        return new Promise((resolve, reject) => {
            // launch app using package name (for Android devices)
            window.plugins.launcher.launch({
                packageName: packageName
            },
            success => {
                resolve(success);
            },
            error => {
                reject(error);
            }
            );
        });
    },
    uriLaunch: appUriStr => {
        return new Promise((resolve, reject) => {
            // launch app using URI (for iOS devices)
            window.plugins.launcher.launch({
                uri: appUriStr
            },
            success => {
                resolve(success);
            },
            error => {
                reject(error);
            }
            );
        });
    }
};
