package com.bcbssc.webdriver.handler;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.ContextAware;
import org.openqa.selenium.WebDriver;

import com.bcbssc.webdriver.framework.ExecutionConstants;
import com.bcbssc.webdriver.framework.OperationHandler;
import com.bcbssc.webdriver.framework.TestStep;
import com.bcbssc.webdriver.framework.TestStepResult;
import com.bcbssc.webdriver.util.ExecutionUtil;

import io.appium.java_client.InteractsWithApps;

public class IOSOperationHandler extends MobileOperationHandler {

	protected static final Logger LOGGER = Logger.getLogger(IOSOperationHandler.class.getName());

	public IOSOperationHandler(OperationHandler operationHandler) {
		super(operationHandler);
	}

	@Override
	public TestStepResult process(TestStep testStep, WebDriver driver) {

		LOGGER.log(Level.INFO, "{0} PROCESSING testStep: {1}", new Object[] { this, testStep });

		TestStepResult testStepResult = null;

		switch (testStep.getOperation()) {

		case IOS_WAIT:
			try {
				if (!testStep.isAttributeValueNumeric()) {
					throw new IllegalArgumentException("numeric attribute value missing for test step: " + testStep);
				}
				ExecutionUtil.sleep(Integer.parseInt(testStep.getAttributeValue()));
				testStepResult = new TestStepResult(testStep, true);
			} catch (Exception e) {
				testStepResult = new TestStepResult(testStep, e);
			}
			break;

		case IOS_SWITCH_FRAME:
			try {
				LOGGER.log(Level.INFO, "Switching to FRAME: {1} for IOS",
						new Object[] { testStep.getAttributeValue() });

				driver.switchTo().frame(testStep.getAttributeValue());
				testStepResult = new TestStepResult(testStep, true);
			} catch (Exception e) {
				testStepResult = new TestStepResult(testStep, e);
			}
			break;

		case IOS_CLICK_COORDINATES:
			try {
				this.clickCoordinates(testStep, driver);
				testStepResult = new TestStepResult(testStep, true);
			} catch (Exception e) {
				testStepResult = new TestStepResult(testStep, e);
			}
			break;

		case IOS_TAP:
			try {
				this.tapElement(testStep, driver);
				testStepResult = new TestStepResult(testStep, true);
			} catch (Exception e) {
				testStepResult = new TestStepResult(testStep, e);
			}
			break;

		case IOS_LONG_PRESS:
			try {
				this.longPress(testStep, driver);
				testStepResult = new TestStepResult(testStep, true);
			} catch (Exception e) {
				testStepResult = new TestStepResult(testStep, e);
			}
			break;

		case IOS_SWIPE_SCREEN:
			try {
				this.swipeScreen(testStep, driver);
				testStepResult = new TestStepResult(testStep, true);
			} catch (Exception e) {
				testStepResult = new TestStepResult(testStep, e);
			}
			break;

		case IOS_HORIZONTAL_SWIPE:
			try {
				this.horizontalSwipe(testStep, driver);
				testStepResult = new TestStepResult(testStep, true);
			} catch (Exception e) {
				testStepResult = new TestStepResult(testStep, e);
			}
			break;

		case IOS_VERTICAL_SWIPE:
			try {
				this.verticalSwipe(testStep, driver);
				testStepResult = new TestStepResult(testStep, true);
			} catch (Exception e) {
				testStepResult = new TestStepResult(testStep, e);
			}
			break;

		case IOS_ACTIVATE_APP:
			try {
				this.activateApp(testStep, driver);
				testStepResult = new TestStepResult(testStep, true);
			} catch (Exception e) {
				testStepResult = new TestStepResult(testStep, e);
			}
			break;

		case IOS_TERMINATE_APP:
			try {
				this.terminateApp(testStep, driver);
				testStepResult = new TestStepResult(testStep, true);
			} catch (Exception e) {
				testStepResult = new TestStepResult(testStep, e);
			}
			break;

		case IOS_CLOSE_APP:
			try {
				this.closeApp(testStep, driver);
				testStepResult = new TestStepResult(testStep, true);
			} catch (Exception e) {
				testStepResult = new TestStepResult(testStep, e);
			}
			break;

		case IOS_LAUNCH_APP:
			try {
				this.launchApp(testStep, driver);
				testStepResult = new TestStepResult(testStep, true);
			} catch (Exception e) {
				testStepResult = new TestStepResult(testStep, e);
			}
			break;

		case IOS_SWITCH_APPIUM_CONTEXT_WEB:
			try {
				this.switchAppiumContextToWebView(testStep, driver);
				testStepResult = new TestStepResult(testStep, true);
			} catch (Exception e) {
				testStepResult = new TestStepResult(testStep, e);
			}
			break;

		case IOS_SWITCH_APPIUM_CONTEXT_NATIVE:
			try {
				this.switchAppiumContextToNativeView(driver);
				testStepResult = new TestStepResult(testStep, true);
			} catch (Exception e) {
				testStepResult = new TestStepResult(testStep, e);
			}
			break;

		case IOS_IONIC_SET_DATE:
			try {
				this.setIonicDate(testStep, driver);
				testStepResult = new TestStepResult(testStep, true);
			} catch (Exception e) {
				testStepResult = new TestStepResult(testStep, e);
			}
			break;

		case IOS_STORE_WEB_CONTEXT:			
			try {
				this.storeWebViewContext(testStep, driver);
				testStepResult = new TestStepResult(testStep, true);
			} catch (Exception e) {
				testStepResult = new TestStepResult(testStep, e);
			}
			break;

		default:
			break;
		}

		return testStepResult;
	}

	@Override
	protected void closeApp(TestStep testStep, WebDriver driver) {
		((InteractsWithApps) driver).closeApp();
	}

	@Override
	protected void launchApp(TestStep testStep, WebDriver driver) {
		((InteractsWithApps) driver).launchApp();
	}

	@Override
	protected void activateApp(TestStep testStep, WebDriver driver) {
		((InteractsWithApps) driver).activateApp(testStep.getAttributeValue());
	}

	@Override
	protected void terminateApp(TestStep testStep, WebDriver driver) {
		((InteractsWithApps) driver).terminateApp(testStep.getAttributeValue());
	}

}
