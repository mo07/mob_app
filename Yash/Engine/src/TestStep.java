	public boolean isAttributeValueNumericAndLessThan(int maxValue) {
		/*
		 * BAG - 2/3/21 - Added !this.hasDynamicLocator() to avoid attempting to parse
		 * any values intended for the locator.
		 */

		 /* Yash - Added the below logic to make WAIT* operations to work with wild card and attr key cols to provide wait time */
		if (!this.getOperation().name().contains("WAIT")) {
			return !this.hasDynamicLocator() && this.isAttributeValueNumeric() && new Long(attributeValue) < maxValue;
		} else {
			if (this.isAttributeValueNumeric()) {
				if (this.hasWildcardValues()) {
					return new Long(attributeValue) < maxValue;
				} else if (this.hasDynamicLocator()) {
					return false;
				} else {
					return new Long(attributeValue) < maxValue;
				}
			}
			return false;
		}
	}

