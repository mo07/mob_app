package com.bcbssc.desktop;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.ContextAware;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.bcbssc.automation.exception.InvalidInputException;
import com.bcbssc.webdriver.exception.InvalidStateException;
import com.bcbssc.webdriver.framework.ExecutionConstants;
import com.bcbssc.webdriver.framework.ExecutionScenario;

import io.appium.java_client.MobileDriver;

public class WebDriverWrapper implements WebDriver, JavascriptExecutor {

	protected static final Logger LOGGER = Logger.getLogger(WebDriverWrapper.class.getName());

	private WebDriver driver;
	private ExecutionScenario scenario;
	private boolean excludeHiddenElements;

	private boolean windowSwitchingEnabled = true;
	private boolean driverReferencingFrame;

	public WebDriverWrapper(WebDriver driver, ExecutionScenario scenario) {
		this(driver, scenario, (scenario != null && !scenario.includeHiddenElementsInSearchResults()));
	}

	public WebDriverWrapper(WebDriver driver, ExecutionScenario scenario, boolean excludeHiddenElements) {
		if (scenario == null) {
			throw new InvalidInputException("argument [scenario] is required");
		}
		this.driver = driver;
		this.scenario = scenario;
		this.excludeHiddenElements = excludeHiddenElements;
	}

	public List<WebElement> getElementsAcrossAllWindows(By by, WebElement parentElement) {

		LOGGER.log(Level.INFO, "[{0}] driverReferencingFrame: {1}",
				new Object[] { scenario.getExecutionId(), driverReferencingFrame });

		/*
		 * BAG - 1/30/19 - If we switched to iFrames on the previous TestStep, then we
		 * need to reset the driver to the default content (ie. the main window).
		 * Otherwise, elements that aren't on that iFrame won't be located.
		 */
		if (this.driverReferencingFrame) {
			try {
				LOGGER.log(Level.INFO, "[{0}] Currently pointing to Frame. Switching to default content",
						scenario.getExecutionId());
				this.switchTo().defaultContent();
			} catch (NoSuchWindowException nswe) {
				throw new InvalidStateException("Error attempting to switch to defaultContent. Please investigate.",
						nswe);
			}
			this.driverReferencingFrame = false;
		}

		Iterator<String> windowHandleIter = this.getOtherWindowHandles().iterator();

		List<WebElement> elements = new ArrayList<>();

		while (true) {

			LOGGER.log(Level.INFO, "[{0}] PINGING: {1}", new Object[] { scenario.getExecutionId(), by });
			try {
				elements = this.findAndFilterElements(parentElement, by);

			} catch (NoSuchWindowException nswe) {
				LOGGER.log(Level.INFO, "[{0}] Gracefully handling NoSuchWindowException", scenario.getExecutionId());
			}

			if (!this.windowSwitchingEnabled) {
				LOGGER.log(Level.INFO, "[{0}] WINDOW SWITCHING IS DISABLED", scenario.getExecutionId());
			}

			if (!elements.isEmpty() || !this.windowSwitchingEnabled || !windowHandleIter.hasNext()) {
				break;
			} else {

				this.switchToNextWindow(windowHandleIter.next());
			}
		}

		LOGGER.log(Level.INFO, "[{0}] returning [{1}] displayed elements",
				new Object[] { scenario.getExecutionId(), elements.size() });
		return elements;
	}

	private List<WebElement> findAndFilterElements(WebElement parentElement, By by) {

		List<WebElement> elements = null;

		if (parentElement != null) {
			LOGGER.log(Level.INFO, "[{0}] LOCATING ELEMENTS UNDER PARENT ELEMENT: {1}",
					new Object[] { scenario.getExecutionId(), parentElement });
			elements = this.filter(parentElement.findElements(by));
		} else {
			long currentTimeInMillis = Calendar.getInstance().getTimeInMillis();
			elements = this.filter(this.findElements(by));
			LOGGER.log(Level.INFO, "[{0}] --- Element lookup performed in {1} ms", new Object[] {
					scenario.getExecutionId(), (Calendar.getInstance().getTimeInMillis() - currentTimeInMillis) });
		}

		if (elements.isEmpty() && this.framesExist()) {
			elements = this.findElementsAcrossAllFrames(by, false);
		}
		return elements;
	}

	private void switchToNextWindow(String nextWindowHandle) {

		if (scenario.hasDetachedWindowHandles() && scenario.getDetachedWindowHandles().containsKey(nextWindowHandle)) {
			LOGGER.log(Level.INFO, "[{0}] SKIPPING DETACHED WINDOW HANDLE: {1}",
					new Object[] { scenario.getExecutionId(), nextWindowHandle });
		} else {
			this.switchTo().window(nextWindowHandle);
			LOGGER.log(Level.INFO, "[{0}] switching to windowHandle: {1}",
					new Object[] { scenario.getExecutionId(), this.getWindowHandle() });
		}
	}

	/*
	 * Locating elements within HTML frames take a little work, since WebDriver
	 * treats frames just like windows. We must look for all frames within the
	 * window, then switch to each one, looking for elements each time. The trick
	 * is, before switching to the next frame, we must switch BACK to the parent
	 * frame first. Otherwise, WebDriver will attempt to find the next frame WITHIN
	 * THE CURRENT FRAME (because that's were the reference point is set).
	 * 
	 * BAG - 7/25/19 - Added boolean argument to only dump the frame source for
	 * operation DUMP_PAGE_BODY.
	 */
	public List<WebElement> findElementsAcrossAllFrames(By by, boolean dumpFrameSourceOnly) {

		LOGGER.log(Level.INFO, ExecutionConstants.TEXT_START);

		List<WebElement> elementsFound = new ArrayList<>();
		List<String> frameNames = new ArrayList<>();

		List<WebElement> frames = null;

		/*
		 * BAG - 9/27/19 - Handle StaleElementReferenceException on Frame setup.
		 */
		try {

			frames = this.getFrames();
			LOGGER.log(Level.INFO, "[{0}] # frames found: {1}",
					new Object[] { scenario.getExecutionId(), frames.size() });

			for (WebElement frame : frames) {
				frameNames.add(frame.getAttribute("name"));
			}

		} catch (StaleElementReferenceException sere) {

			LOGGER.log(Level.INFO, "[{0}] Handling StaleElementReferenceException when processing Frames. Retrying.",
					scenario.getExecutionId());

			frameNames = new ArrayList<>();

			frames = this.getFrames();
			LOGGER.log(Level.INFO, "[{0}] # frames found: {1}",
					new Object[] { scenario.getExecutionId(), frames.size() });

			for (WebElement frame : frames) {
				frameNames.add(frame.getAttribute("name"));
			}
		}

		try {
			elementsFound = this.processFrames(frames, frameNames, by, dumpFrameSourceOnly);

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
		}

		return elementsFound;
	}

	private List<WebElement> processFrames(List<WebElement> frames, List<String> frameNames, By by,
			boolean dumpFrameSourceOnly) {

		List<WebElement> elementsFound = new ArrayList<>();

		for (int i = 0; i < frames.size(); i++) {
			LOGGER.log(Level.INFO, "[{0}] switching to FRAME[{1}]: {2}",
					new Object[] { scenario.getExecutionId(), i, frameNames.get(i) });
			this.switchTo().frame(i);
			this.driverReferencingFrame = true;

			if (dumpFrameSourceOnly) {
				LOGGER.log(Level.INFO, "[{0}] FRAME SOURCE:\n{1}",
						new Object[] { scenario.getExecutionId(), this.getPageSource() });
			} else {
				elementsFound = this.filter(this.findElementsInFrame(by));
			}

			if (!elementsFound.isEmpty()) {
				LOGGER.log(Level.INFO, "[{0}] element(s) were found on FRAME[{1}]: {2}",
						new Object[] { scenario.getExecutionId(), i, frameNames.get(i) });
				/*
				 * BAG - 1/30/19 - Note that we don't switch back to the parent window here,
				 * because methods such as waitUntilValue() will need to interrogate the element
				 * for its value, and switching off of the current frame will cause a
				 * StaleElementReferenceException, because the driver must be pointing to the
				 * frame that contains the element for us to be able to reference the element.
				 */
				return elementsFound;
			} else {

				if (dumpFrameSourceOnly || scenario.isSearchMultiLevelFrames()) {
					/*
					 * 5/22/18: Handle child frames under a parent frame (basically, handle frames
					 * up to 2 levels deep). This was necessary for SAP (ie. Business Objects),
					 * which goes absolutely nutty with embedded IFrames. However, two levels deep
					 * is all we'll handle at this point.
					 */
					elementsFound = this.processChildFrames(this.getFrames(), by, frameNames.get(i),
							dumpFrameSourceOnly);

					if (!elementsFound.isEmpty()) {
						return elementsFound;
					}
				}

				LOGGER.log(Level.INFO, "[{0}] switching back to PARENT", scenario.getExecutionId());
				this.switchTo().parentFrame();
				/*
				 * Since we're switching back to the parent window here, set the
				 * driverReferencingFrame boolean to false.
				 */
				this.driverReferencingFrame = false;
			}
		}
		return elementsFound;
	}

	private List<WebElement> processChildFrames(List<WebElement> childFrames, By by, String parentFrameName,
			boolean dumpFrameSourceOnly) {

		List<WebElement> elementsFound = new ArrayList<>();

		for (int j = 0; j < childFrames.size(); j++) {

			WebElement childFrame = childFrames.get(j);
			String childFrameName = childFrame.getAttribute("name");

			LOGGER.log(Level.INFO, "[{0}] switching to CHILD FRAME: {1}",
					new Object[] { scenario.getExecutionId(), childFrameName });
			this.switchTo().frame(childFrame);

			if (dumpFrameSourceOnly) {
				LOGGER.log(Level.INFO, "[{0}] CHILD FRAME SOURCE:\n{1}",
						new Object[] { scenario.getExecutionId(), this.getPageSource() });
			} else {
				elementsFound = this.filter(this.findElementsInFrame(by));
			}

			if (!elementsFound.isEmpty()) {
				LOGGER.log(Level.INFO, "[{0}] element(s) were found on CHILD FRAME: {1}",
						new Object[] { scenario.getExecutionId(), childFrameName });
				/*
				 * BAG - 1/30/19 - Note that we don't switch back to the parent window here,
				 * because methods such as waitUntilValue() will need to interrogate the element
				 * for its value, and switching off of the current frame will cause a
				 * StaleElementReferenceException, because the driver must be pointing to the
				 * frame that contains the element for us to be able to reference the element.
				 */
				return elementsFound;
			} else {
				LOGGER.log(Level.INFO, "[{0}] switching back to PARENT FRAME: {1}",
						new Object[] { scenario.getExecutionId(), parentFrameName });
				/*
				 * BAG - 1/30/19 - We must switch back to the parent frame before we attempt to
				 * switch to the next child frame.
				 */
				this.switchTo().parentFrame();
			}
		}
		return elementsFound;
	}

	public List<WebElement> filter(List<WebElement> elements) {

		if (!this.excludeHiddenElements) {
			LOGGER.log(Level.INFO, "[{0}] excludeHiddenElements is FALSE. Bypassing filter. Returning [{1}] elements",
					new Object[] { scenario.getExecutionId(), elements.size() });
			return elements;
		}

		List<WebElement> displayedElements = new ArrayList<>();
		for (WebElement element : elements) {
			try {
				/*
				 * BAG - 12/20/18 - Found out today that any html inputs that are type=file (ie.
				 * file upload) will get filtered out here, because the element is considered
				 * 'hidden' (isDisplayed returns false) unless the file selection dialog is
				 * displayed. For now the workaround is to invoke webDriverWrapper.findElement()
				 * directly. See SelectFileForUpload for an example.
				 */
				if (element.isDisplayed()) {
					displayedElements.add(element);
				} else {
					LOGGER.log(Level.INFO, "[{0}] FILTERING OUT NON-DISPLAYED ELEMENT [{1}]: ID:[{2}] NAME:[{3}]",
							new Object[] { scenario.getExecutionId(), element, element.getAttribute("id"),
									element.getAttribute("name") });
				}
			} catch (StaleElementReferenceException sere) {
				LOGGER.log(Level.WARNING, "[{0}] IGNORING STALE ELEMENT: {1}",
						new Object[] { scenario.getExecutionId(), element });
			}
		}
		LOGGER.log(Level.INFO, "[{0}] returning [{1}] displayed elements",
				new Object[] { scenario.getExecutionId(), displayedElements.size() });
		return displayedElements;
	}

	public boolean framesExist() {
		boolean framesExist = !this.getFrames().isEmpty();
		LOGGER.log(Level.INFO, "[{0}] frames exist?: {1}", new Object[] { scenario.getExecutionId(), framesExist });
		return framesExist;
	}

	/**
	 * Returns all frames/iframes defined within the current document.
	 */
	public List<WebElement> getFrames() {
		// Return empty list for mobile app drivers; locating via a tagName is not
		// supported with the Appium Driver
		if (MobileDriver.class.isAssignableFrom(driver.getClass())) {
			return new ArrayList<>();
		}

		List<WebElement> list = this.findElements(By.tagName("frame"));
		list.addAll(this.findElements(By.tagName("iframe"))); // 4/17/18 - Handle iframes as well
		return list;
	}

	public List<String> getOtherWindowHandles() {

		List<String> otherHandles = new ArrayList<>();

		String currentHandle = null;

		try {
			currentHandle = this.getWindowHandle();
			LOGGER.log(Level.INFO, "[{0}] currentHandle: {1}",
					new Object[] { scenario.getExecutionId(), currentHandle });
		} catch (Exception e) {
			LOGGER.log(Level.INFO, "[{0}] No current window handle", scenario.getExecutionId());
		}

		for (String windowHandle : this.getWindowHandles()) {
			if (!windowHandle.equalsIgnoreCase(currentHandle)) {
				otherHandles.add(windowHandle);
			}
		}
		LOGGER.log(Level.INFO, "[{0}] otherHandles: {1}", new Object[] { scenario.getExecutionId(), otherHandles });
		return otherHandles;
	}

	/**
	 * Recursive process that switches to the frame for the given name.
	 * 
	 * @param attributeValue
	 * @return <code>true</code> if the frame was found and switched to,
	 *         <code>false</code> otherwise.
	 */
	public boolean switchToFrame(String attributeValue) {

		LOGGER.log(Level.INFO, "[{0}] looking for FRAME: {1}",
				new Object[] { scenario.getExecutionId(), attributeValue });
		this.switchTo().defaultContent();

		// yash - For Mobile only top level frame is supported. Child frames cannot be inspected as locating via a tagName is not supported
		if (MobileDriver.class.isAssignableFrom(driver.getClass())) {
			
			LOGGER.log(Level.INFO, "[{0}] switching to FRAME: {1} for Mobile",
					new Object[] { scenario.getExecutionId(), attributeValue });

			this.switchTo().frame(attributeValue);
			this.driverReferencingFrame = false;
			return true;
		}

		if (this.inspectFrames(this.getFrames(), attributeValue, 1))

		{
			return true;
		}

		this.switchTo().defaultContent();
		return false;
	}

	private boolean inspectFrames(List<WebElement> frames, String attributeValue, int level) {

		LOGGER.log(Level.INFO, "[{0}] number of frames at level [{1}]: {2}",
				new Object[] { scenario.getExecutionId(), level, frames.size() });

		for (WebElement theFrame : frames) {

			LOGGER.log(Level.INFO, "[{0}] checking FRAME name [{1}] at level {2}",
					new Object[] { scenario.getExecutionId(), theFrame.getAttribute("name"), level });

			if (theFrame.getAttribute("name").equalsIgnoreCase(attributeValue)) {
				LOGGER.log(Level.INFO, "[{0}] FRAME FOUND. switching to it: {1}",
						new Object[] { scenario.getExecutionId(), theFrame });
				this.switchTo().frame(theFrame);

				/*
				 * We set driverReferencingFrame to false here so that the subsequent operation
				 * does not switch the driver back to default content. Not happy with 'tricking'
				 * the logic here, but...
				 */
				this.driverReferencingFrame = false;
				return true;

			} else {

				LOGGER.log(Level.INFO, "[{0}] Frame not found. Checking child frames of level {1}",
						new Object[] { scenario.getExecutionId(), level });

				// Since the frame was not found, we need to recursively check all child frames.
				this.switchTo().frame(theFrame);
				if (this.inspectFrames(this.getFrames(), attributeValue, level + 1)) {
					return true;
				}
				LOGGER.log(Level.INFO, "[{0}] switching back to PARENT", scenario.getExecutionId());
				this.switchTo().parentFrame();
			}
		}

		return false;
	}

	public boolean isExcludeHiddenElements() {
		return excludeHiddenElements;
	}

	public void setExcludeHiddenElements(boolean excludeHiddenElements) {
		LOGGER.log(Level.INFO, "[{0}] Setting excludeHiddenElements to: {1}",
				new Object[] { scenario.getExecutionId(), excludeHiddenElements });
		this.excludeHiddenElements = excludeHiddenElements;
	}

	public boolean isDriverReferencingFrame() {
		return driverReferencingFrame;
	}

	public boolean isWindowSwitchingEnabled() {
		return windowSwitchingEnabled;
	}

	public void setWindowSwitchingEnabled(boolean windowSwitchingEnabled) {
		this.windowSwitchingEnabled = windowSwitchingEnabled;
	}

	public int getWindowCount() {
		return driver.getWindowHandles().size();
	}

	@Override
	public void close() {
		this.driver.close();
	}

	@Override
	public WebElement findElement(By by) {
		LOGGER.log(Level.INFO, "[{0}] By {1}", new Object[] { scenario.getExecutionId(), by });
		return this.driver.findElement(by);
	}

	@Override
	public List<WebElement> findElements(By by) {
		LOGGER.log(Level.INFO, "[{0}] By {1}", new Object[] { scenario.getExecutionId(), by });
		return this.driver.findElements(by);
	}

	public List<WebElement> findElementsInFrame(By by) {
		return this.findElements(by);
	}

	@Override
	public void get(String arg0) {
		this.driver.get(arg0);
	}

	@Override
	public String getCurrentUrl() {
		return this.driver.getCurrentUrl();
	}

	@Override
	public String getPageSource() {
		return this.driver.getPageSource();
	}

	@Override
	public String getTitle() {
		return this.driver.getTitle();
	}

	@Override
	public String getWindowHandle() {
		// Adding override for mobile/Appium as this driver method is not supported for
		// non-webview contexts
		if (scenario.isMobile()
				&& !((ContextAware) driver).getContext().startsWith(ExecutionConstants.WEBVIEW_CONTEXT_PREFIX)
				&& !((ContextAware) driver).getContext().equals(ExecutionConstants.WEBVIEW_CONTEXT_CHROME)) {
			return null;
		}
		return this.driver.getWindowHandle();
	}

	@Override
	public Set<String> getWindowHandles() {
		// Adding override for mobile/Appium as this driver method is not supported for
		// non-webview contexts
		if (scenario.isMobile()
				&& !((ContextAware) driver).getContext().startsWith(ExecutionConstants.WEBVIEW_CONTEXT_PREFIX)
				&& !((ContextAware) driver).getContext().equals(ExecutionConstants.WEBVIEW_CONTEXT_CHROME)) {
			return new HashSet<>();
		}
		return this.driver.getWindowHandles();
	}

	@Override
	public Options manage() {
		return this.driver.manage();
	}

	@Override
	public Navigation navigate() {
		return this.driver.navigate();
	}

	@Override
	public void quit() {
		this.driver.quit();
	}

	@Override
	public TargetLocator switchTo() {
		return this.driver.switchTo();
	}

	@Override
	public Object executeAsyncScript(String arg0, Object... arg1) {
		return ((JavascriptExecutor) this.driver).executeAsyncScript(arg0, arg1);
	}

	@Override
	public Object executeScript(String arg0, Object... arg1) {
		return ((JavascriptExecutor) this.driver).executeScript(arg0, arg1);
	}

	@SuppressWarnings("squid:S3400")
	protected int getWaitInMillis() {
		return 3000;
	}
}
