/*
 * THIS MATERIAL IS THE CONFIDENTIAL, PROPRIETARY AND TRADE SECRET PRODUCT OF
 * BLUECROSS BLUESHIELD OF SOUTH CAROLINA AND ITS SUBSIDIARIES. ANY UNAUTHORIZED 
 * USE, REPRODUCTION OR TRANSFER OF THESE MATERIALS IS STRICTLY PROHIBITED.
 * COPYRIGHT 2019 BLUECROSS BLUESHIELD OF SOUTH CAROLINA   ALL RIGHTS RESERVED.
 */
package com.bcbssc.webdriver.framework;

import java.util.logging.Logger;

import com.bcbssc.webdriver.framework.parser.AttributeParser;
import com.bcbssc.webdriver.framework.parser.BooleanThenOptionalStringParser;
import com.bcbssc.webdriver.framework.parser.NumericThenOptionalStringParser;
import com.bcbssc.webdriver.framework.parser.NumericThenStringParser;
import com.bcbssc.webdriver.framework.parser.NumericThenStringThenOptionalStringParser;
import com.bcbssc.webdriver.framework.parser.SingleCharacterParser;
import com.bcbssc.webdriver.framework.parser.StringThenNumericThenTwoOptionalNumericsParser;
import com.bcbssc.webdriver.framework.parser.StringThenOptionalNumericParser;
import com.bcbssc.webdriver.framework.parser.ThreeNumericsThenStringParser;
import com.bcbssc.webdriver.framework.parser.TwoNumericsThenOptionalStringParser;
import com.bcbssc.webdriver.framework.parser.TwoNumericsThenStringParser;
import com.bcbssc.webdriver.framework.parser.TwoStringsParser;
import com.bcbssc.webdriver.framework.parser.TwoStringsThenNumericParser;
import com.bcbssc.webdriver.framework.parser.TwoStringsThenOptionalNumericParser;
import com.bcbssc.webdriver.framework.parser.TwoStringsThenOptionalStringParser;
import com.bcbssc.webdriver.util.OperationUtil;

/**
 *  Defines a list of test operations.
 *  <ul>
 *  <li>{@link #ADD_MONTHS_TO_STORED_DATE}</li>
 *  <li>{@link #ADD_DAYS_TO_STORED_DATE}</li>
 *  <li>{@link #ADD_YEARS_TO_STORED_DATE}</li>
 *  <li>{@link #ALT_TAB}</li>
 *  <li>{@link #APPEND_TEXT}</li>
 *  <li>{@link #ARROW_DOWN}</li>
 *  <li>{@link #ARROW_UP}</li>
 *  <li>{@link #ATTACH_WINDOWS}</li>
 *  <li>{@link #BACKSPACE}</li>
 *  <li>{@link #CLEAR}</li>
 *  <li>{@link #CLICK}</li>
 *  <li>{@link #CLICK_AND_WAIT_FOR_WINDOW}</li>
 *  <li>{@link #CLICK_BY_INDEX}</li>
 *  <li>{@link #CLICK_BY_PARTIAL_VALUE}</li>
 *  <li>{@link #CLICK_BY_DYNAMIC_VALUE}</li>
 *  <li>{@link #CLICK_BY_VALUE}</li>
 *  <li>{@link #CLICK_CHILD}</li>
 *  <li>{@link #CLICK_EACH}</li>
 *  <li>{@link #CLICK_IF_PRESENT}</li>
 *  <li>{@link #CLICK_LAST}</li>
 *  <li>{@link #CLICK_LAST_WITH_ALERT_CHECK}</li>
 *  <li>{@link #CLICK_MULTIPLE}</li>
 *  <li>{@link #CLICK_RANDOM}</li>
 *  <li>{@link #CLICK_UNTIL_CONTAINS_VALUE}</li>
 *  <li>{@link #CLICK_WITH_ALERT_CHECK}</li>
 *  <li>{@link #CLICK_WITH_POPUP_CHECK}</li>
 *  <li>{@link #CLOSE_LAST_WINDOW}</li>
 *  <li>{@link #CLOSE_WINDOW}</li>
 *  <li>{@link #CLOSE_WINDOW_BY_TITLE}</li>
 *  <li>{@link #CONCAT_AND_STORE}</li>
 *  <li>{@link #CTRL_ALT_KEY}</li>
 *  <li>{@link #CTRL_SHIFT_KEY}</li>
 *  <li>{@link #DRAG_AND_DROP}</li>
 *  <li>{@link #DOUBLE_CLICK}</li>
 *  <li>{@link #DOUBLE_CLICK_AND_WAIT_FOR_WINDOW}</li>
 *  <li>{@link #DETACH_WINDOW}</li>
 *  <li>{@link #DUMP_ELEMENT}</li>
 *  <li>{@link #DUMP_ELEMENT_WITH_VALUE}</li>
 *  <li>{@link #DUMP_ELEMENTS}</li>
 *  <li>{@link #DUMP_PAGE_BODY}</li>
 *  <li>{@link #ENDIF}</li>
 *  <li>{@link #ENFORCE_SCREEN_WIDTH}</li>
 *  <li>{@link #ENFORCE_SCREEN_HEIGHT}</li>
 *  <li>{@link #ERROR}</li>
 *  <li>{@link #EXIT}</li>
 *  <li>{@link #EXIT_IF_ABSENT}</li>
 *  <li>{@link #EXIT_IF_FALSE}</li>
 *  <li>{@link #EXIT_IF_PRESENT}</li>
 *  <li>{@link #EXIT_IF_TRUE}</li>
 *  <li>{@link #EXIT_LOOP}</li>
 *  <li>{@link #EXIT_INNER_LOOP}</li>
 *  <li>{@link #EXIT_LOOP_ITERATION}</li>
 *  <li>{@link #EXIT_MULTI_ROW_LOOP}</li>
 *  <li>{@link #EXIT_MULTI_ROW_LOOP_ITERATION}</li>
 *  <li>{@link #EXIT_TESTSHEET}</li>
 *  <li>{@link #EXIT_TESTSHEET_IF_ABSENT}</li>
 *  <li>{@link #EXIT_TESTSHEET_IF_FALSE}</li>
 *  <li>{@link #EXIT_TESTSHEET_IF_PRESENT}</li>
 *  <li>{@link #EXIT_TESTSHEET_IF_TRUE}</li>
 *  <li>{@link #GETTEXT}</li>
 *  <li>{@link #GOTOURL}</li>
 *  <li>{@link #HALT}</li>
 *  <li>{@link #HIDDEN_ELEMENTS_EXCLUDED}</li>
 *  <li>{@link #HIDDEN_ELEMENTS_INCLUDED}</li>
 *  <li>{@link #IF_ABSENT}</li>
 *  <li>{@link #IF_AMOUNTS_MATCH}</li>
 *  <li>{@link #IF_BROWSER_CHROME}</li>
 *  <li>{@link #IF_BROWSER_FIREFOX}</li>
 *  <li>{@link #IF_BROWSER_IE}</li>
 *  <li>{@link #IF_CHILD_CONTAINS_VALUE}</li>
 *  <li>{@link #IF_CONTAINS_ALL}</li>
 *  <li>{@link #IF_CONTAINS_ANY}</li>
 *  <li>{@link #IF_CONTAINS_VALUE}</li>
 *  <li>{@link #IF_COUNT_EQUAL_TO}</li>
 *  <li>{@link #IF_COUNT_GREATER_THAN}</li>
 *  <li>{@link #IF_COUNT_LESS_THAN}</li>
 *  <li>{@link #IF_DATA_PROPERTY_CONTAINS}</li>
 *  <li>{@link #IF_DATA_PROPERTY_EQUALS}</li>
 *  <li>{@link #IF_DATA_PROPERTY_EXISTS}</li>
 *  <li>{@link #IF_DATA_PROPERTY_NOT_CONTAINS}</li>
 *  <li>{@link #IF_DATA_PROPERTY_NOT_EQUALS}</li>
 *  <li>{@link #IF_DATA_PROPERTY_NOT_EXISTS}</li>
 *  <li>{@link #IF_DISABLED}</li>
 *  <li>{@link #IF_EMPTY}</li>
 *  <li>{@link #IF_ENABLED}</li>
 *  <li>{@link #IF_FAILURE_IN_TESTCASE}</li>
 *  <li>{@link #IF_HELPTEXT}</li>
 *  <li>{@link #IF_KEY_ABSENT}</li>
 *  <li>{@link #IF_KEY_PRESENT}</li>
 *  <li>{@link #IF_INVALID_BY_CSS}</li>
 *  <li>{@link #IF_OPTIONAL_BY_CSS}</li>
 *  <li>{@link #IF_NOT_CONTAINS_ANY}</li>
 *  <li>{@link #IF_NOT_CONTAINS_VALUE}</li>
 *  <li>{@link #IF_NOT_EMPTY}</li>
 *  <li>{@link #IF_NOT_STARTS_WITH_VALUE}</li>
 *  <li>{@link #IF_PRESENT}</li>
 *  <li>{@link #IF_PSEUDO_ELEMENT_CONTAINS}</li>
 *  <li>{@link #IF_PSEUDO_ELEMENT_EQUALS}</li>
 *  <li>{@link #IF_RADIO_CHECKED}</li>
 *  <li>{@link #IF_RADIO_UNCHECKED}</li>
 *  <li>{@link #IF_REQUIRED}</li>
 *  <li>{@link #IF_SELECTED}</li>
 *  <li>{@link #IF_STARTS_WITH_VALUE}</li>
 *  <li>{@link #IF_STORED_VALUE_CONTAINS}</li>
 *  <li>{@link #IF_STORED_VALUE_CONTAINS_ELEMENT_VALUE}</li>
 *  <li>{@link #IF_STORED_VALUE_EMPTY}</li>
 *  <li>{@link #IF_STORED_VALUE_EQUALS}</li>
 *  <li>{@link #IF_STORED_VALUE_STARTS_WITH}</li>
 *  <li>{@link #IF_STORED_VALUE_STARTS_WITH_ELEMENT_VALUE}</li>
 *  <li>{@link #IF_STORED_VALUE_NOT_CONTAINS}</li>
 *  <li>{@link #IF_STORED_VALUE_NOT_CONTAINS_ELEMENT_VALUE}</li>
 *  <li>{@link #IF_STORED_VALUE_NOT_EMPTY}</li>
 *  <li>{@link #IF_STORED_VALUE_NOT_EQUALS}</li>
 *  <li>{@link #IF_STORED_VALUE_NOT_STARTS_WITH}</li>
 *  <li>{@link #IF_STORED_VALUE_NOT_STARTS_WITH_ELEMENT_VALUE}</li>
 *  <li>{@link #IF_UNSELECTED}</li>
 *  <li>{@link #IF_VALID_BY_CSS}</li>
 *  <li>{@link #IF_VALUE_CONTAINS}</li>
 *  <li>{@link #IF_VALUE_CONTAINS_ALL}</li>
 *  <li>{@link #IF_VALUE_CONTAINS_ANY}</li>
 *  <li>{@link #IF_VALUE_CONTAINS_AT_INDEX}</li>
 *  <li>{@link #IF_VALUE_EMPTY}</li>
 *  <li>{@link #IF_VALUE_EQUALS}</li>
 *  <li>{@link #IF_VALUE_EQUALS_ANY}</li>
 *  <li>{@link #IF_VALUE_EQUALS_AT_INDEX}</li>
 *  <li>{@link #IF_VALUE_GREATER_THAN}</li>
 *  <li>{@link #IF_VALUE_IN_RANGE}</li>
 *  <li>{@link #IF_VALUE_LESS_THAN}</li>
 *  <li>{@link #IF_VALUE_STARTS_WITH}</li>
 *  <li>{@link #IF_VALUE_NOT_CONTAINS}</li>
 *  <li>{@link #IF_VALUE_NOT_CONTAINS}</li>
 *  <li>{@link #IF_VALUE_NOT_CONTAINS_ANY}</li>
 *  <li>{@link #IF_VALUE_NOT_EMPTY}</li>
 *  <li>{@link #IF_VALUE_NOT_EQUALS}</li>
 *  <li>{@link #IF_VALUE_NOT_EQUALS}</li>
 *  <li>{@link #IF_VALUE_NOT_EQUALS_ANY}</li>
 *  <li>{@link #IF_VALUE_NOT_STARTS_WITH}</li>
 *  <li>{@link #INFO}</li>
 *  <li>{@link #JSON_TO_CACHE}</li>
 *  <li>{@link #LOCATE_ELEMENTS}</li>
 *  <li>{@link #LOOP}</li>
 *  <li>{@link #PERFORM}</li>
 *  <li>{@link #PERFORM_AND_EXIT_IF_FALSE}</li>
 *  <li>{@link #PERFORM_AND_EXIT_TESTSHEET_IF_FALSE}</li>
 *  <li>{@link #PERFORM_AND_IF_TRUE}</li>
 *  <li>{@link #PERFORM_AND_IF_FALSE}</li>
 *  <li>{@link #PERFORM_FOR_EACH}</li>
 *  <li>{@link #REFRESH_FRAME}</li>
 *  <li>{@link #REFRESH_PAGE}</li>
 *  <li>{@link #REMOVE_KEYS}</li>
 *  <li>{@link #RIGHT_CLICK}</li>
 *  <li>{@link #SAVE_DATA_PROPERTY}</li>
 *  <li>{@link #SCREENSHOT}</li>
 *  <li>{@link #SCREENSHOT_VIA_ROBOT}</li>
 *  <li>{@link #SCROLL_INTO_VIEW}</li>
 *  <li>{@link #SCROLL_TO_BOTTOM}</li>
 *  <li>{@link #SCROLL_TO_TOP}</li>
 *  <li>{@link #SELECT_BY_INDEX}</li>
 *  <li>{@link #SELECT_BY_RANDOM}</li>
 *  <li>{@link #SELECT_BY_VALUE}</li>
 *  <li>{@link #SELECT_BY_VISIBLE_TEXT}</li>
 *  <li>{@link #SELECT_INDEX_FROM_CACHE}</li>
 *  <li>{@link #SELECT_TEXT_FROM_CACHE}</li>
 *  <li>{@link #SELECT_VALUE_FROM_CACHE}</li>
 *  <li>{@link #SETTEXT}</li>
 *  <li>{@link #SETTEXT_FROM_CACHE}</li>
 *  <li>{@link #SETTEXT_VIA_JS}</li>
 *  <li>{@link #SKIP_TO_TESTCASE}</li>
 *  <li>{@link #STORE_CALC}</li>
 *  <li>{@link #STORE_CHILD_VALUE}</li>
 *  <li>{@link #STORE_COUNT_AS_KEY}</li>
 *  <li>{@link #STORE_CURRENT_TIMESTAMP}</li>
 *  <li>{@link #STORE_DATE_AS_KEY}</li>
 *  <li>{@link #STORE_LEFT_CHARS}</li>
 *  <li>{@link #STORE_RIGHT_CHARS}</li>
 *  <li>{@link #STORE_SUBSTR}</li>
 *  <li>{@link #STORE_TIME}</li>
 *  <li>{@link #STORE_VALUE}</li>
 *  <li>{@link #STORE_VALUE_AS_KEY}</li>
 *  <li>{@link #STORE_VALUE_BY_INDEX}</li>
 *  <li>{@link #SWITCH_TO_DEFAULT_CONTENT}</li>
 *  <li>{@link #SWITCH_TO_FRAME}</li>
 *  <li>{@link #VERIFY}</li>
 *  <li>{@link #VERIFY_ABSENT}</li>
 *  <li>{@link #VERIFY_AMOUNTS_MATCH}</li>
 *  <li>{@link #VERIFY_CHILD_ABSENT}</li>
 *  <li>{@link #VERIFY_CHILD_CONTAINS_VALUE}</li>
 *  <li>{@link #VERIFY_CHILD_COUNT}</li>
 *  <li>{@link #VERIFY_CHILD_NOT_CONTAINS_VALUE}</li>
 *  <li>{@link #VERIFY_CHILD_PRESENT}</li>
 *  <li>{@link #VERIFY_CONTAINS_ALL}</li>
 *  <li>{@link #VERIFY_CONTAINS_ANY}</li>
 *  <li>{@link #VERIFY_CONTAINS_VALUE}</li>
 *  <li>{@link #VERIFY_COUNT_EQUAL_TO}</li>
 *  <li>{@link #VERIFY_COUNT_GREATER_THAN}</li>
 *  <li>{@link #VERIFY_COUNT_LESS_THAN}</li>
 *  <li>{@link #VERIFY_DISABLED}</li>
 *  <li>{@link #VERIFY_EMPTY}</li>
 *  <li>{@link #VERIFY_ENABLED}</li>
 *  <li>{@link #VERIFY_HELPTEXT}</li>
 *  <li>{@link #VERIFY_INVALID_BY_CSS}</li>
 *  <li>{@link #VERIFY_LOCATORS}</li>
 *  <li>{@link #VERIFY_NOT_CONTAINS_ANY}</li>
 *  <li>{@link #VERIFY_NOT_CONTAINS_VALUE}</li>
 *  <li>{@link #VERIFY_NOT_EMPTY}</li>
 *  <li>{@link #VERIFY_NOT_STARTS_WITH_VALUE}</li>
 *  <li>{@link #VERIFY_OPTIONAL_BY_CSS}</li>
 *  <li>{@link #VERIFY_PRESENT}</li>
 *  <li>{@link #VERIFY_PSEUDO_ELEMENT_CONTAINS}</li>
 *  <li>{@link #VERIFY_PSEUDO_ELEMENT_EQUALS}</li>
 *  <li>{@link #VERIFY_RADIO_CHECKED}</li>
 *  <li>{@link #VERIFY_RADIO_UNCHECKED}</li>
 *  <li>{@link #VERIFY_SELECTED}</li>
 *  <li>{@link #VERIFY_STARTS_WITH_VALUE}</li>
 *  <li>{@link #VERIFY_STORED_VALUE_CONTAINS}</li>
 *  <li>{@link #VERIFY_STORED_VALUE_CONTAINS_ELEMENT_VALUE}</li>
 *  <li>{@link #VERIFY_STORED_VALUE_EMPTY}</li>
 *  <li>{@link #VERIFY_STORED_VALUE_EQUALS}</li>
 *  <li>{@link #VERIFY_STORED_VALUE_STARTS_WITH}</li>
 *  <li>{@link #VERIFY_STORED_VALUE_STARTS_WITH_ELEMENT_VALUE}</li>
 *  <li>{@link #VERIFY_STORED_VALUE_NOT_CONTAINS}</li>
 *  <li>{@link #VERIFY_STORED_VALUE_NOT_CONTAINS_ELEMENT_VALUE}</li>
 *  <li>{@link #VERIFY_STORED_VALUE_NOT_EMPTY}</li>
 *  <li>{@link #VERIFY_STORED_VALUE_NOT_EQUALS}</li>
 *  <li>{@link #VERIFY_STORED_VALUE_NOT_STARTS_WITH}</li>
 *  <li>{@link #VERIFY_STORED_VALUE_NOT_STARTS_WITH_ELEMENT_VALUE}</li>
 *  <li>{@link #VERIFY_UNSELECTED}</li>
 *  <li>{@link #VERIFY_URL_CONTAINS}</li>
 *  <li>{@link #VERIFY_REQUIRED}</li>
 *  <li>{@link #VERIFY_VALID_BY_CSS}</li>
 *  <li>{@link #VERIFY_VALUE_CONTAINS}</li>
 *  <li>{@link #VERIFY_VALUE_CONTAINS_ALL}</li>
 *  <li>{@link #VERIFY_VALUE_CONTAINS_ANY}</li>
 *  <li>{@link #VERIFY_VALUE_CONTAINS_AT_INDEX}</li>
 *  <li>{@link #VERIFY_VALUE_EQUALS}</li>
 *  <li>{@link #VERIFY_VALUE_EQUALS_ANY}</li>
 *  <li>{@link #VERIFY_VALUE_EQUALS_AT_INDEX}</li>
 *  <li>{@link #VERIFY_VALUE_GREATER_THAN}</li>
 *  <li>{@link #VERIFY_VALUE_IN_RANGE}</li>
 *  <li>{@link #VERIFY_VALUE_LESS_THAN}</li>
 *  <li>{@link #VERIFY_VALUE_STARTS_WITH}</li>
 *  <li>{@link #VERIFY_VALUE_NOT_CONTAINS}</li>
 *  <li>{@link #VERIFY_VALUE_NOT_CONTAINS_ANY}</li>
 *  <li>{@link #VERIFY_VALUE_NOT_EQUALS}</li>
 *  <li>{@link #VERIFY_VALUE_NOT_EQUALS_ANY}</li>
 *  <li>{@link #VERIFY_VALUE_NOT_STARTS_WITH}</li>
 *  <li>{@link #WAIT}</li>
 *  <li>{@link #WAIT_FOR_VALUE}</li>
 *  <li>{@link #WAIT_UNTIL_ABSENT}</li>
 *  <li>{@link #WAIT_UNTIL_CONTAINS_VALUE}</li>
 *  <li>{@link #WAIT_UNTIL_DISABLED}</li>
 *  <li>{@link #WAIT_UNTIL_EMPTY}</li>
 *  <li>{@link #WAIT_UNTIL_ENABLED}</li>
 *  <li>{@link #WAIT_UNTIL_PRESENT}</li>
 *  <li>{@link #WARNING}</li>
 *  <li>{@link #WARN_IF_ABSENT}</li>
 *  <li>{@link #WARN_IF_PRESENT}</li>
 *  <li>{@link #WINDOW_SWITCHING_DISABLED}</li>
 *  <li>{@link #WINDOW_SWITCHING_ENABLED}</li>
 *  <li>{@link #WRITE_TO_CSV_FILE}</li>
 *  </ul>
 *  <p>Keyboard operations:
 *  <ul>
 *  <li>{@link #KEYBOARD_PAUSE_BREAK}</li>
 *  <li>{@link #KEYBOARD_TYPE_TEXT}</li>
 *  <li>{@link #KEYBOARD_TYPE_TEXT_AND_PRESS_ENTER}</li>
 *  <li>{@link #KEYBOARD_TYPE_TEXT_AND_TAB}</li>
 *  <li>{@link #KEYBOARD_TYPE_TEXT_FROM_CACHE}</li>
 *  <li>{@link #KEYBOARD_TYPE_TEXT_FROM_CACHE_AND_PRESS_ENTER}</li>
 *  <li>{@link #KEYBOARD_TYPE_TEXT_FROM_CACHE_AND_TAB}</li>
 *  <li>{@link #KEYBOARD_CLEAR}</li>
 *  <li>{@link #KEYBOARD_COPY}</li>
 *  <li>{@link #KEYBOARD_SELECT_ALL}</li>
 *  <li>{@link #KEYBOARD_TAB}</li>
 *  <li>{@link #KEYBOARD_REVERSE_TAB}</li>
 *  <li>{@link #KEYBOARD_ESCAPE}</li>
 *  <li>{@link #KEYBOARD_FUNCTION_KEY}</li>
 *  <li>{@link #KEYBOARD_PRESS_ENTER}</li>
 *  <li>{@link #KEYBOARD_MAXIMIZE_WINDOW}</li>
 *  <li>{@link #KEYBOARD_RESTORE_AND_MAXIMIZE_WINDOW}</li>
 *  <li>{@link #KEYBOARD_UP_ARROW}</li>
 *  <li>{@link #KEYBOARD_DOWN_ARROW}</li>
 *  <li>{@link #KEYBOARD_NEW_TAB}</li>
 *  <li>{@link #KEYBOARD_CONTROL_TAB}</li>
 *  </ul>
 *  <p>Mouse operations:
 *  <ul>
 *  <li>{@link #MOUSE_CLICK_SCREEN_CENTER}</li>
 *  <li>{@link #MOUSE_OVER}</li>
 *  </ul>
 *  <p>LANFax operations:
 *  <ul>
 *  <li>{@link #LANFAX_OPEN_NEW_WINDOW}</li>
 *  <li>{@link #LANFAX_ADD_ATTACHMENT}</li>
 *  <li>{@link #LANFAX_SUBMIT_NEW_FAX}</li>
 *  <li>{@link #LANFAX_CLOSE_WINDOW}</li>
 *  </ul>
 * 
 *  <p>JSM operations:
 *  <ul>
 *  <li>{@link #JSM_INIT}</li>
 *  <li>{@link #JSM_COMPARE_PDF_TO_XLS}</li>
 *  <li>{@link #JSM_COMPARE_XLS_TO_PDF}</li>
 *  <li>{@link #JSM_VERIFY_APPROVED_DATES}</li>
 *  <li>{@link #JSM_VERIFY_PLATFORMS}</li>
 *  <li>{@link #JSM_VERIFY_SCC_PLATFORMS}</li>
 *  <li>{@link #JSM_CLEANUP}</li>
 *  </ul>
 * 
 *  <p>Android operations:
 *  <ul>
 *  <li>{@link #ANDROID_WAIT}</li>
 *  <li>{@link #ANDROID_CLICK_COORDINATES}</li>
 *  <li>{@link #ANDROID_CLICK_NATIVE_BACK_BUTTON}</li>
 *  <li>{@link #ANDROID_TAP}</li>
 *  <li>{@link #ANDROID_LONG_PRESS}</li>
 *  <li>{@link #ANDROID_SWIPE_SCREEN}</li>
 *  <li>{@link #ANDROID_HORIZONTAL_SWIPE}</li>
 *  <li>{@link #ANDROID_VERTICAL_SWIPE}</li>
 *  <li>{@link #ANDROID_ACTIVATE_APP}</li>
 *  <li>{@link #ANDROID_TERMINATE_APP}</li>
 *  <li>{@link #ANDROID_CLOSE_APP}</li>
 *  <li>{@link #ANDROID_LAUNCH_APP}</li>
 *  <li>{@link #ANDROID_SWITCH_APPIUM_CONTEXT_WEB}</li>
 *  <li>{@link #ANDROID_SWITCH_APPIUM_CONTEXT_NATIVE}</li>
 *  <li>{@link #ANDROID_IONIC_SET_DATE}</li>
 *  </ul>
 * 
 *  <p>iOS operations:
 *  <ul>
 *  <li>{@link #IOS_WAIT}</li>
 *  <li>{@link #IOS_CLICK_COORDINATES}</li>
 *  <li>{@link #IOS_TAP}</li>
 *  <li>{@link #IOS_LONG_PRESS}</li>
 *  <li>{@link #IOS_SWIPE_SCREEN}</li>
 *  <li>{@link #IOS_HORIZONTAL_SWIPE}</li>
 *  <li>{@link #IOS_VERTICAL_SWIPE}</li>
 *  <li>{@link #IOS_ACTIVATE_APP}</li>
 *  <li>{@link #IOS_TERMINATE_APP}</li>
 *  <li>{@link #IOS_CLOSE_APP}</li>
 *  <li>{@link #IOS_LAUNCH_APP}</li>
 *  <li>{@link #IOS_SWITCH_APPIUM_CONTEXT_WEB}</li>
 *  <li>{@link #IOS_SWITCH_APPIUM_CONTEXT_NATIVE}</li>
 *  <li>{@link #IOS_IONIC_SET_DATE}</li>
 *  </ul>
 * 
 *  <p>Flynet operations:
 *  <ul>
 *  <li>{@link #FLYNET_CLEAR_TEXT}</li>
 *  <li>{@link #FLYNET_ESCAPE}</li>
 *  <li>{@link #FLYNET_FUNCTION_KEY}</li>
 *  <li>{@link #FLYNET_GO_TO}</li>
 *  <li>{@link #FLYNET_IF_CURSOR_AT_INPUT}</li>
 *  <li>{@link #FLYNET_IF_CURSOR_NOT_AT_INPUT}</li>
 *  <li>{@link #FLYNET_IF_VALUE_NOT_PRESENT}</li>
 *  <li>{@link #FLYNET_IF_VALUE_NOT_PRESENT_AFTER}</li>
 *  <li>{@link #FLYNET_IF_VALUE_NOT_PRESENT_ON_ROW}</li>
 *  <li>{@link #FLYNET_IF_VALUE_PRESENT}</li>
 *  <li>{@link #FLYNET_IF_VALUE_PRESENT_AFTER}</li>
 *  <li>{@link #FLYNET_IF_VALUE_PRESENT_ON_ROW}</li>
 *  <li>{@link #FLYNET_PAUSE_BREAK}</li>
 *  <li>{@link #FLYNET_PLACE_CURSOR_AT}</li>
 *  <li>{@link #FLYNET_PLACE_CURSOR_AT_INPUT}</li>
 *  <li>{@link #FLYNET_PLACE_CURSOR_AT_FIRST_INPUT}</li>
 *  <li>{@link #FLYNET_PLACE_CURSOR_ON_ROW}</li>
 *  <li>{@link #FLYNET_PRESS_ENTER}</li>
 *  <li>{@link #FLYNET_PRESS_FUNCTION_KEY_UNTIL_CONTAINS_VALUE}</li>
 *  <li>{@link #FLYNET_REVERSE_TAB}</li>
 *  <li>{@link #FLYNET_SET_TEXT}</li>
 *  <li>{@link #FLYNET_SET_TEXT_FOR_INPUT_AT_OFFSET}</li>
 *  <li>{@link #FLYNET_SET_TEXT_FOR_INPUT_ON_ROW_CONTAINING}</li>
 *  <li>{@link #FLYNET_STORE_INPUT_PRESENT_AFTER}</li>
 *  <li>{@link #FLYNET_STORE_VALUE_PRESENT_AFTER}</li>
 *  <li>{@link #FLYNET_STORE_VALUES_ON_ROW}</li>
 *  <li>{@link #FLYNET_STORE_VALUES_ON_ROW_BETWEEN_COLS}</li>
 *  <li>{@link #FLYNET_TAB}</li>
 *  <li>{@link #FLYNET_TYPE_OPTIONAL_TEXT_AND_TAB}</li>
 *  <li>{@link #FLYNET_TYPE_TEXT}</li>
 *  <li>{@link #FLYNET_TYPE_TEXT_AND_PRESS_ENTER}</li>
 *  <li>{@link #FLYNET_TYPE_TEXT_AND_TAB}</li>
 *  <li>{@link #FLYNET_VERIFY_CURSOR_AT_INPUT}</li>
 *  <li>{@link #FLYNET_VERIFY_CURSOR_ON_ROW}</li>
 *  <li>{@link #FLYNET_VERIFY_VALUE_NOT_PRESENT}</li>
 *  <li>{@link #FLYNET_VERIFY_VALUE_NOT_PRESENT_ON_ROW}</li>
 *  <li>{@link #FLYNET_VERIFY_VALUE_NOT_PRESENT_AFTER}</li>
 *  <li>{@link #FLYNET_VERIFY_VALUE_PRESENT}</li>
 *  <li>{@link #FLYNET_VERIFY_VALUE_PRESENT_AFTER}</li>
 *  <li>{@link #FLYNET_VERIFY_VALUE_PRESENT_ON_ROW}</li>
 *  <li>{@link #FLYNET_WAIT_UNTIL_VALUE_NOT_PRESENT}</li>
 *  <li>{@link #FLYNET_WAIT_UNTIL_VALUE_NOT_PRESENT_AFTER}</li>
 *  <li>{@link #FLYNET_WAIT_UNTIL_VALUE_NOT_PRESENT_ON_ROW}</li>
 *  <li>{@link #FLYNET_WAIT_UNTIL_VALUE_PRESENT}</li>
 *  <li>{@link #FLYNET_WAIT_UNTIL_VALUE_PRESENT_AFTER}</li>
 *  <li>{@link #FLYNET_WAIT_UNTIL_VALUE_PRESENT_ON_ROW}</li>
 *  <li>{@link #FLYNET_WAIT_UNTIL_CONTAINS_VALUE}</li>
 *  <li>{@link #FLYNET_WAIT_FOR_STATUS}</li>
 *  </ul>
 * 
 *  <p>Web service operations:
 *  <ul>
 *  <li>{@link #WS_GET_CLAIM}</li>
 *  <li>{@link #WS_GET_REQUEST}</li>
 *  <li>{@link #WS_POST_REQUEST}</li>
 *  <li>{@link #WS_MOCK_REQUEST}</li>
 *  </ul>
 * 
 *  <p>
 *  Operations starting with certain prefixes share common functionality:
 *  <p><ul>
 *  <li>CLICK	Click an element if it meets some condition, or perform some task after clicking it.</li>
 *  <li>DUMP	Dump elements under some parent element. Useful during script development to locate elements for applications that have disabled source view/F12.</li>
 *  <li>EXIT	Exit the test process. Exiting can be performed at the TestCase, TestSheet, or Execution level.</li>
 *  <li>IF		Acts as a checkpoint within the script sequence, providing the ability to only perform subsequent test steps based on the result.</li>
 *  <li>PERFORM	Executes a subclass of com.bcbssc.webdriver.function.CustomFunction, which performs some application-specific process, and report the results.</li>
 *  <li>SELECT	Select an option from a drop down list.</li>
 *  <li>STORE	Stores the value for the given element. Provides the ability to check a value in a later step (ex. User clicks on a DBN, window opens, does the new window display the DBN selected?)</li>
 *  <li>VERIFY	Similar to an assertion. Verify something, such as an element is present/absent, is empty, contains a particular value, etc.</li>
 *  <li>WAIT	Halts execution until a particular condition occurs (element present/absent, empty, contains value, etc.)</li>
 *  <li>WARN	Report a warning if a particular condition occurs.</li>
 *  </ul>
 */
public enum Operation implements Operationable {

	/**
     * Increments/decrements the month value of a date string in cache. If an output cache key is not provided, the 
     * original value in cache will be replaced with the newly calculated date. This operation is typically used in 
     * conjunction with the STORE_DATE_AS_KEY operation to manipulate a date retrieved from an element. Note that 
     * the addition/subtraction of time is based on the rules in java.util.Calendar.add().
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [amount]|[cache key]{|output cache key}</li>
     * <li>Optional: None</li>
     * </ul>
 	 * <p>
 	 * <b>Using with STORE_DATE_AS_KEY</b>
     * <p>
	 * When used in conjunction with STORE_DATE_AS_KEY, the AttributeKey should contain the same cache key PREFIX used
	 * in the STORE_DATE_AS_KEY operation. Since the STORE_DATE_AS_KEY operation will save the date under both MM/DD/YYYY 
	 * and YYYY-MM-DD formats, both of those dates will be updated:
 	 * <p>
 	 * <b>Example 1: Without (optional) Output Cache Key Parameter (assuming date of 01/02/2019)</b>
 	 * <p>
 	 * STORE_DATE_AS_KEY [attributeKey=key_blah] results in the following cache keys/values:
	 * <p><ul>
	 * <li>key_blah_MMDDYYYY : 01/02/2019</li>
	 * <li>key_blah_YYYYMMDD : 2019-01-02</li>
     * </ul>
 	 * <p>
 	 * ADD_MONTHS_TO_STORED_DATE [attributeKey=5|key_blah] results in the following UPDATED cache keys/values:
	 * <p><ul>
	 * <li>key_blah_MMDDYYYY : <b>06</b>/02/2019</li>
	 * <li>key_blah_YYYYMMDD : 2019-<b>06</b>-02</li>
     * </ul>
 	 * <p>
 	 * <b>Example 2: With (optional) Output Cache Key Parameter (assuming date of 01/02/2019)</b>
 	 * <p>
 	 * STORE_DATE_AS_KEY [attributeKey=key_blah] results in the following cache keys/values:
	 * <p><ul>
	 * <li>key_blah_MMDDYYYY : 01/02/2019</li>
	 * <li>key_blah_YYYYMMDD : 2019-01-02</li>
     * </ul>
 	 * <p>
 	 * ADD_MONTHS_TO_STORED_DATE [attributeKey=5|key_blah|<b>key_updated</b>] results in the following cache keys/values:
	 * <p><ul>
	 * <li>key_blah_MMDDYYYY : 01/02/2019</li>
	 * <li>key_blah_YYYYMMDD : 2019-01-02</li>
	 * <li><b>key_updated_MMDDYYYY : 06/02/2019</b></li>
	 * <li><b>key_updated_YYYYMMDD : 2019-06-02</b></li>
     * </ul>
     */
	ADD_MONTHS_TO_STORED_DATE(Consequence.CASCADING, new NumericThenStringThenOptionalStringParser(), false),

	/**
     * Increments/decrements the day value of a date string in cache. If an output cache key is not provided, the 
     * original value in cache will be replaced with the newly calculated date. This operation is typically used in 
     * conjunction with the STORE_DATE_AS_KEY operation to manipulate a date retrieved from an element. Note that 
     * the addition/subtraction of time is based on the rules in java.util.Calendar.add().
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [amount]|[cache key]{|output cache key}</li>
     * <li>Optional: None</li>
     * </ul>
 	 * <p>
 	 * <b>Using with STORE_DATE_AS_KEY</b>
     * <p>
	 * When used in conjunction with STORE_DATE_AS_KEY, the AttributeKey should contain the same cache key PREFIX used
	 * in the STORE_DATE_AS_KEY operation. Since the STORE_DATE_AS_KEY operation will save the date under both MM/DD/YYYY 
	 * and YYYY-MM-DD formats, both of those dates will be updated:
 	 * <p>
 	 * <b>Example 1: Without (optional) Output Cache Key Parameter (assuming date of 01/02/2019)</b>
 	 * <p>
 	 * STORE_DATE_AS_KEY [attributeKey=key_blah] results in the following cache keys/values:
	 * <p><ul>
	 * <li>key_blah_MMDDYYYY : 01/02/2019</li>
	 * <li>key_blah_YYYYMMDD : 2019-01-02</li>
     * </ul>
 	 * <p>
 	 * ADD_DAYS_TO_STORED_DATE [attributeKey=5|key_blah] results in the following UPDATED cache keys/values:
	 * <p><ul>
	 * <li>key_blah_MMDDYYYY : 01/<b>07</b>/2019</li>
	 * <li>key_blah_YYYYMMDD : 2019-01-<b>07</b></li>
     * </ul>
 	 * <p>
 	 * <b>Example 2: With (optional) Output Cache Key Parameter (assuming date of 01/02/2019)</b>
 	 * <p>
 	 * STORE_DATE_AS_KEY [attributeKey=key_blah] results in the following cache keys/values:
	 * <p><ul>
	 * <li>key_blah_MMDDYYYY : 01/02/2019</li>
	 * <li>key_blah_YYYYMMDD : 2019-01-02</li>
     * </ul>
 	 * <p>
 	 * ADD_DAYS_TO_STORED_DATE [attributeKey=5|key_blah|<b>key_updated</b>] results in the following cache keys/values:
	 * <p><ul>
	 * <li>key_blah_MMDDYYYY : 01/02/2019</li>
	 * <li>key_blah_YYYYMMDD : 2019-01-02</li>
	 * <li><b>key_updated_MMDDYYYY : 01/07/2019</b></li>
	 * <li><b>key_updated_YYYYMMDD : 2019-01-07</b></li>
     * </ul>
     */
	ADD_DAYS_TO_STORED_DATE(Consequence.CASCADING, new NumericThenStringThenOptionalStringParser(), false),

	/**
     * Increments/decrements the year value of a date string in cache. If an output cache key is not provided, the 
     * original value in cache will be replaced with the newly calculated date. This operation is typically used in 
     * conjunction with the STORE_DATE_AS_KEY operation to manipulate a date retrieved from an element. Note that 
     * the addition/subtraction of time is based on the rules in java.util.Calendar.add().
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [amount]|[cache key]{|output cache key}</li>
     * <li>Optional: None</li>
     * </ul>
 	 * <p>
 	 * <b>Using with STORE_DATE_AS_KEY</b>
     * <p>
	 * When used in conjunction with STORE_DATE_AS_KEY, the AttributeKey should contain the same cache key PREFIX used
	 * in the STORE_DATE_AS_KEY operation. Since the STORE_DATE_AS_KEY operation will save the date under both MM/DD/YYYY 
	 * and YYYY-MM-DD formats, both of those dates will be updated:
 	 * <p>
 	 * <b>Example 1: Without (optional) Output Cache Key Parameter (assuming date of 01/02/2019)</b>
 	 * <p>
 	 * STORE_DATE_AS_KEY [attributeKey=key_blah] results in the following cache keys/values:
	 * <p><ul>
	 * <li>key_blah_MMDDYYYY : 01/02/2019</li>
	 * <li>key_blah_YYYYMMDD : 2019-01-02</li>
     * </ul>
 	 * <p>
 	 * ADD_YEARS_TO_STORED_DATE [attributeKey=5|key_blah] results in the following UPDATED cache keys/values:
	 * <p><ul>
	 * <li>key_blah_MMDDYYYY : 01/02/<b>2024</b></li>
	 * <li>key_blah_YYYYMMDD : <b>2024</b>-01-02</li>
     * </ul>
 	 * <p>
 	 * <b>Example 2: With (optional) Output Cache Key Parameter (assuming date of 01/02/2019)</b>
 	 * <p>
 	 * STORE_DATE_AS_KEY [attributeKey=key_blah] results in the following cache keys/values:
	 * <p><ul>
	 * <li>key_blah_MMDDYYYY : 01/02/2019</li>
	 * <li>key_blah_YYYYMMDD : 2019-01-02</li>
     * </ul>
 	 * <p>
 	 * ADD_YEARS_TO_STORED_DATE [attributeKey=5|key_blah|<b>key_updated</b>] results in the following cache keys/values:
	 * <p><ul>
	 * <li>key_blah_MMDDYYYY : 01/02/2019</li>
	 * <li>key_blah_YYYYMMDD : 2019-01-02</li>
	 * <li><b>key_updated_MMDDYYYY : 01/02/2024</b></li>
	 * <li><b>key_updated_YYYYMMDD : 2024-01-02</b></li>
     * </ul>
     */
	ADD_YEARS_TO_STORED_DATE(Consequence.CASCADING, new NumericThenStringThenOptionalStringParser(), false),
	
	/**
     * Simulates an ALT+TAB key press combination.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	ALT_TAB(Consequence.NON_CASCADING, false),

	/**
     * Appends the given text to the element associated with the given locator.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or numeric value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	APPEND_TEXT(Consequence.CASCADING, true, true),

	/**
     * Sends the ARROW_DOWN key to the given element. This operation is useful for triggering dynamic sub-menus in
     * some applications, when a CLICK operation won't do.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	ARROW_DOWN(Consequence.CASCADING, true),

	/**
     * Sends the ARROW_UP key to the given element.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	ARROW_UP(Consequence.CASCADING, true),

	ATTACH_WINDOWS(Consequence.NON_CASCADING, false),

	/**
     * Backspaces within the input element a given number of times, as indicated by the attributeKey.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: AttributeKey (can be either data property key or simple value)</li>
     * </ul>
     */
	BACKSPACE(Consequence.NON_CASCADING, true),

	/**
     * Clears the given element.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	CLEAR(Consequence.NON_CASCADING, true),

	/**
     * Clicks the element associated with the given locator.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	CLICK(Consequence.CASCADING, true, false, false, false, false, false, false, false, false, true),

	/**
     * Clicks the element associated with the given locator, and waits for the corresponding window to
     * display before proceeding. If window does not appear within 5 seconds, an exception is thrown.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	CLICK_AND_WAIT_FOR_WINDOW(Consequence.CASCADING, true, false, false, false, false, false, false, false, false, true),

	/**
     * Clicks the element associated with the given locator, at the given index. This operation is useful
     * when multiple elements exist for a given locator, such as a list of links in a search result, or a
     * group of radio buttons.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or numeric value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	CLICK_BY_INDEX(Consequence.CASCADING, true, true, true),

	/**
     * Clicks the element associated with the given locator, whose text contains the given value (case-insensitive).
     * This operation is useful when multiple elements exist for a given locator (such as a list of links in a
     * search result), and you wish to click a link that contains a given value (ex. first name).
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or simple value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	CLICK_BY_PARTIAL_VALUE(Consequence.CASCADING, true, true),

	/**
     * Clicks the element associated with the given locator, by first inserting the provided value into
     * the locator string. The locator string must contain the wildcard identifier '???'.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or simple value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	CLICK_BY_DYNAMIC_VALUE(Consequence.CASCADING, true, true),

	/**
     * Clicks the element associated with the given locator, whose text matches the given value (case-insensitive).
     * This operation is useful when multiple elements exist for a given locator (such as a list of links in a
     * search result), and you wish to click a link for a given value (ex. claim number).
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or simple value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	CLICK_BY_VALUE(Consequence.CASCADING, true, true),

	/**
     * Clicks the element associated with the given locator.
     * NOTE: This operation should only be used within a LOOP construct.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	CLICK_CHILD(Consequence.CASCADING, true, false, false, false, false, false, false, false, true, true),

	/**
     * Clicks all elements associated with the given locator. This operation is useful for opening all tree
     * nodes in the Commercial Desktop category links structure.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	CLICK_EACH(Consequence.NON_CASCADING, true),

	/**
     * Clicks the element associated with the given locator, if present. This operation is useful for elements
     * whose appearance is conditional (such as an EOB link).
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	CLICK_IF_PRESENT(Consequence.CASCADING, true, false, false, false, false, true, false, false, false, true),

	/**
     * Clicks the last element associated with the given locator. This operation is useful for clicking the
     * last row in a results list.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	CLICK_LAST(Consequence.CASCADING, true),

	/**
     * Clicks the last element associated with the given locator, then handle an Alert popup if it appears.
     * If a popup does appear, the execution is halted for a few seconds, and the element is clicked again.
     * This process repeats until the alert no longer appears. This operation is useful for clicking links
     * that refresh the Commercial Desktop emulator, which occasionally results in an 'Emulator busy' alert.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: AttributeKey as either boolean value indicating whether or not the popup should be
     * accepted or rejected, or a pipe-delimited (|) string consisting of the boolean value and a text string
     * to compare against the popup text. If a text string is provided, the result of the operation will
     * be based on whether or not the text string provided exists within the popup text.</li>
     * </ul>
     */
	CLICK_LAST_WITH_ALERT_CHECK(Consequence.CASCADING, new BooleanThenOptionalStringParser(), true),

	/**
     * Clicks all elements associated with the given locator. This Operation differs from CLICK_EACH in that
     * it performs a CTRL+click on all elements after the first element, thereby allowing for the selection 
     * of multiple elements within a multi-select box or a collection of list items.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	CLICK_MULTIPLE(Consequence.CASCADING, true),

	/**
     * Clicks a randomly chosen element associated with the given locator. 
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	CLICK_RANDOM(Consequence.CASCADING, true),

	/**
     * Clicks the element associated with the primary locator until the element associated with the secondary
     * locator contains the given value. This operation is useful for clicking a search button until the
     * search results contain the desired data.
     * <p><ul>
     * <li>Required: Locator (two locators separated by a pipe |), AttributeKey as a pipe-delimited string 
     * in the format [wait in sec between click attempts]|[max number of click attempts]|[comparison value]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	CLICK_UNTIL_CONTAINS_VALUE(Consequence.CASCADING, new TwoNumericsThenStringParser(), true), 

	/**
     * Clicks the element associated with the given locator, then handle an Alert popup if it appears. If a
     * popup does appear, the execution is halted for a few seconds, and the element is clicked again. This
     * process repeats until the alert no longer appears. This operation is useful for clicking links that
     * refresh the Commercial Desktop emulator, which occasionally results in an 'Emulator busy' alert.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: AttributeKey as either boolean value indicating whether or not the popup should be
     * accepted or rejected, or a pipe-delimited (|) string consisting of the boolean value and a text string
     * to compare against the popup text. If a text string is provided, the result of the operation will
     * be based on whether or not the text string provided exists within the popup text.</li>
     * </ul>
     */
	CLICK_WITH_ALERT_CHECK(Consequence.CASCADING, new BooleanThenOptionalStringParser(), true),

	/**
     * Clicks the element associated with the given locator, then handle an Alert popup if it appears. This
     * operation differs from CLICK_WITH_ALERT_CHECK in that this operation simply closes the alert if it
     * appears. It will not attempt to reclick the element.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: AttributeKey as either boolean value indicating whether or not the popup should be
     * accepted or rejected, or a pipe-delimited (|) string consisting of the boolean value and a text string
     * to compare against the popup text. If a text string is provided, the result of the operation will
     * be based on whether or not the text string provided exists within the popup text.</li>
     * </ul>
     */
	CLICK_WITH_POPUP_CHECK(Consequence.CASCADING, new BooleanThenOptionalStringParser(), true),

	/**
     * Closes the last window opened.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	CLOSE_LAST_WINDOW(Consequence.NON_CASCADING, false),

	/**
     * Closes the window associated with the given element. If a locator is not defined, the current
     * window is closed.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: Locator</li>
     * </ul>
     */
	CLOSE_WINDOW(Consequence.CASCADING, false),

    /**
     * Closes the window containing the given title.
     * <p><ul>
     * <li>Required: AttributeKey</li>
     * <li>Optional: None</li>
     * </ul>
     */
    CLOSE_WINDOW_BY_TITLE(Consequence.CASCADING, false, true),

	/**
	 * Temporarily concatenate and store a set of values (literals or from cached keys), possibly could be used in a later step for reference.
	 * The attribute key column will contain a pipe delimited list of items to append. They may be string literals or cached
	 * values. *Note* that the first element is the key for concatenated string to be stored in. The pattern will be:
	 * |store_key|separator|string1|string2|, where string1 and string2 will be concatenated using 'separator' and stored into store_key.
	 * AttributeKey contains the lookup key of the value in which to store the concatenated string.
	 * <p><ul>
     * <li>Required: AttributeKey.|store_key|separator|[literal|cache key] | [literal | cache key] | ...</li>
     * </ul>
	 */

	CONCAT_AND_STORE(Consequence.CASCADING, false, true),

	/**
     * Simulates a CTRL+ALT+[single key] combination.
     * <p><ul>
     * <li>Required: AttributeKey (can be either data property key or simple value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	CTRL_ALT_KEY(Consequence.NON_CASCADING, new SingleCharacterParser(), false),

	/**
     * Simulates a CTRL+SHIFT+[single key] combination.
     * <p><ul>
     * <li>Required: AttributeKey (can be either data property key or simple value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	CTRL_SHIFT_KEY(Consequence.NON_CASCADING, new SingleCharacterParser(), false),

	/**
     * Drags the element associated with the given primary locator to the element associated with the given secondary locator.
     * <p><ul>
     * <li>Required: Locator as locatorA|locatorB</li>
     * <li>Optional: AttributeKey (numeric) that determines the wait time in millis after performing the drag-drop action</li>
     * </ul>
     */
	DRAG_AND_DROP(Consequence.CASCADING, true),

	/**
     * Double-clicks the element associated with the given locator, at the given index. This operation is useful
     * when multiple elements exist for a given locator, such as a list of links in a search result.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: AttributeKey (numeric) that determines the wait time in millis after performing the double-click</li>
     * </ul>
     */
	DOUBLE_CLICK(Consequence.CASCADING, true),

	/**
     * Double-clicks the element associated with the given locator, and waits for the corresponding window to
     * display before proceeding. If window does not appear within 5 seconds, an exception is thrown.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	DOUBLE_CLICK_AND_WAIT_FOR_WINDOW(Consequence.CASCADING, true),

	/**
     * Double-clicks the element associated with the given locator, at the given index. This operation is useful
     * when multiple elements exist for a given locator, such as a list of links in a search result.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or numeric value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	//DBL_CLICK_BY_INDEX(Consequence.CASCADING, true),

	/**
     * Removes the window (on which the given element exists) from the overall test. This is useful
     * in situations where a test harness is temporarily used to launch the application, and we don't
     * want to include that launching window in the test.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	DETACH_WINDOW(Consequence.NON_CASCADING, true),

	/**
     * Display the outerHTML for the element associated with the given locator in the console. As a developer,
     * this operation is can assist in determining locator attributes, especially for those applications that
     * have disabled page source viewing and/or developer tools.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	DUMP_ELEMENT(Consequence.NON_CASCADING, true),

	/**
     * Display the outerHTML for the element associated with the given locator, containing the given text in
     * the console. As a developer, this operation is can assist in determining locator attributes, especially
     * for those applications that have disabled page source viewing and/or developer tools.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	DUMP_ELEMENT_WITH_VALUE(Consequence.NON_CASCADING, true, true),

	/**
     * Display the outerHTML for all elements associated with the given locator in the console. As a developer,
     * this operation is can assist in determining locator attributes, especially for those applications that
     * have disabled page source viewing and/or developer tools.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	DUMP_ELEMENTS(Consequence.NON_CASCADING, true),

	/**
     * Display the outerHTML for the page body in the console. As a developer, this operation is can assist in
     * determining locator attributes, especially for those applications that have disabled page source viewing
     * and/or developer tools.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	DUMP_PAGE_BODY(Consequence.NON_CASCADING, false),

	/**
     * Closes the last enabled IF block that exists within the same TestCase. Note that if any ENDIF operations
     * exist within a TestCase, then all other conditional operations must have an associated ENDIF. Nesting of
     * IF-ENDIF pairs within another IF-ENDIF pair is allowed.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	ENDIF(Consequence.NON_CASCADING, false),

	/**
     * Resizes the current window to the given width.
     * <p><ul>
     * <li>Required: AttributeKey (can be either data property key or numeric value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	ENFORCE_SCREEN_WIDTH(Consequence.NON_CASCADING, false, true, true),

	/**
     * Resizes the current window to the given height.
     * <p><ul>
     * <li>Required: AttributeKey (can be either data property key or numeric value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	ENFORCE_SCREEN_HEIGHT(Consequence.NON_CASCADING, false, true, true),

	/**
     * Triggers an 'error' in the output report. This operation is typically used in conjunction with an 'IF'
     * operation, such as IF_ABSENT. For example, if an normally expected button does not appear (IF_ABSENT),
     * then report that as an error (ERROR).
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	ERROR(Consequence.NON_CASCADING, false),

    ELSE(Consequence.NON_CASCADING, false),

	/**
     * Forces stoppage of the entire execution. This operation is typically used in conjunction with an 'IF'
     * operation, such as IF_NOT_CONTAINS_VALUE, in catastrophic situations. For example, if the login page
     * doesn't display correctly, then don't bother trying to execute the test.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	EXIT(Consequence.CASCADING, false, false, false, true, false, false, false, true),

	/**
     * Forces stoppage of the entire execution if the element associated with the given locator is not found.
     * This operation is simply the combination of IF_ABSENT and EXIT.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	EXIT_IF_ABSENT(Consequence.CASCADING, true, false, false, true, false, true, false, true),

	/**
     * Forces stoppage of the entire execution if the given function returns FALSE. This operation is the
     * combination of PERFORM (custom function class) and EXIT.
     * <p><ul>
     * <li>Required: Locator, CustomFunctionClass</li>
     * <li>Optional: AttributeKey (can be either data property key or string value)</li>
     * </ul>
     */
	EXIT_IF_FALSE(Consequence.CASCADING, true, false, false, true, false, true, false, true),

	/**
     * Forces stoppage of the entire execution if the element associated with the given locator is found.
     * This operation is simply the combination of IF_PRESENT and EXIT.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	EXIT_IF_PRESENT(Consequence.CASCADING, true, false, false, true, false, true, false, true),

	/**
     * Forces stoppage of the entire execution if the given function returns TRUE. This operation is the
     * combination of PERFORM (custom function class) and EXIT.
     * <p><ul>
     * <li>Required: Locator, CustomFunctionClass</li>
     * <li>Optional: AttributeKey (can be either data property key or string value)</li>
     * </ul>
     */
	EXIT_IF_TRUE(Consequence.CASCADING, true, false, false, true, false, true, false, true),

	/**
     * Forces stoppage of a Data/Element/Cache Loop. This operation should only be used within a loop construct. If an inner loop 
     * exists, both inner and outer loops will be stopped. If this operation is not defined within a loop, it will be reported as 
     * a warning.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	EXIT_LOOP(Consequence.CASCADING, false, false, false, false, false, false, false, true),

    /**
     * Forces the current inner Loop (DATALOOP) iteration to stop. Execution continues with the next outer loop iteration.
     * This operation should only be used within a loop construct. If it's not defined within a loop, it will be reported 
     * as a warning.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
    EXIT_INNER_LOOP(Consequence.CASCADING, false, false, false, false, false, false, false, true),

    /**
     * Forces the current iteration within a Loop to stop, similar to a loop 'continue' statement. This operation 
     * should only be used within a loop construct. If it's not defined within a loop, it will be reported as a warning.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
    EXIT_LOOP_ITERATION(Consequence.CASCADING, false, false, false, false, false, false, false, true),

    /**
     * Forces stoppage of a Multi-Row Data Loop. This operation should only be used within a script that's 
     * part of a Multi-Row Data Loop. If it's not within a Multi-Row Data Loop, it acts like an EXIT_TESTSHEET 
     * operation.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
    EXIT_MULTI_ROW_LOOP(Consequence.CASCADING, false, false, false, false, true, false, false, true),

    /**
     * Forces the current iteration within a Multi-Row Data Loop to stop, similar to a loop 'continue' 
     * statement. This operation should only be used within a script that's part of a Multi-Row Data Loop.
     * If it's not within a Multi-Row Data Loop, it acts like an EXIT_TESTSHEET operation.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
    EXIT_MULTI_ROW_LOOP_ITERATION(Consequence.CASCADING, false, false, false, false, true, false, false, true),
	/**
     * Forces stoppage of the current TestSheet (ie. spreadsheet). This operation is typically used in
     * conjunction with an 'IF' operation, such as IF_NOT_CONTAINS_VALUE, in situations for which attempting
     * to continue testing within the group is futile. For example, a spreadsheet titled 'Test_Family_Summary'
     * might have an EXIT_TESTSHEET to handle a 'System not responding' message.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	EXIT_TESTSHEET(Consequence.CASCADING, false, false, false, false, true, false, false, true),

	/**
     * Forces stoppage of the current TestSheet (ie. spreadsheet) if the element associated with the given
     * locator is not found. This operation is simply the combination of IF_ABSENT and EXIT_TESTSHEET.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	EXIT_TESTSHEET_IF_ABSENT(Consequence.CASCADING, true, false, false, false, true, true, false, true),

	/**
     * Forces stoppage of the current TestSheet (ie. spreadsheet) if the given function returns FALSE. This
     * operation is the combination of PERFORM (custom function class) and EXIT_TESTSHEET.
     * <p><ul>
     * <li>Required: Locator, CustomFunctionClass</li>
     * <li>Optional: AttributeKey (can be either data property key or string value)</li>
     * </ul>
     */
	EXIT_TESTSHEET_IF_FALSE(Consequence.CASCADING, true, false, false, false, true, true, false, true),

	/**
     * Forces stoppage of the current TestSheet (ie. spreadsheet) if the element associated with the given
     * locator is found. This operation is simply the combination of IF_PRESENT and EXIT_TESTSHEET.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	EXIT_TESTSHEET_IF_PRESENT(Consequence.CASCADING, true, false, false, false, true, true, false, true),

	/**
     * Forces stoppage of the current TestSheet (ie. spreadsheet) if the given function returns TRUE. This
     * operation is the combination of PERFORM (custom function class) and EXIT_TESTSHEET.
     * <p><ul>
     * <li>Required: Locator, CustomFunctionClass</li>
     * <li>Optional: AttributeKey (can be either data property key or string value)</li>
     * </ul>
     */
	EXIT_TESTSHEET_IF_TRUE(Consequence.CASCADING, true, false, false, false, true, true, false, true),

	//FOCUS,

	/**
     * Returns the text for the element associated with the given locator. Used mainly for debugging
     * purposes.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	GETTEXT(Consequence.NON_CASCADING, true),

	/**
     * Redirects to the given URL.
     * <p><ul>
     * <li>Required: AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: Locator</li>
     * </ul>
     */
	GOTOURL(Consequence.CASCADING, false, true, false, true),

	/**
     * Pauses execution of the test and presents a dialog that must be accepted to continue
     * the test. For multiple tests running in parallel, a single dialog will display and
     * acceptance of this dialog will restart execution of all tests.
     *  This operation is useful for debugging and demos.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	HALT(Consequence.NON_CASCADING, false),

	/**
     * Operand that ensures that all subsequent element operations exclude hidden elements in the element search. 
     * This operation is typically used to 'turn off' the {@link #HIDDEN_ELEMENTS_INCLUDED} operation, since hidden 
     * elements are excluded by default.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	HIDDEN_ELEMENTS_EXCLUDED(Consequence.NON_CASCADING, false),

	/**
     * Operand that ensures that all subsequent element operations return hidden elements in the element search. 
     * Note that the {@link #HIDDEN_ELEMENTS_EXCLUDED} operation can be used downstream to 'turn off' this operation.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	HIDDEN_ELEMENTS_INCLUDED(Consequence.NON_CASCADING, false),

	/**
     * Conditional operand that determines if the element associated with the given locator does not exist.
     * The result is a success if the element is NOT found.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	IF_ABSENT(Consequence.CASCADING, true, false, false, false, false, true),

	/**
     * Conditional operand that determines if sum of the numeric values extracted from element(s) for the
     * given locator match the given amount. The result is a success if the amounts match. Note that this
     * operation is not decimal-safe, in that it simply extracts and concatenates all digits from the element \
     * text. For example, if an element contains the text '1X234.56', the resulting numeric value will be 123456.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	IF_AMOUNTS_MATCH(Consequence.CASCADING, true, true, false, false, false, true),

	/**
     * Conditional operand that determines if the browser being used is CHROME.
     * The result is true if the browser being used is CHROME, false otherwise.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	IF_BROWSER_CHROME(Consequence.CASCADING, false, false, false, false, false, true),

	/**
     * Conditional operand that determines if the browser being used is FIREFOX.
     * The result is true if the browser being used is FIREFOX, false otherwise.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	IF_BROWSER_FIREFOX(Consequence.CASCADING, false, false, false, false, false, true),

	/**
     * Conditional operand that determines if the browser being used is INTERNET EXPLORER.
     * The result is true if the browser being used is INTERNET EXPLORER, false otherwise.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	IF_BROWSER_IE(Consequence.CASCADING, false, false, false, false, false, true),

	/**
     * Conditional operand that determines if the element associated with the given locator contains a
     * given value. The result is a success if the element is found and its contents contain the value.
     * NOTE: This operation should only be used within a LOOP construct.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	IF_CHILD_CONTAINS_VALUE(Consequence.CASCADING, true, false, false, false, false, true, false, false, true, true),

    /**
     * Conditional operand that determines if the element associated with the given locator contains ALL of the values within 
     * the given collection (case-insensitive). The collection of values can be either a comma delimited or space delimited string.  
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
    IF_CONTAINS_ALL(Consequence.CASCADING, true, true, false, false, false, true),

    /**
     * Conditional operand that determines if the element associated with the given locator contains any of the values within 
     * the given collection (case-insensitive). The collection of values can be either a comma delimited or space delimited string. 
     * A cache key name can also be provided as the 3rd argument, and if provided, the index of the value that is deemed a match 
     * will be stored to cache under that name.  
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
    IF_CONTAINS_ANY(Consequence.CASCADING, true, true, false, false, false, true),

    /**
     * Conditional operand that determines if the element associated with the given locator contains a
     * given value. The result is a success if the element is found and its contents contain the value.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	IF_CONTAINS_VALUE(Consequence.CASCADING, true, true, false, false, false, true),

	/**
     * Conditional operand that determines if the number of elements associated with the given locator
     * matches the given value. The result is a success if the aforementioned is true.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or numeric value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	IF_COUNT_EQUAL_TO(Consequence.CASCADING, true, true, true, false, false, true),

	/**
     * Conditional operand that determines if the number of elements associated with the given locator
     * exceed the given value. The result is a success if the aforementioned is true.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or numeric value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	IF_COUNT_GREATER_THAN(Consequence.CASCADING, true, true, true, false, false, true),

	/**
     * Conditional operand that determines if the number of elements associated with the given locator
     * is less than the given value. The result is a success if the aforementioned is true.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or numeric value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	IF_COUNT_LESS_THAN(Consequence.CASCADING, true, true, true, false, false, true),

	/**
	 * Conditional operand that determines if the data property key provided in the attributeValue exists 
	 * and contains the given value (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [data property key]|[comparison value]</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	IF_DATA_PROPERTY_CONTAINS(Consequence.CASCADING, new TwoStringsParser(), false, true),
	
	/**
	 * Conditional operand that determines if the data property key provided in the attributeValue exists 
	 * and equals the given value (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [data property key]|[comparison value]</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	IF_DATA_PROPERTY_EQUALS(Consequence.CASCADING, new TwoStringsParser(), false, true),
	
	/**
	 * Conditional operand that determines if the data property key provided in the attributeValue exists 
	 * and is not empty.
	 * <p><ul>
	 * <li>Required: AttributeKey (can be either data property key or string value)</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	IF_DATA_PROPERTY_EXISTS(Consequence.CASCADING, false, true, false, false, false, true),
	
	/**
	 * Conditional operand that determines if the data property key provided in the attributeValue exists 
	 * and does NOT contain the given value (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [data property key]|[comparison value]</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	IF_DATA_PROPERTY_NOT_CONTAINS(Consequence.CASCADING, new TwoStringsParser(), false, true),
	
	/**
	 * Conditional operand that determines if the data property key provided in the attributeValue exists 
	 * and does NOT equal the given value (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [data property key]|[comparison value]</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	IF_DATA_PROPERTY_NOT_EQUALS(Consequence.CASCADING, new TwoStringsParser(), false, true),
	
	/**
	 * Conditional operand that determines if the data property key provided in the attributeValue does 
	 * NOT exist.
	 * <p><ul>
	 * <li>Required: AttributeKey (can be either data property key or string value)</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	IF_DATA_PROPERTY_NOT_EXISTS(Consequence.CASCADING, false, true, false, false, false, true),
	
	/**
     * Conditional operand that determines if the element associated with the given locator is disabled.
     * The result is a success if the element is found and is disabled.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	IF_DISABLED(Consequence.CASCADING, true, false, false, false, false, true),

	/**
     * Conditional operand that determines if the element associated with the given locator is empty.
     * The result is a success if the element is found and is empty.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	IF_EMPTY(Consequence.CASCADING, true, false, false, false, false, true),

	/**
     * Conditional operand that determines if the element associated with the given locator is enabled.
     * The result is a success if the element is found and is enabled.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	IF_ENABLED(Consequence.CASCADING, true, false, false, false, false, true),

	/**
     * Conditional operand that determines if any TestStep within the TestCase associated with the given
     * identifier failed. The result is a success if a failed TestStep was found.
     * <p><ul>
     * <li>Required: AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	IF_FAILURE_IN_TESTCASE(Consequence.CASCADING, false, false, false, false, false, true),

	/**
	 * Conditional operand that determines if help text/tool tip appears for a given element. This is done by
	 * finding an element and checking for the web element's <b>validationMessage</b> attribute. This could be
	 * used for checking required fields or whenever field validation is done and a tooltip or help text is used
	 * to display a message. The result is a success if the element is found and contains a validation message.
	 * <p><ul>
	 * <li>Required: Locator</li>
	 * <li>Optional: None</li>
     * </ul>
	 */
	IF_HELPTEXT(Consequence.CASCADING, true, false, false, false, false, true),

	/**
	 * Conditional operand that determines if the given cache key does NOT exist in cache. The result is a success if 
	 * the key does not exist in cache.
	 * <p><ul>
     * <li>Required: AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
	 */
	IF_KEY_ABSENT(Consequence.CASCADING, false, true, false, false, false, true),

	/**
	 * Conditional operand that determines if the given cache key exists in cache. The result is a success if 
	 * the key exists in cache.
	 * <p><ul>
     * <li>Required: AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
	 */
	IF_KEY_PRESENT(Consequence.CASCADING, false, true, false, false, false, true),

	/**
	 * Conditional operand that determines if the element associated with the given locator has the :invalid
	 * css pseudo class. The result is a success if the pseudo class is found associated with the web element.
	 * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
	 */
	IF_INVALID_BY_CSS(Consequence.CASCADING, true, false, false, false, false, true),

	/**
	 * Conditonal operand that determines if the element associated with the given locator has the :optional
	 * css pseudo class. The result is a success if the pseudo class is found associated with the web element.
	 * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
	 */
	IF_OPTIONAL_BY_CSS(Consequence.CASCADING, true, false, false, false, false, true),

    /**
     * Conditional operand that determines if the element associated with the given locator DOES NOT contain any of the values within 
     * the given collection (case-insensitive). The collection of values can be either a comma delimited or space delimited string. 
     * A cache key name can also be provided as the 3rd argument, and if provided, the index of the value that is deemed a match 
     * will be stored to cache under that name.  
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
    IF_NOT_CONTAINS_ANY(Consequence.CASCADING, true, true, false, false, false, true),

	/**
     * Conditional operand that determines if the element associated with the given locator DOES NOT
     * contain a given value. The result is a success if the element is found and its contents DO NOT
     * contain the value.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	IF_NOT_CONTAINS_VALUE(Consequence.CASCADING, true, false, false, false, false, true),

	/**
     * Conditional operand that determines if the element associated with the given locator is not empty.
     * The result is a success if the element is found and is not empty.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	IF_NOT_EMPTY(Consequence.CASCADING, true, false, false, false, false, true),

	/**
     * Conditional operand that determines if the element associated with the given locator does NOT start with
     * the given value. The result is a success if the element is found and its contents do NOT start with the value.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	IF_NOT_STARTS_WITH_VALUE(Consequence.CASCADING, true, true, false, false, false, true),

	/**
     * Conditional operand that determines if the element associated with the given locator exists. The
     * result is a success if the element IS found.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	IF_PRESENT(Consequence.CASCADING, true, false, false, false, false, true),

    /**
     * Conditional operand that determines if the content of the given pseudo element associated with the given locator contains the given value.
     * The result is a success if the content of the given pseudo element for the given locator contains the given value.
     * <p><ul>
     * <li>Required: Locator, AttributeKey in the format [pseudo element i.e. :before]|[comparison value]</li>
     * <li>Optional: None</li>
     * </ul>
     */
    IF_PSEUDO_ELEMENT_CONTAINS(Consequence.CASCADING, new TwoStringsParser(), true, true),

    /**
     * Conditional operand that determines if the content of the given pseudo element associated with the given locator is equal to the given value.
     * The result is a success if the content of the given pseudo element for the given locator is equal to the given value.
     * <p><ul>
     * <li>Required: Locator, AttributeKey in the format [pseudo element i.e. :before]|[comparison value]</li>
     * <li>Optional: None</li>
     * </ul>
     */
    IF_PSEUDO_ELEMENT_EQUALS(Consequence.CASCADING, new TwoStringsParser(), true, true),

	/**
     * Conditional operand that determines if the RADIO element associated with the given locator is selected. 
     * The result is a success if the radio element is selected.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	IF_RADIO_CHECKED(Consequence.CASCADING, true, false, false, false, false, true),

	/**
     * Conditional operand that determines if the RADIO element associated with the given locator is NOT selected. 
     * The result is a success if the radio element is NOT selected.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	IF_RADIO_UNCHECKED(Consequence.CASCADING, true, false, false, false, false, true),

	/**
	 * Conditional operand that determines if the element associated with the given locator has the required
	 * attribute.  The result is a success if the element is a required element.
	 * <p><ul>
	 * <li>Required: Locator</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	IF_REQUIRED(Consequence.CASCADING, true, false, false, false, false, true),

	/**
	 * Conditional operand that determines if the element associated with the given locator has the input
	 * element selected.  The result is a success if the element is selected.
	 * <p><ul>
	 * <li>Required: Locator</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	IF_SELECTED(Consequence.CASCADING, true, false, false, false, false, true),

	/**
     * Conditional operand that determines if the element associated with the given locator starts with
     * the given value. The result is a success if the element is found and its contents start with the value.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	IF_STARTS_WITH_VALUE(Consequence.CASCADING, true, true, false, false, false, true),

	/**
	 * Conditional operand that determines if the value stored in cache under the given key contains
	 * the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [cache key]|[comparison value]</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	IF_STORED_VALUE_CONTAINS(Consequence.CASCADING, false, true, false, false, false, true),

	/**
	 * Conditional operand that determines if the value stored in cache under the given key contains
	 * the value retrieved from the element for the given locator. The comparison performed is
	 * case-insensitive.
	 * <p><ul>
	 * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	IF_STORED_VALUE_CONTAINS_ELEMENT_VALUE(Consequence.CASCADING, true, true, false, false, false, true),

    /**
     * Conditional operand that determines if the the given cache key does not exist in cache, or the value 
     * stored in cache under the given key is an empty string, 
     * <p><ul>
     * <li>Required: AttributeKey in the format [cache key]</li>
     * <li>Optional: None</li>
     * </ul>
     */
    IF_STORED_VALUE_EMPTY(Consequence.CASCADING, false, true, false, false, false, true),

	/**
	 * Conditional operand that determines if the value stored in cache under the given key equals
	 * the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [cache key]|[comparison value]</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	IF_STORED_VALUE_EQUALS(Consequence.CASCADING, false, true, false, false, false, true),

	/**
	 * Conditional operand that determines if the value stored in cache under the given key starts with
	 * the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [cache key]|[comparison value]</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	IF_STORED_VALUE_STARTS_WITH(Consequence.CASCADING, false, true, false, false, false, true),

	/**
	 * Conditional operand that determines if the value stored in cache under the given key starts with
	 * the value retrieved from the element for the given locator. The comparison performed is
	 * case-insensitive.
	 * <p><ul>
	 * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	IF_STORED_VALUE_STARTS_WITH_ELEMENT_VALUE(Consequence.CASCADING, true, true, false, false, false, true),

	/**
	 * Conditional operand that determines if the value stored in cache under the given key does NOT
	 * contain the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [cache key]|[comparison value]</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	IF_STORED_VALUE_NOT_CONTAINS(Consequence.CASCADING, false, true, false, false, false, true),

	/**
	 * Conditional operand that determines if the value stored in cache under the given key does NOT
	 * contain the value retrieved from the element for the given locator. The comparison performed 
	 * is case-insensitive.
	 * <p><ul>
	 * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	IF_STORED_VALUE_NOT_CONTAINS_ELEMENT_VALUE(Consequence.CASCADING, true, true, false, false, false, true),

	/**
	 * Conditional operand that determines if the value stored in cache under the given key exists and contains
	 * a non-empty string, so this operation can be used to determine if a cache key exists.
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [cache key]</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	IF_STORED_VALUE_NOT_EMPTY(Consequence.CASCADING, false, true, false, false, false, true),

	/**
	 * Conditional operand that determines if the value stored in cache under the given key does NOT
	 * equal the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [cache key]|[comparison value]</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	IF_STORED_VALUE_NOT_EQUALS(Consequence.CASCADING, false, true, false, false, false, true),

	/**
	 * Conditional operand that determines if the value stored in cache under the given key does NOT start with
	 * the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [cache key]|[comparison value]</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	IF_STORED_VALUE_NOT_STARTS_WITH(Consequence.CASCADING, false, true, false, false, false, true),

	/**
	 * Conditional operand that determines if the value stored in cache under the given key does NOT start with
	 * the value retrieved from the element for the given locator. The comparison performed is
	 * case-insensitive.
	 * <p><ul>
	 * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	IF_STORED_VALUE_NOT_STARTS_WITH_ELEMENT_VALUE(Consequence.CASCADING, true, true, false, false, false, true),

	/**
	 * Conditional operand that determines if the element associated with the given locator has the input
	 * element unselected.  The result is a success if the element is unselected.
	 * <p><ul>
	 * <li>Required: Locator</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	IF_UNSELECTED(Consequence.CASCADING, true, false, false, false, false, true),

	/**
	 * Conditonal operand that determines if the element associated with the given locator has the :valid
	 * css pseudo class. The result is a success if the pseudo class is found associated with the web element.
	 * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
	 */
	IF_VALID_BY_CSS(Consequence.CASCADING, true, false, false, false, false, true),

	/**
	 * Conditional operand that determines if the given value contains the given string (case-insensitive). 
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
	 * <li>Optional: None</li>
	 * </ul>
 	 * <p>
 	 * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>someCacheKey|XXX</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] contains XXX)</td></tr>
	 * <tr><td></td><td>XXX|someCacheKey</td><td></td><td>(checking if XXX contains value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>XXX|+webdriver.data.key</td><td></td><td>(checking if XXX contains value in data properties file under key [webdriver.data.key])</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|XXX</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] contains XXX)</td></tr>
	 * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in cache under key [someCacheKey] contains value in data properties file under key [webdriver.data.key])</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] contains value in cache under key [someCacheKey])</td></tr>
	 * </table>
	 * <p> 
  	 * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
 	 * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
	 */
	IF_VALUE_CONTAINS(Consequence.CASCADING, new TwoStringsParser(), false, true),

    /**
     * Conditional operand that determines if the given value contains ALL of the values within the given collection (case-insensitive). 
     * The collection of values can be either a comma delimited or space delimited string. 
     * <p><ul>
     * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
     * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
     * <table border=0 cellspacing=2>
     * <tr><td>&nbsp;</td><td>someCacheKey|X, Y, Z</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] contains X and Y and Z)</td></tr>
     * <tr><td></td><td>XXX|someCacheKey</td><td></td><td>(checking if XXX contains ALL of the values in cache under key [someCacheKey])</td></tr>
     * <tr><td></td><td>XXX|+webdriver.list.of.values</td><td></td><td>(checking if XXX contains ALL of the values defined in data properties file under key [webdriver.list.of.values])</td></tr>
     * <tr><td></td><td>webdriver.list.of.values+|X Y Z</td><td></td><td>(checking if value in data properties file under key [webdriver.list.of.values] contains X and Y and Z)</td></tr>
     * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in cache under key [someCacheKey] contains ALL of the values defined in data properties file under key [webdriver.data.key])</td></tr>
     * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] contains ALL values in cache under key [someCacheKey])</td></tr>
     * </table>
     * <p> 
     * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
     * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
     */
    IF_VALUE_CONTAINS_ALL(Consequence.CASCADING, new TwoStringsParser(), false, true),

    /**
     * Conditional operand that determines if the given value contains any of the values within the given collection (case-insensitive). The collection 
     * of values can be either a comma delimited or space delimited string. A cache key name can also be provided as the 3rd argument, and if provided,
     * the index of the value that is deemed a match will be stored to cache under that name.  
     * <p><ul>
     * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]{|[literal, cache key, data property key]}</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
     * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
     * <table border=0 cellspacing=2>
     * <tr><td>&nbsp;</td><td>someCacheKey|X, Y, Z</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] contains X or Y or Z)</td></tr>
     * <tr><td></td><td>XXX|someCacheKey</td><td></td><td>(checking if XXX contains any of the values in cache under key [someCacheKey])</td></tr>
     * <tr><td></td><td>XXX|+webdriver.list.of.values</td><td></td><td>(checking if XXX contains any of the values defined in data properties file under key [webdriver.list.of.values])</td></tr>
     * <tr><td></td><td>webdriver.value+|X Y Z</td><td></td><td>(checking if value in data properties file under key [webdriver.value] contains X or Y or Z)</td></tr>
     * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in cache under key [someCacheKey] contains any of the values defined in data properties file under key [webdriver.data.key])</td></tr>
     * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] contains value in cache under key [someCacheKey])</td></tr>
     * <tr><td>&nbsp;</td><td>someCacheKey|X, Y, Z|keyIndex</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] contains X or Y or Z). The index of the matching value will be stored to cache under key [keyIndex].</td></tr>
     * <tr><td></td><td>XXX|someCacheKey|keyIndex</td><td></td><td>(checking if XXX contains any of the values in cache under key [someCacheKey]). The index of the matching value will be stored to cache under key [keyIndex].</td></tr>
     * </table>
     * <p> 
     * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
     * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
     */
    IF_VALUE_CONTAINS_ANY(Consequence.CASCADING, new TwoStringsThenOptionalStringParser(), false, true),

    /**
     * Conditional operand that determines if the given value contains the value within the given collection, at the given index (case-insensitive). 
     * The collection of values can be either a comma delimited or space delimited string. This operation is often used following IF_VALUE_CONTAINS_ANY,
     * using the cache key provided in that operation to establish a reference within a group of related collections. 
     * <p><ul>
     * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]{|[literal, cache key, data property key]}</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
     * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
     * <table border=0 cellspacing=2>
     * <tr><td>&nbsp;</td><td>someCacheKey|X, Y, Z|0</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] contains X)</td></tr>
     * <tr><td>&nbsp;</td><td>someCacheKey|X, Y, Z|1</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] contains Y)</td></tr>
     * <tr><td></td><td>XXX|someCacheKey|indexKey</td><td></td><td>(checking if XXX contains the value at index [indexKey] within the collection [someCacheKey])</td></tr>
     * <tr><td></td><td>XXX|+webdriver.list.of.values+|indexKey</td><td></td><td>(checking if XXX contains the value at index [indexKey] within the collection defined in data properties file under key [webdriver.list.of.values])</td></tr>
     * <tr><td></td><td>webdriver.value+|X Y Z|indexKey</td><td></td><td>(checking if value in data properties file under key [webdriver.value] contains the value at index [indexKey])</td></tr>
     * </table>
     * <p> 
     * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
     * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
     */
    IF_VALUE_CONTAINS_AT_INDEX(Consequence.CASCADING, new TwoStringsThenNumericParser(), false, true),

    /**
     * Conditional operand that determines if the attributeValue field is empty or contains only spaces.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
     * </ul>
     * <p>
     * <b>Note that this operation should NOT be used to test for the EXISTENCE of a cache key, since using a non-existing cache key 
     * would result in the cache key itself being interrogated for emptiness, thereby resulting in a false failure. Use IF_KEY_PRESENT
     * or IF_KEY_ABSENT instead.</b>
     */
    IF_VALUE_EMPTY(Consequence.CASCADING, false, false, false, false, false, true),
    
	/**
	 * Conditional operand that determines if the given value matches the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
	 * <li>Optional: None</li>
	 * </ul>
 	 * <p>
 	 * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>someCacheKey|XXX</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] is equal to XXX)</td></tr>
	 * <tr><td></td><td>XXX|someCacheKey</td><td></td><td>(checking if XXX is equal to value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>XXX|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] is equal to XXX)</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|XXX</td><td></td><td>(checking if XXX is equal to value in data properties file under key [webdriver.data.key])</td></tr>
	 * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] is equal to value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in cache under key [someCacheKey] is equal to value in data properties file under key [webdriver.data.key])</td></tr>
	 * </table>
	 * <p> 
  	 * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
 	 * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
	 */
	IF_VALUE_EQUALS(Consequence.CASCADING, new TwoStringsParser(), false, true),

    /**
     * Conditional operand that determines if the given value equals any of the values within the given collection (case-insensitive). The collection 
     * of values can be either a comma delimited or space delimited string. A cache key name can also be provided as the 3rd argument, and if provided,
     * the index of the value that is deemed a match will be stored to cache under that name.  
     * <p><ul>
     * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]{|[literal, cache key, data property key]}</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
     * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
     * <table border=0 cellspacing=2>
     * <tr><td>&nbsp;</td><td>someCacheKey|X, Y, Z</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] equals X or Y or Z)</td></tr>
     * <tr><td></td><td>XXX|someCacheKey</td><td></td><td>(checking if XXX equals any of the values in cache under key [someCacheKey])</td></tr>
     * <tr><td></td><td>XXX|+webdriver.list.of.values</td><td></td><td>(checking if XXX equals any of the values defined in data properties file under key [webdriver.list.of.values])</td></tr>
     * <tr><td></td><td>webdriver.value+|X Y Z</td><td></td><td>(checking if value in data properties file under key [webdriver.value] equals X or Y or Z)</td></tr>
     * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in cache under key [someCacheKey] equals any of the values defined in data properties file under key [webdriver.data.key])</td></tr>
     * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] equals value in cache under key [someCacheKey])</td></tr>
     * <tr><td>&nbsp;</td><td>someCacheKey|X, Y, Z|keyIndex</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] equals X or Y or Z). The index of the matching value will be stored to cache under key [keyIndex].</td></tr>
     * <tr><td></td><td>XXX|someCacheKey|keyIndex</td><td></td><td>(checking if XXX equals any of the values in cache under key [someCacheKey]). The index of the matching value will be stored to cache under key [keyIndex].</td></tr>
     * </table>
     * <p> 
     * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
     * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
     */
    IF_VALUE_EQUALS_ANY(Consequence.CASCADING, new TwoStringsThenOptionalStringParser(), false, true),

    /**
     * Conditional operand that determines if the given value equals the value within the given collection, at the given index (case-insensitive). 
     * The collection of values can be either a comma delimited or space delimited string. This operation is often used following IF_VALUE_CONTAINS_ANY,
     * using the cache key provided in that operation to establish a reference within a group of related collections. 
     * <p><ul>
     * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]{|[literal, cache key, data property key]}</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
     * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
     * <table border=0 cellspacing=2>
     * <tr><td>&nbsp;</td><td>someCacheKey|X, Y, Z|0</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] equals X)</td></tr>
     * <tr><td>&nbsp;</td><td>someCacheKey|X, Y, Z|1</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] equals Y)</td></tr>
     * <tr><td></td><td>XXX|someCacheKey|indexKey</td><td></td><td>(checking if XXX equals the value at index [indexKey] within the collection [someCacheKey])</td></tr>
     * <tr><td></td><td>XXX|+webdriver.list.of.values+|indexKey</td><td></td><td>(checking if XXX equals the value at index [indexKey] within the collection defined in data properties file under key [webdriver.list.of.values])</td></tr>
     * <tr><td></td><td>webdriver.value+|X Y Z|indexKey</td><td></td><td>(checking if value in data properties file under key [webdriver.value] equals the value at index [indexKey])</td></tr>
     * </table>
     * <p> 
     * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
     * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
     */
    IF_VALUE_EQUALS_AT_INDEX(Consequence.CASCADING, new TwoStringsThenNumericParser(), false, true),

	/**
	 * Conditional operand that determines if the given (numeric) value is greater than given value.
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
	 * <li>Optional: None</li>
	 * </ul>
 	 * <p>
 	 * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>someCacheKey|nnn</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] is greater than nnn)</td></tr>
	 * <tr><td></td><td>nnn|someCacheKey</td><td></td><td>(checking if nnn is greater than the value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>nnn|+webdriver.data.key</td><td></td><td>(checking if nnn is greater than the value in data properties file under key [webdriver.data.key]</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|nnn</td><td></td><td>(checking if the value in data properties file under key [webdriver.data.key] is greater than nnn)</td></tr>
	 * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] is greater than value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in cache under key [someCacheKey] is greater than the value in data properties file under key [webdriver.data.key])</td></tr>
	 * </table>
	 * <p> 
  	 * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
 	 * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
	 */
	IF_VALUE_GREATER_THAN(Consequence.CASCADING, false, true, false, false, false, true),

	/**
	 * Conditional operand that determines if the given (numeric) value is within the given range (or ranges, if multiple ranges are provided) inclusive. 
	 * Any range of values must be in the format r1:r2 (ex. 10 thru 100). Multiple ranges must be separated by a comma (ex. 10:100, 150:300).
	 * Negative values are allowed and should be indicated using a minus (-) sign.  
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
	 * <li>Optional: None</li>
	 * </ul>
 	 * <p>
 	 * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>someCacheKey|10:50</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] is within the range 10 thru 50, inclusive)</td></tr>
     * <tr><td>&nbsp;</td><td>someCacheKey|10:50,75:100</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] is within the range 10 thru 50 OR 75 thru 100, inclusive)</td></tr>
	 * <tr><td></td><td>nnn|someCacheKey</td><td></td><td>(checking if nnn is within the range in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>nnn|+webdriver.data.key</td><td></td><td>(checking if nnn is within the range defined in data properties file under key [webdriver.data.key]</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|10:50</td><td></td><td>(checking if the value in data properties file under key [webdriver.data.key] is within the range 10 thru 50, inclusive)</td></tr>
	 * </table>
	 * <p> 
  	 * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
 	 * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
	 */
	IF_VALUE_IN_RANGE(Consequence.CASCADING, new TwoStringsParser(), false, true),

    /**
     * Conditional operand that determines if the given (numeric) value is less than given value.
     * <p><ul>
     * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
     * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
     * <table border=0 cellspacing=2>
     * <tr><td>&nbsp;</td><td>someCacheKey|nnn</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] is less than nnn)</td></tr>
     * <tr><td></td><td>nnn|someCacheKey</td><td></td><td>(checking if nnn is less than the value in cache under key [someCacheKey])</td></tr>
     * <tr><td></td><td>nnn|+webdriver.data.key</td><td></td><td>(checking if nnn is less than the value in data properties file under key [webdriver.data.key]</td></tr>
     * <tr><td></td><td>webdriver.data.key+|nnn</td><td></td><td>(checking if the value in data properties file under key [webdriver.data.key] is less than nnn)</td></tr>
     * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] is less than value in cache under key [someCacheKey])</td></tr>
     * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in cache under key [someCacheKey] is less than the value in data properties file under key [webdriver.data.key])</td></tr>
     * </table>
     * <p> 
     * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
     * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
     */
    IF_VALUE_LESS_THAN(Consequence.CASCADING, false, true, false, false, false, true),

	/**
	 * Conditional operand that determines if the given value starts with the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
	 * <li>Optional: None</li>
	 * </ul>
 	 * <p>
 	 * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>someCacheKey|XXX</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] starts with XXX)</td></tr>
	 * <tr><td></td><td>XXX|someCacheKey</td><td></td><td>(checking if XXX starts with value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>XXX|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] starts with XXX)</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|XXX</td><td></td><td>(checking if XXX starts with value in data properties file under key [webdriver.data.key])</td></tr>
	 * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] starts with value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in cache under key [someCacheKey] starts with value in data properties file under key [webdriver.data.key])</td></tr>
	 * </table>
	 * <p> 
  	 * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
 	 * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
	 */
	IF_VALUE_STARTS_WITH(Consequence.CASCADING, new TwoStringsParser(), false, true),

	/**
	 * Conditional operand that determines if the given value does NOT contain the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
	 * <li>Optional: None</li>
	 * </ul>
 	 * <p>
 	 * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>someCacheKey|XXX</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] does NOT contain XXX)</td></tr>
	 * <tr><td></td><td>XXX|someCacheKey</td><td></td><td>(checking if XXX does NOT contain value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>XXX|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] does NOT contain XXX)</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|XXX</td><td></td><td>(checking if XXX does NOT contain value in data properties file under key [webdriver.data.key])</td></tr>
	 * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] does NOT contain value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in cache under key [someCacheKey] does NOT contain value in data properties file under key [webdriver.data.key])</td></tr>
	 * </table>
	 * <p> 
  	 * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
 	 * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
	 */
	IF_VALUE_NOT_CONTAINS(Consequence.CASCADING, new TwoStringsParser(), false, true),

    /**
     * Conditional operand that determines if the given value does NOT contain any of the values within the given collection (case-insensitive). The collection 
     * of values can be either a comma delimited or space delimited string.   
     * <p><ul>
     * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
     * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
     * <table border=0 cellspacing=2>
     * <tr><td>&nbsp;</td><td>someCacheKey|X, Y, Z</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] does NOT contain X or Y or Z)</td></tr>
     * <tr><td></td><td>XXX|someCacheKey</td><td></td><td>(checking if XXX does NOT contain any of the values in cache under key [someCacheKey])</td></tr>
     * <tr><td></td><td>XXX|+webdriver.list.of.values</td><td></td><td>(checking if XXX does NOT contain any of the values defined in data properties file under key [webdriver.list.of.values])</td></tr>
     * <tr><td></td><td>webdriver.list.of.values+|X Y Z</td><td></td><td>(checking if value in data properties file under key [webdriver.list.of.values] does NOT contain X or Y or Z)</td></tr>
     * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in cache under key [someCacheKey] does NOT contain any of the values defined in data properties file under key [webdriver.data.key])</td></tr>
     * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] does NOT contain value in cache under key [someCacheKey])</td></tr>
     * </table>
     * <p> 
     * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
     * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
     */
    IF_VALUE_NOT_CONTAINS_ANY(Consequence.CASCADING, new TwoStringsParser(), false, true),

    /**
     * Conditional operand that determines if the attributeValue field is contains any non-space characters.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
     * </ul>
     * <p>
     * <b>Note that this operation should NOT be used to test for the EXISTENCE of a cache key, since using a non-existing cache key 
     * would result in the cache key itself being interrogated for emptiness, thereby resulting in a false failure. Use IF_KEY_PRESENT
     * or IF_KEY_ABSENT instead.</b>
     */
    IF_VALUE_NOT_EMPTY(Consequence.CASCADING, false, false, false, false, false, true),

	/**
	 * Conditional operand that determines if the given value matches the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
	 * <li>Optional: None</li>
	 * </ul>
 	 * <p>
 	 * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>someCacheKey|XXX</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] does NOT equal XXX)</td></tr>
	 * <tr><td></td><td>XXX|someCacheKey</td><td></td><td>(checking if XXX does NOT equal value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>XXX|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] does NOT equal XXX)</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|XXX</td><td></td><td>(checking if XXX does NOT equal value in data properties file under key [webdriver.data.key])</td></tr>
	 * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] does NOT equal value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in cache under key [someCacheKey] does NOT equal value in data properties file under key [webdriver.data.key])</td></tr>
	 * </table>
	 * <p> 
  	 * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
 	 * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
	 */
	IF_VALUE_NOT_EQUALS(Consequence.CASCADING, new TwoStringsParser(), false, true),

    /**
     * Conditional operand that determines if the given value does NOT equal any of the values within the given collection (case-insensitive). The collection 
     * of values can be either a comma delimited or space delimited string.   
     * <p><ul>
     * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
     * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
     * <table border=0 cellspacing=2>
     * <tr><td>&nbsp;</td><td>someCacheKey|X, Y, Z</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] does NOT equal X or Y or Z)</td></tr>
     * <tr><td></td><td>XXX|someCacheKey</td><td></td><td>(checking if XXX does NOT equal any of the values in cache under key [someCacheKey])</td></tr>
     * <tr><td></td><td>XXX|+webdriver.list.of.values</td><td></td><td>(checking if XXX does NOT equal any of the values defined in data properties file under key [webdriver.list.of.values])</td></tr>
     * <tr><td></td><td>webdriver.list.of.values+|X Y Z</td><td></td><td>(checking if value in data properties file under key [webdriver.list.of.values] does NOT equal X or Y or Z)</td></tr>
     * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in cache under key [someCacheKey] does NOT equal any of the values defined in data properties file under key [webdriver.data.key])</td></tr>
     * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] does NOT equal value in cache under key [someCacheKey])</td></tr>
     * </table>
     * <p> 
     * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
     * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
     */
    IF_VALUE_NOT_EQUALS_ANY(Consequence.CASCADING, new TwoStringsParser(), false, true),

	/**
	 * Conditional operand that determines if the given value starts with the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
	 * <li>Optional: None</li>
	 * </ul>
 	 * <p>
 	 * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>someCacheKey|XXX</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] does NOT start with XXX)</td></tr>
	 * <tr><td></td><td>XXX|someCacheKey</td><td></td><td>(checking if XXX does NOT start with value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>XXX|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] does NOT start with XXX)</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|XXX</td><td></td><td>(checking if XXX does NOT start with value in data properties file under key [webdriver.data.key])</td></tr>
	 * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] does NOT start with value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in cache under key [someCacheKey] does NOT start with value in data properties file under key [webdriver.data.key])</td></tr>
	 * </table>
	 * <p> 
  	 * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
 	 * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
	 */
	IF_VALUE_NOT_STARTS_WITH(Consequence.CASCADING, new TwoStringsParser(), false, true),

	/**
     * Triggers an informational message in the output report. This operation is typically used in conjunction with an 'IF'
     * operation, such as IF_ABSENT. For example, if an normally expected button does not appear (IF_ABSENT),
     * then report it (INFO).
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	INFO(Consequence.NON_CASCADING, false),

    /**
     * Triggers an informational message in the output report for each loop iteration. This operation should not be used
     * directly.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
    INFO_LOOP(Consequence.NON_CASCADING, false),

    /**
     * Converts the given Json string into a Json object, then 'flattens' the object into key/value pairs that are then
     * loaded into cache, using the given prefix. For example, the following attributeKey:
     * <p>
     * <ul><li>subscriber|{ id: 123456, name: { first: John, last: Smith }}</li></ul>
     * <p>
     * will result in the following key/values pairs loaded to cache:
     * <p>
     * <table border=0 cellspacing=4>
     * <tr><td>subscriber:id</td><td>123456</td></tr>
     * <tr><td>subscriber:name:first</td><td>John</td></tr>
     * <tr><td>subscriber:name:last</td><td>Smith</td></tr>
     * </table>
     * <p><ul>
     * <li>Required: AttributeKey in the format [cache key prefix]|[json]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	JSON_TO_CACHE(Consequence.CASCADING, new TwoStringsParser(), false),
	
	/**
     * Displays all OBJECT, INPUT, SELECT, BUTTON, and IMAGE elements in the console. As a developer, this
     * operation is can assist in determining locator attributes, especially for those applications that
     * have disabled page source viewing and/or developer tools.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	LOCATE_ELEMENTS(Consequence.NON_CASCADING, false),

	/**
     * Operand that indicates a loop construct, meaning that all remaining TestSteps within the same TestCase
     * will be execute for every element returned for the given locator.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	LOOP(Consequence.NON_CASCADING, true),

	/**
     * Performs the given custom function. A custom function class provides the ability to perform any
     * application-specific process that subclasses com.bcbssc.webdriver.function.CustomFunction.
     * @see com.bcbssc.webdriver.function.SimpleFunction
     * <p><ul>
     * <li>Required: CustomFunctionClass (must be a subclass of SimpleFunction)</li>
     * <li>Optional: Locator, AttributeKey (can be either data property key or string value)</li>
     * </ul>
     */
	PERFORM(Consequence.CASCADING, false),

	/**
     * Performs the given custom function and halts execution if the function returns false. A custom
     * function class provides the ability to perform any application-specific process that subclasses
     * com.bcbssc.webdriver.function.CustomFunction.
     * @see com.bcbssc.webdriver.function.SimpleFunction
     * <p><ul>
     * <li>Required: CustomFunctionClass (must be a subclass of SimpleFunction)</li>
     * <li>Optional: Locator, AttributeKey (can be either data property key or string value)</li>
     * </ul>
     */
	PERFORM_AND_EXIT_IF_FALSE(Consequence.CASCADING, false, false, false, true, false, true, false, true),

	/**
     * Performs the given custom function and skips all remaining steps in the TestSheet (ie. spreadsheet)
     * if the function returns false. A custom function class provides the ability to perform any
     * application-specific process that subclasses com.bcbssc.webdriver.function.CustomFunction.
     * @see com.bcbssc.webdriver.function.SimpleFunction
     * <p><ul>
     * <li>Required: CustomFunctionClass (must be a subclass of SimpleFunction)</li>
     * <li>Optional: Locator, AttributeKey (can be either data property key or string value)</li>
     * </ul>
     */
	PERFORM_AND_EXIT_TESTSHEET_IF_FALSE(Consequence.CASCADING, false, false, false, false, true, true, false, true),

	/**
     * Performs the given custom function and executes downstream steps within the same TestCase if the
     * function returns true. A custom function class provides the ability to perform any application-specific
     * process that subclasses com.bcbssc.webdriver.function.CustomFunction.
     * @see com.bcbssc.webdriver.function.SimpleFunction
     * <p><ul>
     * <li>Required: CustomFunctionClass (must be a subclass of SimpleFunction)</li>
     * <li>Optional: Locator, AttributeKey (can be either data property key or string value)</li>
     * </ul>
     */
	PERFORM_AND_IF_TRUE(Consequence.CASCADING, false, false, false, false, false, true),

	/**
     * Performs the given custom function and executes downstream steps within the same TestCase if the
     * function returns false. A custom function class provides the ability to perform any application-specific
     * process that subclasses com.bcbssc.webdriver.function.CustomFunction.
     * @see com.bcbssc.webdriver.function.SimpleFunction
     * <p><ul>
     * <li>Required: CustomFunctionClass (must be a subclass of SimpleFunction)</li>
     * <li>Optional: Locator, AttributeKey (can be either data property key or string value)</li>
     * </ul>
     */
	PERFORM_AND_IF_FALSE(Consequence.CASCADING, false, false, false, false, false, true),

	/**
     * Performs the given custom function for all elements associated with the given locator. A custom
     * function class provides the ability to perform any application-specific process that subclasses
     * com.bcbssc.webdriver.function.CustomFunction.
     * @see com.bcbssc.webdriver.function.IterativeFunction
     * <p><ul>
     * <li>Required: Locator, CustomFunctionClass (must be a subclass of IterativeFunction)</li>
     * <li>Optional: AttributeKey (can be either data property key or string value)</li>
     * </ul>
     */
	PERFORM_FOR_EACH(Consequence.CASCADING, true),

	/**
     * Commercial Desktop-specific operand that refreshes the frame associated with the given locator.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	REFRESH_FRAME(Consequence.CASCADING, true),

	REFRESH_FRAME_RD3910(Consequence.CASCADING, true),


    /**
     * Performs a refresh/reload of the current page using the WebDriver object. Also performs a WAIT_UNTIL_PRESENT
     * operation for the given locator following the refresh, to provide a reasonable state for the next step in the
     * script to continue from.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: Timeout (in seconds) </li>
     * </ul>
     */
    REFRESH_PAGE(Consequence.CASCADING, true),

	/**
     * Removes any cached data whose key contains the given string.
     * <p><ul>
     * <li>Required: AttributeKey (can be either data property key or literal string)</li>
     * <li>Optional: None</li>
     * </ul>
     */
    REMOVE_KEYS(Consequence.NON_CASCADING, false, true),
    
	/**
     * Right-clicks the element associated with the given locator, at the given index.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	RIGHT_CLICK(Consequence.CASCADING, true),

	/**
	 * Saves the value associated with the given data property key to the associated data properties file. The 
	 * [new value] argument may be a literal string or a cache key. If the data property key already exists in 
	 * the properties file, the existing value will be saved under a timestamped key in the following format: 
	 * <p><ul>
	 * <li>[data property key].VALUE_REPLACED_YYYY_MM_DD_HH_MM_SS</li> 
     * </ul>
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [data property key]|[new value]</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	SAVE_DATA_PROPERTY(Consequence.NON_CASCADING, new TwoStringsParser(), false),
	
	/**
     * Captures a screenshot.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	SCREENSHOT(Consequence.NON_CASCADING, false, false, false, false, false, false, false, false, false, false, true),

	/**
     * Captures the screen using java.util.Robot.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	SCREENSHOT_VIA_ROBOT(Consequence.NON_CASCADING, false, false, false, false, false, false, false, false, false, false, true),
    
    /**
     * Scrolls to the element associated with the given locator.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
    SCROLL_INTO_VIEW(Consequence.NON_CASCADING, true),

	/**
     * Scrolls to the bottom of the element associated with the given locator.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	SCROLL_TO_BOTTOM(Consequence.NON_CASCADING, true),

	/**
     * Scrolls to the top of the element associated with the given locator.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	SCROLL_TO_TOP(Consequence.NON_CASCADING, true),

	/**
     * Selects the option at the given index for the SELECT element associated with the given locator.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or numeric value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	SELECT_BY_INDEX(Consequence.CASCADING, true, true, true),

	/**
     * Selects an option randomly from a SELECT element by selecting an index from 1 to [number of options].
     * It assumes that the first option is a default option and therefore will not include index 0 as a
     * possible selection.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	SELECT_BY_RANDOM(Consequence.CASCADING, true),

	/**
     * Selects the option whose value matches the given value (case-SENSITIVE), for the SELECT element
     * associated with the given locator.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or simple value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	SELECT_BY_VALUE(Consequence.CASCADING, true, true),

	/**
     * Selects the option whose visible text matches the given value (case-SENSITIVE), for the SELECT element
     * associated with the given locator.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or simple value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	SELECT_BY_VISIBLE_TEXT(Consequence.CASCADING, true, true),

	/**
     * Selects a drop down option (based on the index value pulled from cache) for the SELECT element associated
     * with the given locator. This operation is useful when performing a search based on data pulled off
     * a prior page. See <b>CSR_Claims_Health_Search_For_Claim.xls</b> or
     * <b>CSR_Claims_Dental_Search_For_Claim.xls</b> for examples.
     * <p>
     * NOTE: The cache must be populated PRIOR to this operation (typically via a custom class).
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or simple value)</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
     * The AttributeKey should be a pipe delimited string in one of the following formats:
     * <p><ul>
     * <li>[cache-key] | [map-key]</li>
     * <li>[cache-key] | [map-key] |LEFT| [integer-value] - returns the leftmost n characters as defined by [integer-value]</li>
     * <li>[cache-key] | [map-key] |RIGHT| [integer-value] - returns the rightmost n characters as defined by [integer-value]</li>
     * <li>[cache-key] | [map-key] |TOKEN| [delimiter] | [integer-value] - returns the token at position n as defined by [integer-value]</li>
     * <li>[cache-key] | [map-key] |SPLIT| [delimiter] | [integer-value] - returns the value at position n as defined by [integer-value]</li>
     * </ul>
     */
	SELECT_INDEX_FROM_CACHE(Consequence.CASCADING, true, true),

	/**
     * Selects a drop down option (based on the text value pulled from cache) for the SELECT element associated
     * with the given locator. Note that this operand differs from SELECT_VALUE_FROM_CACHE in that it selects
     * based on the text that's seen by the user, NOT by the 'value' attribute of the option. This operation
     * is useful when performing a search based on data pulled off a prior page.
     * See <b>CSR_Claims_Health_Search_For_Claim.xls</b> or <b>CSR_Claims_Dental_Search_For_Claim.xls</b> for
     * examples.
     * <p>
     * NOTE: The cache must be populated PRIOR to this operation (typically via a custom class).
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or simple value)</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
     * The AttributeKey should be a pipe delimited string in one of the following formats:
     * <p><ul>
     * <li>[cache-key] | [map-key]</li>
     * <li>[cache-key] | [map-key] |LEFT| [integer-value] - returns the leftmost n characters as defined by [integer-value]</li>
     * <li>[cache-key] | [map-key] |RIGHT| [integer-value] - returns the rightmost n characters as defined by [integer-value]</li>
     * <li>[cache-key] | [map-key] |TOKEN| [delimiter] | [integer-value] - returns the token at position n as defined by [integer-value]</li>
     * <li>[cache-key] | [map-key] |SPLIT| [delimiter] | [integer-value] - returns the value at position n as defined by [integer-value]</li>
     * </ul>
     */
	SELECT_TEXT_FROM_CACHE(Consequence.CASCADING, true, true),

	/**
     * Selects a drop down (option based on the value pulled from cache) for the SELECT element associated
     * with the given locator. Note that this operand differs from SELECT_TEXT_FROM_CACHE in that it selects
     * based on the 'value' attribute of the option, NOT by the text that's seen by the user. This operation
     * is useful when performing a search based on data pulled off a prior page.
     * See <b>CSR_Claims_Health_Search_For_Claim.xls</b> or <b>CSR_Claims_Dental_Search_For_Claim.xls</b> for
     * examples.
     * <p>
     * NOTE: The cache must be populated PRIOR to this operation (typically via a custom class).
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or simple value)</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
     * The AttributeKey should be a pipe delimited string in one of the following formats:
     * <p><ul>
     * <li>[cache-key] | [map-key]</li>
     * <li>[cache-key] | [map-key] |LEFT| [integer-value] - returns the leftmost n characters as defined by [integer-value]</li>
     * <li>[cache-key] | [map-key] |RIGHT| [integer-value] - returns the rightmost n characters as defined by [integer-value]</li>
     * <li>[cache-key] | [map-key] |TOKEN| [delimiter] | [integer-value] - returns the token at position n as defined by [integer-value]</li>
     * <li>[cache-key] | [map-key] |SPLIT| [delimiter] | [integer-value] - returns the value at position n as defined by [integer-value]</li>
     * </ul>
     */
	SELECT_VALUE_FROM_CACHE(Consequence.CASCADING, true, true),

	/**
     * Sets the text for the element associated with the given locator.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or numeric value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	SETTEXT(Consequence.CASCADING, true, true),

	/**
     * Sets the text for the element associated with the given locator with a value pulled from cache.
     * <p>
     * NOTE: The cache must be populated PRIOR to this operation (typically via a custom class).
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or simple value)</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
     * The AttributeKey should be a pipe delimited string in one of the following formats:
     * <p><ul>
     * <li>[cache-key] | [map-key]</li>
     * <li>[cache-key] | [map-key] |LEFT| [integer-value] - returns the leftmost n characters as defined by [integer-value]</li>
     * <li>[cache-key] | [map-key] |RIGHT| [integer-value] - returns the rightmost n characters as defined by [integer-value]</li>
     * <li>[cache-key] | [map-key] |TOKEN| [delimiter] | [integer-value] - returns the token at position n as defined by [integer-value]</li>
     * <li>[cache-key] | [map-key] |SPLIT| [delimiter] | [integer-value] - returns the value at position n as defined by [integer-value]</li>
     * </ul>
     */
	SETTEXT_FROM_CACHE(Consequence.CASCADING, true, false, false, false, true),

    SETTEXT_VIA_JS(Consequence.CASCADING, true),

	/**
     * Skips all TestCases/TestSteps between the current TestStep and the given TestCase.
     * <p><ul>
     * <li>Required: AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	SKIP_TO_TESTCASE(Consequence.CASCADING, false, true),

	/**
     * Temporarily stores the result of the arithmetic expression provided in AttributeKey, under the key 
     * that is also provided in AttributeKey. Valid arithmetic operators are + (addition), - (subtraction),
     * * (multiplication), and / (division). this operation leverages ScriptEngine.eval to evaluate the
     * arithmetic expression, so complex expressions are possible. 
     * <p><ul>
     * <li>Required: AttributeKey [arithmetic expression]|[key as string])</li>
     * <li>Optional: None</li>
     * </ul>
     */
	STORE_CALC(Consequence.CASCADING, new TwoStringsParser(), false),

	/**
     * Temporarily stores the value of the element associated with the given locator, to be used in a later
     * step. The AttributeKey contains the lookup key of the value. If no AttributeKey is defined, a default
     * key is used.
     * NOTE: This operation should only be used within a LOOP construct, since the search is performed using
     * the LOOP element as the root.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: AttributeKey (can be either data property key or numeric value)</li>
     * </ul>
     */
	STORE_CHILD_VALUE(Consequence.CASCADING, true, false, false, false, false, false, false, false, true, true),

	/**
	 * Temporarily store the number of elements returned for the given locator, most likely to be 
	 * used in a later step. 
	 * <p><ul>
     * <li>Required: AttributeKey in the format [value]|[cache-key]
     * </ul>
	 */
	STORE_COUNT_AS_KEY(Consequence.CASCADING, true, true),

	/**
	 * Temporarily store a generated timestamp, possibly could be used in a later step for reference. The
	 * AttributeKey contains the lookup key of the value.
	 * <p><ul>
     * <li>Required: AttributeKey</li>
     * </ul>
	 */
	STORE_CURRENT_TIMESTAMP(Consequence.CASCADING, false),

	/**
	 * Temporarily store the date value of the element associated with the given locator, most likely to be 
	 * used in a later step. The element value must be a valid date format (either MM/DD/YYYY or YYYY-MM-DD).
	 * <p>
	 * The AttributeKey contains the cache key PREFIX under which the value will be stored. Both MM/DD/YYYY 
	 * and YYYY-MM-DD formats of the dates will be stored as follows:
	 * <p><ul>
	 * <li><cache-key>_MMDDYYYY: key under which the MM/DD/YYYY format is stored</li>
	 * <li><cache-key>_YYYYMMDD: key under which the YYYY-MM-DD format is stored</li>
     * </ul>
	 * <p>
	 * NOTE: The ADD_MONTHS_TO_STORED_DATE, ADD_DAYS_TO_STORED_DATE, ADD_YEARS_TO_STORED_DATE operations
	 * can be used to modify the date prior to any subsequent test steps.
	 * <p><ul>
     * <li>Required: AttributeKey in the format [value]|[cache-key]
     * </ul>
	 */
	STORE_DATE_AS_KEY(Consequence.CASCADING, true, true),

	/**
     * Temporarily stores the leftmost n characters of the element value associated with the given locator,
     * to be used in a later step. The AttributeKey can contain either a numeric value representing the number
     * of characters to store, or a pair of values (pipe-delimited), the first being a numeric value
     * representing the number of characters to store, and the 2nd being the cache key. If a cache key
     * is not provided, a default key is used.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: AttributeKey (numeric value or [numeric value]|[key as string])</li>
     * </ul>
     */
	STORE_LEFT_CHARS(Consequence.CASCADING, new NumericThenOptionalStringParser(), true),

	/**
     * Temporarily stores the rightmost n characters of the element value associated with the given locator,
     * to be used in a later step. The AttributeKey can contain either a numeric value representing the number
     * of characters to store, or a pair of values (pipe-delimited), the first being a numeric value
     * representing the number of characters to store, and the 2nd being the cache key. If a cache key
     * is not provided, a default key is used.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: AttributeKey (numeric value or [numeric value]|[key as string])</li>
     * </ul>
     */
	STORE_RIGHT_CHARS(Consequence.CASCADING, new NumericThenOptionalStringParser(), true),

	/**
     * Temporarily stores a portion of the element value associated with the given locator, to be used in
     * a later step. The AttributeKey contains the starting and ending indexes of the substring to be stored,
     * followed by the cache key (optional). If a cache key is not provided, a default key is used.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: AttributeKey in the format [start-pos]|[end-pos] or [start-pos]|[end-pos]|[key as string])</li>
     * </ul>
     */
	STORE_SUBSTR(Consequence.CASCADING, new TwoNumericsThenOptionalStringParser(), true),

	/**
	 * Store a generated date/time, possibly could be used in a later step for reference. The
	 * AttributeKey contains the cache key, along with any date calculation unit (DAY, MONTH, YEAR) a 
	 * corresponding incrementor/decrementor value, and the format of the date. If no format is provided 
	 * the date will be in format: MM/dd/yyyy. 
	 * <p><ul>
     * <li>Required: AttributeKey in the format [store_key]{|[DAY or MONTH or YEAR|[unit number]|[date_format]}</li>
     * <li>Optional: None</li>
     * </ul>
	 * <p>
	 * Example formats for AttributeKey are:
	 * <p><ul>
	 * <li><code>someCacheKey</code>
	 * <p>results in current date in MM/dd/yyyy being stored to cache under key <code>someCacheKey</code></li> 
	 * <li><code>someCacheKey|DAY|1</code>
	 * <p>results in tomorrow's date in MM/dd/yyyy being stored to cache under key <code>someCacheKey</code></li> 
	 * <li><code>someCacheKey|DAY|-1</code>
	 * <p>results in yesterday's date in MM/dd/yyyy being stored to cache under key <code>someCacheKey</code></li> 
	 * <li><code>someCacheKey|MONTH|1</code>
	 * <p>results in next month's date in MM/dd/yyyy being stored to cache under key <code>someCacheKey</code></li> 
	 * <li><code>someCacheKey|YEAR|1</code>
	 * <p>results in next year's date in MM/dd/yyyy being stored to cache under key <code>someCacheKey</code></li> 
	 * <li><code>someCacheKey|DAY|0|MM-dd-yy</code>
	 * <p>results in today's date in MM-dd-yy being stored to cache under key <code>someCacheKey</code></li> 
     * </ul>
	 */
	STORE_TIME(Consequence.CASCADING, false, true),

	/**
     * Temporarily stores the value of the element associated with the given locator, to be used in a later
     * step. The AttributeKey contains the lookup key of the value. If no AttributeKey is defined, a default
     * key is used.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: AttributeKey (can be either data property key or numeric value)</li>
     * </ul>
     * <p>
 	 * <b>Special Handling of Space-delimited Values</b>
 	 * <p>
	 * Values that contain a string of values (tokens) concatenated with spaces, such as individual names (JOHN R SMITH) 
	 * or procedure codes/modifiers (99213 00 00) will automatically be parsed and each saved to cache under unique 
	 * indexed keys, based off the original key. For example, the cacheKey|value pair:
	 * <p>
	 * <table><tr><td>&nbsp;</td><td>SOME_KEY|99213 01 02</td></tr></table>
 	 * <p>
	 * Results in the following cache key/values pairs being added to cache:
 	 * <p>
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>SOME_KEY</td><td>&nbsp;</td><td>99213 01 02</td></tr>
	 * <tr><td></td><td>SOME_KEY1</td><td></td><td>99213</td></tr>
	 * <tr><td></td><td>SOME_KEY2</td><td></td><td>01</td></tr>
	 * <tr><td></td><td>SOME_KEY3</td><td></td><td>02</td></tr>
	 * </table>
 	 * <p>
	 * This allows us to access either the full value, or individual tokens in downstream test steps.
 	 * <p>
 	 * <b>Special Handling of Values Containing Non-alphanumeric (ie. special) Characters</b>
 	 * <p>
	 * Values that contain non-alphanumeric characters (ex. slashes/dashes in date values, dollar signs/decimals 
     * in amounts) will automatically be parsed and saved to cache under a key in the format:
 	 * <p>
     * [original cache key]_UNFORMATTED
 	 * <p>
     * Below are some examples:
 	 * <p>
	 * <b>Example 1: Simple Value Containing Special Characters</b> 
 	 * <p>
     * The following cacheKey|value pair:
 	 * <p>
	 * <table><tr><td>&nbsp;</td><td>KEY_DATE|10/01/2019</td></tr></table>
 	 * <p>
	 * Results in the following cache key/values pairs being added to cache:
 	 * <p>
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>KEY_DATE</td><td>&nbsp;</td><td>10/01/2019</td></tr>
	 * <tr><td></td><td>KEY_DATE_UNFORMATTED</td><td></td><td>10012019</td></tr>
	 * </table>
 	 * <p>
 	 * <p>
	 * <b>Example 2: Multiple Space-delimited Tokens Containing Special Characters</b>
 	 * <p>
     * The following cacheKey|value pair:
 	 * <p>
	 * <table><tr><td>&nbsp;</td><td>KEY_DATES|10/01/2019 to 10/31/2019</td></tr></table>
 	 * <p>
	 * Results in the following cache key/values pairs being added to cache:
 	 * <p>
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>KEY_DATES</td><td>&nbsp;</td><td>10/01/2019 to 10/31/2019</td></tr>
	 * <tr><td></td><td>KEY_DATES1</td><td></td><td>10/01/2019</td></tr>
	 * <tr><td></td><td>KEY_DATES2</td><td></td><td>to</td></tr>
	 * <tr><td></td><td>KEY_DATES3</td><td></td><td>10/31/2019</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED</td><td></td><td>10012019 to 10312019</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED1</td><td></td><td>10012019</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED2</td><td></td><td>to</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED3</td><td></td><td>10312019</td></tr>
	 * </table>
     */
	STORE_VALUE(Consequence.CASCADING, true),

	/**
	 * Stores the given value in cache under the given key. AttributeKey contains the value|key pair.
	 * <p><ul>
     * <li>Required: AttributeKey in the format [value]|[cache-key]
     * </ul>
     * <p>
 	 * <b>Special Handling of First Token</b>
 	 * <p>
	 * This operation allows for the manipulation of any existing cached values via any number of AttributeFunctions, 
	 * arithmetic expressions, or literal strings, and stores the resulting value back to cache under the given 
	 * (2nd token) cache key. Below are just a few examples of what can be accomplished:
	 * <p>
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>Trimming leading/trailing spaces for cache key [someKey]</td><td>&nbsp;</td><td>TRIM[someKey]|someKey</td></tr>
	 * <tr><td>&nbsp;</td><td>Removing double-quotes for cache key [someKey]</td><td>&nbsp;</td><td>REMOVE[someKey|"]|someKey</td></tr>
	 * <tr><td>&nbsp;</td><td>Appending 'XXX' to the value for cache key [someKey]</td><td>&nbsp;</td><td>someKey+XXX|someKey</td></tr>
	 * <tr><td>&nbsp;</td><td>Adding 10 to the numeric contents for cache key [someKey]</td><td>&nbsp;</td><td>EVAL[ONLY_NUMERIC[someKey] + 10]|someKey</td></tr>
	 * <tr><td>&nbsp;</td><td>Combining cached values</td><td>&nbsp;</td><td>someKeyAsomeKeyB|someKeyC</td></tr>
	 * </table>
     * <p>
 	 * <b>Special Handling of Space-delimited Values</b>
 	 * <p>
	 * Values that contain a string of values (tokens) concatenated with spaces, such as individual names (JOHN R SMITH) 
	 * or procedure codes/modifiers (99213 00 00) will automatically be parsed and each saved to cache under unique 
	 * indexed keys, based off the original key. For example, the cacheKey|value pair:
	 * <p>
	 * <table><tr><td>&nbsp;</td><td>SOME_KEY|99213 01 02</td></tr></table>
 	 * <p>
	 * Results in the following cache key/values pairs being added to cache:
 	 * <p>
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>SOME_KEY</td><td>&nbsp;</td><td>99213 01 02</td></tr>
	 * <tr><td></td><td>SOME_KEY1</td><td></td><td>99213</td></tr>
	 * <tr><td></td><td>SOME_KEY2</td><td></td><td>01</td></tr>
	 * <tr><td></td><td>SOME_KEY3</td><td></td><td>02</td></tr>
	 * </table>
 	 * <p>
	 * This allows us to access either the full value, or individual tokens in downstream test steps.
 	 * <p>
 	 * <b>Special Handling of Values Containing Non-alphanumeric (ie. special) Characters</b>
 	 * <p>
	 * Values that contain non-alphanumeric characters (ex. slashes/dashes in date values, dollar signs/decimals 
     * in amounts) will automatically be parsed and saved to cache under a key in the format:
 	 * <p>
     * [original cache key]_UNFORMATTED
 	 * <p>
     * Below are some examples:
 	 * <p>
	 * <b>Example 1: Simple Value Containing Special Characters</b> 
 	 * <p>
     * The following cacheKey|value pair:
 	 * <p>
	 * <table><tr><td>&nbsp;</td><td>KEY_DATE|10/01/2019</td></tr></table>
 	 * <p>
	 * Results in the following cache key/values pairs being added to cache:
 	 * <p>
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>KEY_DATE</td><td>&nbsp;</td><td>10/01/2019</td></tr>
	 * <tr><td></td><td>KEY_DATE_UNFORMATTED</td><td></td><td>10012019</td></tr>
	 * </table>
 	 * <p>
 	 * <p>
	 * <b>Example 2: Multiple Space-delimited Tokens Containing Special Characters</b>
 	 * <p>
     * The following cacheKey|value pair:
 	 * <p>
	 * <table><tr><td>&nbsp;</td><td>KEY_DATES|10/01/2019 to 10/31/2019</td></tr></table>
 	 * <p>
	 * Results in the following cache key/values pairs being added to cache:
 	 * <p>
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>KEY_DATES</td><td>&nbsp;</td><td>10/01/2019 to 10/31/2019</td></tr>
	 * <tr><td></td><td>KEY_DATES1</td><td></td><td>10/01/2019</td></tr>
	 * <tr><td></td><td>KEY_DATES2</td><td></td><td>to</td></tr>
	 * <tr><td></td><td>KEY_DATES3</td><td></td><td>10/31/2019</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED</td><td></td><td>10012019 to 10312019</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED1</td><td></td><td>10012019</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED2</td><td></td><td>to</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED3</td><td></td><td>10312019</td></tr>
	 * </table>
	 */
	STORE_VALUE_AS_KEY(Consequence.CASCADING, false, true),

	/**
     * Temporarily stores the value of the element associated with the given locator, to be used in a later
     * step. This operand assumes multiple elements exist for the locator. The AttributeKey contains the
     * index to the element. The value is saved under the default cache key.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or numeric value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	STORE_VALUE_BY_INDEX(Consequence.CASCADING, true, true, true),

	/**
     * Forces invocation of driver.switchTo().defaultContent(), selects either the first frame on the page,
     * or the main document when a page contains iframes. This is used in cases where iframes are involved,
     * and returning to the page root is necessary for downstream operations.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	SWITCH_TO_DEFAULT_CONTENT(Consequence.NON_CASCADING, false),

	SWITCH_TO_FRAME(Consequence.CASCADING, false, true),

	VERIFY(Consequence.NON_CASCADING, true),

	/**
     * Verifies that the element associated with the given locator exists. The result is a success if the
     * element does NOT exist.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_ABSENT(Consequence.NON_CASCADING, true),

	/**
     * Verifies that the sum of the numeric values extracted from element(s) for the given locator matches
     * the given amount. The result is a success if the amounts match. Note that this operation is not
     * decimal-safe, in that it simply extracts and concatenates all digits from the element text. For
     * example, if an element contains the text '1X234.56', the resulting numeric value will be 123456.
     *
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or numeric value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_AMOUNTS_MATCH(Consequence.NON_CASCADING, true, true),

	/**
     * Verifies that the element associated with the given locator exists under the LOOP element. The
     * result is a success if the element does NOT exist.
     * NOTE: This operation should only be used within a LOOP construct, since the search is performed using
     * the LOOP element as the root.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_CHILD_ABSENT(Consequence.NON_CASCADING, true, false, false, false, false, false, false, false, true, true),

	/**
     * Verifies that the element associated with the given locator contains the given value.
     * NOTE: This operation should only be used within a LOOP construct, since the search is performed using
     * the LOOP element as the root.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_CHILD_CONTAINS_VALUE(Consequence.NON_CASCADING, true, true, false, false, false, false, false, false, true, true),

	/**
     * Verifies that the number of elements associated with the given locator matches the given numeric value.
     * NOTE: This operation should only be used within a LOOP construct, since the search is performed using
     * the LOOP element as the root.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or numeric value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_CHILD_COUNT(Consequence.NON_CASCADING, true, true, true, false, false, false, false, false, true, true),

	/**
     * Verifies that the element associated with the given locator does NOT contain the given value.
     * NOTE: This operation should only be used within a LOOP construct.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_CHILD_NOT_CONTAINS_VALUE(Consequence.NON_CASCADING, true, true, false, false, false, false, false, false, true, true),

	/**
     * Verifies that the element associated with the given locator exists. The result is a success if the
     * element exists.
     * NOTE: This operation should only be used within a LOOP construct, since the search is performed using
     * the LOOP element as the root.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_CHILD_PRESENT(Consequence.NON_CASCADING, true, false, false, false, false, false, false, false, true, true),

    /**
     * Verifies that the element associated with the given locator contains ALL of the values within the given collection (case-insensitive). 
     * The collection of values can be either a comma delimited or space delimited string.  
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
    VERIFY_CONTAINS_ALL(Consequence.NON_CASCADING, true, true),

    /**
     * Verifies that the element associated with the given locator contains any of the values within the given collection (case-insensitive). 
     * The collection of values can be either a comma delimited or space delimited string. A cache key name can also be provided as the 3rd 
     * argument, and if provided, the index of the value that is deemed a match will be stored to cache under that name.  
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
    VERIFY_CONTAINS_ANY(Consequence.NON_CASCADING, true, true),

	/**
     * Verifies that the element associated with the given locator contains the given value.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_CONTAINS_VALUE(Consequence.NON_CASCADING, true, true),

	/**
     * Verifies that the number of elements associated with the given locator matches the given numeric value. 
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or numeric value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_COUNT_EQUAL_TO(Consequence.NON_CASCADING, true, true, true),

	/**
     * Verifies that the number of elements associated with the given locator exceeds the given numeric value. 
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or numeric value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_COUNT_GREATER_THAN(Consequence.NON_CASCADING, true, true, true),

	/**
     * Verifies that the number of elements associated with the given locator is less than the given numeric value. 
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or numeric value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_COUNT_LESS_THAN(Consequence.NON_CASCADING, true, true, true),

	/**
     * Verifies that the element associated with the given locator is disabled.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_DISABLED(Consequence.NON_CASCADING, true),

	/**
     * Verifies that the element associated with the given locator is empty.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_EMPTY(Consequence.NON_CASCADING, true),

	/**
     * Verifies that the element associated with the given locator is enabled.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_ENABLED(Consequence.NON_CASCADING, true),

	/**
	 * Verifies help text/tool tip appears for a given element. This is done by finding an element and checking
	 * for the web elements <b>validationMessage</b> attribute. This could be used for checking required fields
	 * or whenever field validation is done and a tooltip or help text is used to display a message.
	 * <p><ul>
	 * <li>Required: Locator</li>
	 * <li>Optional: None</li>
     * </ul>
	 */
	VERIFY_HELPTEXT(Consequence.NON_CASCADING, true),

	/**
	 * Verify that the element associated with the given locator has the :invalid css pseudo class. The result
	 * is a success if the pseudo class is found associated with the web element.
	 * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
	 */
	VERIFY_INVALID_BY_CSS(Consequence.NON_CASCADING, true),

	/**
     * Verifies all locators associated with the TestCase. The comparison is performed between the TestCase
     * name and the locator name. If the locator name contains the TestCase name (case-insensitive), then
     * the locator is included in the verification.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_LOCATORS(Consequence.NON_CASCADING, false),

    /**
     * Verifies that the element associated with the given locator does not contain any of the values within the given collection (case-insensitive). 
     * The collection of values can be either a comma delimited or space delimited string. A cache key name can also be provided as the 3rd 
     * argument, and if provided, the index of the value that is deemed a match will be stored to cache under that name.  
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
    VERIFY_NOT_CONTAINS_ANY(Consequence.NON_CASCADING, true, true),
	/**
     * Verifies that the element associated with the given locator does not contain the given value.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_NOT_CONTAINS_VALUE(Consequence.NON_CASCADING, true, true),

	/**
     * Verifies that the element associated with the given locator is NOT empty.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_NOT_EMPTY(Consequence.NON_CASCADING, true),

	/**
     * Verifies that the element associated with the given locator does not start with the given value.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_NOT_STARTS_WITH_VALUE(Consequence.NON_CASCADING, true, true),

	/**
	 * Verify that the element associated with the given locator has the :optional css pseudo class. The result
	 * is a success if the pseudo class is found associated with the web element.
	 * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
	 */
	VERIFY_OPTIONAL_BY_CSS(Consequence.NON_CASCADING, true),

	/**
     * Verifies that the element associated with the given locator exists. The result is a success if the
     * element exists.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_PRESENT(Consequence.NON_CASCADING, true),

    /**
     * Verifies that the content of the given pseudo element associated with the given locator contains the given value.
     * The result is a success if the content of the given pseudo element for the given locator contains the given value.
     * <p><ul>
     * <li>Required: Locator, AttributeKey in the format [pseudo element i.e. :before]|[comparison value]</li>
     * <li>Optional: None</li>
     * </ul>
     */
    VERIFY_PSEUDO_ELEMENT_CONTAINS(Consequence.NON_CASCADING, new TwoStringsParser(), true, false),

    /**
     * Verifies that the content of the given pseudo element associated with the given locator is equal to the given value.
     * The result is a success if the content of the given pseudo element for the given locator is equal to the given value.
     * <p><ul>
     * <li>Required: Locator, AttributeKey in the format [pseudo element i.e. :before]|[comparison value]</li>
     * <li>Optional: None</li>
     * </ul>
     */
    VERIFY_PSEUDO_ELEMENT_EQUALS(Consequence.NON_CASCADING, new TwoStringsParser(), true, false),

	/**
     * Verifies that the RADIO element associated with the given locator is selected. The result is a
     * success if the radio element is selected.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_RADIO_CHECKED(Consequence.NON_CASCADING, true),

	/**
     * Verifies that the RADIO element associated with the given locator is NOT selected. The result is a
     * success if the radio element is NOT selected.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_RADIO_UNCHECKED(Consequence.NON_CASCADING, true),

	/**
	 * Verifies that the element associated with the given locator is REQUIRED. The result is a success
	 * if the element is a required element.
	 * <p><ul>
	 * <li>Required: Locator</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	VERIFY_REQUIRED(Consequence.NON_CASCADING, true),

	/**
	 * Verifies that the element associated with the given locator is selected. The result is a success
	 * if the element is selected.
	 * <p><ul>
	 * <li>Required: Locator</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	VERIFY_SELECTED(Consequence.NON_CASCADING, true),

	/**
     * Verifies that the element associated with the given locator starts with the given value.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	VERIFY_STARTS_WITH_VALUE(Consequence.NON_CASCADING, true, true),

	/**
	 * Verifies that the value stored in cache under the given key contains the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [cache key]|[comparison value]</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	VERIFY_STORED_VALUE_CONTAINS(Consequence.NON_CASCADING, false, true),

	/**
	 * Verifies that the value stored in cache under the given key contains the value retrieved from the element 
	 * for the given locator. The comparison performed is case-insensitive.
	 * <p><ul>
	 * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	VERIFY_STORED_VALUE_CONTAINS_ELEMENT_VALUE(Consequence.NON_CASCADING, true, true),

    /**
     * Verifies that the the given cache key does not exist in cache, or the value stored in cache under the 
     * given key is an empty string, 
     * <p><ul>
     * <li>Required: AttributeKey in the format [cache key]</li>
     * <li>Optional: None</li>
     * </ul>
     */
    VERIFY_STORED_VALUE_EMPTY(Consequence.NON_CASCADING, false, true),

	/**
	 * Verifies that the value stored in cache under the given key equals the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [cache key]|[comparison value]</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	VERIFY_STORED_VALUE_EQUALS(Consequence.NON_CASCADING, false, true),

	/**
	 * Verifies that the value stored in cache under the given key starts with the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [cache key]|[comparison value]</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	VERIFY_STORED_VALUE_STARTS_WITH(Consequence.NON_CASCADING, false, true),

	/**
	 * Verifies that the value stored in cache under the given key starts with the value retrieved from the element 
	 * for the given locator. The comparison performed is case-insensitive.
	 * <p><ul>
	 * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	VERIFY_STORED_VALUE_STARTS_WITH_ELEMENT_VALUE(Consequence.NON_CASCADING, true, true),

	/**
	 * Verifies that the value stored in cache under the given key does NOT contain the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [cache key]|[comparison value]</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	VERIFY_STORED_VALUE_NOT_CONTAINS(Consequence.NON_CASCADING, false, true),

	/**
	 * Verifies that the value stored in cache under the given key does NOT contain the value retrieved from 
	 * the element for the given locator. The comparison performed is case-insensitive.
	 * <p><ul>
	 * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	VERIFY_STORED_VALUE_NOT_CONTAINS_ELEMENT_VALUE(Consequence.NON_CASCADING, true, true),

	/**
	 * Verifies that the value stored in cache under the given key exists and contains a non-empty string, 
	 * so this operation can be used to determine if a cache key exists.
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [cache key]</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	VERIFY_STORED_VALUE_NOT_EMPTY(Consequence.NON_CASCADING, false, true),

	/**
	 * Verifies that the value stored in cache under the given key does NOT equal the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [cache key]|[comparison value]</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	VERIFY_STORED_VALUE_NOT_EQUALS(Consequence.NON_CASCADING, false, true),

	/**
	 * Verifies that the value stored in cache under the given key does NOT start with the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [cache key]|[comparison value]</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	VERIFY_STORED_VALUE_NOT_STARTS_WITH(Consequence.NON_CASCADING, false, true),

	/**
	 * Verifies that the value stored in cache under the given key does NOT start with the value retrieved from 
	 * the element for the given locator. The comparison performed is case-insensitive.
	 * <p><ul>
	 * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	VERIFY_STORED_VALUE_NOT_STARTS_WITH_ELEMENT_VALUE(Consequence.NON_CASCADING, true, true),

	/**
	 * Verifies that the element associated with the given locator is not selected. The result is a success
	 * if the element is unselected.
	 * <p><ul>
	 * <li>Required: Locator</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	VERIFY_UNSELECTED(Consequence.NON_CASCADING, true),

	/**
	 * Verify that the current URL contains the given value.
	 * <p><ul>
     * <li>Required: AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
	 */
	IF_URL_CONTAINS(Consequence.CASCADING, false, true, false, false, false, true),
	VERIFY_URL_CONTAINS(Consequence.NON_CASCADING, false, true),
	IF_URL_NOT_CONTAINS(Consequence.CASCADING, false, true, false, false, false, true),
	VERIFY_URL_NOT_CONTAINS(Consequence.NON_CASCADING, false, true),

	/**
	 * Verify that the element associated with the given locator has the :valid css pseudo class. The result
	 * is a success if the pseudo class is found associated with the web element.
	 * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
	 */
	VERIFY_VALID_BY_CSS(Consequence.NON_CASCADING, true),

	/**
	 * Verifies that the given value contains the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
	 * <li>Optional: None</li>
	 * </ul>
 	 * <p>
 	 * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>someCacheKey|XXX</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] contains XXX)</td></tr>
	 * <tr><td></td><td>XXX|someCacheKey</td><td></td><td>(checking if XXX contains value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>XXX|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] contains XXX)</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|XXX</td><td></td><td>(checking if XXX contains value in data properties file under key [webdriver.data.key])</td></tr>
	 * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] contains value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in cache under key [someCacheKey] contains value in data properties file under key [webdriver.data.key])</td></tr>
	 * </table>
	 * <p> 
  	 * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
 	 * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
	 */
	VERIFY_VALUE_CONTAINS(Consequence.NON_CASCADING, new TwoStringsParser(), false),

    /**
     * Verifies that the given value contains ALL of the values within the given collection (case-insensitive). The collection of values can 
     * be either a comma delimited or space delimited string. 
     * <p><ul>
     * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
     * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
     * <table border=0 cellspacing=2>
     * <tr><td>&nbsp;</td><td>someCacheKey|X, Y, Z</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] contains X and Y and Z)</td></tr>
     * <tr><td></td><td>XXX|someCacheKey</td><td></td><td>(checking if XXX contains ALL of the values in cache under key [someCacheKey])</td></tr>
     * <tr><td></td><td>XXX|+webdriver.list.of.values</td><td></td><td>(checking if XXX contains ALL of the values defined in data properties file under key [webdriver.list.of.values])</td></tr>
     * <tr><td></td><td>webdriver.list.of.values+|X Y Z</td><td></td><td>(checking if value in data properties file under key [webdriver.list.of.values] contains X and Y and Z)</td></tr>
     * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in cache under key [someCacheKey] contains ALL of the values defined in data properties file under key [webdriver.data.key])</td></tr>
     * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] contains ALL values in cache under key [someCacheKey])</td></tr>
     * </table>
     * <p> 
     * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
     * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
     */
    VERIFY_VALUE_CONTAINS_ALL(Consequence.NON_CASCADING, new TwoStringsParser(), false),
 
    /**
     * Verifies that the given value contains any of the values within the given collection (case-insensitive). The collection of values can 
     * be either a comma delimited or space delimited string. A cache key name can also be provided as the 3rd argument, and if provided,
     * the index of the value that is deemed a match will be stored to cache under that name.  
     * <p><ul>
     * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]{|[literal, cache key, data property key]}</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
     * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
     * <table border=0 cellspacing=2>
     * <tr><td>&nbsp;</td><td>someCacheKey|X, Y, Z</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] contains X or Y or Z)</td></tr>
     * <tr><td></td><td>XXX|someCacheKey</td><td></td><td>(checking if XXX contains any of the values in cache under key [someCacheKey])</td></tr>
     * <tr><td></td><td>XXX|+webdriver.list.of.values</td><td></td><td>(checking if XXX contains any of the values defined in data properties file under key [webdriver.list.of.values])</td></tr>
     * <tr><td></td><td>webdriver.list.of.values+|X Y Z</td><td></td><td>(checking if value in data properties file under key [webdriver.list.of.values] contains X or Y or Z)</td></tr>
     * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in cache under key [someCacheKey] contains any of the values defined in data properties file under key [webdriver.data.key])</td></tr>
     * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] contains any value in cache under key [someCacheKey])</td></tr>
     * <tr><td>&nbsp;</td><td>someCacheKey|X, Y, Z|keyIndex</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] contains X or Y or Z). The index of the matching value will be stored to cache under key [keyIndex].</td></tr>
     * <tr><td></td><td>XXX|someCacheKey|keyIndex</td><td></td><td>(checking if XXX contains any of the values in cache under key [someCacheKey]). The index of the matching value will be stored to cache under key [keyIndex].</td></tr>
     * </table>
     * <p> 
     * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
     * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
     */
    VERIFY_VALUE_CONTAINS_ANY(Consequence.NON_CASCADING, new TwoStringsThenOptionalStringParser(), false),
 
    /**
     * Verifies that the given value contains the value within the given collection, at the given index (case-insensitive). The collection 
     * of values can be either a comma delimited or space delimited string. This operation is often used following IF_VALUE_CONTAINS_ANY,
     * using the cache key provided in that operation to establish a reference within a group of related collections. 
     * <p><ul>
     * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]{|[literal, cache key, data property key]}</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
     * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
     * <table border=0 cellspacing=2>
     * <tr><td>&nbsp;</td><td>someCacheKey|X, Y, Z|0</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] contains X)</td></tr>
     * <tr><td>&nbsp;</td><td>someCacheKey|X, Y, Z|1</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] contains Y)</td></tr>
     * <tr><td></td><td>XXX|someCacheKey|indexKey</td><td></td><td>(checking if XXX contains the value at index [indexKey] within the collection [someCacheKey])</td></tr>
     * <tr><td></td><td>XXX|+webdriver.list.of.values+|indexKey</td><td></td><td>(checking if XXX contains the value at index [indexKey] within the collection defined in data properties file under key [webdriver.list.of.values])</td></tr>
     * <tr><td></td><td>webdriver.value+|X Y Z|indexKey</td><td></td><td>(checking if value in data properties file under key [webdriver.value] contains the value at index [indexKey])</td></tr>
     * </table>
     * <p> 
     * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
     * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
     */
	VERIFY_VALUE_CONTAINS_AT_INDEX(Consequence.NON_CASCADING, new TwoStringsThenNumericParser(), false),

    /**
     * Verifies that the attributeValue field is empty or contains only spaces.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
     * </ul>
     * <p>
     * <b>Note that this operation should NOT be used to test for the EXISTENCE of a cache key, since using a non-existing cache key 
     * would result in the cache key itself being interrogated for emptiness, thereby resulting in a false failure. Use IF_KEY_PRESENT
     * or IF_KEY_ABSENT instead.</b>
     */
    VERIFY_VALUE_EMPTY(Consequence.NON_CASCADING),

	/**
	 * Verifies that the given value matches the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
	 * <li>Optional: None</li>
	 * </ul>
 	 * <p>
 	 * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>someCacheKey|XXX</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] is equal to XXX)</td></tr>
	 * <tr><td></td><td>XXX|someCacheKey</td><td></td><td>(checking if XXX is equal to value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>XXX|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] is equal to XXX)</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|XXX</td><td></td><td>(checking if XXX is equal to value in data properties file under key [webdriver.data.key])</td></tr>
	 * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] is equal to value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in cache under key [someCacheKey] is equal to value in data properties file under key [webdriver.data.key])</td></tr>
	 * </table>
	 * <p> 
  	 * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
 	 * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
	 */
	VERIFY_VALUE_EQUALS(Consequence.NON_CASCADING, new TwoStringsParser(), false),

    /**
     * Verifies that the given value equals any of the values within the given collection (case-insensitive). The collection of values can be either 
     * a comma delimited or space delimited string. A cache key name can also be provided as the 3rd argument, and if provided, the index of the 
     * value that is deemed a match will be stored to cache under that name.  
     * <p><ul>
     * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]{|[literal, cache key, data property key]}</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
     * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
     * <table border=0 cellspacing=2>
     * <tr><td>&nbsp;</td><td>someCacheKey|X, Y, Z</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] equals X or Y or Z)</td></tr>
     * <tr><td></td><td>XXX|someCacheKey</td><td></td><td>(checking if XXX equals any of the values in cache under key [someCacheKey])</td></tr>
     * <tr><td></td><td>XXX|+webdriver.list.of.values</td><td></td><td>(checking if XXX equals any of the values defined in data properties file under key [webdriver.list.of.values])</td></tr>
     * <tr><td></td><td>webdriver.list.of.values+|X Y Z</td><td></td><td>(checking if value in data properties file under key [webdriver.list.of.values] equals X or Y or Z)</td></tr>
     * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in cache under key [someCacheKey] equals any of the values defined in data properties file under key [webdriver.data.key])</td></tr>
     * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] equals value in cache under key [someCacheKey])</td></tr>
     * <tr><td>&nbsp;</td><td>someCacheKey|X, Y, Z|keyIndex</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] equals X or Y or Z). The index of the matching value will be stored to cache under key [keyIndex].</td></tr>
     * <tr><td></td><td>XXX|someCacheKey|keyIndex</td><td></td><td>(checking if XXX equals any of the values in cache under key [someCacheKey]). The index of the matching value will be stored to cache under key [keyIndex].</td></tr>
     * </table>
     * <p> 
     * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
     * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
     */
    VERIFY_VALUE_EQUALS_ANY(Consequence.NON_CASCADING, new TwoStringsThenOptionalStringParser(), false),
    VERIFY_VALUE_EQUALS_AT_INDEX(Consequence.NON_CASCADING, new TwoStringsThenNumericParser(), false),

	/**
	 * Verifies that the given (numeric) value is greater than the given value.
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
	 * <li>Optional: None</li>
	 * </ul>
 	 * <p>
 	 * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>someCacheKey|nnn</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] is greater than nnn)</td></tr>
	 * <tr><td></td><td>nnn|someCacheKey</td><td></td><td>(checking if nnn is greater than the value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>nnn|+webdriver.data.key</td><td></td><td>(checking if nnn is greater than the value in data properties file under key [webdriver.data.key]</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|nnn</td><td></td><td>(checking if the value in data properties file under key [webdriver.data.key] is greater than nnn)</td></tr>
	 * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] is greater than value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in cache under key [someCacheKey] is greater than the value in data properties file under key [webdriver.data.key])</td></tr>
	 * </table>
	 * <p> 
  	 * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
 	 * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
	 */
	VERIFY_VALUE_GREATER_THAN(Consequence.NON_CASCADING, new TwoStringsParser(), false),

    /**
     * Verifies that the given (numeric) value is within the given range (or ranges, if multiple ranges are provided) inclusive. Any range of 
     * values must be in the format r1:r2 (ex. 10 thru 100). Multiple ranges must be separated by a comma (ex. 10:100, 150:300). Negative values 
     * are allowed and should be indicated using a minus (-) sign.  
     * <p><ul>
     * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
     * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
     * <table border=0 cellspacing=2>
     * <tr><td>&nbsp;</td><td>someCacheKey|10:50</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] is within the range 10 thru 50, inclusive)</td></tr>
     * <tr><td>&nbsp;</td><td>someCacheKey|10:50,75:100</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] is within the range 10 thru 50 OR 75 thru 100, inclusive)</td></tr>
     * <tr><td></td><td>nnn|someCacheKey</td><td></td><td>(checking if nnn is within the range in cache under key [someCacheKey])</td></tr>
     * <tr><td></td><td>nnn|+webdriver.data.key</td><td></td><td>(checking if nnn is within the range defined in data properties file under key [webdriver.data.key]</td></tr>
     * <tr><td></td><td>webdriver.data.key+|10:50</td><td></td><td>(checking if the value in data properties file under key [webdriver.data.key] is within the range 10 thru 50, inclusive)</td></tr>
     * </table>
     * <p> 
     * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
     * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
     */
	VERIFY_VALUE_IN_RANGE(Consequence.NON_CASCADING, new TwoStringsParser(), false),

	/**
     * Verifies that the given (numeric) value is less than the given value.
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
	 * <li>Optional: None</li>
	 * </ul>
 	 * <p>
 	 * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>someCacheKey|nnn</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] is less than nnn)</td></tr>
	 * <tr><td></td><td>nnn|someCacheKey</td><td></td><td>(checking if nnn is less than the value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>nnn|+webdriver.data.key</td><td></td><td>(checking if nnn is less than the value in data properties file under key [webdriver.data.key]</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|nnn</td><td></td><td>(checking if the value in data properties file under key [webdriver.data.key] is less than nnn)</td></tr>
	 * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] is less than value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in cache under key [someCacheKey] is less than the value in data properties file under key [webdriver.data.key])</td></tr>
	 * </table>
	 * <p> 
  	 * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
 	 * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
	 */
	VERIFY_VALUE_LESS_THAN(Consequence.NON_CASCADING, new TwoStringsParser(), false),

	/**
	 * Verifies that the given value starts with the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
	 * <li>Optional: None</li>
	 * </ul>
 	 * <p>
 	 * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>someCacheKey|XXX</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] starts with XXX)</td></tr>
	 * <tr><td></td><td>XXX|someCacheKey</td><td></td><td>(checking if XXX starts with value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>XXX|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] starts with XXX)</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|XXX</td><td></td><td>(checking if XXX starts with value in data properties file under key [webdriver.data.key])</td></tr>
	 * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] starts with value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in cache under key [someCacheKey] starts with value in data properties file under key [webdriver.data.key])</td></tr>
	 * </table>
	 * <p> 
  	 * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
 	 * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
	 */
	VERIFY_VALUE_STARTS_WITH(Consequence.NON_CASCADING, new TwoStringsParser(), false),

	/**
	 * Verifies that the given value does NOT contain the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
	 * <li>Optional: None</li>
	 * </ul>
 	 * <p>
 	 * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>someCacheKey|XXX</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] does NOT contain XXX)</td></tr>
	 * <tr><td></td><td>XXX|someCacheKey</td><td></td><td>(checking if XXX does NOT contain value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>XXX|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] does NOT contain XXX)</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|XXX</td><td></td><td>(checking if XXX does NOT contain value in data properties file under key [webdriver.data.key])</td></tr>
	 * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] does NOT contain value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in cache under key [someCacheKey] does NOT contain value in data properties file under key [webdriver.data.key])</td></tr>
	 * </table>
	 * <p> 
  	 * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
 	 * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
	 */
	VERIFY_VALUE_NOT_CONTAINS(Consequence.NON_CASCADING, new TwoStringsParser(), false),

    /**
     * Verifies that the given value does NOT contain any of the values within the given collection (case-insensitive). The collection of values 
     * can be either a comma delimited or space delimited string. A cache key name can also be provided as the 3rd argument, and if provided,
     * the index of the value that is deemed a match will be stored to cache under that name.  
     * <p><ul>
     * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]{|[literal, cache key, data property key]}</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
     * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
     * <table border=0 cellspacing=2>
     * <tr><td>&nbsp;</td><td>someCacheKey|X, Y, Z</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] contains X or Y or Z)</td></tr>
     * <tr><td></td><td>XXX|someCacheKey</td><td></td><td>(checking if XXX contains any of the values in cache under key [someCacheKey])</td></tr>
     * <tr><td></td><td>XXX|+webdriver.list.of.values</td><td></td><td>(checking if XXX contains any of the values defined in data properties file under key [webdriver.list.of.values])</td></tr>
     * <tr><td></td><td>webdriver.list.of.values+|X Y Z</td><td></td><td>(checking if value in data properties file under key [webdriver.list.of.values] contains X or Y or Z)</td></tr>
     * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in cache under key [someCacheKey] contains any of the values defined in data properties file under key [webdriver.data.key])</td></tr>
     * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] contains any value in cache under key [someCacheKey])</td></tr>
     * <tr><td>&nbsp;</td><td>someCacheKey|X, Y, Z|keyIndex</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] contains X or Y or Z). The index of the matching value will be stored to cache under key [keyIndex].</td></tr>
     * <tr><td></td><td>XXX|someCacheKey|keyIndex</td><td></td><td>(checking if XXX contains any of the values in cache under key [someCacheKey]). The index of the matching value will be stored to cache under key [keyIndex].</td></tr>
     * </table>
     * <p> 
     * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
     * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
     */
    VERIFY_VALUE_NOT_CONTAINS_ANY(Consequence.NON_CASCADING, new TwoStringsParser(), false),

    /**
     * Verifies that the attributeValue field contains any non-space characters.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
     * </ul>
     * <p>
     * <b>Note that this operation should NOT be used to test for the EXISTENCE of a cache key, since using a non-existing cache key 
     * would result in the cache key itself being interrogated for emptiness, thereby resulting in a false failure. Use IF_KEY_PRESENT
     * or IF_KEY_ABSENT instead.</b>
     */
    VERIFY_VALUE_NOT_EMPTY(Consequence.NON_CASCADING),

	/**
	 * Verifies that the given value does NOT match the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
	 * <li>Optional: None</li>
	 * </ul>
 	 * <p>
 	 * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>someCacheKey|XXX</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] does NOT equal XXX)</td></tr>
	 * <tr><td></td><td>XXX|someCacheKey</td><td></td><td>(checking if XXX does NOT equal value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>XXX|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] does NOT equal XXX)</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|XXX</td><td></td><td>(checking if XXX does NOT equal value in data properties file under key [webdriver.data.key])</td></tr>
	 * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] does NOT equal value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in cache under key [someCacheKey] does NOT equal value in data properties file under key [webdriver.data.key])</td></tr>
	 * </table>
	 * <p> 
  	 * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
 	 * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
	 */
	VERIFY_VALUE_NOT_EQUALS(Consequence.NON_CASCADING, new TwoStringsParser(), false),

    VERIFY_VALUE_NOT_EQUALS_ANY(Consequence.NON_CASCADING, new TwoStringsParser(), false),

	/**
	 * Verifies that the given value does NOT start with the given string (case-insensitive).
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [literal, cache key, data property key]|[literal, cache key, data property key]</li>
	 * <li>Optional: None</li>
	 * </ul>
 	 * <p>
 	 * Either parameter can be a literal, a cache key, or a data property key. For example, the following are valid attribute strings: 
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>someCacheKey|XXX</td><td>&nbsp;&nbsp;</td><td>(checking if value in cache under key [someCacheKey] does NOT start with XXX)</td></tr>
	 * <tr><td></td><td>XXX|someCacheKey</td><td></td><td>(checking if XXX does NOT start with value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>XXX|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] does NOT start with XXX)</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|XXX</td><td></td><td>(checking if XXX does NOT start with value in data properties file under key [webdriver.data.key])</td></tr>
	 * <tr><td></td><td>someCacheKey|+webdriver.data.key</td><td></td><td>(checking if value in data properties file under key [webdriver.data.key] does NOT start with value in cache under key [someCacheKey])</td></tr>
	 * <tr><td></td><td>webdriver.data.key+|someCacheKey</td><td></td><td>(checking if value in cache under key [someCacheKey] does NOT start with value in data properties file under key [webdriver.data.key])</td></tr>
	 * </table>
	 * <p> 
  	 * <b>Note the use of a plus (+) sign to separate the data property key from other portions of the string. The Engine
 	 * uses the + sign to parse/replace any data property keys with its corresponding value at runtime.</b>
	 */
	VERIFY_VALUE_NOT_STARTS_WITH(Consequence.NON_CASCADING, new TwoStringsParser(), false),

	/**
     * Pauses execution for a length of time. AttributeKey contains the number if milliseconds to wait.
     * <p><ul>
     * <li>Required: AttributeKey (can be either data property key or numeric value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	WAIT(Consequence.CASCADING, false),

	/**
     * Waits until the element associated with the given locator exists and contains the given value.
     * An example use of this operation is waiting for a specific page status to appear.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	WAIT_FOR_VALUE(Consequence.CASCADING, true, true),

	/**
     * Waits until the element associated with the given locator does not exist. An example use of this
     * operation is waiting for a page spinner to disappear before continuing.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	WAIT_UNTIL_ABSENT(Consequence.CASCADING, true),

	/**
     * Waits until any element associated with the given locator exists AND contains the given value (case-insensitive).
     * If an attribute value is not provided, the operation will wait until any element contains a non-empty string.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: AttributeKey (can be either data property key or string value)</li>
     * </ul>
     */
	WAIT_UNTIL_CONTAINS_VALUE(Consequence.CASCADING, true),

	/**
     * Waits until the element associated with the given locator exists AND is disabled.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	WAIT_UNTIL_DISABLED(Consequence.CASCADING, true),

	/**
     * Waits until the element associated with the given locator exists AND is empty. An example use of
     * this operation is waiting for an input field to be cleared.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	WAIT_UNTIL_EMPTY(Consequence.CASCADING, true),

	/**
     * Waits until the element associated with the given locator exists AND is enabled.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	WAIT_UNTIL_ENABLED(Consequence.CASCADING, true),

	/**
     * Waits until the element associated with the given locator exists. This operation is typically used
     * following a button/link click to wait for elements on the page to appear before continuing.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or numeric value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	WAIT_UNTIL_PRESENT(Consequence.CASCADING, true),

	/**
     * Triggers a 'warning' in the output report. This operation is typically used in conjunction with an 'IF'
     * operation, such as IF_ABSENT. For example, if an normally expected button does not appear (IF_ABSENT),
     * then report a warning (WARNING).
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	WARNING(Consequence.NON_CASCADING, false, false, false, false, false, false, true),

	/**
     * Conditional operand that reports a warning if the element associated with the given locator does
     * not exist.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	WARN_IF_ABSENT(Consequence.NON_CASCADING, true, false, false, false, false, true, true),

	/**
     * Conditional operand that reports a warning if the element associated with the given locator exists.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	WARN_IF_PRESENT(Consequence.NON_CASCADING, true, false, false, false, false, true, true),

	/**
     * Operand that forces Selenium to remain on the current window.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	WINDOW_SWITCHING_DISABLED(Consequence.NON_CASCADING),

	/**
     * Operand that resets the Selenium window restriction. This should be performed sometime after performing
     * the WINDOW_SWITCHING_DISABLED operation.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	WINDOW_SWITCHING_ENABLED(Consequence.NON_CASCADING),

	/**
	 * Writes the given tokens to a file with the given name. The file path begins at the current application root. 
	 * The file name may contain a sub-folder structure under which the file will be written:
	 * <p><ul>
	 * <li>someFileName.txt (simple file name)</li> 
	 * <li>/Data/someFileName.txt (existing folder + file name)</li> 
	 * <li>/Output/someFileName.txt (non-existing folder + file name)</li> 
	 * <li>/Data/Output/someFileName.txt (existing folder + non-existing folder + file name)</li> 
     * </ul>
	 * If the file already exists, the string will be appended as a line at the end of the file. A timestamp in 
	 * the format yyyy_MM_dd_HH_mm_ss will be appended as the final token. 
	 * <p><ul>
	 * <li>Required: AttributeKey in the format [file name]|[value]{|[value]}</li>
	 * <li>Optional: None</li>
	 * </ul>
	 */
	WRITE_TO_CSV_FILE(Consequence.NON_CASCADING, false),
	
	/************************************************************************************************
	 * SAP OPERATIONS
	 ************************************************************************************************/

	/**
     * SAP (Business Objects) Operand that will wait until a report is displayed on the screen.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	SAP_WAIT_FOR_REPORT(Consequence.NON_CASCADING),

	/************************************************************************************************
	 * KEYBOARD OPERATIONS
	 ************************************************************************************************/

	/**
     * Keyboard operand that clears the window.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_PAUSE_BREAK(Consequence.NON_CASCADING),

	/**
     * Keyboard operand that enters the given text at the current cursor location.
     * <p><ul>
     * <li>Required: attributeKey in format [text]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_TYPE_TEXT(Consequence.NON_CASCADING),

	/**
     * Keyboard operand that enters the given text at the current cursor location, then simulates an
     * ENTER key press.
     * <p><ul>
     * <li>Required: attributeKey in format [text]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_TYPE_TEXT_AND_PRESS_ENTER(Consequence.NON_CASCADING),

	/**
     * Keyboard operand that enters the given text at the current cursor location, then simulates an
     * TAB key press.
     * <p><ul>
     * <li>Required: attributeKey in format [text]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_TYPE_TEXT_AND_TAB(Consequence.NON_CASCADING),

	/**
     * Keyboard operand that retrieves a value from cache for the given key, and enters that value
     * at the current cursor location. The AttributeKey contains the lookup key of the value.
     * <p><ul>
     * <li>Required: attributeKey</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_TYPE_TEXT_FROM_CACHE(Consequence.CASCADING, false, true, false, false, true),

	/**
     * Keyboard operand that retrieves a value from cache for the given key, enters that value
     * at the current cursor location, then simulates an ENTER key press. The AttributeKey contains
     * the lookup key of the value.
     * <p><ul>
     * <li>Required: attributeKey</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_TYPE_TEXT_FROM_CACHE_AND_PRESS_ENTER(Consequence.CASCADING, false, true, false, false, true),

	/**
     * Keyboard operand that retrieves a value from cache for the given key, enters that value
     * at the current cursor location, then simulates a TAB key press. The AttributeKey contains
     * the lookup key of the value.
     * <p><ul>
     * <li>Required: attributeKey</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_TYPE_TEXT_FROM_CACHE_AND_TAB(Consequence.CASCADING, false, true, false, false, true),

	/**
     * Keyboard operand that simulates the user pressing the Pause/Break button on the keyboard, which will
     * clear the host screen.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_CLEAR(Consequence.NON_CASCADING),

	/**
     * Keyboard operand that simulates the user performing a Ctrl+C (Copy) keyboard combination. This operation
     * is useful in copying the current selection to the clipboard.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_COPY(Consequence.NON_CASCADING),

	/**
     * Keyboard operand that simulates the user performing a Ctrl+A (Select All) keyboard combination. This
     * operation is used primarily to highlight the entire host screen of 24 rows x 80 columns.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_SELECT_ALL(Consequence.NON_CASCADING),

	/**
     * Keyboard operand that simulates the user pressing the Tab button on the keyboard.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_TAB(Consequence.NON_CASCADING),

	/**
     * Keyboard operand that simulates the user performing aN Alt+Tab keyboard combination to tab in
     * reverse order.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_ALT_TAB(Consequence.NON_CASCADING),

	/**
     * Keyboard operand that simulates the user performing a Shift+Tab keyboard combination to tab in
     * reverse order.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_REVERSE_TAB(Consequence.NON_CASCADING),

	/**
     * Keyboard operand that simulates the user pressing the Esc button on the keyboard.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_ESCAPE(Consequence.NON_CASCADING),

	/**
     * Keyboard operand that simulates the user pressing the given function key on the keyboard.
     * <p><ul>
     * <li>Required: attributeKey in format [FunctionKey as text]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_FUNCTION_KEY(Consequence.NON_CASCADING),

	/**
     * Keyboard operand that simulates the user pressing the Enter button on the keyboard.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_PRESS_ENTER(Consequence.NON_CASCADING),

	/**
     * Keyboard operand that maximizes the current window.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_MAXIMIZE_WINDOW(Consequence.NON_CASCADING),

	/**
     * Keyboard operand that performs a restore, then maximizes the current window.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_RESTORE_AND_MAXIMIZE_WINDOW(Consequence.NON_CASCADING),

	/**
     * Keyboard operand that closes the current browser tab.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_CLOSE_CURRENT_TAB(Consequence.NON_CASCADING),

	/**
     * Keyboard operand that simulates the user pressing the Up arrow on the keyboard.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_UP_ARROW(Consequence.NON_CASCADING),

	/**
     * Keyboard operand that simulates the user pressing the Down arrow on the keyboard.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_DOWN_ARROW(Consequence.NON_CASCADING),

	/**
     * Keyboard operand that simulates a CTRL+T to open a new browser tab.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_NEW_TAB(Consequence.NON_CASCADING),

	/**
     * Keyboard operand that simulates a CTRL+Tab to cycle through all open browser tabs.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_CONTROL_TAB(Consequence.NON_CASCADING),

	/**
     * Keyboard operand that simulates a CTRL+ALT+key.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_CONTROL_ALT_PLUS_KEY(Consequence.NON_CASCADING),
	
	/**
     * Keyboard operand that simulates a CTRL+SHIFT+key.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	KEYBOARD_CONTROL_SHIFT_PLUS_KEY(Consequence.NON_CASCADING),
	
	/************************************************************************************************
	 * MOUSE OPERATIONS
	 ************************************************************************************************/

	/**
     * Mouse operand that moves the mouse to the center of the screen and clicks it.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	MOUSE_CLICK_SCREEN_CENTER(Consequence.NON_CASCADING),

	/**
     * Mouse operand that moves the mouse over the element associated withthe given locator.
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	MOUSE_OVER(Consequence.NON_CASCADING, true),

	/************************************************************************************************
	 * LANFAX OPERATIONS
	 ************************************************************************************************/

	/**
     * LanFax operand that opens a new LanFax window.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	LANFAX_OPEN_NEW_WINDOW(Consequence.CASCADING, false, false, false, true),

	LANFAX_ADD_ATTACHMENT(Consequence.CASCADING),

	/**
     * LanFax operand that creates a new fax, attaches a test file to it, and submits it.
     * <p><ul>
     * <li>Required: attributeKey in format [attachmentPath], [subject], [phonebookRowNumber], [appendCurrentDate], 
     * [deleteRecord], [newFaxWindowWaitInSec], [attachDialogWaitInSec]</li>
     * <li>Optional: None</li>
     * </ul>
	 * <p>
 	 * <table border=0 cellspacing=2>
	 * <tr><td>attachmentPath</td><td>The full path to the file attachment.</code></td></tr> 
	 * <tr><td>subject</td><td>The subject line text to be entered in the fax.</td></tr> 
	 * <tr><td>phonebookRowNumber</td><td>The row number of the Phonebook record to be selected.</td></tr> 
	 * <tr><td>appendCurrentDate</td><td>Boolean true|false that indicates whether or not the current date should be appended to the subject.</td></tr> 
	 * <tr><td>deleteRecord</td><td>Boolean true|false that indicates whether or not the fax should be deleted after submission.</td></tr> 
	 * <tr><td>newFaxWindowWaitInSec (OPTIONAL)</td><td>Wait time (in seconds) that occurs after the Control+N is performed to open the New Fax dialog. This value should be increased if any latency in display occurs. 
	 * Defaults to 3 if not provided.</td></tr> 
	 * <tr><td>attachDialogWaitInSec (OPTIONAL)</td><td>Wait time (in seconds) that occurs after the Add Attachment dialog window is triggered. This value should be increased if any latency in display occurs. 
	 * Defaults to 3 if not provided.</td></tr> 
     * </table>
     */
	LANFAX_SUBMIT_NEW_FAX(Consequence.CASCADING),

	/**
     * LanFax operand that performs a File...Exit on the current LanFax window.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	LANFAX_CLOSE_WINDOW(Consequence.NON_CASCADING),

    /************************************************************************************************
     * JSM OPERATIONS
     ************************************************************************************************/

	JSM_INIT(Consequence.NON_CASCADING),
	JSM_COMPARE_PDF_TO_XLS(Consequence.NON_CASCADING),
	JSM_COMPARE_XLS_TO_PDF(Consequence.NON_CASCADING),
	JSM_VERIFY_APPROVED_DATES(Consequence.NON_CASCADING),
	JSM_VERIFY_PLATFORMS(Consequence.NON_CASCADING),
	JSM_VERIFY_SCC_PLATFORMS(Consequence.NON_CASCADING),
	JSM_CLEANUP(Consequence.NON_CASCADING),

    /************************************************************************************************
     * ANDROID OPERATIONS
     ************************************************************************************************/

    ANDROID_WAIT(Consequence.NON_CASCADING),
    ANDROID_SWITCH_FRAME(Consequence.CASCADING),
    ANDROID_CLICK_COORDINATES(Consequence.NON_CASCADING),
    ANDROID_CLICK_NATIVE_BACK_BUTTON(Consequence.NON_CASCADING),
    ANDROID_TAP(Consequence.NON_CASCADING, true),
    ANDROID_LONG_PRESS(Consequence.NON_CASCADING, true),
    ANDROID_SWIPE_SCREEN(Consequence.NON_CASCADING),
    ANDROID_HORIZONTAL_SWIPE(Consequence.NON_CASCADING),
    ANDROID_VERTICAL_SWIPE(Consequence.NON_CASCADING),
    ANDROID_ACTIVATE_APP(Consequence.CASCADING, false, true),
    ANDROID_TERMINATE_APP(Consequence.CASCADING, false, true),
    ANDROID_CLOSE_APP(Consequence.CASCADING),
    ANDROID_LAUNCH_APP(Consequence.CASCADING),
    ANDROID_SWITCH_APPIUM_CONTEXT_WEB(Consequence.NON_CASCADING),
    ANDROID_SWITCH_APPIUM_CONTEXT_NATIVE(Consequence.NON_CASCADING),

    /**
    * Android mobile operation that sets the date value on an Ionic <ion-datetime> component programatically. This operation 
    * avoids having to interact with the picker component and instead converts and inputs the date directly to the component.
    * The locator provided must correspond to the <ion-datetime> element on the page. The attribute provided should specify 
    * the desired date to be input, specified in either format MM/DD/YYYY or YYYY-MM-DD. Note also that for this to work, the 
    * mobile application will need to be in the Appium WEBVIEW context first.
    * <p><ul>
    * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
    * <li>Optional: None</li>
    * </ul>
    */
    ANDROID_IONIC_SET_DATE(Consequence.CASCADING, true),

    /************************************************************************************************
     * IOS OPERATIONS
     ************************************************************************************************/

    IOS_WAIT(Consequence.NON_CASCADING),
    IOS_SWITCH_FRAME(Consequence.CASCADING),
    IOS_CLICK_COORDINATES(Consequence.NON_CASCADING),
    IOS_TAP(Consequence.NON_CASCADING, true),
    IOS_LONG_PRESS(Consequence.NON_CASCADING, true),
    IOS_SWIPE_SCREEN(Consequence.NON_CASCADING),
    IOS_HORIZONTAL_SWIPE(Consequence.NON_CASCADING),
    IOS_VERTICAL_SWIPE(Consequence.NON_CASCADING),
    IOS_ACTIVATE_APP(Consequence.CASCADING, false, true),
    IOS_TERMINATE_APP(Consequence.CASCADING, false, true),
    IOS_CLOSE_APP(Consequence.CASCADING),
    IOS_LAUNCH_APP(Consequence.CASCADING),
    IOS_SWITCH_APPIUM_CONTEXT_WEB(Consequence.NON_CASCADING),
    IOS_SWITCH_APPIUM_CONTEXT_NATIVE(Consequence.NON_CASCADING),

    /**
    * iOS mobile operation that sets the date value on an Ionic <ion-datetime> component programatically. This operation 
    * avoids having to interact with the picker component and instead converts and inputs the date directly to the component.
    * The locator provided must correspond to the <ion-datetime> element on the page. The attribute provided should specify 
    * the desired date to be input, specified in either format MM/DD/YYYY or YYYY-MM-DD. Note also that for this to work, the 
    * mobile application will need to be in the Appium WEBVIEW context first.
    * <p><ul>
    * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
    * <li>Optional: None</li>
    * </ul>
    */
    IOS_IONIC_SET_DATE(Consequence.CASCADING, true),
    IOS_STORE_WEB_CONTEXT(Consequence.NON_CASCADING),

	/**
     * Flynet operand that clears the text of the INPUT element associated with the given locator. The locator provided
     * must result in a true Flynet 'input' element (which is any [span] element whose 'id' value begins with 'VS__').
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_CLEAR_TEXT(Consequence.CASCADING, true),

	/**
     * Flynet operand that performs an ESCAPE key press. This operation is useful for clearing the
     * host screen.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_ESCAPE(Consequence.CASCADING),

	/**
     * Flynet operand that simulates the pressing of a function key. AttributeKey should contain the name of the 
     * function key, or a numeric value that corresponds to the function key. Below are all valid examples:
     * <p><ul>
     * <li>7</li>
     * <li>F7</li>
     * <li>PF7</li>
     * </ul>
     * <p>Numeric values between 13-24 (inclusive) will result in the following key combination:</p>
     * <p><ul>
     * <li>SHIFT+[numeric value - 12]</li>
     * </ul>
     * <p><ul>
     * <li>Required: AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: Timeout (in seconds) </li>
     * </ul>
     */
	FLYNET_FUNCTION_KEY(Consequence.CASCADING, new StringThenOptionalNumericParser(), false),

	/**
     * Flynet operand that simulates the clicking of an option listed at the bottom of the screen, typically  
     * used to transfer to another screen. AttributeKey should contain the name listed on the screen 'button'.
     * An example of clickable options is the RMIH (Claim Display) host screen.
     * <p><ul>
     * <li>Required: AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_GO_TO(Consequence.CASCADING, false, true),

	/**
     * Conditional Flynet operand that determines if the cursor is located at the FIRST CHARACTER of the INPUT 
     * element associated with the given locator. This is more or less a sanity check prior to using any of
     * the FLYNET_TYPE_TEXT_ operations, which are dependent on cursor position. 
     * 
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_IF_CURSOR_AT_INPUT(Consequence.CASCADING, true, false, false, false, false, true),

	/**
     * Conditional Flynet operand that determines if the cursor is NOT located at the FIRST CHARACTER of the INPUT 
     * element associated with the given locator. This is more or less a sanity check prior to using any of
     * the FLYNET_TYPE_TEXT_ operations, which are dependent on cursor position. 
     * 
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_IF_CURSOR_NOT_AT_INPUT(Consequence.CASCADING, true, false, false, false, false, true),

	/**
     * Conditional Flynet operand that determines if the given search value does NOT appear anywhere on
     * the screen. The result is a success if the search value is not found. Note that this operation uses a 
     * predefined locator, and will ignore any locators that are provided.
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [label text]|[search value]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_IF_VALUE_NOT_PRESENT(Consequence.CASCADING, false, true, false, false, false, true),

	/**
     * Conditional Flynet operand that determines if the given search value does NOT appear after the given
     * label text. The result is a success if the search value is not found. Note that this operation uses a 
     * predefined locator, and will ignore any locators that are provided.
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [label text]|[search value]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_IF_VALUE_NOT_PRESENT_AFTER(Consequence.CASCADING, new TwoStringsThenOptionalNumericParser(), false, true),

	/**
     * Conditional Flynet operand that determines if the given search value does NOT appear on the given row. An optional 
     * starting column or a starting/ending column range can be provided to restrict the comparison to a given column range. 
     * The result is a success if the search value is not found. Note that this operation uses a predefined locator, and 
     * will ignore any locators that are provided.
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [search value]|[row number]|[starting column]|[ending column]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_IF_VALUE_NOT_PRESENT_ON_ROW(Consequence.CASCADING, new StringThenNumericThenTwoOptionalNumericsParser(), false, true),

	/**
     * Conditional Flynet operand that determines if the given search value appears anywhere on
     * the screen. The result is a success if the search value is found. Note that this operation uses a 
     * predefined locator, and will ignore any locators that are provided.
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [label text]|[search value]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_IF_VALUE_PRESENT(Consequence.CASCADING, false, true, false, false, false, true),

	/**
     * Conditional Flynet operand that determines if the given search value appears after the given label
     * text. The result is a success if the search value is found. Note that this operation uses a predefined 
     * locator, and will ignore any locators that are provided.
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [label text]|[search value]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_IF_VALUE_PRESENT_AFTER(Consequence.CASCADING, new TwoStringsThenOptionalNumericParser(), false, true),

	/**
     * Conditional Flynet operand that determines if the given search value appears on the given row. An optional 
     * starting column or a starting/ending column range can be provided to restrict the comparison to a given column range. 
     * The result is a success if the search value is not found. Note that this operation uses a predefined locator, and 
     * will ignore any locators that are provided.
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [search value]|[row number]|[starting column]|[ending column]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_IF_VALUE_PRESENT_ON_ROW(Consequence.CASCADING, new StringThenNumericThenTwoOptionalNumericsParser(), false, true),

	/**
     * Flynet operand that performs a PAUSE/BREAK key press.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_PAUSE_BREAK(Consequence.CASCADING),

	/**
     * Flynet operand that moves the cursor to the given offset.
     * 
     * <p><ul>
     * <li>Required: AttributeKey (can be either data property key or numeric value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_PLACE_CURSOR_AT(Consequence.CASCADING, false, true, true),

	/**
     * Flynet operand that moves the cursor to the FIRST CHARACTER of the INPUT element associated with 
     * the given locator. This is useful prior to using any of the FLYNET_TYPE_TEXT_ operations, which are 
     * dependent on cursor position. 
     * 
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_PLACE_CURSOR_AT_INPUT(Consequence.CASCADING, true),

	/**
     * Flynet operand that moves the cursor to the FIRST CHARACTER of the FIRST INPUT element on the screen
     * (starting from row 0, column 0 and reading from left to right). This is useful prior to using any of the 
     * FLYNET_TYPE_TEXT_ operations, which are dependent on cursor position. 
     * 
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_PLACE_CURSOR_AT_FIRST_INPUT(Consequence.CASCADING),

	/**
     * Flynet operand that moves the cursor to the first position of the given row.
     * 
     * <p><ul>
     * <li>Required: AttributeKey (can be either data property key or numeric value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_PLACE_CURSOR_ON_ROW(Consequence.CASCADING, false, true, true),

	/**
     * Flynet operand that performs an ENTER key press, typically used to trigger a search/submission.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_PRESS_ENTER(Consequence.CASCADING),

	/**
     * Flynet operand that simulates the pressing of a function key until the element associated with the given 
     * locator contains the given value. This operation is useful for the INFOrm screens (INFC, INFP) to cycle
     * through the lists of informs to look for OPEN, CLOSED, ALL informs. 
     * 
     * <p>Please refer to {@link #FLYNET_FUNCTION_KEY} for valid function key formats.</p>
     * <p><ul>
     * <li>Required: Locator, AttributeKey in the format [function key]|[comparison value]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_PRESS_FUNCTION_KEY_UNTIL_CONTAINS_VALUE(Consequence.CASCADING, true, true),

	/**
     * Flynet operand that performs a SHIFT+TAB key press to cycle backwards through all enterable fields.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_REVERSE_TAB(Consequence.CASCADING),

	FLYNET_SAVE_AS(Consequence.NON_CASCADING, false, true),
	
	/**
     * Flynet operand that sets the text of the INPUT element associated with the given locator. The locator provided must
     * result in a true Flynet 'input' element (which is any [span] element whose 'id' value begins with 'VS__').
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_SET_TEXT(Consequence.CASCADING, true),
	
	/**
     * Flynet operand that sets the text of the INPUT element that starts at the given offset. The offset provided must
     * result in a true Flynet 'input' element (which is any [span] element whose 'id' value begins with 'VS__').
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [offset]|[text]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_SET_TEXT_FOR_INPUT_AT_OFFSET(Consequence.CASCADING, new NumericThenStringParser(), false),
	
	/**
     * Flynet operand that sets the text of the INPUT element that exists on the row containing the given value. This operation is
     * useful for selecting a row from a list of displayed rows, based on some unique value within that row. If multiple INPUT 
     * elements exist on the selected row, the first INPUT element will be populated. If multiple rows contain the given search
     * value, only the first row in the list (from top down) will be populated.
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [text to enter]|[search text]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_SET_TEXT_FOR_INPUT_ON_ROW_CONTAINING(Consequence.CASCADING, new TwoStringsParser(), false),
	
	/**
     * Temporarily stores the value of the FIRST input (ie. green colored) element that appears after the given label 
     * text. The attributeKey should be a pipe-delimited string containing the label text and the cache key, respectively.  
     * Note that this operation uses a predefined locator, and will ignore any locators that are provided.
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [label text]|[cache key]</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
 	 * <b>Special Handling of Space-delimited Values</b>
 	 * <p>
	 * Values that contain a string of values (tokens) concatenated with spaces, such as individual names (JOHN R SMITH) 
	 * or procedure codes/modifiers (99213 00 00) will automatically be parsed and each saved to cache under unique 
	 * indexed keys, based off the original key. For example, the cacheKey|value pair:
	 * <p>
	 * <table><tr><td>&nbsp;</td><td>SOME_KEY|99213 01 02</td></tr></table>
 	 * <p>
	 * Results in the following cache key/values pairs being added to cache:
 	 * <p>
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>SOME_KEY</td><td>&nbsp;</td><td>99213 01 02</td></tr>
	 * <tr><td></td><td>SOME_KEY1</td><td></td><td>99213</td></tr>
	 * <tr><td></td><td>SOME_KEY2</td><td></td><td>01</td></tr>
	 * <tr><td></td><td>SOME_KEY3</td><td></td><td>02</td></tr>
	 * </table>
 	 * <p>
	 * This allows us to access either the full value, or individual tokens in downstream test steps.
 	 * <p>
 	 * <b>Special Handling of Values Containing Non-alphanumeric (ie. special) Characters</b>
 	 * <p>
	 * Values that contain non-alphanumeric characters (ex. slashes/dashes in date values, dollar signs/decimals 
     * in amounts) will automatically be parsed and saved to cache under a key in the format:
 	 * <p>
     * [original cache key]_UNFORMATTED
 	 * <p>
     * Below are some examples:
 	 * <p>
	 * <b>Example 1: Simple Value Containing Special Characters</b> 
 	 * <p>
     * The following cacheKey|value pair:
 	 * <p>
	 * <table><tr><td>&nbsp;</td><td>KEY_DATE|10/01/2019</td></tr></table>
 	 * <p>
	 * Results in the following cache key/values pairs being added to cache:
 	 * <p>
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>KEY_DATE</td><td>&nbsp;</td><td>10/01/2019</td></tr>
	 * <tr><td></td><td>KEY_DATE_UNFORMATTED</td><td></td><td>10012019</td></tr>
	 * </table>
 	 * <p>
 	 * <p>
	 * <b>Example 2: Multiple Space-delimited Tokens Containing Special Characters</b>
 	 * <p>
     * The following cacheKey|value pair:
 	 * <p>
	 * <table><tr><td>&nbsp;</td><td>KEY_DATES|10/01/2019 to 10/31/2019</td></tr></table>
 	 * <p>
	 * Results in the following cache key/values pairs being added to cache:
 	 * <p>
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>KEY_DATES</td><td>&nbsp;</td><td>10/01/2019 to 10/31/2019</td></tr>
	 * <tr><td></td><td>KEY_DATES1</td><td></td><td>10/01/2019</td></tr>
	 * <tr><td></td><td>KEY_DATES2</td><td></td><td>to</td></tr>
	 * <tr><td></td><td>KEY_DATES3</td><td></td><td>10/31/2019</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED</td><td></td><td>10012019 to 10312019</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED1</td><td></td><td>10012019</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED2</td><td></td><td>to</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED3</td><td></td><td>10312019</td></tr>
	 * </table>
     */
	FLYNET_STORE_INPUT_PRESENT_AFTER(Consequence.NON_CASCADING, new TwoStringsThenOptionalNumericParser(), false),

	/**
     * Temporarily stores the value of the FIRST 'display' (ie. blue colored) element that appears after the given 
     * label text. The attributeKey should be a pipe-delimited string containing the label text and the cache key, 
     * respectively. Note that this operation uses a predefined locator, and will ignore any locators that are
     * provided.
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [label text]|[cache key]</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
 	 * <b>Special Handling of Space-delimited Values</b>
 	 * <p>
	 * Values that contain a string of values (tokens) concatenated with spaces, such as individual names (JOHN R SMITH) 
	 * or procedure codes/modifiers (99213 00 00) will automatically be parsed and each saved to cache under unique 
	 * indexed keys, based off the original key. For example, the cacheKey|value pair:
	 * <p>
	 * <table><tr><td>&nbsp;</td><td>SOME_KEY|99213 01 02</td></tr></table>
 	 * <p>
	 * Results in the following cache key/values pairs being added to cache:
 	 * <p>
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>SOME_KEY</td><td>&nbsp;</td><td>99213 01 02</td></tr>
	 * <tr><td></td><td>SOME_KEY1</td><td></td><td>99213</td></tr>
	 * <tr><td></td><td>SOME_KEY2</td><td></td><td>01</td></tr>
	 * <tr><td></td><td>SOME_KEY3</td><td></td><td>02</td></tr>
	 * </table>
 	 * <p>
	 * This allows us to access either the full value, or individual tokens in downstream test steps.
 	 * <p>
 	 * <b>Special Handling of Values Containing Non-alphanumeric (ie. special) Characters</b>
 	 * <p>
	 * Values that contain non-alphanumeric characters (ex. slashes/dashes in date values, dollar signs/decimals 
     * in amounts) will automatically be parsed and saved to cache under a key in the format:
 	 * <p>
     * [original cache key]_UNFORMATTED
 	 * <p>
     * Below are some examples:
 	 * <p>
	 * <b>Example 1: Simple Value Containing Special Characters</b> 
 	 * <p>
     * The following cacheKey|value pair:
 	 * <p>
	 * <table><tr><td>&nbsp;</td><td>KEY_DATE|10/01/2019</td></tr></table>
 	 * <p>
	 * Results in the following cache key/values pairs being added to cache:
 	 * <p>
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>KEY_DATE</td><td>&nbsp;</td><td>10/01/2019</td></tr>
	 * <tr><td></td><td>KEY_DATE_UNFORMATTED</td><td></td><td>10012019</td></tr>
	 * </table>
 	 * <p>
 	 * <p>
	 * <b>Example 2: Multiple Space-delimited Tokens Containing Special Characters</b>
 	 * <p>
     * The following cacheKey|value pair:
 	 * <p>
	 * <table><tr><td>&nbsp;</td><td>KEY_DATES|10/01/2019 to 10/31/2019</td></tr></table>
 	 * <p>
	 * Results in the following cache key/values pairs being added to cache:
 	 * <p>
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>KEY_DATES</td><td>&nbsp;</td><td>10/01/2019 to 10/31/2019</td></tr>
	 * <tr><td></td><td>KEY_DATES1</td><td></td><td>10/01/2019</td></tr>
	 * <tr><td></td><td>KEY_DATES2</td><td></td><td>to</td></tr>
	 * <tr><td></td><td>KEY_DATES3</td><td></td><td>10/31/2019</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED</td><td></td><td>10012019 to 10312019</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED1</td><td></td><td>10012019</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED2</td><td></td><td>to</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED3</td><td></td><td>10312019</td></tr>
	 * </table>
     */
	FLYNET_STORE_VALUE_PRESENT_AFTER(Consequence.NON_CASCADING, new TwoStringsThenOptionalNumericParser(), false),

	/**
     * Temporarily stores all values on the given screen row. Values will be parsed using a space as the delimiter,
     * so each value will be stored into cache under a unique indexed key, based off the given key (see below for
     * more details). The attributeKey should be a pipe-delimited string containing the row number and the base 
     * cache key, respectively. Note that this operation uses a predefined locator, and will ignore any locators 
     * that are provided.
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [row number]|[base cache key]</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
 	 * <b>Special Handling of Space-delimited Values</b>
 	 * <p>
	 * Values that contain a string of values (tokens) concatenated with spaces, such as individual names (JOHN R SMITH) 
	 * or procedure codes/modifiers (99213 00 00) will automatically be parsed and each saved to cache under unique 
	 * indexed keys, based off the original key. For example, the cacheKey|value pair:
	 * <p>
	 * <table><tr><td>&nbsp;</td><td>SOME_KEY|99213 01 02</td></tr></table>
 	 * <p>
	 * Results in the following cache key/values pairs being added to cache:
 	 * <p>
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>SOME_KEY</td><td>&nbsp;</td><td>99213 01 02</td></tr>
	 * <tr><td></td><td>SOME_KEY1</td><td></td><td>99213</td></tr>
	 * <tr><td></td><td>SOME_KEY2</td><td></td><td>01</td></tr>
	 * <tr><td></td><td>SOME_KEY3</td><td></td><td>02</td></tr>
	 * </table>
 	 * <p>
	 * This allows us to access either the full value, or individual tokens in downstream test steps.
 	 * <p>
 	 * <b>Special Handling of Values Containing Non-alphanumeric (ie. special) Characters</b>
 	 * <p>
	 * Values that contain non-alphanumeric characters (ex. slashes/dashes in date values, dollar signs/decimals 
     * in amounts) will automatically be parsed and saved to cache under a key in the format:
 	 * <p>
     * [original cache key]_UNFORMATTED
 	 * <p>
     * Below are some examples:
 	 * <p>
	 * <b>Example 1: Simple Value Containing Special Characters</b> 
 	 * <p>
     * The following cacheKey|value pair:
 	 * <p>
	 * <table><tr><td>&nbsp;</td><td>KEY_DATE|10/01/2019</td></tr></table>
 	 * <p>
	 * Results in the following cache key/values pairs being added to cache:
 	 * <p>
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>KEY_DATE</td><td>&nbsp;</td><td>10/01/2019</td></tr>
	 * <tr><td></td><td>KEY_DATE_UNFORMATTED</td><td></td><td>10012019</td></tr>
	 * </table>
 	 * <p>
 	 * <p>
	 * <b>Example 2: Multiple Space-delimited Tokens Containing Special Characters</b>
 	 * <p>
     * The following cacheKey|value pair:
 	 * <p>
	 * <table><tr><td>&nbsp;</td><td>KEY_DATES|10/01/2019 to 10/31/2019</td></tr></table>
 	 * <p>
	 * Results in the following cache key/values pairs being added to cache:
 	 * <p>
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>KEY_DATES</td><td>&nbsp;</td><td>10/01/2019 to 10/31/2019</td></tr>
	 * <tr><td></td><td>KEY_DATES1</td><td></td><td>10/01/2019</td></tr>
	 * <tr><td></td><td>KEY_DATES2</td><td></td><td>to</td></tr>
	 * <tr><td></td><td>KEY_DATES3</td><td></td><td>10/31/2019</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED</td><td></td><td>10012019 to 10312019</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED1</td><td></td><td>10012019</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED2</td><td></td><td>to</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED3</td><td></td><td>10312019</td></tr>
	 * </table>
     */
	FLYNET_STORE_VALUES_ON_ROW(Consequence.NON_CASCADING, new NumericThenStringParser(), false),
	
	/**
     * Temporarily stores all values on the given screen row that exist between the given starting/ending columns. Values 
     * will be parsed using a space as the delimiter, so each value will be stored into cache under a unique indexed key, 
     * based off the given key (see below for more details). The attributeKey should be a pipe-delimited string containing 
     * the row number and the base cache key, respectively. Note that this operation uses a predefined locator, and will 
     * ignore any locators that are provided.
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [row number]|[starting column]|[ending column]|[base cache key]</li>
     * <li>Optional: None</li>
     * </ul>
     * <p>
 	 * <b>Special Handling of Space-delimited Values</b>
 	 * <p>
	 * Values that contain a string of values (tokens) concatenated with spaces, such as individual names (JOHN R SMITH) 
	 * or procedure codes/modifiers (99213 00 00) will automatically be parsed and each saved to cache under unique 
	 * indexed keys, based off the original key. For example, the cacheKey|value pair:
	 * <p>
	 * <table><tr><td>&nbsp;</td><td>SOME_KEY|99213 01 02</td></tr></table>
 	 * <p>
	 * Results in the following cache key/values pairs being added to cache:
 	 * <p>
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>SOME_KEY</td><td>&nbsp;</td><td>99213 01 02</td></tr>
	 * <tr><td></td><td>SOME_KEY1</td><td></td><td>99213</td></tr>
	 * <tr><td></td><td>SOME_KEY2</td><td></td><td>01</td></tr>
	 * <tr><td></td><td>SOME_KEY3</td><td></td><td>02</td></tr>
	 * </table>
 	 * <p>
	 * This allows us to access either the full value, or individual tokens in downstream test steps.
 	 * <p>
 	 * <b>Special Handling of Values Containing Non-alphanumeric (ie. special) Characters</b>
 	 * <p>
	 * Values that contain non-alphanumeric characters (ex. slashes/dashes in date values, dollar signs/decimals 
     * in amounts) will automatically be parsed and saved to cache under a key in the format:
 	 * <p>
     * [original cache key]_UNFORMATTED
 	 * <p>
     * Below are some examples:
 	 * <p>
	 * <b>Example 1: Simple Value Containing Special Characters</b> 
 	 * <p>
     * The following cacheKey|value pair:
 	 * <p>
	 * <table><tr><td>&nbsp;</td><td>KEY_DATE|10/01/2019</td></tr></table>
 	 * <p>
	 * Results in the following cache key/values pairs being added to cache:
 	 * <p>
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>KEY_DATE</td><td>&nbsp;</td><td>10/01/2019</td></tr>
	 * <tr><td></td><td>KEY_DATE_UNFORMATTED</td><td></td><td>10012019</td></tr>
	 * </table>
 	 * <p>
 	 * <p>
	 * <b>Example 2: Multiple Space-delimited Tokens Containing Special Characters</b>
 	 * <p>
     * The following cacheKey|value pair:
 	 * <p>
	 * <table><tr><td>&nbsp;</td><td>KEY_DATES|10/01/2019 to 10/31/2019</td></tr></table>
 	 * <p>
	 * Results in the following cache key/values pairs being added to cache:
 	 * <p>
 	 * <table border=0 cellspacing=2>
	 * <tr><td>&nbsp;</td><td>KEY_DATES</td><td>&nbsp;</td><td>10/01/2019 to 10/31/2019</td></tr>
	 * <tr><td></td><td>KEY_DATES1</td><td></td><td>10/01/2019</td></tr>
	 * <tr><td></td><td>KEY_DATES2</td><td></td><td>to</td></tr>
	 * <tr><td></td><td>KEY_DATES3</td><td></td><td>10/31/2019</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED</td><td></td><td>10012019 to 10312019</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED1</td><td></td><td>10012019</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED2</td><td></td><td>to</td></tr>
	 * <tr><td></td><td>KEY_DATES_UNFORMATTED3</td><td></td><td>10312019</td></tr>
	 * </table>
     */
	FLYNET_STORE_VALUES_ON_ROW_BETWEEN_COLS(Consequence.NON_CASCADING, new ThreeNumericsThenStringParser(), false),
	
	/**
     * Flynet operand that performs a TAB key press to cycle through all enterable fields.
     * <p><ul>
     * <li>Required: None</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_TAB(Consequence.CASCADING),

	/**
     * Flynet operand that 'types' the given text starting from the current cursor position and simulates 
     * pressing of the TAB key. The cursor will be repositioned as the text is typed. If no attributeValue
     * is provided, only the TAB key will be pressed.
     * <p><b>THIS OPERATION IS DEPENDENT ON CURSOR POSITION, AND SHOULD BE USED JUDICIOUSLY.</b></p>
     * <p>The {@link #FLYNET_SET_TEXT} operation is a more stable method. since it is NOT dependent on the cursor 
     * position, and  injects the value directly into the element. Therefore, the {@link #FLYNET_SET_TEXT} 
     * operation should be used whenever possible.</p>
     * <p><ul>
     * <li>Required: AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_TYPE_OPTIONAL_TEXT_AND_TAB(Consequence.CASCADING, false),
	
	/**
     * Flynet operand that 'types' the given text starting from the current cursor position. The cursor will 
     * be repositioned as the text is typed. 
     * <p><b>THIS OPERATION IS DEPENDENT ON CURSOR POSITION, AND SHOULD BE USED JUDICIOUSLY.</b></p>
     * <p>The {@link #FLYNET_SET_TEXT} operation is a more stable method. since it is NOT dependent on the cursor 
     * position, and  injects the value directly into the element. Therefore, the {@link #FLYNET_SET_TEXT} 
     * operation should be used whenever possible.</p>
     * <p><ul>
     * <li>Required: AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_TYPE_TEXT(Consequence.CASCADING, false, true),

	/**
     * Flynet operand that 'types' the given text starting from the current cursor position and simulates 
     * pressing of the ENTER key. The cursor will be repositioned as the text is typed. 
     * <p><b>THIS OPERATION IS DEPENDENT ON CURSOR POSITION, AND SHOULD BE USED JUDICIOUSLY.</b></p>
     * <p>The {@link #FLYNET_SET_TEXT} operation is a more stable method. since it is NOT dependent on the cursor 
     * position, and  injects the value directly into the element. Therefore, the {@link #FLYNET_SET_TEXT} 
     * operation should be used whenever possible.</p>
     * <p><ul>
     * <li>Required: AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_TYPE_TEXT_AND_PRESS_ENTER(Consequence.CASCADING, false, true),

	/**
     * Flynet operand that 'types' the given text starting from the current cursor position and simulates 
     * pressing of the TAB key. The cursor will be repositioned as the text is typed. 
     * <p><b>THIS OPERATION IS DEPENDENT ON CURSOR POSITION, AND SHOULD BE USED JUDICIOUSLY.</b></p>
     * <p>The {@link #FLYNET_SET_TEXT} operation is a more stable method. since it is NOT dependent on the cursor 
     * position, and  injects the value directly into the element. Therefore, the {@link #FLYNET_SET_TEXT} 
     * operation should be used whenever possible.</p>
     * <p><ul>
     * <li>Required: AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_TYPE_TEXT_AND_TAB(Consequence.CASCADING, false, true),

	/**
     * Flynet operand that determines if the cursor is located at the FIRST CHARACTER of the INPUT 
     * element associated with the given locator. This is more or less a sanity check prior to using any of
     * the FLYNET_TYPE_TEXT_ operations, which are dependent on cursor position. 
     * 
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_VERIFY_CURSOR_AT_INPUT(Consequence.NON_CASCADING, true),

	/**
     * Flynet operand that determines if the cursor is located at the given row. This is more or less a sanity 
     * check prior to using any of the FLYNET_TYPE_TEXT_ operations, which are dependent on cursor position. 
     * 
     * <p><ul>
     * <li>Required: Locator</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_VERIFY_CURSOR_ON_ROW(Consequence.NON_CASCADING, false, true, true),

	/**
     * Flynet operand that determines if the given search value does NOT appear after the given label text. 
     * The result is a success if the search value is not found. Note that this operation uses a predefined 
     * locator, and will ignore any locators that are provided.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_VERIFY_VALUE_NOT_PRESENT(Consequence.NON_CASCADING, false, true),

	/**
     * Flynet operand that determines if the given search value does NOT appear after the given label text. 
     * The result is a success if the search value is not found. Note that this operation uses a predefined 
     * locator, and will ignore any locators that are provided.
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [label text]|[search value]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_VERIFY_VALUE_NOT_PRESENT_AFTER(Consequence.NON_CASCADING, new TwoStringsThenOptionalNumericParser(), false),

	/**
     * Flynet operand that determines if the given search value does NOT appear on the given row. An optional starting column
     * or a starting/ending column range can be provided to restrict the comparison to a given column range.
     * The result is a success if the search value is not found. Note that this operation uses a predefined 
     * locator, and will ignore any locators that are provided.
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [search value]|[row number]|[starting column]|[ending column]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_VERIFY_VALUE_NOT_PRESENT_ON_ROW(Consequence.NON_CASCADING, new StringThenNumericThenTwoOptionalNumericsParser(), false),

	/**
     * Flynet operand that determines if the given search value appears anywhere on the screen. The 
     * result is a success if the search value is found. Note that this operation uses a predefined locator, 
     * and will ignore any locators that are provided.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_VERIFY_VALUE_PRESENT(Consequence.NON_CASCADING, false, true),
	
	/**
     * Flynet operand that determines if the given search value appears after the given label text. The 
     * result is a success if the search value is found. Note that this operation uses a predefined locator, 
     * and will ignore any locators that are provided.
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [label text]|[search value]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_VERIFY_VALUE_PRESENT_AFTER(Consequence.NON_CASCADING, new TwoStringsThenOptionalNumericParser(), false),

	/**
     * Flynet operand that determines if the given search value appears on the given row. An optional starting column
     * or a starting/ending column range can be provided to restrict the comparison to a given column range. The result 
     * is a success if the search value is found. Note that this operation uses a predefined locator, and will ignore any 
     * locators that are provided.
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [search value]|[row number]|[starting column]|[ending column]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_VERIFY_VALUE_PRESENT_ON_ROW(Consequence.NON_CASCADING, new StringThenNumericThenTwoOptionalNumericsParser(), false),

	/**
     * Flynet operand that waits until the given search value does not appear anywhere on the screen. The result 
     * is a success if the search value is found. Note that this operation uses a predefined locator, and will 
     * ignore any locators that are provided.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_WAIT_UNTIL_VALUE_NOT_PRESENT(Consequence.CASCADING, false, true),
	
	/**
     * Flynet operand that waits until the given search value does not appear after the given label text. Note 
     * that this operation uses a predefined locator, and will ignore any locators that are provided.
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [label text]|[search value]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_WAIT_UNTIL_VALUE_NOT_PRESENT_AFTER(Consequence.CASCADING, new TwoStringsThenOptionalNumericParser(), false),

	/**
     * Flynet operand that waits until the given search value does not appear after the given label text. An optional 
     * starting column or a starting/ending column range can be provided to restrict the comparison to a given column 
     * range. The result is a success if the search value is found. Note that this operation uses a predefined locator, 
     * and will ignore any locators that are provided.
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [search value]|[row number]|[starting column]|[ending column]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_WAIT_UNTIL_VALUE_NOT_PRESENT_ON_ROW(Consequence.CASCADING, new StringThenNumericThenTwoOptionalNumericsParser(), false),

	/**
     * Flynet operand that waits until the given search value appears anywhere on the screen. The result is 
     * a success if the search value is found. Note that this operation uses a predefined locator, and will 
     * ignore any locators that are provided.
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_WAIT_UNTIL_VALUE_PRESENT(Consequence.CASCADING, false, true),
	
	/**
     * Flynet operand that waits until the given search value appears after the given label text. Note that this
     * operation uses a predefined locator, and will ignore any locators that are provided.
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [label text]|[search value]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_WAIT_UNTIL_VALUE_PRESENT_AFTER(Consequence.CASCADING, new TwoStringsThenOptionalNumericParser(), false),

	/**
     * Flynet operand that waits until the given search value appears after the given label text. An optional starting column
     * or a starting/ending column range can be provided to restrict the comparison to a given column range. The result is 
     * a success if the search value is found. Note that this operation uses a predefined locator, and will 
     * ignore any locators that are provided.
     * <p><ul>
     * <li>Required: AttributeKey as a pipe-delimited string in the format [search value]|[row number]|[starting column]|[ending column]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_WAIT_UNTIL_VALUE_PRESENT_ON_ROW(Consequence.CASCADING, new StringThenNumericThenTwoOptionalNumericsParser(), false),

	/**
     * Flynet operand that waits until the element associated with the given locator exists AND contains the given 
     * value (case-insensitive). 
     * <p><ul>
     * <li>Required: Locator, AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_WAIT_UNTIL_CONTAINS_VALUE(Consequence.CASCADING, true, true),

	/**
     * Flynet operand that waits until the emulator status matches the given value. This operation is useful
     * for waiting until the screen has finishing processing the prior request and is READY. Note that this
     * operation uses a predefined locator, and will ignore any locators that are provided.
     * <p><ul>
     * <li>Required: AttributeKey (can be either data property key or string value)</li>
     * <li>Optional: None</li>
     * </ul>
     */
	FLYNET_WAIT_FOR_STATUS(Consequence.CASCADING, false, true),

	/**
     * Web service operand that calls the HealthClaims LEDS REST service for all claim information (including 
     * all line detail).
     * <p><ul>
     * <li>Required: AttributeKey in the format [Host region]|[claim number]</li>
     * <li>Optional: None</li>
     * </ul>
     */
	WS_GET_CLAIM(Consequence.CASCADING, false, true),

	/**
     * Web service operand for performing a GET request against a REST service.
     * <p><ul>
     * <li>Required: AttributeKey in the format [resource]|[access token]|[environment]{|[key=value]}</li>
     * <li>Optional: None</li>
     * </ul>
     */
	WS_GET_REQUEST(Consequence.CASCADING, false, true),
	WS_POST_REQUEST(Consequence.CASCADING, false, true),
	WS_MOCK_REQUEST(Consequence.CASCADING, false, true),
    WS_JSON_COMPARE(Consequence.CASCADING, false, true);
	
	protected static final Logger LOGGER = Logger.getLogger(Operation.class.getName());

	private Consequence consequence;

	private boolean elementOperation;
	private boolean attributeRequiredOperation;
	private boolean numericAttributeRequiredOperation;
	private boolean executionOperation;
	private boolean testSheetOperation;
	private boolean conditionalOperation;
	private boolean warningOperation;
	private boolean exitOperation;
	private boolean childOperation;
	private boolean loopableOperation;
	private boolean screenshotOperation;

	private AttributeParser parser;

	/*
	 * Any operation that uses an AttributeParser is required to have an attribute value.
	 */
	private Operation(Consequence consequence, AttributeParser parser, boolean elementOperation) {
		this(consequence, elementOperation, true, false);
		this.parser = parser;
	}

	/*
	 * Any operation that uses an AttributeParser is required to have an attribute value.
	 */
	private Operation(Consequence consequence, AttributeParser parser, boolean elementOperation, boolean conditionalOperation) {
		this(consequence, elementOperation, true, false, false, false, conditionalOperation);
		this.parser = parser;
	}

	private Operation(Consequence consequence) {
		this(consequence, false);
	}

	private Operation(Consequence consequence, boolean elementOperation) {
		this(consequence, elementOperation, false);
	}

	private Operation(Consequence consequence, boolean elementOperation, boolean attributeRequiredOperation) {
		this(consequence, elementOperation, attributeRequiredOperation, false);
	}

	private Operation(Consequence consequence, boolean elementOperation, boolean attributeRequiredOperation, boolean numericAttributeRequiredOperation) {
		this(consequence, elementOperation, attributeRequiredOperation, numericAttributeRequiredOperation, false);
	}

	private Operation(Consequence consequence, boolean elementOperation, boolean attributeRequiredOperation, boolean numericAttributeRequiredOperation, boolean executionOperation) {
		this(consequence, elementOperation, attributeRequiredOperation, numericAttributeRequiredOperation, executionOperation, false);
	}

	private Operation(Consequence consequence, boolean elementOperation, boolean attributeRequiredOperation, boolean numericAttributeRequiredOperation, boolean executionOperation, 
			boolean testSheetOperation) {
		this(consequence, elementOperation, attributeRequiredOperation, numericAttributeRequiredOperation, executionOperation, testSheetOperation, false);
	}

	private Operation(Consequence consequence, boolean elementOperation, boolean attributeRequiredOperation, boolean numericAttributeRequiredOperation, boolean executionOperation, 
			boolean testSheetOperation,	boolean conditionalOperation) {
		this(consequence, elementOperation, attributeRequiredOperation, numericAttributeRequiredOperation, executionOperation, testSheetOperation, conditionalOperation, false);
	}

	private Operation(Consequence consequence, boolean elementOperation, boolean attributeRequiredOperation, boolean numericAttributeRequiredOperation, boolean executionOperation, 
			boolean testSheetOperation,	boolean conditionalOperation, boolean warningOperation) {
		this(consequence, elementOperation, attributeRequiredOperation, numericAttributeRequiredOperation, executionOperation, testSheetOperation, conditionalOperation, warningOperation, false);
	}

	private Operation(Consequence consequence, boolean elementOperation, boolean attributeRequiredOperation, boolean numericAttributeRequiredOperation, boolean executionOperation, 
			boolean testSheetOperation,	boolean conditionalOperation, boolean warningOperation, boolean exitOperation) {
		this(consequence, elementOperation, attributeRequiredOperation, numericAttributeRequiredOperation, executionOperation, testSheetOperation, conditionalOperation, warningOperation,
			exitOperation, false);
	}

	private Operation(Consequence consequence, boolean elementOperation, boolean attributeRequiredOperation, boolean numericAttributeRequiredOperation, boolean executionOperation, 
			boolean testSheetOperation,	boolean conditionalOperation, boolean warningOperation, boolean exitOperation, boolean childOperation) {
		this(consequence, elementOperation, attributeRequiredOperation, numericAttributeRequiredOperation, executionOperation, testSheetOperation, conditionalOperation, warningOperation,
			exitOperation, childOperation, false);
	}

	private Operation(Consequence consequence, boolean elementOperation, boolean attributeRequiredOperation, boolean numericAttributeRequiredOperation, boolean executionOperation, 
			boolean testSheetOperation,	boolean conditionalOperation, boolean warningOperation, boolean exitOperation, boolean childOperation, boolean loopableOperation) {
		this(consequence, elementOperation, attributeRequiredOperation, numericAttributeRequiredOperation, executionOperation, testSheetOperation, conditionalOperation, warningOperation,
			exitOperation, childOperation, loopableOperation, false);
	}

	private Operation(Consequence consequence, boolean elementOperation, boolean attributeRequiredOperation, boolean numericAttributeRequiredOperation,
			boolean executionOperation, boolean testSheetOperation, boolean conditionalOperation, boolean warningOperation, boolean exitOperation,
			boolean childOperation, boolean loopableOperation, boolean screenshotOperation) {
		this.consequence = consequence;
		this.elementOperation = elementOperation;
		this.attributeRequiredOperation = attributeRequiredOperation;
		this.numericAttributeRequiredOperation = numericAttributeRequiredOperation;
		this.executionOperation = executionOperation;
		this.testSheetOperation = testSheetOperation;
		this.conditionalOperation = conditionalOperation;
		this.warningOperation = warningOperation;
		this.exitOperation = exitOperation;
		this.childOperation = childOperation;
		this.loopableOperation = loopableOperation;
		this.screenshotOperation = screenshotOperation;

		/*
		 *  All 'Child' operations (ie. operations that are performed using a parent element as the root)
		 *  are always 'Loopable' operations (ie. operations that may be defined within a LOOP construct).
		 */
		if (this.childOperation) this.loopableOperation = true;

		OperationUtil.validate(this);
	}

	public Consequence getConsequence() {
		return consequence;
	}

	@Override
	public boolean isCascadingType() {
		return consequence == Consequence.CASCADING;
	}

	@Override
	public boolean isNonCascadingType() {
		return consequence == Consequence.NON_CASCADING;
	}

	@Override
	public boolean isElementOperation() {
		return elementOperation;
	}

	@Override
	public boolean isAttributeRequiredOperation() {
		return attributeRequiredOperation;
	}

	@Override
	public boolean isExecutionOperation() {
		return executionOperation;
	}

	@Override
	public boolean isTestSheetOperation() {
		return testSheetOperation;
	}

	@Override
	public boolean isConditionalOperation() {
		return conditionalOperation;
	}

	@Override
	public boolean isConditionalTrueOperation() {
		return conditionalOperation && this.name().contains(String.valueOf(Boolean.TRUE).toUpperCase());
	}

	@Override
	public boolean isConditionalFalseOperation() {
		return conditionalOperation && this.name().contains(String.valueOf(Boolean.FALSE).toUpperCase());
	}

	@Override
	public boolean isWarningOperation() {
		return warningOperation;
	}

	@Override
	public boolean isExitOperation() {
		return exitOperation;
	}

	@Override
	public boolean isChildOperation() {
		return childOperation;
	}

	@Override
	public boolean isLoopableOperation() {
		return loopableOperation;
	}

	@Override
	public boolean isScreenshotOperation() {
		return screenshotOperation;
	}

	@Override
	public boolean isNumericAttributeRequiredOperation() {
		return numericAttributeRequiredOperation;
	}

	@Override
	public boolean isPerformOperation() {
		return this.name().startsWith("PERFORM");
	}

	@Override
	public boolean isWaitOperation() {
		return this.name().startsWith("WAIT");
	}

	@Override
	public boolean isSkipToOperation() {
		return this.name().startsWith("SKIP_TO");
	}

	@Override
	public boolean isVerifyOperation() {
		return this.name().startsWith("VERIFY");
	}

	@Override
	public boolean isStoreOperation() {
		/*
		 * BAG 11/2/19 - Included all IF_STORED_ and VERIFY_STORED_ operations 
		 * BAG 12/31/19 - Include all FLYNET_STORE operations 
		 * BAG 03/17/20 - Include CONCAT_AND_STORE operation 
		 */
		return this.name().startsWith("STORE") || this.name().startsWith("FLYNET_STORE") || this.name().startsWith("IF_STORED")
				|| this.name().startsWith("VERIFY_STORED") || this.name().contains("STORED_DATE") || this == Operation.CONCAT_AND_STORE;
	}

	@Override
	public boolean isKeyOperation() {
		/*
		 * BAG - 3/30/21 - Added REMOVE_KEYS operation
		 */
		return this.name().startsWith("IF_KEY") || this == Operation.REMOVE_KEYS;
	}

	@Override
	public boolean isElseOperation() {
		return this == Operation.ELSE;
	}

    @Override
    public boolean isEndIfOperation() {
        return this == Operation.ENDIF;
    }

	@Override
	public boolean isSubOperation() {
		return this.isKeyboardOperation() || this.isSAPOperation() || this.isLanFaxOperation() ||
				this.isAndroidOperation() || this.isIOSOperation();
	}

	public boolean isKeyboardOperation() {
		return this.name().startsWith("KEYBOARD");
	}

	public boolean isMouseOperation() {
		return this.name().startsWith("MOUSE");
	}

	public boolean isSAPOperation() {
		return this.name().startsWith("SAP");
	}

	public boolean isLanFaxOperation() {
		return this.name().startsWith("LANFAX");
	}

	public boolean isJSMOperation() {
		return this.name().startsWith("JSM");
	}

	public boolean isFlynetOperation() {
		return this.name().startsWith("FLYNET");
	}

	public boolean isAndroidOperation() {
		return this.name().startsWith("ANDROID");
	}

	public boolean isIOSOperation() {
		return this.name().startsWith("IOS");
	}

	public boolean isWebServiceOperation() {
		return this.name().startsWith("WS_");
	}

	public boolean isDataPropertyOperation() {
		return this.name().contains("DATA_PROPERTY");
	}

	public AttributeParser getParser() {
		return parser;
	}

	public boolean hasParser() {
		return parser != null;
	}
}
