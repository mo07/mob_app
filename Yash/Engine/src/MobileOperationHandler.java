package com.bcbssc.webdriver.handler;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.ContextAware;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.bcbssc.automation.util.DateUtil;
import com.bcbssc.webdriver.exception.InvalidStateException;
import com.bcbssc.webdriver.framework.ExecutionConstants;
import com.bcbssc.webdriver.framework.OperationHandler;
import com.bcbssc.webdriver.framework.TestStep;
import com.bcbssc.webdriver.util.ExecutionUtil;
import com.bcbssc.webdriver.util.WebElementFinder;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.StartsActivity;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;

public abstract class MobileOperationHandler extends SubOperationHandler {

	protected static final Logger LOGGER = Logger.getLogger(MobileOperationHandler.class.getName());

	private WebElementFinder webElementFinder;

	private static final int DEFAULT_PRE_CLICK_COORDS_WAIT_IN_MILLIS = 2000;
	private static final int DEFAULT_POST_CLICK_COORDS_WAIT_IN_MILLIS = 500;
	private static final int DEFAULT_POST_TAP_WAIT_IN_MILLIS = 500;
	private static final int DEFAULT_LONG_PRESS_WAIT_IN_MILLIS = 2000;
	private static final String SWITCH_CONTEXT_LOG_TEXT = "Switching context to: {0}";

	protected MobileOperationHandler(OperationHandler operationHandler) {
		super(null, operationHandler);
		webElementFinder = new WebElementFinder(this.getOperationHandler().getDriver(), this.getExecutionScenario());
	}

	protected void clickCoordinates(TestStep testStep, WebDriver driver) {

		if (!testStep.hasAttributeValue()) {
			throw new IllegalArgumentException(MSG_PREFIX_ATTR_VAL_MISSING + testStep);
		}

		String[] parameters = testStep.getAttributeValue().split(ExecutionConstants.PARAM_DELIMITER_REGEX);
		if (parameters.length != 2 || !StringUtils.isNumeric(parameters[0].trim())
				|| !StringUtils.isNumeric(parameters[1].trim())) {
			throw new IllegalArgumentException(
					"Attribute value must contain 2 comma-delimited tokens, x and y coordinates. Actual: "
							+ parameters.length);
		}

		ExecutionUtil.sleep(this.getPreClickCoordinatesWaitInMillis());

		TouchAction<?> action = this.getTouchActionInstance(driver);
		action.press(PointOption.point(Integer.parseInt(parameters[0].trim()), Integer.parseInt(parameters[1].trim())));
		action.release();
		action.perform();

		ExecutionUtil.sleep(this.getPostClickCoordinatesWaitInMillis());
	}

	protected void swipeScreen(TestStep testStep, WebDriver driver) {
		if (!testStep.hasAttributeValue()) {
			throw new IllegalArgumentException(MSG_PREFIX_ATTR_VAL_MISSING + testStep);
		}

		String[] parameters = testStep.getAttributeValue().split(ExecutionConstants.PARAM_DELIMITER_REGEX);
		if (parameters.length != 4 || !StringUtils.isNumeric(parameters[0].trim())
				|| !StringUtils.isNumeric(parameters[1].trim()) || !StringUtils.isNumeric(parameters[2].trim())
				|| !StringUtils.isNumeric(parameters[3].trim())) {
			throw new IllegalArgumentException(
					"Attribute value must contain 4 comma-delimited tokens Starting x and y and ending x and y. Actual: "
							+ parameters.length);
		}

		TouchAction<?> action = this.getTouchActionInstance(driver);
		action.press(PointOption.point(Integer.parseInt(parameters[0].trim()), Integer.parseInt(parameters[1].trim())));
		action.waitAction(WaitOptions.waitOptions(Duration.ofMillis(200)));
		action.moveTo(
				PointOption.point(Integer.parseInt(parameters[2].trim()), Integer.parseInt(parameters[3].trim())));
		action.release();
		action.perform();
	}

	protected void horizontalSwipe(TestStep testStep, WebDriver driver) {
		if (!testStep.hasAttributeValue()) {
			throw new IllegalArgumentException(MSG_PREFIX_ATTR_VAL_MISSING + testStep);
		}

		String[] parameters = testStep.getAttributeValue().split(ExecutionConstants.PARAM_DELIMITER_REGEX);
		if (parameters.length != 2 || !StringUtils.isNumeric(parameters[0].trim())
				|| !StringUtils.isNumeric(parameters[1].trim())) {
			throw new IllegalArgumentException(
					"Attribute value must contain 2 comma-delimited tokens, starting x and ending x. Actual: "
							+ parameters.length);
		}

		TouchAction<?> action = this.getTouchActionInstance(driver);
		action.press(PointOption.point(Integer.parseInt(parameters[0].trim()), 1000));
		action.moveTo(PointOption.point(Integer.parseInt(parameters[1].trim()), 1000));
		action.release();
		action.perform();
	}

	protected void verticalSwipe(TestStep testStep, WebDriver driver) {
		if (!testStep.hasAttributeValue()) {
			throw new IllegalArgumentException(MSG_PREFIX_ATTR_VAL_MISSING + testStep);
		}

		String[] parameters = testStep.getAttributeValue().split(ExecutionConstants.PARAM_DELIMITER_REGEX);
		if (parameters.length != 2 || !StringUtils.isNumeric(parameters[0].trim())
				|| !StringUtils.isNumeric(parameters[1].trim())) {
			throw new IllegalArgumentException(
					"Attribute value must contain 2 comma-delimited tokens, starting y and ending y. Actual: "
							+ parameters.length);
		}

		TouchAction<?> action = this.getTouchActionInstance(driver);
		action.press(PointOption.point(500, Integer.parseInt(parameters[0].trim())));
		action.moveTo(PointOption.point(500, Integer.parseInt(parameters[1].trim())));
		action.release();
		action.perform();
	}

	protected void tapElement(TestStep testStep, WebDriver driver) {
		TouchAction<?> action = this.getTouchActionInstance(driver);
		action.tap(ElementOption.element((webElementFinder.getElementForStep(testStep))));
		action.perform();

		ExecutionUtil.sleep(this.getPostTapWaitInMillis());
	}

	protected void longPress(TestStep testStep, WebDriver driver) {
		TouchAction<?> action = this.getTouchActionInstance(driver);
		action.longPress(ElementOption.element((webElementFinder.getElementForStep(testStep))));
		action.waitAction(WaitOptions.waitOptions(Duration.ofMillis(this.getLongPressWaitInMillis())));
		action.release();
		action.perform();
	}

	protected void switchAppiumContextToWebView(TestStep testStep, WebDriver driver) {
		int attemptLimit = 10;
		int attempt = 0;
		Set<String> contextNames = null;
		// boolean firstContext = true;

		while (attempt < attemptLimit) {
			LOGGER.log(Level.INFO, "Attempting to switch context to WebView, attempt number: {0}", attempt + 1);

			contextNames = ((ContextAware) driver).getContextHandles();
			LOGGER.log(Level.INFO, "Available contexts are: {0}", contextNames);

			if (testStep.hasAttributeValue()) {
				LOGGER.log(Level.INFO, "WebView context name passed in, checking available contexts for [{0}]",
						testStep.getAttributeValue());
				for (String contextName : contextNames) {
					// if (testStep.getAttributeValue().equals(contextName)) {
					if (contextName.startsWith(testStep.getAttributeValue())) {
						LOGGER.log(Level.INFO, SWITCH_CONTEXT_LOG_TEXT, contextName);
						((ContextAware) driver).context(contextName);
						ExecutionUtil.sleep(5000);
						return;
					}
				}
			} else {
				LOGGER.log(Level.INFO,
						"WebView context name NOT passed in, checking available contexts for default WebView context name based on platform type");
				for (String contextName : contextNames) {
					if (testStep.getTestSheet().getExecutionScenario().isAndroid()) {
						if (contextName.equals(ExecutionConstants.WEBVIEW_CONTEXT_PREFIX
								+ ((StartsActivity) driver).getCurrentPackage())) {
							LOGGER.log(Level.INFO, SWITCH_CONTEXT_LOG_TEXT, contextName);
							((ContextAware) driver).context(contextName);
							return;
						}
					} else { // Assume iOS, 1st webview is app's webview
						if (contextName.startsWith(ExecutionConstants.WEBVIEW_CONTEXT_PREFIX)) {
							LOGGER.log(Level.INFO, SWITCH_CONTEXT_LOG_TEXT, contextName);
							// if (contextName.startsWith("WEBVIEW_50279")) {
							// ExecutionUtil.sleep(10000);
							// break;
							// }
							// if (contextNames.size() > 2 && firstContext )
							// {
							// firstContext = false;
							// continue;
							// }
							// LOGGER.log(Level.INFO, SWITCH_CONTEXT_LOG_TEXT, contextName);
							((ContextAware) driver).context(contextName);
							return;
						}
					}
				}
			}

			LOGGER.log(Level.INFO,
					"Requested context not available currently, pausing briefly then performing a retry...");
			ExecutionUtil.sleep(500);
			attempt++;
		}

		// After multiple attempts limit reached, throw error
		LOGGER.log(Level.SEVERE, "Error finding WEBVIEW context after {0} attempts, throwing error", attemptLimit);
		throw new InvalidStateException("Afer " + attemptLimit
				+ " attempts, a valid WEBVIEW context was either not found"
				+ " in the list of available contexts, or the WEBVIEW name provided as an attribute did not match any of the available"
				+ " contexts for this session. Check the current app to ensure it is capable of a WEBVIEW context (i.e. a hybrid app)"
				+ " or verify the attribute provided is a valid WEBVIEW context that should be present.");
	}

	protected void storeWebViewContext(TestStep testStep, WebDriver driver) {
		Set<String> contextNames = null;

		if (!testStep.hasAttributeValue()) {
			throw new IllegalArgumentException(MSG_PREFIX_ATTR_VAL_MISSING + testStep);
		}
		contextNames = ((ContextAware) driver).getContextHandles();
		LOGGER.log(Level.INFO, "Available contexts are: {0}", contextNames);

		LOGGER.log(Level.INFO, "Checking available contexts for default WebView context names for IOS");
		for (String contextName : contextNames) {
			// Assume iOS, 1st webview is app's webview
			if (contextName.startsWith(ExecutionConstants.WEBVIEW_CONTEXT_PREFIX)) {
				LOGGER.log(Level.INFO, "Storing the context name {0} in cache using key {1}",
						new Object[] { contextName, testStep.getAttributeValue() });
				this.getOperationHandler().getCache().save(testStep.getAttributeValue(), contextName, true);
				return;
			}
		}
	}

	protected void switchAppiumContextToNativeView(WebDriver driver) {
		Set<String> contextNames = null;

		contextNames = ((ContextAware) driver).getContextHandles();
		LOGGER.log(Level.INFO, "Available contexts are: {0}", contextNames);
		((ContextAware) driver).context("NATIVE_APP");
	}

	protected TouchAction<?> getTouchActionInstance(WebDriver driver) {
		return new TouchAction<>((PerformsTouchActions) driver);
	}

	protected int getPreClickCoordinatesWaitInMillis() {
		return DEFAULT_PRE_CLICK_COORDS_WAIT_IN_MILLIS;
	}

	protected int getPostClickCoordinatesWaitInMillis() {
		return DEFAULT_POST_CLICK_COORDS_WAIT_IN_MILLIS;
	}

	protected int getPostTapWaitInMillis() {
		return DEFAULT_POST_TAP_WAIT_IN_MILLIS;
	}

	protected int getLongPressWaitInMillis() {
		return DEFAULT_LONG_PRESS_WAIT_IN_MILLIS;
	}

	protected void setIonicDate(TestStep testStep, WebDriver driver) {
		if (!testStep.hasAttributeValue()) {
			throw new IllegalArgumentException(MSG_PREFIX_ATTR_VAL_MISSING + testStep);
		}
		if (!testStep.hasLocator()) {
			throw new IllegalArgumentException(MSG_PREFIX_LOCATOR_VAL_MISSING + testStep);
		}

		LOGGER.log(Level.INFO, "String of inputDate: {0}", testStep.getAttributeValue());

		Date inputDate = DateUtil.convertToDate(testStep.getAttributeValue());

		SimpleDateFormat sdf;
		sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		sdf.setTimeZone(TimeZone.getDefault());
		String isoFormattedInputDate = sdf.format(inputDate);

		LOGGER.log(Level.INFO, "ISO string of inputDate: {0}", isoFormattedInputDate);

		WebElement dateTimeElement = webElementFinder.waitForElementForStep(testStep);

		LOGGER.log(Level.INFO, "Running javascript to set ion-datetime value to: {0}", isoFormattedInputDate);

		// Set value of ion-datetime element to ISO formatted input date attibute
		((JavascriptExecutor) driver).executeScript("arguments[0].setAttribute('value', arguments[1]);",
				dateTimeElement, isoFormattedInputDate);

		// Pause briefly for the animations to update
		ExecutionUtil.sleep(250);
	}

	protected abstract void activateApp(TestStep testStep, WebDriver driver);

	protected abstract void terminateApp(TestStep testStep, WebDriver driver);

	protected abstract void closeApp(TestStep testStep, WebDriver driver);

	protected abstract void launchApp(TestStep testStep, WebDriver driver);
}
