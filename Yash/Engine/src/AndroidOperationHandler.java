package com.bcbssc.webdriver.handler;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;

import com.bcbssc.webdriver.framework.OperationHandler;
import com.bcbssc.webdriver.framework.TestStep;
import com.bcbssc.webdriver.framework.TestStepResult;
import com.bcbssc.webdriver.util.ExecutionUtil;

import io.appium.java_client.InteractsWithApps;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.android.nativekey.PressesKey;

public class AndroidOperationHandler extends MobileOperationHandler {

    protected static final Logger LOGGER = Logger.getLogger(AndroidOperationHandler.class.getName());

    public AndroidOperationHandler(OperationHandler operationHandler) {
        super(operationHandler);
    }

    @Override
	public TestStepResult process(TestStep testStep, WebDriver driver) {

        LOGGER.log(Level.INFO, "{0} PROCESSING testStep: {1}", new Object[]{this, testStep});
        
        LOGGER.log(Level.INFO, "The driver class is: {0}", driver.getClass().getName());

        TestStepResult testStepResult = null;

        switch (testStep.getOperation()) {

            case ANDROID_WAIT:
                try {
                    if (!testStep.isAttributeValueNumeric()) {
                        throw new IllegalArgumentException("numeric attribute value missing for test step: " + testStep);
                    }
                    ExecutionUtil.sleep(Integer.parseInt(testStep.getAttributeValue()));
                    testStepResult = new TestStepResult(testStep, true);
                } catch (Exception e) {
                    testStepResult = new TestStepResult(testStep, e);
                }
                break;

            case ANDROID_CLICK_COORDINATES:
                try {
                    this.clickCoordinates(testStep, driver);
                    testStepResult = new TestStepResult(testStep, true);
                } catch (Exception e) {
                    testStepResult = new TestStepResult(testStep, e);
                }
                break;

            case ANDROID_CLICK_NATIVE_BACK_BUTTON:
            	try {
            	((PressesKey) driver).pressKey(new KeyEvent(AndroidKey.BACK));
            	testStepResult = new TestStepResult(testStep, true);
            	} catch (Exception e) {
            	testStepResult = new TestStepResult(testStep, e);
            	}
            	break;

            case ANDROID_PRESS_TAB:
            	try {
            	((PressesKey) driver).pressKey(new KeyEvent(AndroidKey.TAB));
            	testStepResult = new TestStepResult(testStep, true);
            	} catch (Exception e) {
            	testStepResult = new TestStepResult(testStep, e);
            	}
            	break;
            	//((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.ENTER));
            	
            case ANDROID_PRESS_ENTER:
            	try {
            	((PressesKey) driver).pressKey(new KeyEvent(AndroidKey.ENTER));
            	testStepResult = new TestStepResult(testStep, true);
            	} catch (Exception e) {
            	testStepResult = new TestStepResult(testStep, e);
            	}
            	break;
            	
            case ANDROID_TAP:
                try {
                    this.tapElement(testStep, driver);
                    testStepResult = new TestStepResult(testStep, true);
                } catch (Exception e) {
                    testStepResult = new TestStepResult(testStep, e);
                }
                break;

            case ANDROID_SWIPE_SCREEN:
                try {
                    this.swipeScreen(testStep, driver);
                    testStepResult = new TestStepResult(testStep, true);
                } catch (Exception e) {
                    testStepResult = new TestStepResult(testStep, e);
                }
                break;

            case ANDROID_HORIZONTAL_SWIPE:
                try {
                    this.horizontalSwipe(testStep, driver);
                    testStepResult = new TestStepResult(testStep, true);
                } catch (Exception e) {
                    testStepResult = new TestStepResult(testStep, e);
                }
                break;

            case ANDROID_VERTICAL_SWIPE:
                try {
                    this.verticalSwipe(testStep, driver);
                    testStepResult = new TestStepResult(testStep, true);
                } catch (Exception e) {
                    testStepResult = new TestStepResult(testStep, e);
                }
                break;

            case ANDROID_ACTIVATE_APP:
                try {
                    this.activateApp(testStep, driver);
                    testStepResult = new TestStepResult(testStep, true);
                } catch (Exception e) {
                    testStepResult = new TestStepResult(testStep, e);
                }
                break;

            case ANDROID_TERMINATE_APP:
                try {
                    this.terminateApp(testStep, driver);
                    testStepResult = new TestStepResult(testStep, true);
                } catch (Exception e) {
                    testStepResult = new TestStepResult(testStep, e);
                }
                break;

            case ANDROID_CLOSE_APP:
                try {
                    this.closeApp(testStep, driver);
                    testStepResult = new TestStepResult(testStep, true);
                } catch (Exception e) {
                    testStepResult = new TestStepResult(testStep, e);
                }
                break;

            case ANDROID_LAUNCH_APP:
                try {
                    this.launchApp(testStep, driver);
                    testStepResult = new TestStepResult(testStep, true);
                } catch (Exception e) {
                    testStepResult = new TestStepResult(testStep, e);
                }
                break;

            case ANDROID_SWITCH_APPIUM_CONTEXT_WEB:
                try {
                    this.switchAppiumContextToWebView(testStep, driver);
                    testStepResult = new TestStepResult(testStep, true);
                } catch (Exception e) {
                    testStepResult = new TestStepResult(testStep, e);
                }
                break;

            case ANDROID_SWITCH_APPIUM_CONTEXT_NATIVE:
                try {
                    this.switchAppiumContextToNativeView(driver);
                    testStepResult = new TestStepResult(testStep, true);
                } catch (Exception e) {
                    testStepResult = new TestStepResult(testStep, e);
                }
                break;

            case ANDROID_IONIC_SET_DATE:
                try {
                    this.setIonicDate(testStep, driver);
                    testStepResult = new TestStepResult(testStep, true);
                } catch (Exception e) {
                    testStepResult = new TestStepResult(testStep, e);
                }
                break;

            default:
                break;
        }

        return testStepResult;
    }

    @Override
    protected void activateApp(TestStep testStep, WebDriver driver) {
        ((InteractsWithApps)driver).activateApp(testStep.getAttributeValue());
    }

    @Override
    protected void closeApp(TestStep testStep, WebDriver driver) {
        ((InteractsWithApps)driver).closeApp();
    }

    @Override
    protected void terminateApp(TestStep testStep, WebDriver driver) {
        ((InteractsWithApps)driver).terminateApp(testStep.getAttributeValue());
    }

    @Override
    protected void launchApp(TestStep testStep, WebDriver driver) {
        ((InteractsWithApps)driver).launchApp();
    }
}
