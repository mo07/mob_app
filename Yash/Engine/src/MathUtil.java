package com.bcbssc.webdriver.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.lang3.StringUtils;

import com.bcbssc.webdriver.exception.InvalidInputException;
import com.bcbssc.webdriver.framework.ExecutionConstants;

public class MathUtil {

	protected static final Logger LOGGER = Logger.getLogger(MathUtil.class.getName());
	
	public static final String TEXT_MATH_FUNCTION = "Math.";
	
	private MathUtil() {}
	
	/*
	 * Evaluates the given arithmetic expression and returns the computed result.
	 */
	public static BigDecimal performArithmetic(String expression) throws ScriptException { 

		LOGGER.log(Level.INFO, "original expression: {0}", expression);

		if (StringUtils.isBlank(expression)) {
			throw new InvalidInputException("expression is required");
		}

		/*
		 * This check will catch 99.9% of any non-arithmetic expressions, thereby preventing any undesired 
		 * script from being executed by the ScriptEngine. 
		 */
		/*
		 * BAG - 1/10/21 - Commented out, since we want to be able to pass in function calls such as Math.round().
		 */
		if (expression.chars().anyMatch(Character::isLetter) && !expression.contains(TEXT_MATH_FUNCTION)) {
			throw new InvalidInputException("string contains alphabetic characters: " + expression);
		}

		/*
		 * This check will ensure that the string contains at least one arithmetic operator. 
		 */
		if (!StringUtils.containsAny(expression, "+", "-", "*", "/", "%", TEXT_MATH_FUNCTION)) {
			throw new InvalidInputException("string does not contain any arithmetic operator (+, -, *, /, Math.): " + expression);
		}

		expression = StringUtils.deleteWhitespace(expression);
		expression = StringUtils.remove(expression, ExecutionConstants.DOLLAR_SIGN);
		expression = StringUtils.remove(expression, ExecutionConstants.COMMA);

		/*
		 * Convert any repeating operators into their single equivalent. For example, subtracting a negative 
		 * number is the same as adding that number. 
		 */
		expression = StringUtils.replace(expression, "+-", "-");
		expression = StringUtils.replace(expression, "--", "+");
		
		LOGGER.log(Level.INFO, "cleansed expression: {0}", expression);
		
		/*
		 * Use ScriptEngine.eval() to perform all arithmetic expression evaluation, so we don't have to try to
		 * do it ourselves.
		 */
		//ScriptEngine engine = new ScriptEngineManager().getEngineByName("js");
		//ScriptEngine engine = new ScriptEngineManager().getEngineByName("AppleScript");
		ScriptEngine engine = new ScriptEngineManager().getEngineByName("java");
		Object result = engine.eval(expression);
		LOGGER.log(Level.INFO, "eval result: {0}", result);
		
		if (result == null) {
			throw new ScriptException("Error attempting to evaluate expression: " + expression); 
		}
		
		BigDecimal bd =  new BigDecimal(result.toString());
		
		if (result instanceof Double) {
			bd = bd.setScale(2, RoundingMode.HALF_UP);
		}
		
		LOGGER.log(Level.INFO, "BigDecimal result: {0}", bd.toPlainString());

		return bd;
	}
}
