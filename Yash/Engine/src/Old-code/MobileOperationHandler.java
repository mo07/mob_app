package com.bcbssc.webdriver.handler;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.ContextAware;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.bcbssc.automation.util.DateUtil;
import com.bcbssc.webdriver.exception.InvalidStateException;
import com.bcbssc.webdriver.framework.ExecutionConstants;
import com.bcbssc.webdriver.framework.OperationHandler;
import com.bcbssc.webdriver.framework.TestStep;
import com.bcbssc.webdriver.util.ExecutionUtil;
import com.bcbssc.webdriver.util.WebElementFinder;

import io.appium.java_client.InteractsWithApps;
import io.appium.java_client.MultiTouchAction;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.StartsActivity;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;

public abstract class MobileOperationHandler extends SubOperationHandler {

	protected static final Logger LOGGER = Logger.getLogger(MobileOperationHandler.class.getName());

	private WebElementFinder webElementFinder;

	private static final int DEFAULT_PRE_CLICK_COORDS_WAIT_IN_MILLIS = 2000;
	private static final int DEFAULT_POST_CLICK_COORDS_WAIT_IN_MILLIS = 500;
	private static final int DEFAULT_POST_TAP_WAIT_IN_MILLIS = 500;
	private static final String SWITCH_CONTEXT_LOG_TEXT = "Switching context to: {0}";

	public MobileOperationHandler(OperationHandler operationHandler) {
		super(null, operationHandler);
		webElementFinder = new WebElementFinder(this.getOperationHandler().getDriver(), this.getExecutionScenario());
	}

	protected void clickCoordinates(TestStep testStep, WebDriver driver) {

		if (!testStep.hasAttributeValue()) {
			throw new IllegalArgumentException(MSG_PREFIX_ATTR_VAL_MISSING + testStep);
		}

		String[] parameters = testStep.getAttributeValue().split(PARAM_DELIMITER_REGEX);
		if (parameters.length != 2 || !StringUtils.isNumeric(parameters[0].trim())
				|| !StringUtils.isNumeric(parameters[1].trim())) {
			throw new IllegalArgumentException(
					"Attribute value must contain 2 comma-delimited tokens, x and y coordinates. Actual: "
							+ parameters.length);
		}

		ExecutionUtil.sleep(this.getPreClickCoordinatesWaitInMillis());

		TouchAction<?> action = this.getTouchActionInstance(driver);
		action.press(PointOption.point(Integer.parseInt(parameters[0].trim()), Integer.parseInt(parameters[1].trim())));
		action.release();
		action.perform();

		ExecutionUtil.sleep(this.getPostClickCoordinatesWaitInMillis());
	}

	protected void swipeScreen(TestStep testStep, WebDriver driver) {
		if (!testStep.hasAttributeValue()) {
			throw new IllegalArgumentException(MSG_PREFIX_ATTR_VAL_MISSING + testStep);
		}

		String[] parameters = testStep.getAttributeValue().split(PARAM_DELIMITER_REGEX);
		if (parameters.length != 4 || !StringUtils.isNumeric(parameters[0].trim())
				|| !StringUtils.isNumeric(parameters[1].trim()) || !StringUtils.isNumeric(parameters[2].trim())
				|| !StringUtils.isNumeric(parameters[3].trim())) {
			throw new IllegalArgumentException(
					"Attribute value must contain 4 comma-delimited tokens Starting x and y and ending x and y. Actual: "
							+ parameters.length);
		}

		TouchAction<?> action = this.getTouchActionInstance(driver);
		action.press(PointOption.point(Integer.parseInt(parameters[0].trim()), Integer.parseInt(parameters[1].trim())));
		action.moveTo(
				PointOption.point(Integer.parseInt(parameters[2].trim()), Integer.parseInt(parameters[3].trim())));
		action.release();
		action.perform();
	}

	protected void horizontalSwipe(TestStep testStep, WebDriver driver) {
		if (!testStep.hasAttributeValue()) {
			throw new IllegalArgumentException(MSG_PREFIX_ATTR_VAL_MISSING + testStep);
		}

		String[] parameters = testStep.getAttributeValue().split(PARAM_DELIMITER_REGEX);
		if (parameters.length != 2 || !StringUtils.isNumeric(parameters[0].trim())
				|| !StringUtils.isNumeric(parameters[1].trim())) {
			throw new IllegalArgumentException(
					"Attribute value must contain 2 comma-delimited tokens, starting x and ending x. Actual: "
							+ parameters.length);
		}

		TouchAction<?> action = this.getTouchActionInstance(driver);
		action.press(PointOption.point(Integer.parseInt(parameters[0].trim()), 1000));
		action.moveTo(PointOption.point(Integer.parseInt(parameters[1].trim()), 1000));
		action.release();
		action.perform();
	}

	protected void verticalSwipe(TestStep testStep, WebDriver driver) {
		if (!testStep.hasAttributeValue()) {
			throw new IllegalArgumentException(MSG_PREFIX_ATTR_VAL_MISSING + testStep);
		}

		String[] parameters = testStep.getAttributeValue().split(PARAM_DELIMITER_REGEX);
		if (parameters.length != 2 || !StringUtils.isNumeric(parameters[0].trim())
				|| !StringUtils.isNumeric(parameters[1].trim())) {
			throw new IllegalArgumentException(
					"Attribute value must contain 2 comma-delimited tokens, starting y and ending y. Actual: "
							+ parameters.length);
		}

		TouchAction<?> action = this.getTouchActionInstance(driver);
		action.press(PointOption.point(500, Integer.parseInt(parameters[0].trim())));
		action.moveTo(PointOption.point(500, Integer.parseInt(parameters[1].trim())));
		action.release();
		action.perform();
	}

	protected void tapElement(TestStep testStep, WebDriver driver) {
		TouchAction<?> action = this.getTouchActionInstance(driver);
		action.tap(ElementOption.element((webElementFinder.getElementForStep(testStep))));
		action.perform();

		ExecutionUtil.sleep(this.getPostTapWaitInMillis());
	}

	protected void switchAppiumContextToWebView(TestStep testStep, WebDriver driver) {
		int attemptLimit = 10;
		int attempt = 0;
		Set<String> contextNames = null;

		while (attempt < attemptLimit) {
			LOGGER.log(Level.INFO, "Attempting to switch context to WebView, attempt number: {0}", attempt + 1);

			contextNames = ((ContextAware) driver).getContextHandles();
			// LOGGER.log(Level.INFO, "Available contexts are: {0}|{1}", new
			// Object[]{contextNames,((StartsActivity)driver).getCurrentPackage()});
			LOGGER.log(Level.INFO, "Available contexts are: {0}", contextNames);

			if (testStep.hasAttributeValue()) {
				LOGGER.log(Level.INFO, "WebView context name passed in, checking available contexts for [${0}]",
						testStep.getAttributeValue());
				if (testStep.getAttributeValue().contains("LAST")) {
					ArrayList<String> contexts = new ArrayList<String>(((ContextAware) driver).getContextHandles());
					String contextName = contexts.get(contexts.size() - 1);
					LOGGER.log(Level.INFO, "Attempting to switch context to LAST WebView");
					LOGGER.log(Level.INFO, SWITCH_CONTEXT_LOG_TEXT, contextName);
					((ContextAware) driver).context(contextName);
					return;

				}
				for (String contextName : contextNames) {
					if (testStep.getAttributeValue().equals(contextName)) {
						LOGGER.log(Level.INFO, SWITCH_CONTEXT_LOG_TEXT, contextName);
						((ContextAware) driver).context(contextName);
						return;
					}
				}
				return;
			} else {
				LOGGER.log(Level.INFO,
						"WebView context name NOT passed in, checking available contexts for default WebView context name based on platform type");
				for (String contextName : contextNames) {
					if (testStep.getTestSheet().getExecutionScenario().isAndroid()) {
						if (contextName.equals(ExecutionConstants.WEBVIEW_CONTEXT_PREFIX
								+ ((StartsActivity) driver).getCurrentPackage())) {
							LOGGER.log(Level.INFO, SWITCH_CONTEXT_LOG_TEXT, contextName);
							((ContextAware) driver).context(contextName);
							return;
						}
					} else { // Assume iOS, 1st webview is app's webview
						if (contextName.startsWith(ExecutionConstants.WEBVIEW_CONTEXT_PREFIX)) {
							LOGGER.log(Level.INFO, SWITCH_CONTEXT_LOG_TEXT, contextName);
							((ContextAware) driver).context(contextName);
							return;
						}
					}
				}
			}

			LOGGER.log(Level.INFO,
					"Requested context not available currently, pausing briefly then performing a retry...");
			ExecutionUtil.sleep(500);
			attempt++;
		}

		// After multiple attempts limit reached, throw error
		LOGGER.log(Level.SEVERE, "Error finding WEBVIEW context after {0} attempts, throwing error", attemptLimit);
		throw new InvalidStateException("Afer " + attemptLimit
				+ " attempts, a valid WEBVIEW context was either not found"
				+ " in the list of available contexts, or the WEBVIEW name provided as an attribute did not match any of the available"
				+ " contexts for this session. Check the current app to ensure it is capable of a WEBVIEW context (i.e. a hybrid app)"
				+ " or verify the attribute provided is a valid WEBVIEW context that should be present.");
	}

	

	protected void switchAppiumContextToWebView_YASh(TestStep testStep, WebDriver driver) {
		int attemptLimit = 10;
		int attempt = 0;
 		Set<String> contextNames = null;

		while (attempt < attemptLimit) {
			LOGGER.log(Level.INFO, "Attempting to switch context to WebView, attempt number: {0}", attempt + 1);

			contextNames = ((ContextAware) driver).getContextHandles();
			//LOGGER.log(Level.INFO, "Available contexts are: {0}|{1}", new Object[]{contextNames,((StartsActivity)driver).getCurrentPackage()});
			LOGGER.log(Level.INFO, "Available contexts are: {0}", contextNames);

			
			//Optional<Map<String, Object>> webView;

//			webView = contextNames.stream().findFirst();
//
//			driver().context(webView.get().get("id").toString());
//		     Iterator<String> it = contextNames.iterator();
//		     while(it.hasNext()){
//		    	 String contextMap = it.next();
//		    	 LOGGER.log(Level.INFO, " context : {0}", contextMap);
//		         String[] keyVals = contextMap.split(",");
//		         for(String keyVal:keyVals)
//		         {
//		           String[] parts = keyVal.split("=",2);
//			       LOGGER.log(Level.INFO, " context : {0} = {1}", new Object[]{parts[0], parts[1]});
//		         }
//		     }

			
//				for (int i = 0; i < contextNames.size(); i++) {
//					String contextName = contextNames.get(i);
//					LOGGER.log(Level.INFO, "Attempting to switch context to LAST WebView");
//					LOGGER.log(Level.INFO, SWITCH_CONTEXT_LOG_TEXT, contextName);
//					((ContextAware) driver).context(contextName);
//					((IOSDriver<WebElement>) driver).close();
//					// Pause briefly for the animations to update
//					ExecutionUtil.sleep(500);
//				}
//			} else {
//				LOGGER.log(Level.INFO, "Cannot close the LAST WebView as there is only one webview");
//			}	
			

//		    Optional<Map<String, Object>> appiumProWebview;
//	        Set<Map<String, Object>> contextsMap =  ((ContextAware) driver).getContextHandles();
//			int numOfContexts = contextsMap.size();
//			for (int i=0; i<numOfContexts; i++) {
//				
//				LOGGER.log(Level.INFO, " context : {0}", contextsMap.stream());
//		        // get full list of contexts
//		        //Set<Map<String, Object>> contexts = <Map<String, Object>>((ContextAware) driver).getContextHandles();
//				
//		        appiumProWebview = contextsMap.stream()
//		                .filter(c -> !c.get("id").equals("NATIVE_APP"))
//		                .filter(c -> c.get("url").equals("https://appiumpro.com/"))
//		                .findFirst();
//				
//		    }
		    
		    
			
			
			
			if (testStep.hasAttributeValue()) {
				LOGGER.log(Level.INFO, "WebView context name passed in, checking available contexts for [${0}]",
						testStep.getAttributeValue());
				if (testStep.getAttributeValue().contains("LAST")) {
					ArrayList<String> contexts = new ArrayList<String>(((ContextAware) driver).getContextHandles());
					String contextName = contexts.get(contexts.size() - 1);
					LOGGER.log(Level.INFO, "Attempting to switch context to LAST WebView");
					LOGGER.log(Level.INFO, SWITCH_CONTEXT_LOG_TEXT, contextName);
					((ContextAware) driver).context(contextName);
					return;

				}
				for (String contextName : contextNames) {
					if (testStep.getAttributeValue().equals(contextName)) {
						LOGGER.log(Level.INFO, SWITCH_CONTEXT_LOG_TEXT, contextName);
						((ContextAware) driver).context(contextName);
						return;
					}
				}
				return;
			} else {
				LOGGER.log(Level.INFO,
						"WebView context name NOT passed in, checking available contexts for default WebView context name based on platform type");
				for (String contextName : contextNames) {
					if (testStep.getTestSheet().getExecutionScenario().isAndroid()) {
						if (contextName.equals(ExecutionConstants.WEBVIEW_CONTEXT_PREFIX
								+ ((StartsActivity) driver).getCurrentPackage())) {
							LOGGER.log(Level.INFO, SWITCH_CONTEXT_LOG_TEXT, contextName);
							((ContextAware) driver).context(contextName);
							return;
						}
					} else { // Assume iOS, 1st webview is app's webview
						if (contextName.startsWith(ExecutionConstants.WEBVIEW_CONTEXT_PREFIX)) {
							LOGGER.log(Level.INFO, SWITCH_CONTEXT_LOG_TEXT, contextName);
							((ContextAware) driver).context(contextName);
							return;
						}
					}
				}
			}

			LOGGER.log(Level.INFO,
					"Requested context not available currently, pausing briefly then performing a retry...");
			ExecutionUtil.sleep(500);
			attempt++;
		}

		// After multiple attempts limit reached, throw error
		LOGGER.log(Level.SEVERE, "Error finding WEBVIEW context after {0} attempts, throwing error", attemptLimit);
		throw new InvalidStateException("Afer " + attemptLimit
				+ " attempts, a valid WEBVIEW context was either not found"
				+ " in the list of available contexts, or the WEBVIEW name provided as an attribute did not match any of the available"
				+ " contexts for this session. Check the current app to ensure it is capable of a WEBVIEW context (i.e. a hybrid app)"
				+ " or verify the attribute provided is a valid WEBVIEW context that should be present.");
	}
	
	protected void switchAppiumContextToNativeView(WebDriver driver) {
		((ContextAware) driver).context("NATIVE_APP");
	}

	protected TouchAction<?> getTouchActionInstance(WebDriver driver) {
		return new TouchAction<>((PerformsTouchActions) driver);
	}

	protected int getPreClickCoordinatesWaitInMillis() {
		return DEFAULT_PRE_CLICK_COORDS_WAIT_IN_MILLIS;
	}

	protected int getPostClickCoordinatesWaitInMillis() {
		return DEFAULT_POST_CLICK_COORDS_WAIT_IN_MILLIS;
	}

	protected int getPostTapWaitInMillis() {
		return DEFAULT_POST_TAP_WAIT_IN_MILLIS;
	}

	protected void setIonicDate(TestStep testStep, WebDriver driver) {
		if (!testStep.hasAttributeValue()) {
			throw new IllegalArgumentException(MSG_PREFIX_ATTR_VAL_MISSING + testStep);
		}
		if (!testStep.hasLocator()) {
			throw new IllegalArgumentException(MSG_PREFIX_LOCATOR_VAL_MISSING + testStep);
		}

		LOGGER.log(Level.INFO, "String of inputDate: {0}", testStep.getAttributeValue());

		Date inputDate = DateUtil.convertToDate(testStep.getAttributeValue());

		SimpleDateFormat sdf;
		sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		sdf.setTimeZone(TimeZone.getDefault());
		String isoFormattedInputDate = sdf.format(inputDate);

		LOGGER.log(Level.INFO, "ISO string of inputDate: {0}", isoFormattedInputDate);

		WebElement dateTimeElement = webElementFinder.waitForElementForStep(testStep);

		LOGGER.log(Level.INFO, "Running javascript to set ion-datetime value to: {0}", isoFormattedInputDate);

		// Set value of ion-datetime element to ISO formatted input date attibute
		((JavascriptExecutor) driver).executeScript("arguments[0].setAttribute('value', arguments[1]);",
				dateTimeElement, isoFormattedInputDate);

		// Pause briefly for the animations to update
		ExecutionUtil.sleep(250);
	}

	protected void openDeveloperMode(TestStep testStep, WebDriver driver) {
		// int duration = 20; // in milliseconds
		int numOfClicks = 12;
		int X, Y;

		if (!testStep.hasLocator()) {
			throw new IllegalArgumentException(MSG_PREFIX_LOCATOR_VAL_MISSING + testStep);
		}

		if (testStep.hasAttributeValue()) {
			String attrVal = testStep.getAttributeValue();
			if (!StringUtils.isNumeric(attrVal.trim())) {
				throw new IllegalArgumentException(
						"Attribute value must be numerical value about how many times the click should be performed on the element. Actual: [ "
								+ attrVal + " ]");
			}
			if (Integer.valueOf(attrVal) < numOfClicks) {
				numOfClicks = Integer.valueOf(attrVal);
			}

		}

		WebElement titleLogo = webElementFinder.waitForElementForStep(testStep);
		X = titleLogo.getLocation().getX();
		Y = titleLogo.getLocation().getY();
		LOGGER.log(Level.INFO, "Login logo co-ordinates are: {0}|{1}", new Object[] { X, Y }); //Safari 71/128 hence need to increase it for safari

		X = X + 50;
		Y = Y + 10;

		// Y = Y + 100; // Safari

		LOGGER.log(Level.INFO, "Modified Login logo co-ordinates are: {0}|{1}", new Object[] { X, Y });

		// // Only for Apple in Web context
		// Dimension windowSize = driver.manage().window().getSize();
		//
		// int X1 = windowSize.width;
		// int Y1 = windowSize.height;
		// LOGGER.log(Level.INFO, "Window Size: {0}|{1}", new Object[]{X1, Y1 });

		TouchAction<?> action = this.getTouchActionInstance(driver);

		LOGGER.log(Level.INFO, "Number of clicks to be performed: [{0}]", new Object[] { numOfClicks });

		for (int i = 1; i <= numOfClicks; i++) {
			action.tap(PointOption.point(X, Y));
			action.waitAction(WaitOptions.waitOptions(Duration.ofMillis(100)));
			action.perform();
		}

		action.longPress(PointOption.point(X, Y));
		action.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2)));
		action.release();
		action.perform();
	}

	protected void openDeveloperMode1(TestStep testStep, WebDriver driver) {
		// int duration = 2000; // in milliseconds
		int X, Y;

		// driver.findElement(By.id("my id")).getLocation().getX();
		// driver.findElement(MobileBy.id("some_ID")).getSize().getWidth();

		if (!testStep.hasLocator()) {
			throw new IllegalArgumentException(MSG_PREFIX_LOCATOR_VAL_MISSING + testStep);
		}
		// this.switchAppiumContextToNativeView(driver);
		WebElement titleLogo = webElementFinder.waitForElementForStep(testStep);
		X = titleLogo.getLocation().getX();
		Y = titleLogo.getLocation().getY();
		LOGGER.log(Level.INFO, "Login logo co-ordinates are: {0}|{1}", new Object[] { X, Y });

		// Point location = titleLogo.getLocation();

		// Dimension windowSize = driver.manage().window().getSize();
		//
		// X = windowSize.width;
		// Y = windowSize.height;
		// LOGGER.log(Level.INFO, "Login logo co-ordinates are: {0}|{1}", new Object[]
		// {X, Y });
		//
		// JavascriptExecutor js = (JavascriptExecutor) driver;
		// int width = ((Long) js.executeScript("return window.innerWidth ||
		// document.body.clientWidth")).intValue();
		//
		// int height = ((Long) js.executeScript("return window.innerHeight ||
		// document.body.clientHeight")).intValue();
		// LOGGER.log(Level.INFO, "Modfied Login logo co-ordinates are: {0}|{1}", new
		// Object[] { width, height });
		// // Dimension dimension = new Dimension(width, height);
		//
		// X = (int) (width * 0.3);
		// Y = (int) (height * 0.3);

		X = X + 50; // X = 450; //Android
		Y = Y + 90; // Y = 420;

		LOGGER.log(Level.INFO, "Modified Login logo co-ordinates are: {0}|{1}", new Object[] { X, Y });

		/*
		 * if (!testStep.hasAttributeValue()) { throw new
		 * IllegalArgumentException(MSG_PREFIX_ATTR_VAL_MISSING + testStep); }
		 * 
		 * String[] parameters =
		 * testStep.getAttributeValue().split(PARAM_DELIMITER_REGEX); if
		 * (parameters.length != 2 || !StringUtils.isNumeric(parameters[0].trim()) ||
		 * !StringUtils.isNumeric(parameters[1].trim())) { throw new
		 * IllegalArgumentException("Attribute value must contain 2 comma-delimited tokens, x and y coordinates. Actual: "
		 * + parameters.length); }
		 */

		// MultiTouchAction action = new MultiTouchAction();
		// action.add(actionOne);
		// action.add(actionTwo);
		// action.perform();

		TouchAction<?> action = this.getTouchActionInstance(driver);

		for (int i = 1; i <= 12; i++) {
			action.tap(PointOption.point(X, Y));
			action.waitAction(WaitOptions.waitOptions(Duration.ofMillis(20)));
			action.perform();
		}

		// for (int i = 1; i <= 12; i++) {
		// action.press(PointOption.point(X, Y));
		// action.release();
		// action.perform();
		// }

		// JavascriptExecutor js = (JavascriptExecutor) driver;
		// Map<String, Object> params = new HashMap<>();
		// params.put("numberOfTaps", 10);
		// params.put("numberOfTouches",1);
		// params.put("element", titleLogo.getId());
		// js.executeScript("mobile: tapWithNumberOfTaps", params);

		// TouchAction<?> action = this.getTouchActionInstance(driver);
		action.longPress(PointOption.point(X, Y));
		action.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2)));
		action.release();
		action.perform();
	}

	protected void terminateApp(TestStep testStep, WebDriver driver) {
		if (!testStep.hasAttributeValue()) {
			throw new IllegalArgumentException(MSG_PREFIX_ATTR_VAL_MISSING + testStep);
		}

		String appToClose = testStep.getAttributeValue();
		LOGGER.log(Level.INFO, "Mobile App - input application to terminate: {0}", appToClose);

		((InteractsWithApps) driver).terminateApp(appToClose);

		ExecutionUtil.sleep(500);

		// Set<String> contextView = ((ContextAware) driver).getContextHandles();
		// LOGGER.log(Level.INFO, "Available contexts are: {0}", contextView);
		// ArrayList<String> s = new ArrayList<String>(contextView);
		// ((ContextAware) driver).context(s.get(contextView.size() - 1));
		// driver.close();
	}

	protected void closeLastDriverWindow(TestStep testStep, WebDriver driver) {
		Set<String> contextNames = null;

		contextNames = ((ContextAware) driver).getContextHandles();
		LOGGER.log(Level.INFO, "Available contexts are: {0}", contextNames);

		int numOfContexts = contextNames.size();
		LOGGER.log(Level.INFO, "Available contexts Size: {0}", numOfContexts);

		ArrayList<String> contextList = new ArrayList<String>(contextNames);

		// // Close last safari browser tab using driver.close().
		// if (numOfContexts > 2) {
		// String contextName = contextList.get(numOfContexts - 1);
		// LOGGER.log(Level.INFO, "Attempting to switch context to LAST WebView");
		// LOGGER.log(Level.INFO, SWITCH_CONTEXT_LOG_TEXT, contextName);
		// ((ContextAware) driver).context(contextName);
		// driver.close();
		// } else {
		// LOGGER.log(Level.INFO, "Cannot close the LAST WebView as there is only one
		// webview");
		// }

		// Close all safari browser tabs using driver.close().
		if (numOfContexts > 2) {
			for (int i = (numOfContexts - 1); i > 1; i--) {
				String contextName = contextList.get(i);
				LOGGER.log(Level.INFO, "Attempting to switch context to LAST WebView");
				LOGGER.log(Level.INFO, SWITCH_CONTEXT_LOG_TEXT, contextName);
				((ContextAware) driver).context(contextName);
				((IOSDriver<WebElement>) driver).close();
				// Pause briefly for the animations to update
				ExecutionUtil.sleep(500);
			}
		} else {
			LOGGER.log(Level.INFO, "Cannot close the LAST WebView as there is only one webview");
		}
	}

	protected void longPress(TestStep testStep, WebDriver driver) {

		TouchAction<?> action = this.getTouchActionInstance(driver);

		action.longPress(ElementOption.element((webElementFinder.getElementForStep(testStep))));
		action.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2)));
		action.release();
		action.perform();

	}

	protected abstract void activateApp(TestStep testStep, WebDriver driver);

	protected abstract void closeApp(TestStep testStep, WebDriver driver);

	protected abstract void launchApp(TestStep testStep, WebDriver driver);
}
