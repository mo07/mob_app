/*
 * THIS MATERIAL IS THE CONFIDENTIAL, PROPRIETARY AND TRADE SECRET PRODUCT OF
 * BLUECROSS BLUESHIELD OF SOUTH CAROLINA AND ITS SUBSIDIARIES. ANY UNAUTHORIZED 
 * USE, REPRODUCTION OR TRANSFER OF THESE MATERIALS IS STRICTLY PROHIBITED.
 * COPYRIGHT 2019 BLUECROSS BLUESHIELD OF SOUTH CAROLINA   ALL RIGHTS RESERVED.
 */
package com.bcbssc.desktop;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.bcbssc.automation.AutoConstants;
import com.bcbssc.webdriver.exception.DriverException;
import com.bcbssc.webdriver.exception.InvalidInputException;
import com.bcbssc.webdriver.exception.ProtectedModeException;
import com.bcbssc.webdriver.framework.ExecutionConstants;
import com.bcbssc.webdriver.framework.ExecutionScenario;
import com.bcbssc.webdriver.handler.ui.UIFrame;
import com.bcbssc.webdriver.mock.MockWebDriver;
import com.bcbssc.webdriver.mock.TestResultType;
import com.bcbssc.webdriver.util.ExecutionUtil;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

/**
 * A WebDriverFactory creates <code>WebDriver</code> implementations based on
 * <code>BrowserChoice</code> selections. It reads <code>TestEnvironment</code>
 * configurations to configure WebDriver instances.
 *
 */
public class WebDriverFactory implements AutoConstants {

	protected static final Logger LOGGER = Logger.getLogger(WebDriverFactory.class.getName());

	public static final String PROP_KEY_PREFIX_CHROME_DRIVER = "webdriver.chrome.driver";

	public static final String PROP_KEY_IE_DRIVER = "webdriver.ie.driver";
	public static final String PROP_KEY_IE_DRIVER_LOG_FILE = "webdriver.ie.driver.logfile";
	public static final String PROP_KEY_IE_DRIVER_LOG_LEVEL = "webdriver.ie.driver.loglevel";

	public static final String DEFAULT_VAL_IE_DRIVER_LOG_FILE = "D:" + File.separator + "InternetExplorerDriver.log";
	public static final String DEFAULT_VAL_IE_DRIVER_LOG_LEVEL = "DEBUG";

	public static final String PROP_KEY_CHROME_DRIVER_LOG_FILE = "webdriver.chrome.logfile";
	public static final String PROP_KEY_CHROME_DRIVER_LOG_VERBOSE = "webdriver.chrome.verboseLogging";

	public static final String DEFAULT_VAL_CHROME_DRIVER_LOG_FILE = "D:" + File.separator + "chromedriver.log";
	public static final String DEFAULT_VAL_CHROME_DRIVER_LOG_VERBOSE = "true";

	public static final String PROP_KEY_FIREFOX_DRIVER = "webdriver.gecko.driver";
	public static final String PROP_KEY_EDGE_DRIVER = "webdriver.edge.driver";

	public static final String PROP_KEY_ANDROID_DRIVER_URL = "driver.capabilities.androidDriverUrl";
	public static final String DEFAULT_VAL_ANDROID_DRIVER_URL = "http://127.0.0.1:4723/wd/hub";

	public static final String PROP_KEY_IOS_DRIVER_URL = "driver.capabilities.iosDriverUrl";
	public static final String DEFAULT_VAL_IOS_DRIVER_URL = "http://127.0.0.1:4723/wd/hub";

	public static final String PROP_KEY_MOBILE_FULL_RESET = "fullReset";
	public static final String PROP_KEY_MOBILE_NO_RESET = "noReset";
	public static final String PROP_KEY_MOBILE_UNICODE_KEYBOARD = "unicodeKeyboard";
	public static final String PROP_KEY_MOBILE_RESET_KEYBOARD = "resetKeyboard";
	public static final String PROP_KEY_MOBILE_NEW_CMD_TIMEOUT = "newCommandTimeout";

	public static final String ERROR_MSG_DEV_TOOLS_ACTIVE_PORT = "DevToolsActivePort file doesn't exist";
	public static final String ERROR_MSG_ATTEMPT_LOCATE_RWD = "Error attempting to locate RemoteWebDriver";

	public static final int DEFAULT_VAL_MOBILE_NEW_CMD_TIMEOUT = 160;

	public WebDriverFactory() {
		super();
	}

	public WebDriver getDriver(ExecutionScenario scenario, TestProperties testProperties)
			throws ProtectedModeException {
		return getDriver(scenario, scenario.getBrowserChoices().get(0), testProperties);
	}

	/**
	 * Builds a WebDriver instance based on the choice of browser
	 * 
	 * @param scenario
	 *            browser choice
	 * @return an instance of WebDriver that should match the browser choice.
	 *         <b>Note</b>: the WebDriver is wrapped in a threadsafe protector, so
	 *         <code>instanceof</code> checks may not work.
	 * @throws ProtectedModeException
	 * @ @throws
	 *       IllegalArgumentException if any required env or system properties are
	 *       not found.
	 */
	public WebDriver getDriver(ExecutionScenario scenario, BrowserChoice choice, TestProperties testProperties)
			throws ProtectedModeException {

		LOGGER.log(Level.INFO, "BrowserChoice: {0}", choice);

		WebDriver driver = null;

		if (choice.isRemote() && scenario == null) {
			throw new InvalidInputException("ExecutionScenario cannot be null for remote browsers");
		}

		switch (choice) {
		case FIREFOX:
			driver = getFirefox(testProperties);
			break;
		case CHROME:
			driver = getChrome(scenario, testProperties);
			break;
		case IE:
			driver = getIE(testProperties);
			break;
		case EDGE:
			driver = getEdge(testProperties);
			break;
		case FIREFOX_REMOTE:
			driver = getFirefoxRemote(scenario);
			break;
		case CHROME_REMOTE:
			driver = getChromeRemote(scenario);
			break;
		case IE_REMOTE:
			driver = getIERemote(scenario);
			break;
		case ANDROID:
			driver = getAndroid(scenario, testProperties);
			break;
		case IOS:
			driver = getIOS(scenario, testProperties);
			break;
		case MOCK:
			driver = new MockWebDriver(TestResultType.THREE_ELEMENTS_FOUND);
			break;
		default:
			throw new DriverException("Browser choice not handled: " + choice);
		}

		/*
		 * Reposition the browser window to the upper left corner of the primary window
		 * and maximize it. This will prevent any errors with mouse movements/coordinate
		 * calculations when the browser window was last closed on the secondary
		 * monitor, and therefore defaults to opening on the secondary monitor.
		 */

		// Selenium Grid will error out if chrome remote is maximized. Since maximize is
		// not needed for remote webdrivers,
		// only maximize the window when the browser is not REMOTE.
		if (driver != null && !choice.isRemote() && choice != BrowserChoice.CHROME && choice != BrowserChoice.ANDROID
				&& choice != BrowserChoice.IOS && choice != BrowserChoice.MOCK) {
			LOGGER.log(java.util.logging.Level.INFO, "Moving window to [0, 0]");
			driver.manage().window().setPosition(new Point(0, 0));
			LOGGER.log(java.util.logging.Level.INFO, "Attempting to maximize window");
			driver.manage().window().maximize();
		}

		LOGGER.log(Level.INFO, "returning: {0}", driver);

		return driver;
	}

	private WebDriver getFirefox(TestProperties testProperties) {

		try {
			String driverProp = testProperties.getProperty(PROP_KEY_FIREFOX_DRIVER);
			LOGGER.log(Level.INFO, "{0}: {1}", new Object[] { PROP_KEY_FIREFOX_DRIVER, driverProp });
			setSysPropIfUnset(PROP_KEY_FIREFOX_DRIVER, driverProp);

			FirefoxProfile profile = new FirefoxProfile();
			profile.setPreference("dom.popup_maximum", 0);
			profile.setPreference("dom.disable_beforeunload", true);
			profile.setPreference("privacy.popups.showBrowserMessage", false);
			FirefoxOptions options = new FirefoxOptions();
			options.setProfile(profile);
			return getFirefoxDriver(options);

		} catch (Exception e) {
			throw new WebDriverException("Error attempting to locate Firefox driver", e);
		}
	}

	protected WebDriver getFirefoxDriver(FirefoxOptions options) {
		return new FirefoxDriver(options);
	}

	private WebDriver getAndroid(ExecutionScenario scenario, TestProperties testProperties) {
		DesiredCapabilities capabilities = this.getMobileCapabilities(testProperties);
		capabilities.setCapability("nativeWebScreenshot", true);

		setCapabilitiesFromProperties(scenario, testProperties, capabilities);

		try {
			return getAndroidDriver(testProperties, capabilities);
		} catch (Exception e) {
			throw new WebDriverException("Error attempting to locate Android driver", e);
		}
	}

	protected WebDriver getAndroidDriver(TestProperties testProperties, DesiredCapabilities capabilities)
			throws MalformedURLException {
		return new AndroidDriver<MobileElement>(
				new URL(testProperties.getProperty(PROP_KEY_ANDROID_DRIVER_URL, DEFAULT_VAL_ANDROID_DRIVER_URL)),
				capabilities);
	}

	private WebDriver getIOS(ExecutionScenario scenario, TestProperties testProperties) {
		DesiredCapabilities capabilities = this.getMobileCapabilities(testProperties);
		capabilities.setCapability("SHOW_XCODE_LOG", false);
		// capabilities.setCapability("forceMjsonwp", true);
		// //https://github.com/appium/appium/issues/11510

		setCapabilitiesFromProperties(scenario, testProperties, capabilities);

		try {
			return this.getIOSDriver(testProperties, capabilities);
		} catch (Exception e) {
			throw new WebDriverException("Error attempting to locate IOS driver", e);
		}
	}

	protected WebDriver getIOSDriver(TestProperties testProperties, DesiredCapabilities capabilities)
			throws MalformedURLException {
		return new IOSDriver<MobileElement>(
				new URL(testProperties.getProperty(PROP_KEY_IOS_DRIVER_URL, DEFAULT_VAL_IOS_DRIVER_URL)), capabilities);
	}

	private DesiredCapabilities getMobileCapabilities(TestProperties testProperties) {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(PROP_KEY_MOBILE_FULL_RESET, false);
		capabilities.setCapability(PROP_KEY_MOBILE_NO_RESET, true);
		capabilities.setCapability(PROP_KEY_MOBILE_UNICODE_KEYBOARD, true);
		capabilities.setCapability(PROP_KEY_MOBILE_RESET_KEYBOARD, true);
		capabilities.setCapability(PROP_KEY_MOBILE_NEW_CMD_TIMEOUT, testProperties
				.getProperty(PROP_KEY_MOBILE_NEW_CMD_TIMEOUT, String.valueOf(DEFAULT_VAL_MOBILE_NEW_CMD_TIMEOUT)));
		return capabilities;
	}

	private void setCapabilitiesFromProperties(ExecutionScenario scenario, TestProperties testProperties,
			DesiredCapabilities capabilities) {
		if (scenario.hasCapabilitiesPropertiesFileName()) {
			LOGGER.log(Level.INFO, "Setting custom driver capabilities from properties file...");
			testProperties.getProperties().forEach((key, value) -> {
				String keyString = key.toString();
				String valueString = value.toString();
				if (StringUtils.startsWith(keyString, ExecutionConstants.CAPABILITIES_KEY_IDENTIFIER)) {
					String capabilityKey = StringUtils.substringAfter(keyString,
							ExecutionConstants.CAPABILITIES_KEY_IDENTIFIER);
					Object capabilityValue;
					if (StringUtils.equalsIgnoreCase(valueString, "true")
							|| StringUtils.equalsIgnoreCase(valueString, "false")) {
						capabilityValue = Boolean.valueOf(valueString);
					} else {
						capabilityValue = valueString;
					}
					LOGGER.log(Level.INFO, "Setting custom driver capability, key|value: {0}|{1}",
							new Object[] { capabilityKey, capabilityValue });
					capabilities.setCapability(capabilityKey, capabilityValue);
				}
			});
			LOGGER.log(Level.INFO, "Done setting custom driver capabilities from properties file!");
		}
	}

	private WebDriver getIE(TestProperties testProperties) throws ProtectedModeException {

		String driverProp = testProperties.getProperty(PROP_KEY_IE_DRIVER);
		LOGGER.log(Level.INFO, "{0}: {1}", new Object[] { PROP_KEY_IE_DRIVER, driverProp });

		setSysPropIfUnset(PROP_KEY_IE_DRIVER, driverProp);
		setSysPropIfUnset(PROP_KEY_IE_DRIVER_LOG_FILE,
				testProperties.getProperty(PROP_KEY_IE_DRIVER_LOG_FILE, DEFAULT_VAL_IE_DRIVER_LOG_FILE));
		setSysPropIfUnset(PROP_KEY_IE_DRIVER_LOG_LEVEL,
				testProperties.getProperty(PROP_KEY_IE_DRIVER_LOG_LEVEL, DEFAULT_VAL_IE_DRIVER_LOG_LEVEL));

		InternetExplorerOptions options = new InternetExplorerOptions();

		try {
			return this.getIEDriver(options);

			/*
			 * Handle the scenarios in which a SessionNotFoundException is thrown, due to
			 * Protected Mode settings not being the same across all zones. This is
			 * considered a temporary solution, but really the only solution if the settings
			 * are locked down.
			 * 
			 * BAG - 1/4/19 - No longer allow the process to run with flakiness enabled.
			 */
		} catch (Exception snfe) {

			if (snfe.getMessage().contains("Protected Mode")) {
				throw new ProtectedModeException(
						"Protected Mode settings are not the same for all zones. Please uncheck Enable Protected Mode for all zones.");
			} else {
				throw new WebDriverException("Error attempting to locate IE driver", snfe);
			}
		}
	}

	protected WebDriver getIEDriver(InternetExplorerOptions options) {
		return new InternetExplorerDriver(options);
	}

	private WebDriver getChrome(ExecutionScenario scenario, TestProperties testProperties) {

		setSysPropIfUnset(PROP_KEY_CHROME_DRIVER_LOG_FILE,
				testProperties.getProperty(PROP_KEY_CHROME_DRIVER_LOG_FILE, DEFAULT_VAL_CHROME_DRIVER_LOG_FILE));
		setSysPropIfUnset(PROP_KEY_CHROME_DRIVER_LOG_VERBOSE,
				testProperties.getProperty(PROP_KEY_CHROME_DRIVER_LOG_VERBOSE, DEFAULT_VAL_CHROME_DRIVER_LOG_VERBOSE));
		/*
		 * We could add more capabilities here in the future Chrome specific:
		 * 
		 * Start chrome with --disable-popup-blocking
		 * http://peter.sh/experiments/chromium-command-line-switches/#disable-popup-
		 * blocking
		 * 
		 * Pass that cmd line arg to web driver initialization:
		 * 
		 * https://seleniumhq.github.io/selenium/docs/api/java/org/openqa/selenium/
		 * chrome/ChromeOptions.html
		 * 
		 * specifically looking to add command line arguments (like disable popup)
		 * https://seleniumhq.github.io/selenium/docs/api/java/org/openqa/selenium/
		 * chrome/ChromeOptions.html#addArguments-java.util.List-
		 */
		ChromeOptions chromeOptions = new ChromeOptions();

		chromeOptions.addArguments("--no-sandbox");
		chromeOptions.addArguments("--disable-dev-shm-usage");
		chromeOptions.addArguments("--disable-popup-blocking");
		chromeOptions.addArguments("--disable-extensions");
		chromeOptions.addArguments("--start-maximized");
		// chromeOptions.addArguments("--enable-features=SameSiteByDefaultCookies,CookiesWithoutSameSiteMustBeSecure");
		// //NOSONAR
		chromeOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		chromeOptions.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

		/*
		 * BAG - 1/4/19 - These settings are necessary to prevent the 'Loading of
		 * unpacked extensions is disabled by the administrator' popup message from
		 * displaying on some pcs when running Chrome. This is configurable via the
		 * 'automation.extension.disabled' property in config.properties (true|false).
		 * 
		 * BAG - 4/23/20 - Added --headless option to prevent 'DevToolsActivePort file
		 * doesn't exist' error that occurs after setting useAutomationExtension=false,
		 * per suggestion from this thread:
		 * 
		 * https://github.com/heroku/heroku-buildpack-google-chrome/issues/46
		 */
		if (scenario != null && scenario.isAutomationExtensionDisabled()) {
			LOGGER.log(java.util.logging.Level.INFO, "Setting useAutomationExtension to false");
			chromeOptions.setExperimentalOption("useAutomationExtension", false);
			chromeOptions.addArguments("--headless");
		}

		/*
		 * BAG - 7/23/19 - Force PDFs to always open in the browser, and not download.
		 */
		Map<String, Object> preferences = new Hashtable<>();
		preferences.put("plugins.always_open_pdf_externally", false);
		chromeOptions.setExperimentalOption("prefs", preferences);

		/*
		 * BAG - 9/19/19 - Match the user's version of Chrome (pulled from the registry)
		 * against all driver version in the properties file (webdriver.chrome.driver.n,
		 * where n is a sequential value starting from 1). A message is displayed and an
		 * exception thrown if a driver is not found.
		 */
		try {
			System.setProperty(PROP_KEY_PREFIX_CHROME_DRIVER, getDriverForChromeVersion(testProperties));
			return this.getChromeDriver(chromeOptions);

		} catch (Exception e) {
			throw new WebDriverException("Error attempting to locate Chrome driver", e);
		}
	}

	protected WebDriver getChromeDriver(ChromeOptions options) {
		return new ChromeDriver(options);
	}

	protected String getDriverForChromeVersion(TestProperties testProperties) throws IOException {

		int versionIncr = 1;

		// Get the user's major version number of Chrome from the Windows registry.
		String currentChromeVersion = this.performChromeVersionRetrievalLogic();

		while (true) {

			LOGGER.log(Level.INFO, "checking property key: {0}.{1}",
					new Object[] { PROP_KEY_PREFIX_CHROME_DRIVER, versionIncr });

			String driverProp = testProperties.getProperty(PROP_KEY_PREFIX_CHROME_DRIVER + PERIOD + versionIncr);

			if (driverProp == null) {
				this.performMessageDialogDisplayLogic(currentChromeVersion);
				throw new DriverException("Unable to match Chrome driver with Chrome version: " + currentChromeVersion);
			}
			/*
			 * BAG - 8/24/20 - Prepend an underscore to prevent files that contain the
			 * version number as the last two characters of the generated timestamp, such as
			 * chromedriver_84.0.4147.30_7085103359967115183.exe, which would be erroneously
			 * returned for Chrome version 83, since the file name ends with 83.exe.
			 */
			else if (driverProp.contains(UNDERSCORE + currentChromeVersion)) {
				LOGGER.log(Level.INFO, "returning: {0}", driverProp);
				return driverProp;
			}
			versionIncr++;
		}
	}

	protected void performMessageDialogDisplayLogic(String currentChromeVersion) {
		JOptionPane.showMessageDialog(new UIFrame("Chrome Version Not Supported"),
				"Unable to find Chrome driver for your version (" + currentChromeVersion
						+ ").\n\nPLEASE NOTIFY THE AUTOMATION TEAM (RS.TA).\n\n");
	}

	protected String performChromeVersionRetrievalLogic() throws IOException {
		return ExecutionUtil.getChromeVersion();
	}

	private WebDriver getEdge(TestProperties testProperties) {

		try {
			String driverProp = testProperties.getProperty(PROP_KEY_EDGE_DRIVER);

			LOGGER.log(Level.INFO, PROP_KEY_EDGE_DRIVER + ": {0}", driverProp);
			setSysPropIfUnset(PROP_KEY_EDGE_DRIVER, driverProp);

			return this.getEdgeDriver();

		} catch (Exception e) {
			throw new WebDriverException("Error attempting to locate Edge driver", e);
		}
	}

	protected WebDriver getEdgeDriver() {
		return new EdgeDriver();
	}

	private WebDriver getIERemote(ExecutionScenario scenario) {

		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		capabilities.setCapability(CapabilityType.BROWSER_NAME, "internet explorer");
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
		capabilities.setPlatform(Platform.WINDOWS);
		capabilities.setCapability("ignoreZoomSetting", true);
		capabilities.setCapability("cssSelectorsEnabled", true);
		capabilities.setJavascriptEnabled(true);
		LOGGER.log(Level.INFO, "Launching IE RemoteWebDriver for remoteUrl: {0}", scenario.getRemoteUrl());

		try {
			return this.getRemoteWebDriver(scenario, capabilities);
		} catch (Exception e) {
			throw new WebDriverException(ERROR_MSG_ATTEMPT_LOCATE_RWD, e);
		}
	}

	private WebDriver getFirefoxRemote(ExecutionScenario scenario) {

		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		capabilities.setPlatform(Platform.LINUX);

		LOGGER.log(Level.INFO, "Launching RemoteWebDriver for remoteUrl: {0}", scenario.getRemoteUrl());
		try {
			return this.getRemoteWebDriver(scenario, capabilities);
		} catch (Exception e) {
			throw new WebDriverException(ERROR_MSG_ATTEMPT_LOCATE_RWD, e);
		}
	}

	private WebDriver getChromeRemote(ExecutionScenario scenario) {

		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		capabilities.setCapability("cssSelectorsEnabled", false);
		capabilities.setJavascriptEnabled(true);
		capabilities.setPlatform(Platform.LINUX);

		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.addArguments("--disable-popup-blocking");
		chromeOptions.addArguments("--disable-extensions");
		capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

		LOGGER.log(Level.INFO, "Launching RemoteWebDriver for remoteUrl: {0}", scenario.getRemoteUrl());
		try {
			return this.getRemoteWebDriver(scenario, capabilities);
		} catch (Exception e) {
			throw new WebDriverException(ERROR_MSG_ATTEMPT_LOCATE_RWD, e);
		}
	}

	protected WebDriver getRemoteWebDriver(ExecutionScenario scenario, DesiredCapabilities capabilities)
			throws MalformedURLException {
		return new RemoteWebDriver(new URL(scenario.getRemoteUrl()), capabilities);
	}

	private boolean setSysPropIfUnset(String key, String value) {
		boolean applied = false;
		if (!System.getProperties().containsKey(key)) {
			System.setProperty(key, value);
			applied = true;
		}

		return applied;
	}
}
