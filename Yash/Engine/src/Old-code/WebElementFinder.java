package com.bcbssc.webdriver.util;

import java.awt.Point;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.bcbssc.automation.exception.InvalidInputException;
import com.bcbssc.automation.util.StringUtil;
import com.bcbssc.cryptonite.CryptoUtil;
import com.bcbssc.desktop.WebDriverWrapper;
import com.bcbssc.webdriver.exception.DisabledElementException;
import com.bcbssc.webdriver.framework.Comparison;
import com.bcbssc.webdriver.framework.ExecutionConstants;
import com.bcbssc.webdriver.framework.ExecutionScenario;
import com.bcbssc.webdriver.framework.TestStep;
import com.bcbssc.webdriver.framework.ui.UILocator;

/*
 * This class handles the switching between browser windows for the Commercial Desktop. The basic
 * hierarchical structure of the application is:
 */
public class WebElementFinder implements ExecutionConstants {

	protected static final Logger LOGGER = Logger.getLogger(WebElementFinder.class.getName());

	public static final int DEFAULT_WAIT_IN_SEC 				= 20;
    public static final int MAX_WAIT_IN_SEC 					= 300;
    public static final int DEFAULT_SLEEP_IN_MILLIS				= 1000;

    protected static final int MAX_HIGHLIGHT_COUNT 				= 10;

    protected static final String HIGHLIGHT_COLOR_DEFAULT 		= "#2F2";
    protected static final String HIGHLIGHT_COLOR_CONDITIONAL	= "#FA0";
    protected static final String HIGHLIGHT_COLOR_WAIT 			= "#F0A";

    private static final String ATTR_VALIDATION_MSG				= "validationMessage";
    private static final String CSS_INPUT_INVALID				= "input:invalid";
    private static final String CSS_INPUT_VALID					= "input:valid";
    private static final String CSS_INPUT_OPTIONAL				= "input:optional";
    
    private static final String LOG_MSG_NO_ELEMENT_FOUND		= "----> {0}";
    private static final String LOG_MSG_TESTSTEP				= "testStep: {0}";
    private static final String LOG_MSG_ELEMENT_TEXT			= "element text: {0}";
    private static final String LOG_MSG_RETURNING				= "returning: {0}";
    
    public static final String TEXT_VALUE						= "value";
    public static final String TEXT_OUTERHTML					= "outerHTML";
    private static final String LOG_MATCH_FOUND					= "[MATCH FOUND]";
    private static final String LOG_MATCH_NOT_FOUND				= "[MATCH NOT FOUND]";
    
    private static final String MSG_NO_SUCH_ELEMENT				= "Handling NoSuchElementException. Returning false.";
    private static final String MSG_TIMEOUT						= "Handling TimeoutException. Returning false.";
    private static final String MSG_VAL_MUST_BE_NUMERIC			= "Attribute value must contain a numeric value";
    private static final String MSG_NO_ATTR_VAL_DEFINED			= "[ELEMENT FOUND] (no attribute value defined - text comparison bypassed)";

	WebElement element = null;
	WebElement secondaryElement = null;

	List<WebElement> displayedElements;

	private WebDriverWrapper webDriverWrapper;
	private ExecutionScenario scenario;

	private int waitTimeoutInSec;
	private int waitUntilAbsentInitialPresenceCheckWaitInSec = 5;
	private int millisBetweenPolls = DEFAULT_SLEEP_IN_MILLIS;

	private int id;
	
	private static int nextId = 1;
	
	public WebElementFinder(WebDriver driver) {
        this(driver, null, true);
	}

	public WebElementFinder(WebDriver driver, ExecutionScenario scenario) {
        this(driver, scenario, scenario == null || !scenario.includeHiddenElementsInSearchResults());
	}

	public WebElementFinder(WebDriver driver, ExecutionScenario scenario, boolean excludeHiddenElements) {
		this.webDriverWrapper = new WebDriverWrapper(driver, scenario, excludeHiddenElements);
		this.scenario = scenario;
		
		this.id = nextId++;
        LOGGER.log(Level.INFO, "WebElementFinder has been instantiated: {0}", this);
	}
	
	public int getId() {
		return id;
	}

	public WebDriverWrapper getWebDriverWrapper() {
		return webDriverWrapper;
	}
		
	public ExecutionScenario getScenario() {
		return scenario;
	}

	public int getWindowCount() {
    	return webDriverWrapper.getWindowCount();
	}

	public WebElement waitForElementForStep(TestStep testStep) {
		return this.waitForElementForStep(testStep, this.getDefaultWaitInSec());
	}

	public WebElement waitForElementForStep(TestStep testStep, int timeoutInSec) {
		return this.waitForElementForStep(testStep, testStep.getLocator(), testStep.getSecondaryLocator(), timeoutInSec);
	}

	public WebElement waitForElementForStep(TestStep testStep, UILocator primaryLocator, UILocator secondaryLocator) {
		return this.waitForElementForStep(testStep, primaryLocator, secondaryLocator,
				testStep.isAttributeValueNumericAndLessThan(MAX_WAIT_IN_SEC) ? testStep.getAttributeValueAsNumeric() : this.getDefaultWaitInSec());
	}

	/*
	 * Suppress the 'Conditionally executed blocks should be reachable' false positive on the return statement. SonarQube doesn't recognize
	 * that the object reference is being set in the inner method.
	 */
	@SuppressWarnings("squid:S2583")
	public WebElement waitForElementForStep(TestStep testStep, UILocator primaryLocator, UILocator secondaryLocator, int timeoutInSec) {

		if (primaryLocator == null) {
			throw new InvalidInputException("Primary locator cannot be null");
		}

		this.waitTimeoutInSec = timeoutInSec;
		
		if (timeoutInSec > 1000) {
			LOGGER.log(java.util.logging.Level.WARNING, "WARNING: Timeout value should be in seconds and not milliseconds. Please correct.");
			timeoutInSec = timeoutInSec / 1000;
		}

		LOGGER.log(Level.INFO, "timeoutInSec: {0}", timeoutInSec);

		this.element = null;
		this.secondaryElement = null;
		
		/*
		 * BAG - 12/2/19 - This bogus conditional stmt is only here to prevent SonarQube from incorrectly reporting that the
		 * condition on line 201 always evaluates to false. because SQ doesn't realize that the reference is being updated in
		 * the apply() inner method. Therefore, even though the conditional statement below will logically never evaluate to
		 * true (since every TestStep would be enabled at this point, and the timeoutInSec wouldn't be negative), it's 
		 * enough to get SQ off our back. 
		 */
		if (!testStep.isEnabled() && timeoutInSec < 0) {
			this.element = getElementForStep(testStep, primaryLocator);
		}

	   	boolean exists = (new WebDriverWait(webDriverWrapper, timeoutInSec, this.millisBetweenPolls)).until(new ExpectedCondition<Boolean>() {

	    	int counter = 1;

	    	public Boolean apply(WebDriver drive) {
	    		
	    		if (locateElementForPrimaryLocator(testStep, primaryLocator, counter)) {
	    			return true;
	    		}
	    		
                if (secondaryLocator != null && locateElementForSecondaryLocator(testStep, secondaryLocator, counter)) {
    	   			return true;
    	 		}

    	    	LOGGER.log(Level.INFO, "Is element displayed?: FALSE [attempt {0}]", counter);
                counter++;

                return false;
            }
        });

		LOGGER.log(Level.INFO, LOG_MSG_RETURNING, exists);

		return (this.element != null ? this.element : this.secondaryElement);
	}

	private boolean locateElementForPrimaryLocator(TestStep testStep, UILocator primaryLocator, int counter) {
        try {

    		LOGGER.log(Level.INFO, "Looking for element by primary locator: {0}", primaryLocator);
    		this.element = getElementForStep(testStep, primaryLocator);

            if (this.element != null) {
        		LOGGER.log(Level.INFO, "is element displayed?: TRUE [attempt {0}]", counter);
               	highlight(testStep, this.element, this.webDriverWrapper);
            	return true;
            }

        } catch (NoSuchElementException nsee) {
    		LOGGER.log(Level.INFO, LOG_MSG_NO_ELEMENT_FOUND, nsee.getMessage());
		}
        return false;
	}
	
	private boolean locateElementForSecondaryLocator(TestStep testStep, UILocator secondaryLocator,	int counter) {
        try {
    		LOGGER.log(Level.INFO, "Looking for element by secondary locator: {0}", secondaryLocator);
    		this.secondaryElement = getElementForStep(testStep, secondaryLocator);

        } catch (NoSuchElementException nsee) {
    		LOGGER.log(Level.INFO, LOG_MSG_NO_ELEMENT_FOUND, nsee.getMessage());
		}

		LOGGER.log(Level.INFO, "Is secondary element displayed?: {0} [attempt {1}]", new Object[] {String.valueOf(this.secondaryElement != null).toUpperCase(), counter});

		if (this.secondaryElement != null) {
        	highlight(testStep, this.secondaryElement, this.webDriverWrapper);
        	return true;
    	}
		return false;
	}

	public WebElement getElementForStep(TestStep testStep) {
    	return this.getElementForStep(testStep, testStep.getLocator());
    }

	public WebElement getElementForStep(TestStep testStep, UILocator uiLocator) {
		return getElementsForStep(testStep, uiLocator, null).get(0);
	}

	public WebElement getElementForStep(TestStep testStep, WebElement parentElement) {
		return getElementsForStep(testStep, testStep.getLocator(), parentElement).get(0);
	}

    public List<WebElement> getElementsForStep(TestStep testStep) {
    	return this.getElementsForStep(testStep, testStep.getLocator());
    }

    public List<WebElement> getElementsForStep(TestStep testStep, UILocator uiLocator) {
    	return this.getElementsForStep(testStep, uiLocator, null);
    }

    public List<WebElement> getElementsForStep(TestStep testStep, WebElement parentElement) {
    	return this.getElementsForStep(testStep, testStep.getLocator(), parentElement);
    }

    public List<WebElement> getElementsForStep(TestStep testStep, UILocator uiLocator, WebElement parentElement) {
		return this.getElements(testStep, uiLocator, parentElement);
    }

    private List<WebElement> getElements(TestStep testStep, UILocator uiLocator, WebElement parentElement) {

		LOGGER.log(Level.INFO, "testStep|uiLocator: {0}|{1}", new Object[]{testStep, uiLocator});
    	
    	By by = LocatorUtil.getBy(uiLocator);

    	List<WebElement> elements = this.webDriverWrapper.getElementsAcrossAllWindows(by, parentElement);

    	if (!elements.isEmpty()) {
        	return elements;

    	} else {
    		throw new NoSuchElementException("Elements could not be located: " + by);
    	}
    }

    public List<WebElement> filter(List<WebElement> elements) {
    	return this.webDriverWrapper.filter(elements);
    }

	public boolean verifyCount(TestStep testStep) {
		return this.verifyCount(testStep, Integer.parseInt(testStep.getAttributeValue()));
	}

	public boolean verifyCount(TestStep testStep, int expectedCount) {
		return this.verifyCount(testStep, null, expectedCount);
	}

	public boolean verifyCount(TestStep testStep, WebElement parentElement) {
		LOGGER.log(Level.INFO, "attributeValue: {0}", testStep.getAttributeValue());
		return this.verifyCount(testStep, parentElement, Integer.parseInt(testStep.getAttributeValue()));
	}

	public boolean verifyCount(TestStep testStep, WebElement parentElement, int expectedCount) {
		LOGGER.log(Level.INFO, "expectedCount: {0}", expectedCount);
		try {
			List<WebElement> elements = getElementsForStep(testStep, parentElement);
			LOGGER.log(Level.INFO, "comparing number of elements [{0}] to expectedCount: {1}", new Object[]{elements.size(), expectedCount});
			return elements.size() == expectedCount;
		} catch (NoSuchElementException nsee) {
			LOGGER.log(java.util.logging.Level.WARNING, MSG_NO_SUCH_ELEMENT);
			return expectedCount == 0;
		} catch (TimeoutException te) {
			LOGGER.log(java.util.logging.Level.WARNING, MSG_TIMEOUT);
		}
		return false;
	}

	public boolean isCountGreaterThan(TestStep testStep) {

		if (!testStep.isAttributeValueNumeric()) {
	    	throw new IllegalArgumentException(MSG_VAL_MUST_BE_NUMERIC);
		}
		try {
			return this.getElementsForStep(testStep).size() > testStep.getAttributeValueAsNumeric();
		} catch (NoSuchElementException nsee) {
			LOGGER.log(java.util.logging.Level.WARNING, MSG_NO_SUCH_ELEMENT);
		} catch (TimeoutException te) {
			LOGGER.log(java.util.logging.Level.WARNING, MSG_TIMEOUT);
		}
		return false;
	}

	public boolean isCountLessThan(TestStep testStep) {

		if (!testStep.isAttributeValueNumeric()) {
	    	throw new IllegalArgumentException(MSG_VAL_MUST_BE_NUMERIC);
		}
		try {
			return this.getElementsForStep(testStep).size() < testStep.getAttributeValueAsNumeric();
		} catch (NoSuchElementException nsee) {
			LOGGER.log(java.util.logging.Level.WARNING, MSG_NO_SUCH_ELEMENT);
			//return 0 < testStep.getAttributeValueAsNumeric();
		} catch (TimeoutException te) {
			LOGGER.log(java.util.logging.Level.WARNING, MSG_TIMEOUT);
		}
		return false;
	}

	public boolean isCountEqualTo(TestStep testStep) {

		if (!testStep.isAttributeValueNumeric()) {
	    	throw new IllegalArgumentException(MSG_VAL_MUST_BE_NUMERIC);
		}
		try {
			return this.getElementsForStep(testStep).size() == testStep.getAttributeValueAsNumeric();
		} catch (NoSuchElementException nsee) {
			LOGGER.log(java.util.logging.Level.WARNING, MSG_NO_SUCH_ELEMENT);
		} catch (TimeoutException te) {
			LOGGER.log(java.util.logging.Level.WARNING, MSG_TIMEOUT);
		}
		return false;
	}

	public boolean verifyHelpText(TestStep testStep) {
		final WebElement elementForStep = getElementForStep(testStep);

		if (elementForStep != null) {
			final String msg = elementForStep.getAttribute(ATTR_VALIDATION_MSG);
			if (msg != null && !msg.isEmpty()) {
				return true;
			}
		}
		return false;
	}


	public boolean waitUntilPresent(TestStep testStep) {
		LOGGER.log(java.util.logging.Level.INFO, TEXT_START);
		if (testStep.isAttributeValueNumericAndLessThan(MAX_WAIT_IN_SEC)) {
			return this.waitForElementForStep(testStep, testStep.getAttributeValueAsNumeric()) != null;
		}
		return this.waitForElementForStep(testStep) != null;
	}

	public boolean waitUntilAbsent(TestStep testStep) {
	if (testStep.isAttributeValueNumericAndLessThan(MAX_WAIT_IN_SEC)) {
		return this.waitUntilAbsent(testStep, testStep.getAttributeValueAsNumeric());
	}
	return this.waitUntilAbsent(testStep, DEFAULT_WAIT_IN_SEC);
	}

	public boolean waitUntilAbsent(TestStep testStep, int waitTimeInSec) {
		return this.waitUntilAbsent(testStep.getLocator(), waitTimeInSec);
	}

	public boolean waitUntilAbsent(UILocator locator, int waitTimeInSec) {

		LOGGER.log(Level.INFO, "waiting {0} seconds for presence of element for locator: {1}", new Object[]{this.waitUntilAbsentInitialPresenceCheckWaitInSec, locator});
		
    	try {
        	new WebDriverWait(webDriverWrapper, this.waitUntilAbsentInitialPresenceCheckWaitInSec, this.millisBetweenPolls).until(
        		ExpectedConditions.presenceOfElementLocated(LocatorUtil.getBy(locator)));
        } catch (Exception e) {
            LOGGER.log(java.util.logging.Level.INFO, "Element not found within {0} seconds. Continuing on.", this.waitUntilAbsentInitialPresenceCheckWaitInSec);
            return true;
        }

		LOGGER.log(Level.INFO, "Element is present. Waiting until element absent or timeout ({0} sec)", waitTimeInSec);
        
        long startTimeInMillis = Calendar.getInstance().getTimeInMillis();

        try {
	    	boolean retVal = (new WebDriverWait(webDriverWrapper, waitTimeInSec, this.millisBetweenPolls)).until(
	    			ExpectedConditions.invisibilityOfElementLocated(LocatorUtil.getBy(locator)));

			LOGGER.log(Level.INFO, "Element considered invisible after {0} seconds", ((Calendar.getInstance().getTimeInMillis() - startTimeInMillis) / 1000));
	    	return retVal;

        } catch (TimeoutException te) {
            LOGGER.log(java.util.logging.Level.INFO, "Timeout reached. Continuing on.");
            return true;
        }
	}

	public boolean waitUntilAbsent(WebElement element, int waitTimeInSec) {

		LOGGER.log(Level.INFO, "waiting until element absent or timeout ({0} sec)", waitTimeInSec);

        long startTimeInMillis = Calendar.getInstance().getTimeInMillis();

        try {
	    	boolean retVal = (new WebDriverWait(webDriverWrapper, waitTimeInSec, this.millisBetweenPolls)).until(
	    			ExpectedConditions.or(
	    					ExpectedConditions.stalenessOf(element),
	    					ExpectedConditions.invisibilityOf(element))
        			);
			LOGGER.log(Level.INFO, "Element considered invisible after {0} seconds", ((Calendar.getInstance().getTimeInMillis() - startTimeInMillis) / 1000));
	    	return retVal;

        } catch (TimeoutException te) {
            LOGGER.log(java.util.logging.Level.INFO, "Timeout reached. Continuing on.");
            return true;
        }
	}

	public boolean waitUntilEmpty(TestStep testStep) {

		LOGGER.log(java.util.logging.Level.INFO, TEXT_START);

	   	boolean retVal = (new WebDriverWait(webDriverWrapper, DEFAULT_WAIT_IN_SEC, this.millisBetweenPolls)).until(new ExpectedCondition<Boolean>() {

	    	WebElement element = null;

	    	public Boolean apply(WebDriver drive) {
                try {

	                if (element == null) {
		        		element = getElementForStep(testStep);
	        		}
	                else {
	            		LOGGER.log(Level.INFO, LOG_MSG_ELEMENT_TEXT, element.getText());
		            	return StringUtils.isEmpty(element.getText());
	                }
                } catch (NoSuchElementException nsee) {
        			LOGGER.log(Level.INFO, LOG_MSG_NO_ELEMENT_FOUND, nsee.getMessage());
				}
            	return false;
            }
        });

		LOGGER.log(Level.INFO, LOG_MSG_RETURNING, retVal);
		return retVal;
	}

	public boolean waitUntilContainsValue(TestStep testStep, int timeoutInSec) {
		return this.waitUntilContainsValue(testStep, testStep.getLocator(), testStep.getAttributeValue(), timeoutInSec);
	}

	public boolean waitUntilContainsValue(TestStep testStep, UILocator locator, String value, int timeoutInSec) {

		LOGGER.log(java.util.logging.Level.INFO, TEXT_START);

	   	boolean retVal = (new WebDriverWait(webDriverWrapper, timeoutInSec, this.millisBetweenPolls)).until(new ExpectedCondition<Boolean>() {

	   		List<WebElement> elements = null;

	    	public Boolean apply(WebDriver drive) {
                try {

	                /*
	                 * BAG - 10/2/19 - Changed to handle multiple elements.
	                 */
	                if (elements == null) {
	                	elements = getElementsForStep(testStep, locator);
	        		}
	                /*
	                 * BAG - 10/2/19 - Changed to wait until the element contains any value if no attribute value 
	                 * is provided.
	                 */
        			for (WebElement e : elements) {
        				LOGGER.log(Level.INFO, LOG_MSG_ELEMENT_TEXT, getText(e));
	        			if (doesElementContainText(e, value)) return true;
        			}
            	} catch (StaleElementReferenceException sere) {
			        LOGGER.log(java.util.logging.Level.INFO, "----> HANDLING STALE ELEMENT EXCEPTION");
			        elements = getElementsForStep(testStep, locator);
                } catch (Exception e) {
        			LOGGER.log(Level.INFO, LOG_MSG_NO_ELEMENT_FOUND, e.getMessage());
				}
            	return false;
            }
        });

		LOGGER.log(Level.INFO, LOG_MSG_RETURNING, retVal);
		return retVal;
	}

	private boolean doesElementContainText(WebElement element, String value) { 
		
        if (StringUtils.isNotEmpty(value)) {
        	return contains(element, value);
        } else {
        	return StringUtils.isNotEmpty(getText(element));
        }
	}

	public boolean waitUntilEnabled(TestStep testStep) {
		return this.waitUntilEnabled(testStep, DEFAULT_WAIT_IN_SEC);
	}

	public boolean waitUntilEnabled(TestStep testStep, int timeoutInSeconds) {

		LOGGER.log(java.util.logging.Level.INFO, TEXT_START);

	   	boolean retVal = (new WebDriverWait(webDriverWrapper, timeoutInSeconds, this.millisBetweenPolls)).until(new ExpectedCondition<Boolean>() {

	    	WebElement element = null;

	    	public Boolean apply(WebDriver drive) {
                try {

	                if (element == null) {
		        		element = getElementForStep(testStep);
	        		}
	                else {
	            		LOGGER.log(Level.INFO, LOG_MSG_ELEMENT_TEXT, element.getText());
	                	return isEnabled(element);
	                }
                } catch (NoSuchElementException nsee) {
        			LOGGER.log(Level.INFO, LOG_MSG_NO_ELEMENT_FOUND, nsee.getMessage());
				}
            	return false;
            }
        });

		LOGGER.log(Level.INFO, LOG_MSG_RETURNING, retVal);
		return retVal;
	}

	public boolean waitUntilDisabled(TestStep testStep) {
		return this.waitUntilDisabled(testStep, DEFAULT_WAIT_IN_SEC);
	}

	public boolean waitUntilDisabled(TestStep testStep, int timeoutInSeconds) {

		LOGGER.log(java.util.logging.Level.INFO, TEXT_START);

	   	boolean retVal = (new WebDriverWait(webDriverWrapper, timeoutInSeconds, this.millisBetweenPolls)).until(new ExpectedCondition<Boolean>() {

	    	WebElement element = null;

	    	public Boolean apply(WebDriver drive) {
                try {

	                if (element == null) {
		        		element = getElementForStep(testStep);
	        		}
	                else {
        				LOGGER.log(Level.INFO, LOG_MSG_ELEMENT_TEXT, getText(element));
	                	return !isEnabled(element);
	                }
                } catch (NoSuchElementException nsee) {
        			LOGGER.log(Level.INFO, LOG_MSG_NO_ELEMENT_FOUND, nsee.getMessage());
				}
            	return false;
            }
        });

		LOGGER.log(Level.INFO, LOG_MSG_RETURNING, retVal);
		return retVal;
	}

	public boolean waitForValue(TestStep testStep) {
		return this.waitForValue(testStep, DEFAULT_WAIT_IN_SEC);
	}

	public boolean waitForValue(TestStep testStep, int timeoutInSec) {
		return this.waitForValue(testStep, testStep.getLocator(), testStep.getAttributeValue(), timeoutInSec);
	}

	public boolean waitForValue(TestStep testStep, UILocator uiLocator, String value, int timeoutInSec) {

        LOGGER.log(java.util.logging.Level.INFO, TEXT_START);

		if (StringUtils.isEmpty(value)) {
	    	throw new IllegalArgumentException("value cannot be empty");
		}
        
    	boolean retVal = false;

    	try {
    		retVal = (new WebDriverWait(webDriverWrapper, timeoutInSec, this.millisBetweenPolls))
    				.ignoring(StaleElementReferenceException.class)
    				.until(new ExpectedCondition<Boolean>() {

 	    		List<WebElement> elements = null;
 	    		int attempt = 0;

 	    		public Boolean apply(WebDriver drive) {
	            	try {
	            		attempt++;

	            		/*
	            		 * Reload the element every 10 attempts to prevent a stale reference exception.
	            		 */
	            		if (elements == null || attempt >= 10) {
			        		elements = getElementsForStep(testStep, uiLocator);
			        		attempt = 0;
	            		}

	        			for (WebElement theElement : elements) {
	        	           	if (isLogicalMatch(theElement, value.toLowerCase(), Comparison.CONTAINS)) {
	        	               	highlight(testStep, theElement, webDriverWrapper);
	        	           		return true;
	        	           	}
	        			}

	                	return false;

	            	} catch (NoSuchElementException nsee) {
	        			LOGGER.log(Level.INFO, LOG_MSG_NO_ELEMENT_FOUND, nsee.getMessage());
	            	} catch (StaleElementReferenceException sere) {
				        LOGGER.log(java.util.logging.Level.INFO, "----> HANDLING STALE ELEMENT EXCEPTION");
				        LOGGER.log(java.util.logging.Level.SEVERE, sere.getMessage(), sere);
				        elements = getElementsForStep(testStep, uiLocator);
					}
	            	return false;
	            }
	        });
    	} catch (TimeoutException te) {
            LOGGER.log(java.util.logging.Level.WARNING, te.getMessage(), te);
    	}
        LOGGER.log(java.util.logging.Level.INFO, retVal ? LOG_MATCH_FOUND : LOG_MATCH_NOT_FOUND);
        return retVal;
	}


	public boolean containsValue(TestStep testStep) {
       	return this.containsValue(testStep, null, testStep.getAttributeValue());
	}

	public boolean containsValue(TestStep testStep, WebElement parentElement) {
       	return this.containsValue(testStep, parentElement, testStep.getAttributeValue());
	}

	public boolean containsValue(TestStep testStep, String value) {
       	return this.containsValue(testStep, null, value);
	}

	public boolean containsValue(TestStep testStep, WebElement parentElement, String value) {
       	return this.containsValue(testStep, testStep.getLocator(), parentElement, value);
	}

	public boolean containsValue(TestStep testStep, UILocator uiLocator, WebElement parentElement, String value) {

		LOGGER.log(Level.INFO, "value|uiLocator|parentElement: {0}|{1}|{2}", new Object[]{value, uiLocator, parentElement});

    	try {

			for (WebElement theElement : this.getElementsForStep(testStep, uiLocator, parentElement)) {
	           	if (this.contains(theElement, value)) {
	               	this.highlight(testStep, theElement, webDriverWrapper);
	           		return true;
	           	}
			}

		} catch (NoSuchElementException nsee) {
			LOGGER.log(Level.INFO, LOG_MSG_NO_ELEMENT_FOUND, nsee.getMessage());
	    /*
	     * BAG - 10/8/19 - Handle StaleElementReferenceException     
	     */
	    } catch (StaleElementReferenceException sere) {
	        LOGGER.log(java.util.logging.Level.WARNING, "HANDLING StaleElementReferenceException");
			for (WebElement theElement : this.getElementsForStep(testStep, uiLocator, parentElement)) {
	           	if (this.contains(theElement, value)) {
	               	this.highlight(testStep, theElement, webDriverWrapper);
	           		return true;
	           	}
			}
	    }
        LOGGER.log(java.util.logging.Level.INFO, LOG_MATCH_NOT_FOUND);
    	return false;
	}

	public boolean listContains(TestStep testStep, List<WebElement> elements, String value) {

		for (WebElement e : elements) {
           	if (this.contains(e, value)) {
               	this.highlight(testStep, e, webDriverWrapper);
           		return true;
           	}
		}
		return false;
	}

	public boolean contains(WebElement element, String value) {

		LOGGER.log(Level.INFO, "element|value: {0}|{1}", new Object[]{element, value});
		
       	if (StringUtils.isEmpty(value)) {
            LOGGER.log(java.util.logging.Level.INFO, MSG_NO_ATTR_VAL_DEFINED);
        	return true;
		}
        if (isLogicalMatch(element, value.toLowerCase(), Comparison.CONTAINS)) {
            LOGGER.log(java.util.logging.Level.INFO, LOG_MATCH_FOUND);
        	return true;
        }
        LOGGER.log(java.util.logging.Level.INFO, LOG_MATCH_NOT_FOUND);
    	return false;
	}

	public boolean childContainsValue(TestStep testStep, WebElement parentElement) {
       	return this.childContainsValue(testStep, parentElement, testStep.getAttributeValue());
	}

	public boolean childContainsValue(TestStep testStep, WebElement parentElement, String value) {

		LOGGER.log(Level.INFO, "value: {0}", value);
	
    	try {
			for (WebElement e : this.getElementsForStep(testStep, parentElement)) {

	           	if (StringUtils.isEmpty(value)) {
	                LOGGER.log(java.util.logging.Level.INFO, MSG_NO_ATTR_VAL_DEFINED);
	            	return true;
	    		}
                if (isLogicalMatch(e, value.toLowerCase(), Comparison.CONTAINS)) {
                    LOGGER.log(java.util.logging.Level.INFO, LOG_MATCH_FOUND);
    	           	this.highlight(testStep, e, webDriverWrapper);
                	return true;
                }
			}

		} catch (NoSuchElementException nsee) {
			LOGGER.log(Level.INFO, LOG_MSG_NO_ELEMENT_FOUND, nsee.getMessage());
		}
        LOGGER.log(java.util.logging.Level.INFO, LOG_MATCH_NOT_FOUND);
    	return false;
	}

	public boolean isLogicalMatch(WebElement element, String testValue, Comparison comparison) {

		LOGGER.log(Level.INFO, "comparison type: {0}", comparison);

		if (element == null) {
			LOGGER.log(Level.WARNING, "element is null. Returning false");
			return false;
		}
		if (StringUtils.isEmpty(testValue)) {
			LOGGER.log(Level.WARNING, "value is null/empty. Returning false");
			return false;
		}

		testValue = testValue.trim().toLowerCase();

		/*
		 * BAG - 11/1/19 - Restructured to return true if the element text matches, to avoid having to
		 * invoke getAttribute(), which is a slightly more expensive invocation.
		 * 
		 */
		String elementText = element.getText().trim().toLowerCase();

		if (this.isLogicalMatch(elementText, testValue, "text", comparison)) {
			return true;
		}
		
		String elementValue = element.getAttribute(TEXT_VALUE);
		
		elementValue = elementValue != null ? elementValue.trim().toLowerCase() : StringUtils.EMPTY;

		if (this.isLogicalMatch(elementValue, testValue, TEXT_VALUE, comparison)) {
			return true;
		}

		LOGGER.log(Level.INFO, "value [{0}] not found in element text|value [{1}|{2}]", new Object[]{testValue, elementText, elementValue});
		return false;
	}

	public boolean isLogicalMatch(String elementData, String testValue, String type, Comparison comparison) { 

		if (StringUtils.isNotEmpty(elementData)) {
			
			if (testValue.contains(PIPE)) {

				String[] tokens = StringUtils.split(testValue, PIPE);
				
				for (String token : tokens) {
					
                    token = token.trim();

                    if (comparison.isContains() && token.contains(elementData)) {
                        LOGGER.log(Level.INFO, "attribute token [{0}] CONTAINS element {1} [{2}]", new Object[]{token, type, elementData});
                        return true;
                    }
                    if (comparison.isStartsWith() && token.startsWith(elementData)) {
                        LOGGER.log(Level.INFO, "attribute token [{0}] STARTS WITH element {1} [{2}]", new Object[]{token, type, elementData});
                        return true;
                    }
                    if (comparison.isEquals() && token.equalsIgnoreCase(elementData)) {
                        LOGGER.log(Level.INFO, "attribute token [{0}] EQUALS element {1} [{2}]", new Object[]{token, type, elementData});
                        return true;
                    }
				}
			// check for existence of curly braces at the beginning and end, to identify a list of values
            } else if (ExecutionUtil.isListOfValues(testValue)) {
                String[] values = ExecutionUtil.getListOfValuesAsArray(testValue);
                for (String value : values) {
                    if (comparison.isContains() && elementData.contains(value)) {
                        LOGGER.log(Level.INFO, "element {0} [{1}] CONTAINS value [{2}] from list of values [{3}]", new Object[]{type, elementData, value, testValue});
                        return true;
                    } else if (comparison.isStartsWith() && elementData.startsWith(value)) {
                        LOGGER.log(Level.INFO, "element {0} [{1}] STARTS WITH value [{2}] from list of values [{3}]", new Object[]{type, elementData, value, testValue});
                        return true;
                    } else if (comparison.isEquals() && elementData.equalsIgnoreCase(value)) {
                        LOGGER.log(Level.INFO, "element {0} [{1}] EQUALS value [{2}] from list of values [{3}]", new Object[]{type, elementData, value, testValue});
                        return true;
                    }
                }
            }
            else if (comparison.isContains() && elementData.contains(testValue)) {
				LOGGER.log(Level.INFO, "element {0} [{1}] CONTAINS value [{2}]", new Object[]{type, elementData, testValue});
				return true;
			} 
			else if (comparison.isStartsWith() && elementData.startsWith(testValue)) {
				LOGGER.log(Level.INFO, "element {0} [{1}] STARTS WITH value [{2}]", new Object[]{type, elementData, testValue});
				return true;
			}
			else if (comparison.isEquals() && elementData.equalsIgnoreCase(testValue)) {
				LOGGER.log(Level.INFO, "element {0} [{1}] EQUALS value [{2}]", new Object[]{type, elementData, testValue});
				return true;
			}
		}
		return false;
	}

	public boolean startsWithValue(TestStep testStep) {
       	return this.startsWithValue(testStep, null, testStep.getAttributeValue());
	}

	public boolean startsWithValue(TestStep testStep, WebElement parentElement) {
       	return this.startsWithValue(testStep, parentElement, testStep.getAttributeValue());
	}

	public boolean startsWithValue(TestStep testStep, String value) {
       	return this.startsWithValue(testStep, null, value);
	}

	public boolean startsWithValue(TestStep testStep, WebElement parentElement, String value) {
       	return this.startsWithValue(testStep, testStep.getLocator(), parentElement, value);
	}

	public boolean startsWithValue(TestStep testStep, UILocator uiLocator, WebElement parentElement, String value) {

		LOGGER.log(Level.INFO, "value|uiLocator|parentElement: {0}|{1}|{2}", new Object[]{value, uiLocator, parentElement});

    	try {

			for (WebElement theElement : this.getElementsForStep(testStep, uiLocator, parentElement)) {
	           	if (this.startsWith(theElement, value)) {
	               	this.highlight(testStep, theElement, webDriverWrapper);
	           		return true;
	           	}
			}

		} catch (NoSuchElementException nsee) {
			LOGGER.log(Level.INFO, LOG_MSG_NO_ELEMENT_FOUND, nsee.getMessage());
	    /*
	     * BAG - 10/8/19 - Handle StaleElementReferenceException     
	     */
	    } catch (StaleElementReferenceException sere) {
	        LOGGER.log(java.util.logging.Level.WARNING, "HANDLING StaleElementReferenceException");
			for (WebElement theElement : this.getElementsForStep(testStep, uiLocator, parentElement)) {
	           	if (this.startsWith(theElement, value)) {
	               	this.highlight(testStep, theElement, webDriverWrapper);
	           		return true;
	           	}
			}
	    }
        LOGGER.log(java.util.logging.Level.INFO, LOG_MATCH_NOT_FOUND);
    	return false;
	}

	public boolean startsWith(WebElement element, String value) {

		LOGGER.log(Level.INFO, "element|value: {0}|{1}", new Object[]{element, value});
		
       	if (StringUtils.isEmpty(value)) {
            LOGGER.log(java.util.logging.Level.INFO, MSG_NO_ATTR_VAL_DEFINED);
        	return true;
		}
        if (isLogicalMatch(element, value.toLowerCase(), Comparison.STARTS_WITH)) {
            LOGGER.log(java.util.logging.Level.INFO, LOG_MATCH_FOUND);
        	return true;
        }
        LOGGER.log(java.util.logging.Level.INFO, LOG_MATCH_NOT_FOUND);
    	return false;
	}

	public String getText(WebElement element) {
		
		if (element == null) return StringUtils.EMPTY;
		
		String elementText = element.getText();

        if (StringUtils.isNotBlank(elementText)) {
    		LOGGER.log(Level.INFO, "returning element text: {0}", elementText);
	        return elementText;
		}

		String elementValue = element.getAttribute(TEXT_VALUE);

        if (StringUtils.isNotBlank(elementValue)) {
    		LOGGER.log(Level.INFO, "returning element value attribute: {0}", elementValue);
	        return elementValue;
		}
		return StringUtils.EMPTY;
	}

	public void appendText(TestStep testStep, String textToAppend) {
		LOGGER.log(Level.INFO, "text: {0}", (testStep.isAttributeKeySecured() ? "[SECURED]" : textToAppend));

		try {
			WebElement targetElement = this.waitForElementForStep(testStep, 10);
	        StringBuilder sb = new StringBuilder(targetElement.getText());

			LOGGER.log(Level.INFO, "current text: {0}", sb);
	
	    	// Check if text is encrypted. If it is, decrypt it prior to sending.
			
			/*
			 * BAG - 11/19/19 - This only handles an attributeValue containing a single encrypted data property
			 * value.  
			 */
	    	if (testStep.hasEncryptedDataProperties()) {
		    	textToAppend = CryptoUtil.decrypt(scenario.getSecurityKey(), textToAppend);
		    }

	    	// Append the string to the existing string.
	    	sb.append(textToAppend);

	    	targetElement.sendKeys(sb.toString());

		} catch (StaleElementReferenceException see) {
	        LOGGER.log(java.util.logging.Level.INFO, "*** HANDLING StaleElementReferenceException ***");
			WebElement targetElement = this.waitForElementForStep(testStep, 10);
	        StringBuilder sb = new StringBuilder(targetElement.getText());

	        // Check if text is encrypted. If it is, decrypt it prior to sending.
			/*
			 * BAG - 11/19/19 - This only handles an attributeValue containing a single encrypted data property
			 * value.  
			 */
	    	if (testStep.hasEncryptedDataProperties()) {
		    	textToAppend = CryptoUtil.decrypt(scenario.getSecurityKey(), textToAppend);
		    }

	    	// Append the string to the existing string.
	    	sb.append(textToAppend);
	    	targetElement.sendKeys(textToAppend);
		}

	}

	public void setText(TestStep testStep, String text) {
		LOGGER.log(Level.INFO, "text: {0}", (testStep.isAttributeKeySecured() ? "[SECURED]" : text));
	
		try {
			WebElement targetElement = this.waitForElementForStep(testStep, 10);

			targetElement.clear();
			
			/* Skipping the CTRL+A for Mobile
			 * Types symbols for Android and IOS Application.
			 */
			if(!scenario.isMobile()) {
		        /*
		         * Perform a CTRL+A prior to sending the keys, just in case the clear()
		         * call did not clear the field (which happens in DocFinity)
		         */
		    	targetElement.sendKeys(Keys.chord(Keys.CONTROL, "a"));
			}
	    	// Check if text is encrypted. If it is, decrypt it prior to sending.
			/*
			 * BAG - 11/19/19 - This only handles an attributeValue containing a single encrypted data property
			 * value.  
			 */
	    	if (testStep.hasEncryptedDataProperties()) {
		    	text = CryptoUtil.decrypt(scenario.getSecurityKey(), text);
		    }

	    	targetElement.sendKeys(text);
	    	
		} catch (StaleElementReferenceException see) {
	        LOGGER.log(java.util.logging.Level.INFO, "*** HANDLING StaleElementReferenceException ***");
			WebElement targetElement = this.waitForElementForStep(testStep, 10);
	        targetElement.clear();
	        
	        /* Skipping the CTRL+A for Mobile
			 * Types symbols for Android and IOS Application.
			 */
			if(!scenario.isMobile()) {
		        /*
		         * Perform a CTRL+A prior to sending the keys, just in case the clear()
		         * call did not clear the field (which happens in DocFinity)
		         */
		    	targetElement.sendKeys(Keys.chord(Keys.CONTROL, "a"));
			}
		    targetElement.sendKeys(text);
		}
	}

	public boolean isEmpty(TestStep testStep) {

		LOGGER.log(java.util.logging.Level.INFO, TEXT_START);

		boolean empty = false;

        try {
        	WebElement e = getElementForStep(testStep);
           	this.highlight(testStep, e, webDriverWrapper);

           	empty = StringUtils.isEmpty(e.getAttribute(TEXT_VALUE));

		} catch (NoSuchElementException nsee) {
			LOGGER.log(Level.INFO, LOG_MSG_NO_ELEMENT_FOUND, nsee.getMessage());
		}

		LOGGER.log(java.util.logging.Level.INFO, LOG_MSG_RETURNING, empty);
        return empty;
	}

	public boolean isPresent(TestStep testStep) {
        return this.isPresent(testStep, true);
	}

	public boolean isPresent(TestStep testStep, boolean highlight) {
        return this.isPresent(testStep, testStep.getLocator(), highlight);
	}

	public boolean isPresent(TestStep testStep, WebElement parentElement) {
        return this.isPresent(testStep, testStep.getLocator(), parentElement);
	}

	public boolean isPresent(TestStep testStep, UILocator uiLocator) {
        return this.isPresent(testStep, uiLocator, null);
	}

	public boolean isPresent(TestStep testStep, UILocator uiLocator, WebElement parentElement) {
        return this.isPresent(testStep, uiLocator, parentElement, true);
	}

	public boolean isPresent(TestStep testStep, UILocator uiLocator, boolean highlight) {
        return this.isPresent(testStep, uiLocator, null, highlight);
	}

	public boolean isPresent(TestStep testStep, UILocator uiLocator, WebElement parentElement, boolean highlight) {
        return testStep.isAttributeValueNumericAndLessThan(MAX_WAIT_IN_SEC) ?
        	this.isPresent(testStep, uiLocator, parentElement, highlight, testStep.getAttributeValueAsNumeric()) :
            this.isPresent(testStep, uiLocator, parentElement, highlight, MAX_HIGHLIGHT_COUNT);
	}

	public boolean isPresent(TestStep testStep, UILocator uiLocator, WebElement parentElement, boolean highlight, int maxHighlightCount) {
		LOGGER.log(Level.INFO, "maxHighlightCount: {0}", maxHighlightCount);
    
        List<WebElement> elements = null;

        try {
        	elements = getElementsForStep(testStep, uiLocator, parentElement);

        	if (highlight) {

        		LOGGER.log(Level.INFO, "highlighting [{0}] elements", (elements.size() > maxHighlightCount ? elements.size() : maxHighlightCount));
    
            	for (int i = 0; i < maxHighlightCount && i < elements.size(); i++) {
                   	this.highlight(testStep, elements.get(i), webDriverWrapper);
            	}
        	}

        } catch (NoSuchElementException nsee) {
			LOGGER.log(Level.INFO, LOG_MSG_NO_ELEMENT_FOUND, nsee.getMessage());
		}
		LOGGER.log(Level.INFO, LOG_MSG_RETURNING, (elements != null && !elements.isEmpty()));
        return (elements != null && !elements.isEmpty());
	}

	public boolean isOptional(TestStep testStep) {
		return containsCssSelector(testStep, CSS_INPUT_OPTIONAL);
	}

	public boolean isInvalid(TestStep testStep) {
		return containsCssSelector(testStep, CSS_INPUT_INVALID);
	}

	public boolean isValid(TestStep testStep) {
		return containsCssSelector(testStep, CSS_INPUT_VALID);
	}

	private boolean containsCssSelector(TestStep testStep, String selector) {
		try {
			final WebElement field = getElementForStep(testStep);
			final List<WebElement> elements = webDriverWrapper.findElements(By.cssSelector(selector));
			if (field != null && elements != null) {
				for (WebElement we : elements) {
					if (we.getAttribute(TEXT_ID) != null && we.getAttribute(TEXT_ID).equals(field.getAttribute(TEXT_ID))) {
						return true;
					}
				}
			}
		} catch (NoSuchElementException nsee) {
			LOGGER.log(Level.INFO, LOG_MSG_NO_ELEMENT_FOUND, nsee.getMessage());
		}
		return false;
	}

	public boolean isRequired(TestStep testStep) {
        try {
        	final WebElement e = getElementForStep(testStep);

        	if (e != null) {
        		return Boolean.parseBoolean(e.getAttribute("required"));
        	}
        } catch (NoSuchElementException nsee) {
			LOGGER.log(Level.INFO, LOG_MSG_NO_ELEMENT_FOUND, nsee.getMessage());
		}
		return false;
	}

	public boolean isAbsent(TestStep testStep) {
        return this.isAbsent(testStep, null);
	}

	public boolean isAbsent(TestStep testStep, WebElement parentElement) {

		boolean absent = false;

        try {
        	getElementForStep(testStep, parentElement);
		} catch (NoSuchElementException nsee) {
			absent = true;
		}
		LOGGER.log(Level.INFO, LOG_MSG_RETURNING, absent);
        return absent;
	}

	public boolean isEnabled(TestStep testStep) {
        LOGGER.log(java.util.logging.Level.INFO, TEXT_START);

        boolean enabled = false;

        try {
        	WebElement e = getElementForStep(testStep);

    		this.highlight(testStep, e, webDriverWrapper);
            enabled = this.isEnabled(e);

		} catch (NoSuchElementException nsee) {
			LOGGER.log(Level.INFO, LOG_MSG_NO_ELEMENT_FOUND, nsee.getMessage());
		}
		LOGGER.log(Level.INFO, LOG_MSG_RETURNING, enabled);
        return enabled;
	}

	public boolean isSelected(TestStep testStep) {
		LOGGER.log(java.util.logging.Level.INFO, TEXT_START);

		try {
			WebElement theElement = getElementForStep(testStep);

			this.highlight(testStep, theElement, webDriverWrapper);
			return theElement.isSelected();

		} catch (NoSuchElementException nsee) {
			LOGGER.log(Level.INFO, LOG_MSG_NO_ELEMENT_FOUND, nsee.getMessage());
		}
		return false;
	}

	public boolean isUnselected(TestStep testStep) {
		return !this.isSelected(testStep);
	}

	public boolean isDisabled(TestStep testStep) {
        boolean disabled = !this.isEnabled(testStep);
		LOGGER.log(Level.INFO, LOG_MSG_RETURNING, disabled);
        return disabled;
	}

	public boolean isEnabled(WebElement element) {
        LOGGER.log(java.util.logging.Level.INFO, TEXT_START);

        boolean enabled = false;

    	if (element != null) { 
    		
    		/*
    		 * BAG - 12/31/19 - Neither Android nor iOS supports the 'disabled' or 'aria-disabled' attributes.
    		 */
    		if (this.scenario.isMobile()) {
    			return element.isEnabled();
    		}
    		/*
    		 * BAG - 6/3/19 - getAttribute("disabled") is deemed to be a 'boolean' attribute, and will return either
    		 * true or null. Therefore, performing an isEmpty() against the value will determine if the element is
    		 * disabled or not (since null would indicate that it's enabled). However, getAttribute("aria-disabled")
    		 * may return null, true, or false, so we handle it differently.
    		 */
           	String disabledAttrVal = element.getAttribute("disabled");
           	String ariaDisabledAttrVal = element.getAttribute("aria-disabled");
    		LOGGER.log(Level.INFO, "disabled|aria-disabled: {0}|{1}", new Object[]{disabledAttrVal, ariaDisabledAttrVal});

            boolean ariaDisabled = StringUtils.isNotEmpty(ariaDisabledAttrVal) && Boolean.parseBoolean(ariaDisabledAttrVal);

            enabled = element.isEnabled() && StringUtils.isEmpty(disabledAttrVal) && !ariaDisabled;
    	}
        return enabled;
	}

    public boolean doesAmountMatch(TestStep testStep) {
        LOGGER.log(java.util.logging.Level.INFO, TEXT_START);

        try {
            List<WebElement> elements = getElementsForStep(testStep);

            BigDecimal totalAmount = new BigDecimal("0.0", StringUtil.MATH_CONTEXT);

            for (WebElement e : elements) {
                BigDecimal amount = StringUtil.getDigits(this.getText(e));
                LOGGER.log(Level.INFO, "amount: {0}", amount);
                totalAmount = totalAmount.add(amount);
            }

            BigDecimal attributeValue = StringUtil.getDigits(testStep.getAttributeValue());
            boolean returnVal = (totalAmount.compareTo(attributeValue) == 0);
            LOGGER.log(Level.INFO, "comparing total amount from elements [{0}] to attribute value: {1}", new Object[]{totalAmount, attributeValue});
            LOGGER.log(Level.INFO, "totalAmount: {0}", totalAmount);
            LOGGER.log(Level.INFO, "attributeValue: {0}", attributeValue);
            LOGGER.log(Level.INFO, "returning [{0}]", returnVal);
            return returnVal;

        } catch (NoSuchElementException nsee) {
            LOGGER.log(java.util.logging.Level.INFO, LOG_MSG_NO_ELEMENT_FOUND, nsee.getMessage());
        }
        return false;
    }

    public WebElement findByIndex(TestStep testStep, int timeoutInSec) {

		LOGGER.log(Level.INFO, LOG_MSG_TESTSTEP, testStep);

        // Use getElement() simply to establish the correct window handle
        this.waitForElementForStep(testStep, timeoutInSec);

        this.element = this.getElementsForStep(testStep).get(Integer.parseInt(testStep.getAttributeValue()));

        return this.element;
    }

    public WebElement findByPartialValue(TestStep testStep) {
        return this.findByPartialValue(testStep, testStep.getAttributeValue());
    }

    public WebElement findByPartialValue(TestStep testStep, String value) {

		LOGGER.log(Level.INFO, "testStep|value: {0}|{1}", new Object[]{testStep, value});

        // Use waitForElementForStep() simply to establish the correct window handle
        this.waitForElementForStep(testStep, 10);

    	List<WebElement> results = this.getElementsForStep(testStep);

        for (WebElement result : results) {
    		LOGGER.log(Level.INFO, "checking for existence of value [{0}] in [{1}]", new Object[]{value, result.getText()});

           	this.highlight(testStep, result, webDriverWrapper);

            if (result.getText().toLowerCase().contains(value.toLowerCase())) {
        		LOGGER.log(Level.INFO, "returning WebElement for: {0}", result.getText());
            	this.element = result;
            	return result;
            }
        }

        throw new NoSuchElementException("WebElement not found for TestStep: " + testStep);
    }

    public WebElement findByValue(TestStep testStep) {
        return this.findByValue(testStep, testStep.getAttributeValue());
    }

    public WebElement findByValue(TestStep testStep, String searchValue) {

		LOGGER.log(Level.INFO, LOG_MSG_TESTSTEP, testStep);

        // Use getElement() simply to establish the correct window handle
        this.waitForElementForStep(testStep, 10);

    	List<WebElement> results = this.getElementsForStep(testStep);

        for (WebElement result : results) {
	    	LOGGER.log(Level.INFO, "comparing [{0}] against [{1}]", new Object[]{searchValue, result.getText()});
       
           	this.highlight(testStep, result, webDriverWrapper);

            if (result.getText().equalsIgnoreCase(searchValue)) {
        		LOGGER.log(Level.INFO, "returning WebElement for: {0}", result.getText());
                this.element = result;
            	return result;
            }
        }

        throw new NoSuchElementException("WebElement not found for TestStep: " + testStep);
    }

    public WebElement getLastElement(TestStep testStep, int timeoutInSec) {

		LOGGER.log(Level.INFO, LOG_MSG_TESTSTEP, testStep);

        // Use getElement() simply to establish the correct window handle
        this.waitForElementForStep(testStep, timeoutInSec);

        List<WebElement> foundElements = this.getElementsForStep(testStep);
        this.element = foundElements.get(foundElements.size() - 1);

		LOGGER.log(Level.INFO, "returning element at index: {0}", (foundElements.size() - 1));
        return this.element;
    }

	public void clickElement(TestStep testStep, WebElement theElement) {

		LOGGER.log(Level.INFO, "CLICKING element for: {0}", testStep);
    	
    	try {
    		if (!isEnabled(theElement)) {
    			throw new DisabledElementException("Element is disabled: " + theElement);
    		}
    		theElement.click();
    	} catch (ElementNotVisibleException enve) {
        	LOGGER.log(java.util.logging.Level.INFO, "Handling ElementNotVisibleException. Attempting to scroll element into view and click it.");
        	webDriverWrapper.executeScript("arguments[0].click();", theElement);
    	}
	}

	public void clickChildElement(TestStep testStep, WebElement parentElement) {

		WebElement theElement = this.getElementForStep(testStep, parentElement);

    	try {
    		if (!isEnabled(theElement)) {
    			throw new DisabledElementException("ELement is disabled: " + theElement);
    		}
    		theElement.click();
    	} catch (ElementNotVisibleException enve) {
        	LOGGER.log(java.util.logging.Level.INFO, "Handling ElementNotVisibleException. Attempting to scroll element into view and click it.");
        	webDriverWrapper.executeScript("arguments[0].click();", theElement);
    	}
	}

    /*
     * 2:	<TR>
	 *			<TD id=portletTitle_lm:oid:Z7_Q20002002O6U90IEU07D2L1852@oid:Z6_JNILHNHLOLQ7F0IEL0EH0J20H5>
	 * 1:			<DIV id=dndHandlelm:oid:Z7_Q20002002O6U90IEU07D2L1852@oid:Z6_JNILHNHLOLQ7F0IEL0EH0J20H5 dndId="dndSrclm:oid:Z7_Q20002002O6U90IEU07D2L1852@oid:Z6_JNILHNHLOLQ7F0IEL0EH0J20H5" dndHandle="false">Contact History</DIV>
	 *			</TD>
	 *			<TD id=portletActions_lm:oid:Z7_Q20002002O6U90IEU07D2L1852@oid:Z6_JNILHNHLOLQ7F0IEL0EH0J20H5 class=wpsPortletIcons style="VISIBILITY: visible">
	 * 3:			<A id=refresh_lm:oid:Z7_Q20002002O6U90IEU07D2L1852@oid:Z6_JNILHNHLOLQ7F0IEL0EH0J20H5_anchor href="javascript:void(0);">
	 *					<IMG id=refresh_lm:oid:Z7_Q20002002O6U90IEU07D2L1852@oid:Z6_JNILHNHLOLQ7F0IEL0EH0J20H5 title=Refresh class=wpsPortletIcons alt=Refresh src="/BCBSPortalWeb2/themes/html/BCBSPortalWeb2/colors/default/refresh.gif" width=20 height=12>
	 *				</A>
	 *			</TD>
	 *		</TR>
	 */
    public void refreshFrame(TestStep testStep) {
    	// Step 1: Find the initial element (which should be the DIV node in the above structure)
    	WebElement headerElement = this.waitForElementForStep(testStep, 10);
    	// Step 2: Find the grandparent of that element (which should be the TR node in the above structure)
    	WebElement tableRowElement = headerElement.findElement(By.xpath("../.."));
    	// Step 3: Find the <A> element within the <TR> element
    	WebElement refreshLinkElement = tableRowElement.findElement(By.xpath(".//a[contains(@id,'refresh')]"));

    	List<WebElement> elements = tableRowElement.findElements(By.xpath(".//a[contains(@id,'refresh')]"));
		LOGGER.log(Level.INFO, "elements found: {0}", elements.size());

		LOGGER.log(Level.INFO, "clicking refreshLinkElement: {0}", refreshLinkElement.getAttribute(TEXT_OUTERHTML));
    	refreshLinkElement.click();
    }

    /*
     *	2	<div class="app-tab-panel-main col">
	 *   		<div class="app-tab-panel-controls">
	 *  3 			<a href="javascript:void(0)" class="fa fa-refresh">
	 *   				<span class="sr-only">Refresh</span>
	 *   			</a>
	 *   		</div>
	 *	    	<div class="card app-tab-panel-tabs">
	 *	        	<div id="__BVID__52" class="tabs">
	 *	            	<div class="card-header">
	 *	                	<ul role="tablist" tabindex="0" id="__BVID__52__BV_tab_controls_" class="nav nav-tabs card-header-tabs">
	 *	                    	<li role="presentation" class="nav-item">
	 *	1							<a role="tab" tabindex="-1" href="#" id="__BVID__53___BV_tab_button__" aria-selected="true" aria-setsize="1" aria-posinset="1" aria-controls="__BVID__52__BV_tab_container_" class="nav-link active">
	 *									Subscriber Summary
	 *								</a>
	 *							</li>
	 *	                	</ul>
	 *	            	</div>
	 *	            	<div id="__BVID__52__BV_tab_container_" class="tab-content">
	 *	                	<div role="tabpanel" id="__BVID__53" aria-hidden="false" aria-expanded="true" aria-lablelledby="__BVID__53___BV_tab_button__" class="tab-pane card-body show fade active" style="">
	 *	                    	<section aria-label="Subscriber Summary" class="app-section-subscriber-summary app-section-padded" style="">
	 *	                        	&nbsp
	 *	                    	</section>
	 *	                	</div>
	 *	            	</div>
	 *	        	</div>
	 *	    	</div>
	 *		</div>
	 */
    public void refreshFrameRD3910(TestStep testStep) {
    	// Step 1: Find the initial element (which should be the DIV node in the above structure)
    	WebElement headerElement = this.waitForElementForStep(testStep, 10);
    	// Step 2: Move up six levels from that element (which should be the topmost DIV node in the above structure)
    	WebElement tableRowElement = headerElement.findElement(By.xpath("../../../../../.."));
    	// Step 3: Find the <A> element within the <TR> element
    	WebElement refreshLinkElement = tableRowElement.findElement(By.xpath(".//a[contains(@class,'fa-refresh')]"));

		LOGGER.log(Level.INFO, "clicking refreshLinkElement: {0}", refreshLinkElement.getAttribute(TEXT_OUTERHTML));
    	refreshLinkElement.click();
    }

    public void enforceScreenWidth(TestStep testStep) {

    	if (testStep.hasAttributeValue()) {
        	Point screenSize = ExecutionUtil.getScreenSize(webDriverWrapper, scenario);

    		if (!testStep.isAttributeValueNumeric()) {
    	    	throw new IllegalArgumentException(MSG_VAL_MUST_BE_NUMERIC);
    		}

    		LOGGER.log(Level.INFO, "desired width: {0}", testStep.getAttributeValue());

    		if (testStep.getAttributeValueAsNumeric() < screenSize.x) {
        		ExecutionUtil.resizeWindow(testStep.getAttributeValueAsNumeric(), screenSize.y, webDriverWrapper);
    		}
    	}
    }

    public void enforceScreenHeight(TestStep testStep) {

    	if (testStep.hasAttributeValue()) {
        	Point screenSize = ExecutionUtil.getScreenSize(webDriverWrapper, scenario);

    		if (!testStep.isAttributeValueNumeric()) {
    	    	throw new IllegalArgumentException(MSG_VAL_MUST_BE_NUMERIC);
    		}

    		LOGGER.log(Level.INFO, "desired height: {0}", testStep.getAttributeValue());

    		if (testStep.getAttributeValueAsNumeric() < screenSize.y) {
        		ExecutionUtil.resizeWindow(screenSize.x, testStep.getAttributeValueAsNumeric(), webDriverWrapper);
    		}
    	}
    }

	public void mouseOver(WebDriver webDriverWrapper, WebElement element) {
		LOGGER.log(Level.INFO, "element outerHTML: {0}", element.getAttribute(TEXT_OUTERHTML));
		Actions builder = new Actions(webDriverWrapper);
		builder.moveToElement(element).build().perform();
	}

	public void highlight(TestStep testStep, WebElement element, WebDriver driver) {

		String highlightColor = HIGHLIGHT_COLOR_DEFAULT;

		if (testStep.getOperation().isConditionalOperation()) {
			highlightColor = HIGHLIGHT_COLOR_CONDITIONAL;
		}
		if (testStep.getOperation().isWaitOperation()) {
			highlightColor = HIGHLIGHT_COLOR_WAIT;
		}

		this.highlight(element, highlightColor, driver);
	}

	public void highlight(WebElement element, String highlightColor, WebDriver driver) {

        if (this.scenario == null) {
        	return;
        }

  		/*
		 * BAG - 8/14/19 - IE 11 does not support CSS variables, so don't bother trying to highlight any elements in IE.
		 */
        if (this.scenario.getBrowserChoices().get(0).isIE()) {
        	return;
        }

		try {

			if (element != null && scenario.isHighlightElement()) {
		        JavascriptExecutor js = (JavascriptExecutor) driver;

		        String script = "arguments[0].style.setProperty('background', '" + highlightColor + "', 'important');";
				LOGGER.log(Level.INFO, "executing script: {0}", script);

		    	js.executeScript(script, element);
		        ExecutionUtil.sleep(100);
		        js.executeScript("arguments[0].style.removeProperty('background');", element);
			}

		/*
		 * BAG - 4/10/19 - Handle StaleElementReferenceException on highlight, which may occur if the element gets refreshed
		 * following a click (Operation=CLICK) event.
		 */
		} catch (StaleElementReferenceException|JavascriptException sere) {
	        LOGGER.log(java.util.logging.Level.INFO, "HANDLING [{0}] ON HIGHLIGHT. Continuing on.", sere.getClass().getSimpleName());
	        LOGGER.log(java.util.logging.Level.SEVERE, sere.getMessage(), sere);
		}
	}

	public void setWindowSwitchingEnabled(boolean b) {
		this.webDriverWrapper.setWindowSwitchingEnabled(b);
	}

	public boolean isWindowSwitchingEnabled() {
		return this.webDriverWrapper.isWindowSwitchingEnabled();
	}

	/**
	 * Recursive process that switches to the frame for the given name.
	 *
	 * @param attributeValue
	 * @return <code>true</code> if the frame was found and switched to, <code>false</code> otherwise.
	 */
	public boolean switchToFrame(String attributeValue) {
		return webDriverWrapper.switchToFrame(attributeValue);
	}

	public int getWaitTimeoutInSec() {
		return waitTimeoutInSec;
	}
	
	public void setWaitUntilAbsentInitialPresenceCheckWaitInSec(int waitInSec) {
		this.waitUntilAbsentInitialPresenceCheckWaitInSec = waitInSec;
	}

	public void setMillisBetweenPolls(int millisBetweenPolls) {
		this.millisBetweenPolls = millisBetweenPolls;
	}

    public boolean isExcludeHiddenElements() {
        return this.webDriverWrapper.isExcludeHiddenElements();
    }

	public void setExcludeHiddenElements(boolean exclude) {
		this.webDriverWrapper.setExcludeHiddenElements(exclude);
    }

	// For overriding via jUnits 
	protected int getDefaultWaitInSec() {
		return DEFAULT_WAIT_IN_SEC;
    }
    
    public String getPseudoContent(WebElement targetElement, String pseudoElement) {
        // Use javascript to pull the content value of the desired WebElement's pseudo-element i.e. ::before

        LOGGER.log(Level.INFO, "retrieving content for pseudo element [{0}] on element [tag: {1}, text: {2}]", new Object[] {pseudoElement, targetElement.getTagName(), targetElement.getText()} );
        Object pseudoContent = webDriverWrapper.executeScript(
            "return window.getComputedStyle(arguments[0], arguments[1]).getPropertyValue('content');",
                targetElement, pseudoElement);

        String pseudoContentString = pseudoContent == null ? StringUtils.EMPTY : (String) pseudoContent;

        pseudoContentString = StringUtils.replace(pseudoContentString, "\"", StringUtils.EMPTY).trim();

        LOGGER.log(Level.INFO, "content retrieved for pseudo element is [{0}] on element [tag: {1}, text: {2}]", new Object[] {pseudoContentString, targetElement.getTagName(), targetElement.getText()} );
        return pseudoContentString;
    }

	public String toString() {
		return "[WebElementFinder:" + this.getId() + "]";
	}

}

