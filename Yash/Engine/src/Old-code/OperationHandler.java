/*
 * THIS MATERIAL IS THE CONFIDENTIAL, PROPRIETARY AND TRADE SECRET PRODUCT OF
 * BLUECROSS BLUESHIELD OF SOUTH CAROLINA AND ITS SUBSIDIARIES. ANY UNAUTHORIZED 
 * USE, REPRODUCTION OR TRANSFER OF THESE MATERIALS IS STRICTLY PROHIBITED.
 * COPYRIGHT 2019 BLUECROSS BLUESHIELD OF SOUTH CAROLINA   ALL RIGHTS RESERVED.
 */
package com.bcbssc.webdriver.framework;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ISelect;
import org.openqa.selenium.support.ui.Select;

import com.bcbssc.automation.util.DateUtil;
import com.bcbssc.cryptonite.CryptoUtil;
import com.bcbssc.desktop.WebDriverManager;
import com.bcbssc.flynet.FlynetConstants;
import com.bcbssc.jsm.JSMOperationHandler;
import com.bcbssc.webdriver.excel.TestData;
import com.bcbssc.webdriver.exception.HandlerException;
import com.bcbssc.webdriver.exception.ScriptException;
import com.bcbssc.webdriver.exception.WindowNotFoundException;
import com.bcbssc.webdriver.framework.parser.AttributeTokens;
import com.bcbssc.webdriver.framework.ui.UILocators;
import com.bcbssc.webdriver.handler.AndroidOperationHandler;
import com.bcbssc.webdriver.handler.FlynetOperationHandler;
import com.bcbssc.webdriver.handler.IOSOperationHandler;
import com.bcbssc.webdriver.handler.KeyboardOperationHandler;
import com.bcbssc.webdriver.handler.LanFaxOperationHandler;
import com.bcbssc.webdriver.handler.MouseOperationHandler;
import com.bcbssc.webdriver.handler.SAPOperationHandler;
import com.bcbssc.webdriver.handler.ui.UIFrame;
import com.bcbssc.webdriver.util.CacheUtil;
import com.bcbssc.webdriver.util.CaptureUtil;
import com.bcbssc.webdriver.util.CustomFunctionUtil;
import com.bcbssc.webdriver.util.ExecutionUtil;
import com.bcbssc.webdriver.util.LocatorUtil;
import com.bcbssc.webdriver.util.SimpleFunctionResults;
import com.bcbssc.webdriver.util.WebElementFinder;

@SuppressWarnings("squid:S1141")
public class OperationHandler implements ExecutionConstants {

	protected static final Logger LOGGER = Logger.getLogger(OperationHandler.class.getName());

    private static final String TEXT_HANDLING_SEE_EXCEPTION		= "----> HANDLING STALE ELEMENT EXCEPTION";
    private static final String TEXT_CACHED_VALUE_INVALID_DATE	= "cached value is not in a valid date format: ";
	public static final String DEFAULT_CACHE_KEY 				= 	"default-cache-key";
	private static final String TEXT_PREFIX_KEY					= "key [";
	private static final String TEXT_NOT_FOUND_IN_CACHE			= "] not found in cache";
	private static final String TEXT_ATTR_VALUE_CANNOT_BE_BLANK	= "attributeValue cannot be blank";
	private static final String TEXT_PREFIX_MISSING_ATTR_VAL    = "Attribute value missing for test step: ";

	private int id;
	
	private ExecutionCache cache;
	private ExecutionScenario scenario;
    private WebDriver driver;
    private UILocators uiLocators;
    private TestData testData;

    private WebElementFinder webElementFinder;

    private SAPOperationHandler sapOperationHandler;
    private KeyboardOperationHandler keyboardOperationHandler;
    private MouseOperationHandler mouseOperationHandler;
    private LanFaxOperationHandler lanFaxOperationHandler;
    private JSMOperationHandler jsmOperationHandler;
    private AndroidOperationHandler androidOperationHandler;
    private IOSOperationHandler iosOperationHandler;
    private FlynetOperationHandler flynetOperationHandler;

    private String originalWindowHandle;
    private String lastOpenedWindowHandle;

    private static String haltedBy;
    private static boolean halted;
    private static boolean executionHaltedAfterFirstOperation;
    
    private int overrideWaitTimeoutInSeconds = 10;
    
    private static int nextId = 1;

    public OperationHandler(ExecutionCache cache, ExecutionScenario scenario, WebDriver driver, UILocators uiLocators, TestData testData) throws HandlerException {
    	this.cache = cache;
    	this.scenario = scenario;
    	this.driver = driver;
        this.uiLocators = uiLocators;
        this.testData = testData;
        this.id = nextId++;

    	this.webElementFinder = this.getWebElementFinderInstance(driver, scenario);

        /*
         * Only instantiate the 'sub' operation handler classes if we're not running remotely (ie. headless, and against the
         * Selenium Grid). Otherwise, we'll get a [java.awt.AWTException: headless environment] error.
         */
        if (scenario.isRemoteBrowser()) {
        	LOGGER.log(java.util.logging.Level.INFO, "SKIPPING INSTANTIATION OF SUB OPERATION HANDLERS DUE TO REMOTE EXECUTION");
        }
       	else if (scenario.isMobile()) {
        	LOGGER.log(java.util.logging.Level.INFO, "SKIPPING INSTANTIATION OF SUB OPERATION HANDLERS DUE TO MOBILE EXECUTION");
        }
       	else {
            keyboardOperationHandler = new KeyboardOperationHandler(ExecutionUtil.getScreenSize(driver, scenario), this);
            sapOperationHandler = new SAPOperationHandler(ExecutionUtil.getScreenSize(driver, scenario), this);
            lanFaxOperationHandler = new LanFaxOperationHandler(ExecutionUtil.getScreenSize(driver, scenario), this);
            jsmOperationHandler = new JSMOperationHandler(this);
        }

        /*
         * Only instantiate the mobile handlers for mobile (Android, iOS) tests, but instantiate FlynetOperationHandler
         * for all non-mobile tests.
         */
        if (scenario.isAndroid()) {
            androidOperationHandler = new AndroidOperationHandler(this);
        }
        else if (scenario.isIOS()) {
            iosOperationHandler = new IOSOperationHandler(this);
        } 
        else {
        	mouseOperationHandler = new MouseOperationHandler(ExecutionUtil.getScreenSize(driver, scenario), this);
            flynetOperationHandler = new FlynetOperationHandler(this);
        }

        LOGGER.log(Level.INFO, "OperationHandler has been instantiated: {0}", this);
    }

    /*
     * Allows for overriding by jUnits
     */
    protected WebElementFinder getWebElementFinderInstance(WebDriver driver, ExecutionScenario scenario) { 
    	return new WebElementFinder(driver, scenario);
    }
    
    public TestStepResult process(TestStep testStep) {
    	return this.process(testStep, null);
    }

    @SuppressWarnings("squid:S3776")
    public TestStepResult process(TestStep testStep, WebElement targetElement) {

		LOGGER.log(Level.INFO, "{0} PROCESSING testStep: {1}", new Object[]{this, testStep});
		LOGGER.log(Level.INFO, "[MONITOR] PROCESSING TestStep: {0}:{1}", new Object[]{testStep.getCode(), testStep.getDesc()});

    	TestStepResult testStepResult = null;

    	boolean success = false;

		if (!scenario.isMobile()) {
			
			/*
			 * BAG - 9/10/19 - Handle UnhandledAlertException, which would be thrown if the prior operation caused an
			 * alert to display.
			 */
			try {
				
		    	if (scenario.isInstantiateDriver() && !scenario.isSupport() && webElementFinder.getWindowCount() == 0) {

    	    		WebDriverException wde = new WebDriverException("NO BROWSER WINDOWS ARE CURRENTLY OPEN. EXITING TEST.");
    	    		new TestStepResult(testStep, wde);
    	    		throw wde;
				}

			} catch (UnhandledAlertException uae) {
	        	LOGGER.log(java.util.logging.Level.INFO, "Ignoring UnhandledAlertException thrown attempting to get window count");
			}
    	}

    	/*
    	 * Pre-Operation logic. Forces any element search to return hidden elements in the result set.
    	 */
		if (PreOperationCode.H.equals(testStep.getPreOperationCode())) {
			webElementFinder.setExcludeHiddenElements(false);
		}
    	/*
    	 * Pre-Operation logic. The result of the PERFORM determines if the primary Operation will be performed.
    	 */
		if (PreOperationCode.P.equals(testStep.getPreOperationCode())) {

			LOGGER.log(Level.INFO, "performing Pre-Operation process: {0}", testStep.getCustomFunctionClassName());
        	try {
        		SimpleFunctionResults results = CustomFunctionUtil.perform(testStep, uiLocators, cache, scenario, driver);

        		if (!results.isSuccess()) {
        			LOGGER.log(Level.INFO, "Pre-Operation process was deemed a failure. Skipping primary Operation: {0}", testStep.getOperation());
	           		testStepResult = new TestStepResult(testStep, Outcome.PRE_OPER_FAILURE);
	           		this.takeScreenshot(testStepResult.getTestStep(), driver);
	           		return testStepResult;
        		}
        	} catch (Exception e) {
        		testStepResult = new TestStepResult(testStep, e);
           		return testStepResult;
        	}
		}

		/*
		 *  Check all pipe-delimited tokens in the attributeValue. If any of the tokens are keys within cache, replace 
		 *  it with the cached value.
		 */
		if (testStep.isStoreOperation() || testStep.isKeyOperation()) {
			// BAG - 9/25/19 - DO NOT ATTEMPT TO REPLACE CACHE KEYS WITH CACHE VALUES FOR STORE OPERATIONS
		}
		else if (testStep.getOperation() == Operation.FLYNET_SET_TEXT && testStep.hasParameterizedLocator()) {
			// BAG - 4/27/20 - DO NOT ATTEMPT TO REPLACE CACHE KEYS FOR FLYNET_SET_TEXT IF IT'S LEVERAGING THE
			// PARAMETERIZATION FEATURE. Otherwise, the bracketed parameter (ex. the value 'columnKey' in the
			// attributeValue 'webdriver.test.[columnKey]') will be replaced with the String equivalent of the
			// array that's loaded into cache, thereby causing the parameter logic built into the 
			// FLYNET_SET_TEXT operation to fail.
		}
		else if (testStep.hasAttributeValue() && !this.cache.isEmpty()) {
			/*
			 * BAG - 3/5/20 - Updated to use replaceKeysWithValues()
			 */
			testStep.setAttributeValue(this.cache.replaceKeysWithValues(testStep.getAttributeValue()));
		}

		/*
		 * BAG - 11/9/19 - Replace any cache keys in the step description with the cache value. 
		 */
		if (!this.cache.isEmpty()) {
			testStep.setDesc(this.cache.replaceKeysWithValues(testStep.getDesc()));
		}
		
    	switch (testStep.getOperation()) {

    		case ADD_MONTHS_TO_STORED_DATE:
    			try {	
	    	    	AttributeTokens attrTokens = testStep.getOperation().getParser().parse(testStep.getAttributeValue(), FlynetConstants.PIPE);
	
	    			List<CachedDate> cachedDates = this.getCachedDatesForAddOperation(attrTokens);
	    			
	    			for (CachedDate cachedDate : cachedDates) {
		    			this.cache.save(
		    				attrTokens.hasSecondaryString() ? attrTokens.getSecondaryString() + cachedDate.getKeySuffix() : attrTokens.getPrimaryString() + cachedDate.getKeySuffix(), 
			    			new SimpleDateFormat(cachedDate.getFormat()).format(DateUtil.addMonths(cachedDate.getDate(), attrTokens.getPrimaryNumeric())));
	    			}
	        		
	    			testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

    			} catch (Exception e) {
		    		testStepResult = new TestStepResult(testStep, e);
		    	}
	            break;

    		case ADD_DAYS_TO_STORED_DATE:
    			try {	
	    	    	AttributeTokens attrTokens = testStep.getOperation().getParser().parse(testStep.getAttributeValue(), FlynetConstants.PIPE);
	
	    			List<CachedDate> cachedDates = this.getCachedDatesForAddOperation(attrTokens);
	    			
	    			for (CachedDate cachedDate : cachedDates) {
		    			this.cache.save(
		    				attrTokens.hasSecondaryString() ? attrTokens.getSecondaryString() + cachedDate.getKeySuffix() : attrTokens.getPrimaryString() + cachedDate.getKeySuffix(), 
			    			new SimpleDateFormat(cachedDate.getFormat()).format(DateUtil.addDays(cachedDate.getDate(), attrTokens.getPrimaryNumeric())));
	    			}
	        		
	    			testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

    			} catch (Exception e) {
		    		testStepResult = new TestStepResult(testStep, e);
		    	}
	            break;

    		case ADD_YEARS_TO_STORED_DATE:
    			try {	
	    	    	AttributeTokens attrTokens = testStep.getOperation().getParser().parse(testStep.getAttributeValue(), FlynetConstants.PIPE);
	
	    			List<CachedDate> cachedDates = this.getCachedDatesForAddOperation(attrTokens);
	    			
	    			for (CachedDate cachedDate : cachedDates) {
		    			this.cache.save(
		    				attrTokens.hasSecondaryString() ? attrTokens.getSecondaryString() + cachedDate.getKeySuffix() : attrTokens.getPrimaryString() + cachedDate.getKeySuffix(), 
			    			new SimpleDateFormat(cachedDate.getFormat()).format(DateUtil.addYears(cachedDate.getDate(), attrTokens.getPrimaryNumeric())));
	    			}
	        		
	    			testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

    			} catch (Exception e) {
		    		testStepResult = new TestStepResult(testStep, e);
		    	}
	            break;

    		case ALT_TAB:
    			/*
    			 * BAG - 9/17/20 - Let KeyboardOperationHandler handle the Alt+Tab.
    			 */
    			testStep.setOperation(Operation.KEYBOARD_ALT_TAB);
    			testStepResult = this.getKeyboardOperationHandler().process(testStep, driver);
	            break;

	        case APPEND_TEXT:
	        	try {
	        		webElementFinder.appendText(testStep, testStep.getAttributeValue());
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case ARROW_DOWN:
	        	try {
		        	targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
		        	targetElement.sendKeys(Keys.ARROW_DOWN);
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case ARROW_UP:
	        	try {
		        	targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
		        	targetElement.sendKeys(Keys.ARROW_UP);
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case ATTACH_WINDOWS:
    	    	LOGGER.log(java.util.logging.Level.INFO, "clearing detachedWindowHandles map");
    			this.scenario.getDetachedWindowHandles().clear();
        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
    			break;

			case BACKSPACE:
	        	try {
		        	targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);

	        		/*
	        		 * Backspace for the given number of times as indicated by the attribute value.
	        		 */
	        		if (testStep.isAttributeValueNumeric()) {
	        			for (int i = 0; i < testStep.getAttributeValueAsNumeric(); i++) {
	    			    	targetElement.sendKeys(Keys.BACK_SPACE);
	        			}
	        		} else {
				    	targetElement.sendKeys(Keys.BACK_SPACE);
	        		}

		        	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

			case CLEAR:
	        	try {
		        	targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
		        	targetElement.clear();
		        	/*
			         * Perform a CTRL+A (select all), then Backspace, just in case the clear()
			         * call did not clear the field (which happens in DocFinity)
			         * Skipping the CTRL+A for Mobile
					 * Types symbols for Android and IOS Application.
					 */
		        	/*
		        	 * BAG - Gracefully handle any StaleElementReferenceException that occurs between the clear() and the 
		        	 * sendKeys(). Some apps such as MHTK have pages that auto-search when fields are changed, resulting 
		        	 * in the page elements getting refreshed, and the StaleElementReferenceException getting thrown. If
		        	 * this happens, just swallow the sendKeys() invocation.
		        	 */
		        	try {
			        	if(!scenario.isMobile()) {
			        		targetElement.sendKeys(Keys.chord(Keys.CONTROL, "a"), Keys.BACK_SPACE);
			        	}

		        	} catch (StaleElementReferenceException sere) {
				        LOGGER.log(java.util.logging.Level.INFO, TEXT_HANDLING_SEE_EXCEPTION);
					}

		        	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case CLICK_BY_DYNAMIC_VALUE:
    		case CLICK:
	        	try {
	            	try {
	        			/*
	        			 * Handles this operation being defined within a LOOP construct
	        			 */
		        		if (targetElement == null) {
		        			// BAG - 3/8/19 - Allow for wait in sec to be passed in via attributeValue
		        			targetElement = webElementFinder.waitForElementForStep(
		        					testStep, testStep.isAttributeValueNumericAndLessThan(WebElementFinder.MAX_WAIT_IN_SEC) ? testStep.getAttributeValueAsNumeric() : this.overrideWaitTimeoutInSeconds);
			        	}

	            		webElementFinder.clickElement(testStep, targetElement);
	            		
	            	/*
	            	 * BAG - 4/22/19 - Handle StaleElementReferenceException that may occur due to element being refreshed between
	            	 * locating it and clicking it.
	            	 */
	            	} catch (StaleElementReferenceException sere) {
				        LOGGER.log(java.util.logging.Level.INFO, TEXT_HANDLING_SEE_EXCEPTION);
				        targetElement = webElementFinder.waitForElementForStep(
				        		testStep, testStep.isAttributeValueNumericAndLessThan(WebElementFinder.MAX_WAIT_IN_SEC) ? testStep.getAttributeValueAsNumeric() : this.overrideWaitTimeoutInSeconds);
				        webElementFinder.clickElement(testStep, targetElement);
					}

	            	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        		ExecutionUtil.sleep(this.getTimeoutInMillis(500));
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case CLICK_AND_WAIT_FOR_WINDOW:
	        	try {
        			/*
        			 * Handles this operation being defined within a LOOP construct
        			 */
	        		if (targetElement == null) {
	        			targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
		        	}

	            	this.originalWindowHandle = driver.getWindowHandle();
	            	LOGGER.log(java.util.logging.Level.INFO, "current window handle: {0}", this.originalWindowHandle);

	            	Set<String> origWindowHandles = driver.getWindowHandles();
	            	LOGGER.log(java.util.logging.Level.INFO, "all window handles:    {0}", origWindowHandles);

	            	webElementFinder.clickElement(testStep, targetElement);

		        	this.lastOpenedWindowHandle = this.waitForNewWindow(origWindowHandles, testStep);

	            	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        		ExecutionUtil.sleep(this.getTimeoutInMillis(2000));

	        	} catch (WindowNotFoundException wnfe) {
	        		testStepResult = new TestStepResult(testStep, false);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case CLICK_BY_INDEX:
	        	try {
	            	/*
	            	 * BAG - 4/22/19 - Handle StaleElementReferenceException that may occur due to element being refreshed between
	            	 * locating it and clicking it.
	            	 */
	            	try {
		        		targetElement = webElementFinder.findByIndex(testStep, this.overrideWaitTimeoutInSeconds);
	            		webElementFinder.clickElement(testStep, targetElement);

	            	} catch (StaleElementReferenceException sere) {
				        LOGGER.log(java.util.logging.Level.INFO, TEXT_HANDLING_SEE_EXCEPTION);
		        		targetElement = webElementFinder.findByIndex(testStep, this.overrideWaitTimeoutInSeconds);
		                webElementFinder.clickElement(testStep, targetElement);
					}

	            	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        		ExecutionUtil.sleep(this.getTimeoutInMillis(500));
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case CLICK_BY_PARTIAL_VALUE:
	        	try {
	            	/*
	            	 * BAG - 4/22/19 - Handle StaleElementReferenceException that may occur due to element being refreshed between
	            	 * locating it and clicking it.
	            	 */
	            	try {
		        		targetElement = webElementFinder.findByPartialValue(testStep);
	            		webElementFinder.clickElement(testStep, targetElement);

	            	} catch (StaleElementReferenceException sere) {
				        LOGGER.log(java.util.logging.Level.INFO, TEXT_HANDLING_SEE_EXCEPTION);
		        		targetElement = webElementFinder.findByPartialValue(testStep);
				        webElementFinder.clickElement(testStep, targetElement);
					}

	            	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        		ExecutionUtil.sleep(this.getTimeoutInMillis(500));
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case CLICK_BY_VALUE:
	        	try {
	            	/*
	            	 * BAG - 4/22/19 - Handle StaleElementReferenceException that may occur due to element being refreshed between
	            	 * locating it and clicking it.
	            	 */
	            	try {
		        		targetElement = webElementFinder.findByValue(testStep);
	            		webElementFinder.clickElement(testStep, targetElement);

	            	} catch (StaleElementReferenceException sere) {
				        LOGGER.log(java.util.logging.Level.INFO, TEXT_HANDLING_SEE_EXCEPTION);
				        targetElement = webElementFinder.findByValue(testStep);
				        webElementFinder.clickElement(testStep, targetElement);
					}

	            	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        		ExecutionUtil.sleep(this.getTimeoutInMillis(500));
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case CLICK_CHILD:
	        	try {
	            	LOGGER.log(java.util.logging.Level.INFO, "CLICKING child element for: {0}", testStep);
	            	webElementFinder.clickChildElement(testStep, targetElement);

	            	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        		ExecutionUtil.sleep(this.getTimeoutInMillis(500));
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case CLICK_EACH:
	        	try {
	        		/*
	        		 * Since this operation is used primarily to auto-expand all closed nodes
	        		 * in the Commercial Desktop Category tree, we need to perform a psuedo-recursive
	        		 * process to expand all child nodes as well.
	        		 */
	        		List<WebElement> elements = webElementFinder.getElementsForStep(testStep);

	        		while (!elements.isEmpty()) {
		        		for (WebElement element : elements) {
			            	webElementFinder.clickElement(testStep, element);
			        		ExecutionUtil.sleep(this.getTimeoutInMillis(500));
			        	}
		        		/*
		        		 * BAG - 4/5/19 - Changed to instantiate a new WebElementFinder each time to
		        		 * prevent having the same collection of elements being returned, due to the
		        		 * streamlining logic in getElementsForStep[Teststep, UILocator, WebElement]
		        		 */
		        		elements = this.getWebElementFinderInstance(driver, scenario).getElementsForStep(testStep);
	        		}

	            	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	            /*
	             * No elements found is a valid scenario
	             */
	        	} catch (NoSuchElementException nsee) {
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case CLICK_IF_PRESENT:
	        	try {
	        		if (!webElementFinder.isPresent(testStep, false)) {
	        			testStepResult = new TestStepResult(testStep, false);
	        		} else {
	        			/*
	        			 * Handles this operation being defined within a LOOP construct
	        			 */
		        		if (targetElement == null) {
		        			targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
			        	}
		            	webElementFinder.clickElement(testStep, targetElement);

		            	testStepResult = new TestStepResult(testStep, true);
		        		ExecutionUtil.sleep(this.getTimeoutInMillis(500));
	        		}

	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case CLICK_LAST:
	        	try {
	            	/*
	            	 * BAG - 4/22/19 - Handle StaleElementReferenceException that may occur due to element being refreshed between
	            	 * locating it and clicking it.
	            	 */
	            	try {
		        		targetElement = webElementFinder.getLastElement(testStep, this.overrideWaitTimeoutInSeconds);
	            		webElementFinder.clickElement(testStep, targetElement);

	            	} catch (StaleElementReferenceException sere) {
				        LOGGER.log(java.util.logging.Level.INFO, TEXT_HANDLING_SEE_EXCEPTION);
				        targetElement = webElementFinder.getLastElement(testStep, this.overrideWaitTimeoutInSeconds);
				        webElementFinder.clickElement(testStep, targetElement);
					}

	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        		ExecutionUtil.sleep(this.getTimeoutInMillis(500));
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case CLICK_LAST_WITH_ALERT_CHECK:
	        	try {
	        		targetElement = webElementFinder.getLastElement(testStep, this.overrideWaitTimeoutInSeconds);
		        	webElementFinder.clickElement(testStep, targetElement);

	        		// Check for the 'Emulator Busy' alert message
	        		while (this.handleAlertIfPresent(testStep)) {
	        			ExecutionUtil.sleep(this.getAlertCheckWaitInMillis(4000));
	        			targetElement = webElementFinder.getLastElement(testStep, this.overrideWaitTimeoutInSeconds);
		        		webElementFinder.clickElement(testStep, targetElement);
	        		}
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        		ExecutionUtil.sleep(this.getTimeoutInMillis(500));
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case CLICK_MULTIPLE:
	        	try {

	        		List<WebElement> elements = webElementFinder.getElementsForStep(testStep);

	            	webElementFinder.clickElement(testStep, elements.get(0));
	        		
	            	if (elements.size() > 1) {
	            	
		        		for (int i = 1; i < elements.size(); i++) {
					        LOGGER.log(java.util.logging.Level.INFO, "performing CTRL_click on element: {0}", elements.get(i));
			            	new Actions(driver).keyDown(Keys.CONTROL).click(elements.get(i)).keyUp(Keys.CONTROL).perform();
			        	}
	        		}

	            	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	            /*
	             * No elements found is a valid scenario
	             */
	        	} catch (NoSuchElementException nsee) {
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case CLICK_UNTIL_CONTAINS_VALUE:
	        	try {

	        		if (!testStep.hasSecondaryLocator()) {
	        	    	throw new IllegalArgumentException("Operation requires secondary locator: " + testStep);
	        		}

	        		AttributeTokens attrTokens = testStep.getOperation().getParser().parse(testStep.getAttributeValue(), PIPE);
	        		
	        		int waitInSecBetweenClickAttempts = attrTokens.getPrimaryNumeric();
	        		int maxClickAttempts = attrTokens.getSecondaryNumeric();
	        		String stringToCompare = attrTokens.getString();

	        		/*
	        		 * We pass null as the 3rd argument, since this Operation uses the secondary locator for the CONTAINS_VALUE
	        		 * portion of the operation. 
	        		 */
	        		targetElement = webElementFinder.waitForElementForStep(testStep, testStep.getLocator(), null);
	        		
	        		int attempt = 1;

	        		/*
	        		 * CLick the primary element until the secondary element contains the desired string.
	        		 */
	        		while (!success && attempt <= maxClickAttempts) {
	        			
		        		try {
		            		webElementFinder.clickElement(testStep, targetElement);

		        		/*
		            	 * BAG - 4/22/19 - Handle StaleElementReferenceException that may occur due to element being refreshed between
		            	 * locating it and clicking it.
		            	 */
		            	} catch (StaleElementReferenceException sere) {
					        LOGGER.log(java.util.logging.Level.INFO, TEXT_HANDLING_SEE_EXCEPTION);
			        		targetElement = webElementFinder.waitForElementForStep(testStep, testStep.getLocator(), null);
					        webElementFinder.clickElement(testStep, targetElement);
						}

		        		ExecutionUtil.sleep(this.getTimeoutInMillis(1000));

		        		/*
		        		 * Handle multiple elements returned for the secondary locator.
		        		 */
		        		success = webElementFinder.containsValue(testStep, testStep.getSecondaryLocator(), null, stringToCompare);

		        		if (!success) {
			        		ExecutionUtil.sleep(waitInSecBetweenClickAttempts * this.getTimeoutInMillis(1000));
			        		attempt++;
		    			}
	        		}
	        		
	            	testStepResult = new TestStepResult(testStep, success);
	        		ExecutionUtil.sleep(this.getTimeoutInMillis(500));
	        		
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case CLICK_WITH_ALERT_CHECK:
	        	try {
	            	/*
	            	 * BAG - 4/22/19 - Handle StaleElementReferenceException that may occur due to element being refreshed between
	            	 * locating it and clicking it.
	            	 */
	            	try {
		        		targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
		        		webElementFinder.clickElement(testStep, targetElement);

	            	} catch (StaleElementReferenceException sere) {
				        LOGGER.log(java.util.logging.Level.INFO, TEXT_HANDLING_SEE_EXCEPTION);
				        targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
		 		        webElementFinder.clickElement(testStep, targetElement);
					}

	        		// Check for the 'Emulator Busy' alert message
	        		while (this.handleAlertIfPresent(testStep)) {
	        			ExecutionUtil.sleep(this.getAlertCheckWaitInMillis(4000));
		        		webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds).click();
	        		}
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        		ExecutionUtil.sleep(this.getTimeoutInMillis(500));
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case CLICK_WITH_POPUP_CHECK:
	        	try {
	            	/*
	            	 * BAG - 4/22/19 - Handle StaleElementReferenceException that may occur due to element being refreshed between
	            	 * locating it and clicking it.
	            	 */
	            	try {
		        		targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
	            		webElementFinder.clickElement(testStep, targetElement);

	            	} catch (StaleElementReferenceException sere) {
				        LOGGER.log(java.util.logging.Level.INFO, TEXT_HANDLING_SEE_EXCEPTION);
				        targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
				        webElementFinder.clickElement(testStep, targetElement);
					}

	            	ExecutionUtil.sleep(this.getTimeoutInMillis(1000));

	        		this.handleAlertIfPresent(testStep);
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        		ExecutionUtil.sleep(this.getTimeoutInMillis(500));
	        	} catch (UnhandledAlertException uae) {
	        		this.handleAlertIfPresent(testStep);
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case CLOSE_LAST_WINDOW:
    			if (StringUtils.isNotEmpty(this.lastOpenedWindowHandle)) {

    		        LOGGER.log(java.util.logging.Level.INFO, "Start of CLOSE_LAST_WINDOW logic");
	            	LOGGER.log(java.util.logging.Level.INFO, "driver.getWindowHandles(): {0}", driver.getWindowHandles());
    		        LOGGER.log(java.util.logging.Level.INFO, "originalWindowHandle:      {0}", this.originalWindowHandle);
    		        LOGGER.log(java.util.logging.Level.INFO, "lastOpenedWindowHandle:    {0}", this.lastOpenedWindowHandle);

    				success = ExecutionUtil.closeWindowByHandle(this.lastOpenedWindowHandle, driver);

	            	/*
	            	 * BAG - 12/27/18 - We switch the driver back to the original window here as
	            	 * a precaution, in case the next operation is something that requires the
	            	 * window to be established (such as GOTOURL). Otherwise, we'll end up getting
	            	 * a 'NoSuchWindowException: no such window: target window already closed'
	            	 * exception.
	            	 */
    		        LOGGER.log(java.util.logging.Level.INFO, "switching to original window: {0}", this.originalWindowHandle);
    				driver.switchTo().window(this.originalWindowHandle);

        			this.originalWindowHandle = null;
        			this.lastOpenedWindowHandle = null;

            		testStepResult = new TestStepResult(testStep, success);
    			} else {
            		testStepResult = new TestStepResult(testStep, new Exception("NO STORED WINDOW HANDLE"));
    			}
	        	break;

    		case CLOSE_WINDOW:
	        	try {
	        		if (testStep.hasLocator()) {
		        		
	        			webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);

		        		if (driver.getWindowHandles().size() == 1) {
			            	LOGGER.log(java.util.logging.Level.INFO, "ONLY ONE WINDOW OPEN. IGNORING CLOSE_WINDOW REQUEST");
		        		} else {
			            	LOGGER.log(java.util.logging.Level.INFO, "Closing current window");
			        		driver.close();
		        		}
		        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        		} else {
		        		if (driver.getWindowHandles().size() == 1) {
			            	LOGGER.log(java.util.logging.Level.INFO, "ONLY ONE WINDOW OPEN. IGNORING CLOSE_WINDOW REQUEST");
		        		} else {
			            	LOGGER.log(java.util.logging.Level.INFO, "Closing current window");
			        		driver.close();
		        		}
		        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        		}
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

    		case CLOSE_WINDOW_BY_TITLE:
    			success = ExecutionUtil.closeWindowByTitle(testStep.getAttributeValue(), driver);
        		testStepResult = new TestStepResult(testStep, success);
	        	break;

    		case CONCAT_AND_STORE:
    	        boolean result = this.cache.concatenateAndStore(testStep.getAttributeValue());
    	        testStepResult = new TestStepResult(testStep, result);
	        	break;

			case CTRL_ALT_KEY:
	        	try {
	        		
	        		AttributeTokens attrTokens = testStep.getOperation().getParser().parse(testStep.getAttributeValue());

	            	new Actions(driver).keyDown(Keys.CONTROL).keyDown(Keys.ALT).sendKeys(attrTokens.getString()).keyUp(Keys.ALT).keyUp(Keys.CONTROL).perform();

		        	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

			case CTRL_SHIFT_KEY:
	        	try {

	        		AttributeTokens attrTokens = testStep.getOperation().getParser().parse(testStep.getAttributeValue());

	            	new Actions(driver).keyDown(Keys.CONTROL).keyDown(Keys.SHIFT).sendKeys(attrTokens.getString()).keyUp(Keys.SHIFT).keyUp(Keys.CONTROL).perform();

		        	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case DRAG_AND_DROP:
	        	try {
	        		
	        		if (!testStep.hasSecondaryLocator()) {
	        	    	throw new IllegalArgumentException("Operation requires secondary locator: " + testStep);
	        		}

	        		WebElement sourceElement = webElementFinder.getElementForStep(testStep);
	        		targetElement = webElementFinder.getElementForStep(testStep, testStep.getSecondaryLocator());

	        		LOGGER.log(java.util.logging.Level.INFO, "DRAGGING element from [{0}] to [{1}]", new Object[]{testStep.getLocator(), testStep.getSecondaryLocator()});
		        	new Actions(driver).dragAndDrop(sourceElement, targetElement).build().perform();

	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

	        		if (testStep.isAttributeValueNumeric()) {
	        			ExecutionUtil.sleep(this.getTimeoutInMillis(testStep.getAttributeValueAsNumeric()));
	        		} else {
	        			ExecutionUtil.sleep(this.getTimeoutInMillis(1000));
	        		}
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case DOUBLE_CLICK:
    		//case DBL_CLICK_BY_INDEX:
	        	try {
	        		targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
	            	LOGGER.log(java.util.logging.Level.INFO, "DOUBLE-CLICKING element for: {0}", testStep);
		        	new Actions(driver).doubleClick(targetElement).build().perform();
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

	        		if (testStep.isAttributeValueNumeric()) {
	        			ExecutionUtil.sleep(this.getTimeoutInMillis(testStep.getAttributeValueAsNumeric()));
	        		} else {
	        			ExecutionUtil.sleep(this.getTimeoutInMillis(1000));
	        		}
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case DOUBLE_CLICK_AND_WAIT_FOR_WINDOW:
	        	try {
	        		targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);

	            	this.originalWindowHandle = driver.getWindowHandle();
	            	LOGGER.log(java.util.logging.Level.INFO, "current window handle: {0}", this.originalWindowHandle);

	            	Set<String> origWindowHandles = driver.getWindowHandles();
	            	LOGGER.log(java.util.logging.Level.INFO, "all window handles:    {0}", origWindowHandles);

	            	LOGGER.log(java.util.logging.Level.INFO, "DOUBLE-CLICKING element for: {0}", testStep);
		        	new Actions(driver).doubleClick(targetElement).build().perform();

		        	this.lastOpenedWindowHandle = this.waitForNewWindow(origWindowHandles, testStep);

	            	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        		ExecutionUtil.sleep(this.getTimeoutInMillis(2000));

	        	} catch(WindowNotFoundException wnfe) {
	        		testStepResult = new TestStepResult(testStep, false);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case DETACH_WINDOW:
	        	try {
	        		if (testStep.hasLocator()) {
		        		targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
			        	if (targetElement != null) {
			        		this.scenario.getDetachedWindowHandles().put(driver.getWindowHandle(), testStep.toString());
			        	}
		        		testStepResult = new TestStepResult(testStep, (targetElement != null));
	        		} else {
		        		driver.close();
		        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        		}
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

    		case DUMP_ELEMENT:
	        	try {
		        	LocatorUtil.dumpElement(testStep, webElementFinder);
	        	} catch (Exception e) {
	        		LOGGER.log(java.util.logging.Level.SEVERE, e.getMessage(), e);
	        	}
    			testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	break;

    		case DUMP_ELEMENT_WITH_VALUE:
	        	try {
		        	LocatorUtil.dumpElementForDivContainingText(testStep, driver);
	        	} catch (Exception e) {
	        		LOGGER.log(java.util.logging.Level.SEVERE, e.getMessage(), e);
	        	}
    			testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	break;

    		case DUMP_ELEMENTS:
	        	try {
		        	LocatorUtil.dumpElements(testStep, webElementFinder);
	        	} catch (Exception e) {
	        		LOGGER.log(java.util.logging.Level.SEVERE, e.getMessage(), e);
	        	}
    			testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	break;

    		case DUMP_PAGE_BODY:
   				LOGGER.log(java.util.logging.Level.INFO, "PAGE SOURCE:\n{0}", driver.getPageSource());
   				webElementFinder.getWebDriverWrapper().findElementsAcrossAllFrames(null, true);
    			testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	break;

    		case ENDIF:
           		testStepResult = new TestStepResult(testStep, true);
	        	break;

	        case ENFORCE_SCREEN_WIDTH:
	        	try {
		        	webElementFinder.enforceScreenWidth(testStep);
		        	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case ENFORCE_SCREEN_HEIGHT:
	        	try {
		        	webElementFinder.enforceScreenHeight(testStep);
		        	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case ERROR:
    			testStepResult = new TestStepResult(testStep, false);
	        	break;

    		case EXIT:
    		case EXIT_TESTSHEET:
    		case EXIT_MULTI_ROW_LOOP:	
    		case EXIT_MULTI_ROW_LOOP_ITERATION:	
    			testStepResult = new TestStepResult(testStep, true);
	        	break;

    		case EXIT_IF_PRESENT:
    		case EXIT_TESTSHEET_IF_PRESENT:
	        	try {
	        		success = webElementFinder.containsValue(testStep, targetElement);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case EXIT_IF_ABSENT:
    		case EXIT_TESTSHEET_IF_ABSENT:

    			/*
    			 * BAG - 3/8/19 - Disable highlighting when checking for absence
    			 */
    			boolean previousHighlightSetting = scenario.isHighlightElement();
    			scenario.setHighlightElement(false);

	        	try {
	        		success = !webElementFinder.containsValue(testStep, targetElement);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}

    			scenario.setHighlightElement(previousHighlightSetting);
	            break;

    		case EXIT_IF_TRUE:
    		case EXIT_TESTSHEET_IF_TRUE:
	        	try {
	        		SimpleFunctionResults results = CustomFunctionUtil.perform(testStep, uiLocators, cache, scenario, driver);
	           		testStepResult = new TestStepResult(testStep, results.isSuccess());
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case EXIT_IF_FALSE:
    		case EXIT_TESTSHEET_IF_FALSE:
	        	try {
	        		SimpleFunctionResults results = CustomFunctionUtil.perform(testStep, uiLocators, cache, scenario, driver);
	           		testStepResult = new TestStepResult(testStep, results.isSuccess());
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case GETTEXT:

	            //Get text of an element
	        	try {
		        	webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds).getText();
	           		testStepResult = new TestStepResult(testStep, true);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

    		case GOTOURL:
	        	try {

	        		if (webElementFinder.getWindowCount() == 0) {
		            	LOGGER.log(java.util.logging.Level.INFO, "NO WINDOWS EXIST. Attempting to open a new window.");
		            	driver = WebDriverManager.createDriver(scenario);
		            	webElementFinder = this.getWebElementFinderInstance(driver, this.scenario);
	        		}

	        		String[] parameters = StringUtils.split(testStep.getAttributeValue(), ExecutionConstants.PIPE);

        			String url = parameters[0];
	        		String authUserid = null;
	        		String authPassword = null;
	        		boolean openNewWindow = false;

	        		if (parameters.length == 2) {
	        			openNewWindow = Boolean.valueOf(parameters[1]);
	        		}
	        		else if (parameters.length == 3) {
	        			authUserid = parameters[1];
	        			authPassword = parameters[2];
	        		}

	        		int attempt = 1;
	        		/*
	        		 * This loop handles the situation in which the URL does not load correctly
	        		 * due to a 404 error (which happens occasionally with Commercial Desktop).
	        		 * In this case, we simply attempt to reload the URL up to a max of 5 attempts.
	        		 * Note that the retries are only performed if a Locator was defined for the
	        		 * GOTOURL step.
	        		 */
	        		do {

		        		/*
		        		 * Handle an authentication dialog if it appears, and a userid/password combo was supplied
		        		 * in the AttributeKey field.
		        		 */
		        		if (StringUtils.isNotEmpty(authUserid) && StringUtils.isNotEmpty(authPassword)) {
		        			
		        			/*
		        			 * BAG - 11/21/19 - Handle decryption of encrypted auth password. This logic assumes that the
		        			 * encrypted data property IS the password, which is a reasonably solid assumption.
		        			 */
		        			if (testStep.hasEncryptedDataProperties()) {
		        				LOGGER.log(Level.INFO, "{0} decrypting authPassword for testStep: {1}", new Object[]{this, testStep});
		        				authPassword = CryptoUtil.decrypt(this.getExecutionScenario().getSecurityKey(), authPassword);
		        			}
		        			
			        		LOGGER.log(java.util.logging.Level.INFO, "going to URL with authentication credentials [Attempt {0}]: {1}", new Object[]{attempt, url});
		
		            		url = url.replace(DOUBLE_FORWARD_SLASHES, DOUBLE_FORWARD_SLASHES + authUserid + COLON + authPassword + AT_SIGN);

				            driver.get(url);
		        		} 
		        		else if (openNewWindow) {
			        		LOGGER.log(java.util.logging.Level.INFO, "opening new window for URL [Attempt {0}]: {1}", new Object[]{attempt, url});
			            	((JavascriptExecutor) driver).executeScript("window.open(arguments[0])", url);
		        		} 
		        		else {
			        		LOGGER.log(java.util.logging.Level.INFO, "going to URL [Attempt {0}]: {1}", new Object[]{attempt, url});
			                driver.get(url);
		        		}

			            // wait for login page to load or 404 error
			            ExecutionUtil.sleep(this.getTimeoutInMillis(2000));
			            attempt++;
			        
			        /*
			         * Continue to loop if a) a locator was provided, and b) the element associated with that locator is not found.     
			         */
	        		} while (testStep.hasLocator() && !webElementFinder.isPresent(testStep) && attempt <= 5);

	        		testStepResult = new TestStepResult(testStep, attempt <= 5);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case HALT:
                haltExecution();
	       		testStepResult = new TestStepResult(testStep, true);
	        	break;

    		case HIDDEN_ELEMENTS_EXCLUDED:
    			webElementFinder.setExcludeHiddenElements(true);
	       		testStepResult = new TestStepResult(testStep, true);
	        	break;

    		case HIDDEN_ELEMENTS_INCLUDED:
    			webElementFinder.setExcludeHiddenElements(false);
	       		testStepResult = new TestStepResult(testStep, true);
	        	break;
    			
    		case IF_ABSENT:
    		case VERIFY_ABSENT:
	        	try {
	        		success = webElementFinder.isAbsent(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case IF_AMOUNTS_MATCH:
    		case VERIFY_AMOUNTS_MATCH:
	        	try {
	        		success = webElementFinder.doesAmountMatch(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case IF_BROWSER_CHROME:
	        	try {
	        		testStepResult = new TestStepResult(testStep, scenario.getBrowserChoices().get(0).isChrome());
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case IF_BROWSER_FIREFOX:
	        	try {
	        		testStepResult = new TestStepResult(testStep, scenario.getBrowserChoices().get(0).isFirefox());
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case IF_BROWSER_IE:
	        	try {
	        		testStepResult = new TestStepResult(testStep, scenario.getBrowserChoices().get(0).isIE());
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case IF_CHILD_CONTAINS_VALUE:
    		case VERIFY_CHILD_CONTAINS_VALUE:
	        	try {
	        		success = webElementFinder.childContainsValue(testStep, targetElement);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

            case STORE_COUNT_AS_KEY:
                try {
                    if (!testStep.hasAttributeValue()) {
                        throw new IllegalArgumentException(TEXT_PREFIX_MISSING_ATTR_VAL + testStep);
                    }

 

                    List<WebElement> elements = webElementFinder.getElementsForStep(testStep);

 

                    this.getCache().save(testStep.getAttributeValue(), String.valueOf(elements.size()));
                    testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

 

                } catch (NoSuchElementException nsee) {
                    
                    this.getCache().save(testStep.getAttributeValue(), String.valueOf(0));
                    testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
                    
                } catch (Exception e) {
                    testStepResult = new TestStepResult(testStep, e);
                }
                break;
	        case IF_COUNT_EQUAL_TO:
	        case VERIFY_COUNT_EQUAL_TO:
	        	try {
	        		success = webElementFinder.isCountEqualTo(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case IF_COUNT_GREATER_THAN:
	        case VERIFY_COUNT_GREATER_THAN:
	        	try {
				success = webElementFinder.isCountGreaterThan(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case IF_COUNT_LESS_THAN:
	        case VERIFY_COUNT_LESS_THAN:
	        	try {
	        		success = webElementFinder.isCountLessThan(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case IF_CONTAINS_VALUE:
	        case VERIFY_CONTAINS_VALUE:
	        	try {
	        		success = webElementFinder.containsValue(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case IF_DATA_PROPERTY_CONTAINS:
	        	
	        	AttributeTokens attrTokens = testStep.getOperation().getParser().parse(testStep.getAttributeValue(), FlynetConstants.PIPE);
	        	
        		success = this.testData.contains(attrTokens.getPrimaryString()) 
        			&& this.testData.getProperty(attrTokens.getPrimaryString()).toLowerCase().contains(attrTokens.getSecondaryString().toLowerCase());
        		testStepResult = new TestStepResult(testStep, success);
	        	break;
	        	
	        case IF_DATA_PROPERTY_EQUALS:
	        	
	        	attrTokens = testStep.getOperation().getParser().parse(testStep.getAttributeValue(), FlynetConstants.PIPE);
	        	
        		success = this.testData.contains(attrTokens.getPrimaryString()) 
            		&& this.testData.getProperty(attrTokens.getPrimaryString()).equalsIgnoreCase(attrTokens.getSecondaryString());
        		testStepResult = new TestStepResult(testStep, success);
	        	break;
	        	
	        case IF_DATA_PROPERTY_EXISTS:
        		success = this.testData.contains(testStep.getAttributeValue()) 
        			&& StringUtils.isNotEmpty(this.testData.getProperty(testStep.getAttributeValue()));
        		testStepResult = new TestStepResult(testStep, success);
	        	break;
	        	
	        case IF_DATA_PROPERTY_NOT_CONTAINS:
	        	
	        	attrTokens = testStep.getOperation().getParser().parse(testStep.getAttributeValue(), FlynetConstants.PIPE);
	        	
        		success = !this.testData.contains(attrTokens.getPrimaryString()) 
        			|| !this.testData.getProperty(attrTokens.getPrimaryString()).toLowerCase().contains(attrTokens.getSecondaryString().toLowerCase());
        		testStepResult = new TestStepResult(testStep, success);
	        	break;
	        	
	        case IF_DATA_PROPERTY_NOT_EQUALS:
	        	
	        	attrTokens = testStep.getOperation().getParser().parse(testStep.getAttributeValue(), FlynetConstants.PIPE);
	        	
        		success = !this.testData.contains(attrTokens.getPrimaryString()) 
            		|| !this.testData.getProperty(attrTokens.getPrimaryString()).equalsIgnoreCase(attrTokens.getSecondaryString());
        		testStepResult = new TestStepResult(testStep, success);
	        	break;
	        	
	        case IF_DATA_PROPERTY_NOT_EXISTS:
        		success = !this.testData.contains(testStep.getAttributeValue()) 
        			|| StringUtils.isEmpty(this.testData.getProperty(testStep.getAttributeValue()));
        		testStepResult = new TestStepResult(testStep, success);
	        	break;
	        	
	        case IF_DISABLED:
	        case VERIFY_DISABLED:
	        	try {
	        		success = webElementFinder.isDisabled(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case IF_EMPTY:
	        case VERIFY_EMPTY:
	        	try {
	        		success = webElementFinder.isEmpty(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case IF_ENABLED:
	        case VERIFY_ENABLED:
	        	try {
	        		success = webElementFinder.isEnabled(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case IF_FAILURE_IN_TESTCASE:
	        	try {
	        		/*
	        		 * A 'success' is considered any steps within the target TestCase having failed.
	        		 */
	        		TestCase targetTestCase = testStep.getTestSheet().getTestCase(testStep.getAttributeValue());
	        		testStepResult = new TestStepResult(testStep, targetTestCase.hasErrors());
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case IF_HELPTEXT:
	        	try {
	        		success = webElementFinder.verifyHelpText(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case IF_KEY_ABSENT:
	        	try {
	        		testStepResult = new TestStepResult(testStep, !this.cache.contains(testStep.getAttributeValue().trim()));
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case IF_KEY_PRESENT:
	        	try {
	        		testStepResult = new TestStepResult(testStep, this.cache.contains(testStep.getAttributeValue().trim()));
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

			case IF_INVALID_BY_CSS:
				try {
					success = webElementFinder.isInvalid(testStep);
					testStepResult = new TestStepResult(testStep, success);
				} catch (Exception e) {
					testStepResult = new TestStepResult(testStep, e);
				}
				break;

			case IF_VALID_BY_CSS:
				try {
					success = webElementFinder.isValid(testStep);
					testStepResult = new TestStepResult(testStep, success);
				} catch (Exception e) {
					testStepResult = new TestStepResult(testStep, e);
				}
				break;

			case IF_OPTIONAL_BY_CSS:
				try {
					success = webElementFinder.isOptional(testStep);
					testStepResult = new TestStepResult(testStep, success);
				} catch (Exception e) {
					testStepResult = new TestStepResult(testStep, e);
				}
				break;

	        case IF_NOT_CONTAINS_VALUE:
	        case VERIFY_NOT_CONTAINS_VALUE:
	        	try {
	        		success = !webElementFinder.containsValue(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case IF_NOT_EMPTY:
	        case VERIFY_NOT_EMPTY:
	        	try {
	        		success = !webElementFinder.isEmpty(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case IF_NOT_STARTS_WITH_VALUE:
	        case VERIFY_NOT_STARTS_WITH_VALUE:
	        	try {
	        		success = !webElementFinder.startsWithValue(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case IF_PRESENT:
    		case VERIFY_PRESENT:
	        	try {
	        		success = webElementFinder.isPresent(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case IF_RADIO_CHECKED:
	        case VERIFY_RADIO_CHECKED:
	        	try {
	        		targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
		        	testStepResult = new TestStepResult(testStep, targetElement.isSelected());
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case IF_RADIO_UNCHECKED:
	        case VERIFY_RADIO_UNCHECKED:
	        	try {
	        		targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
		        	testStepResult = new TestStepResult(testStep, !targetElement.isSelected());
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case IF_REQUIRED:
    		case VERIFY_REQUIRED:
	        	try {
	        		success = webElementFinder.isRequired(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case IF_STARTS_WITH_VALUE:
    		case VERIFY_STARTS_WITH_VALUE:
	        	try {
	        		success = webElementFinder.startsWithValue(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case IF_SELECTED:
    		case VERIFY_SELECTED:
	        	try {
	        		success = webElementFinder.isSelected(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case IF_STORED_VALUE_CONTAINS:
	        case VERIFY_STORED_VALUE_CONTAINS:
	        	try {
	        		success = this.performStoredValueComparison(testStep.getAttributeValue(), Comparison.CONTAINS);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case IF_STORED_VALUE_CONTAINS_ELEMENT_VALUE:
	        case VERIFY_STORED_VALUE_CONTAINS_ELEMENT_VALUE:
	        	try {
	        		if (StringUtils.isEmpty(testStep.getAttributeValue())) { 
	        			throw new IllegalArgumentException(TEXT_ATTR_VALUE_CANNOT_BE_BLANK);
	        		}
	        		if (!this.cache.contains(testStep.getAttributeValue())) {
	        			throw new IllegalArgumentException(TEXT_PREFIX_KEY + testStep.getAttributeValue() + TEXT_NOT_FOUND_IN_CACHE);
	        		}
	        		targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
	        		success = this.performValueComparison(this.cache.getCachedValue(testStep.getAttributeValue()), webElementFinder.getText(targetElement), Comparison.CONTAINS);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case IF_STORED_VALUE_EQUALS:
	        case VERIFY_STORED_VALUE_EQUALS:
	        	try {
	        		success = this.performStoredValueComparison(testStep.getAttributeValue(), Comparison.EQUALS);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case IF_STORED_VALUE_STARTS_WITH:
	        case VERIFY_STORED_VALUE_STARTS_WITH:
	        	try {
	        		success = this.performStoredValueComparison(testStep.getAttributeValue(), Comparison.STARTS_WITH);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case IF_STORED_VALUE_STARTS_WITH_ELEMENT_VALUE:
	        case VERIFY_STORED_VALUE_STARTS_WITH_ELEMENT_VALUE:
	        	try {
	        		if (StringUtils.isEmpty(testStep.getAttributeValue())) { 
	        			throw new IllegalArgumentException(TEXT_ATTR_VALUE_CANNOT_BE_BLANK);
	        		}
	        		if (!this.cache.contains(testStep.getAttributeValue())) {
	        			throw new IllegalArgumentException(TEXT_PREFIX_KEY + testStep.getAttributeValue() + TEXT_NOT_FOUND_IN_CACHE);
	        		}
	        		targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
	        		success = this.performValueComparison(this.cache.getCachedValue(testStep.getAttributeValue()), webElementFinder.getText(targetElement), Comparison.STARTS_WITH);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case IF_STORED_VALUE_NOT_CONTAINS:
	        case VERIFY_STORED_VALUE_NOT_CONTAINS:
	        	try {
	        		success = !this.performStoredValueComparison(testStep.getAttributeValue(), Comparison.CONTAINS);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case IF_STORED_VALUE_NOT_CONTAINS_ELEMENT_VALUE:
	        case VERIFY_STORED_VALUE_NOT_CONTAINS_ELEMENT_VALUE:
	        	try {
	        		if (StringUtils.isEmpty(testStep.getAttributeValue())) { 
	        			throw new IllegalArgumentException(TEXT_ATTR_VALUE_CANNOT_BE_BLANK);
	        		}
	        		if (!this.cache.contains(testStep.getAttributeValue())) {
	        			throw new IllegalArgumentException(TEXT_PREFIX_KEY + testStep.getAttributeValue() + TEXT_NOT_FOUND_IN_CACHE);
	        		}
	        		targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
	        		success = !this.performValueComparison(this.cache.getCachedValue(testStep.getAttributeValue()), webElementFinder.getText(targetElement), Comparison.CONTAINS);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case IF_STORED_VALUE_NOT_EMPTY:
	        case VERIFY_STORED_VALUE_NOT_EMPTY:
	        	try {
	        		success = this.cache.contains(testStep.getAttributeValue().trim()) && StringUtils.isNotEmpty(this.cache.getCachedValue(testStep.getAttributeValue().trim()));
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case IF_STORED_VALUE_NOT_EQUALS:
	        case VERIFY_STORED_VALUE_NOT_EQUALS:
	        	try {
	        		success = !this.performStoredValueComparison(testStep.getAttributeValue(), Comparison.EQUALS);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case IF_STORED_VALUE_NOT_STARTS_WITH:
	        case VERIFY_STORED_VALUE_NOT_STARTS_WITH:
	        	try {
	        		success = !this.performStoredValueComparison(testStep.getAttributeValue(), Comparison.STARTS_WITH);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case IF_STORED_VALUE_NOT_STARTS_WITH_ELEMENT_VALUE:
	        case VERIFY_STORED_VALUE_NOT_STARTS_WITH_ELEMENT_VALUE:
	        	try {
	        		if (StringUtils.isEmpty(testStep.getAttributeValue())) { 
	        			throw new IllegalArgumentException(TEXT_ATTR_VALUE_CANNOT_BE_BLANK);
	        		}
	        		if (!this.cache.contains(testStep.getAttributeValue())) {
	        			throw new IllegalArgumentException(TEXT_PREFIX_KEY + testStep.getAttributeValue() + TEXT_NOT_FOUND_IN_CACHE);
	        		}
	        		targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
	        		success = !this.performValueComparison(this.cache.getCachedValue(testStep.getAttributeValue()), webElementFinder.getText(targetElement), Comparison.STARTS_WITH);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case IF_VALUE_CONTAINS:
	        case VERIFY_VALUE_CONTAINS:
	        	try {
	        		success = this.performValueComparison(testStep.getAttributeValue(), Comparison.CONTAINS);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case IF_VALUE_EQUALS:
	        case VERIFY_VALUE_EQUALS:
	        	try {
	        		success = this.performValueComparison(testStep.getAttributeValue(), Comparison.EQUALS);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case IF_VALUE_STARTS_WITH:
	        case VERIFY_VALUE_STARTS_WITH:
	        	try {
	        		success = this.performValueComparison(testStep.getAttributeValue(), Comparison.STARTS_WITH);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case IF_VALUE_NOT_CONTAINS:
	        case VERIFY_VALUE_NOT_CONTAINS:
	        	try {
	        		success = !this.performValueComparison(testStep.getAttributeValue(), Comparison.CONTAINS);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case IF_VALUE_NOT_EQUALS:
	        case VERIFY_VALUE_NOT_EQUALS:
	        	try {
	        		success = !this.performValueComparison(testStep.getAttributeValue(), Comparison.EQUALS);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case IF_VALUE_NOT_STARTS_WITH:
	        case VERIFY_VALUE_NOT_STARTS_WITH:
	        	try {
	        		success = !this.performValueComparison(testStep.getAttributeValue(), Comparison.STARTS_WITH);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case IF_UNSELECTED:
	        case VERIFY_UNSELECTED:
	        	try {
	        		success = webElementFinder.isUnselected(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

    		case INFO:
    			testStepResult = new TestStepResult(testStep, true);
	        	break;

	        case LOCATE_ELEMENTS:
	        	try {
		        	LocatorUtil.locateAllElements(driver);
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case PERFORM:
	        	try {
	        		SimpleFunctionResults results = CustomFunctionUtil.perform(testStep, uiLocators, cache, scenario, driver);
	           		testStepResult = new TestStepResult(testStep, results.isSuccess() && !results.isErrorMessageDisplayed());
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case PERFORM_AND_EXIT_IF_FALSE:
	        case PERFORM_AND_EXIT_TESTSHEET_IF_FALSE:
	        	try {
	        		SimpleFunctionResults results = CustomFunctionUtil.perform(testStep, uiLocators, cache, scenario, driver);
	           		testStepResult = new TestStepResult(testStep, results.isSuccess());
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case PERFORM_AND_IF_FALSE:
	        	try {
	        		SimpleFunctionResults results = CustomFunctionUtil.perform(testStep, uiLocators, cache, scenario, driver);
	           		testStepResult = new TestStepResult(testStep, results.isSuccess());
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case PERFORM_AND_IF_TRUE:
	        	try {
	        		SimpleFunctionResults results = CustomFunctionUtil.perform(testStep, uiLocators, cache, scenario, driver);
	           		testStepResult = new TestStepResult(testStep, results.isSuccess());
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case PERFORM_FOR_EACH:
	        	try {
	        		SimpleFunctionResults results = CustomFunctionUtil.performForEach(testStep, uiLocators, cache, scenario, driver);
	        		testStepResult = new TestStepResult(testStep, results.isSuccess());
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case REFRESH_FRAME:
	        	try {
		        	webElementFinder.refreshFrame(testStep);
		        	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case REFRESH_FRAME_RD3910:
	        	try {
		        	webElementFinder.refreshFrameRD3910(testStep);
		        	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case RIGHT_CLICK:
    			try {
	        		targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
	            	LOGGER.log(java.util.logging.Level.INFO, "RIGHT-CLICKING element for: {0}", testStep);
		        	new Actions(driver).contextClick(targetElement).build().perform();
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

        			ExecutionUtil.sleep(this.getTimeoutInMillis(500));

    			} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case SAVE_DATA_PROPERTY:

	        	try {
		        	attrTokens = testStep.getOperation().getParser().parse(testStep.getAttributeValue(), FlynetConstants.PIPE);

		        	this.testData.saveDataProperty(attrTokens.getPrimaryString(), attrTokens.getSecondaryString());
		        	testStepResult = new TestStepResult(testStep, true);

	        	} catch (Exception e) {
		        	testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;
	        	
	        case SCREENSHOT:
	       		testStepResult = new TestStepResult(testStep, true);
	            break;

	        case SCREENSHOT_VIA_ROBOT:
	       		testStepResult = new TestStepResult(testStep, true);
	            break;

            case SCROLL_INTO_VIEW:
                try {
                    // turn off hidden element filter
                    boolean currentExcludeValue = webElementFinder.isExcludeHiddenElements();
                    webElementFinder.setExcludeHiddenElements(false);

                    // find the desired element
                    targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);

                    // Use javascript to scroll into view for desired element
                    ((JavascriptExecutor) driver).executeScript(
                        "arguments[0].scrollIntoView()",
                            targetElement);

                    // restore previous value of hidden element filter
                    webElementFinder.setExcludeHiddenElements(currentExcludeValue);

                    // create test step result
                    testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
                } catch (Exception e) {
                    testStepResult = new TestStepResult(testStep, e);
                }
                break;

	        case SCROLL_TO_BOTTOM:
	        	try {
		        	targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
		        	targetElement.sendKeys(Keys.END);
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case SCROLL_TO_TOP:
	        	try {
		        	targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
		        	targetElement.sendKeys(Keys.HOME);
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case SELECT_BY_INDEX:
	        	try {
	            	try {
			        	targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
			        	this.getSelectInstance(targetElement).selectByIndex(Integer.parseInt(testStep.getAttributeValue()));
	            		
	            	/*
	            	 * BAG - 8/4/20 - Handle StaleElementReferenceException that may occur due to element being refreshed between
	            	 * locating it and selecting it.
	            	 */
	            	} catch (StaleElementReferenceException sere) {
				        LOGGER.log(java.util.logging.Level.INFO, TEXT_HANDLING_SEE_EXCEPTION);
			        	targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
			        	this.getSelectInstance(targetElement).selectByIndex(Integer.parseInt(testStep.getAttributeValue()));
					}
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case SELECT_BY_RANDOM:
	        	try {
	            	try {
			        	targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
			        	ISelect select = this.getSelectInstance(targetElement);

			        	int randomIndex = ThreadLocalRandom.current().nextInt(1, select.getOptions().size()); //NOSONAR
		        		LOGGER.log(java.util.logging.Level.INFO, "Selecting option at randomly generated index: {0}", randomIndex);

			        	this.getSelectInstance(targetElement).selectByIndex(randomIndex);
	            		
	            	/*
	            	 * BAG - 8/4/20 - Handle StaleElementReferenceException that may occur due to element being refreshed between
	            	 * locating it and selecting it.
	            	 */
	            	} catch (StaleElementReferenceException sere) {
				        LOGGER.log(java.util.logging.Level.INFO, TEXT_HANDLING_SEE_EXCEPTION);
			        	targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
			        	ISelect select = this.getSelectInstance(targetElement);

			        	int randomIndex = ThreadLocalRandom.current().nextInt(1, select.getOptions().size()); //NOSONAR
		        		LOGGER.log(java.util.logging.Level.INFO, "Selecting option at randomly generated index: {0}", randomIndex);

			        	this.getSelectInstance(targetElement).selectByIndex(randomIndex);
					}
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case SELECT_BY_VALUE:
	        	try {
	            	try {
			        	targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
			        	this.getSelectInstance(targetElement).selectByValue(testStep.getAttributeValue());
	            		
	            	/*
	            	 * BAG - 8/4/20 - Handle StaleElementReferenceException that may occur due to element being refreshed between
	            	 * locating it and selecting it.
	            	 */
	            	} catch (StaleElementReferenceException sere) {
				        LOGGER.log(java.util.logging.Level.INFO, TEXT_HANDLING_SEE_EXCEPTION);
			        	targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
			        	this.getSelectInstance(targetElement).selectByValue(testStep.getAttributeValue());
					}
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case SELECT_BY_VISIBLE_TEXT:
	        	try {
	            	try {
			        	targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
			        	this.getSelectInstance(targetElement).selectByVisibleText(testStep.getAttributeValue());
	            		
	            	/*
	            	 * BAG - 8/4/20 - Handle StaleElementReferenceException that may occur due to element being refreshed between
	            	 * locating it and selecting it.
	            	 */
	            	} catch (StaleElementReferenceException sere) {
				        LOGGER.log(java.util.logging.Level.INFO, TEXT_HANDLING_SEE_EXCEPTION);
			        	targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
			        	this.getSelectInstance(targetElement).selectByVisibleText(testStep.getAttributeValue());
					}
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case SELECT_INDEX_FROM_CACHE:
	        	try {
	            	try {
			        	targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
			        	/*
			        	 * BAG - 5/11/20 - We retrieve the original attributeKey and not the attributeValue, since the attributeValue
			        	 * may have been modified above in the 'cache key replacement logic'.
			        	 */
		            	this.getSelectInstance(targetElement).selectByIndex(Integer.parseInt(this.cache.getCachedValue(testStep.getAttributeKey())));
	            		
	            	/*
	            	 * BAG - 8/4/20 - Handle StaleElementReferenceException that may occur due to element being refreshed between
	            	 * locating it and selecting it.
	            	 */
	            	} catch (StaleElementReferenceException sere) {
				        LOGGER.log(java.util.logging.Level.INFO, TEXT_HANDLING_SEE_EXCEPTION);
			        	targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
			        	/*
			        	 * BAG - 5/11/20 - We retrieve the original attributeKey and not the attributeValue, since the attributeValue
			        	 * may have been modified above in the 'cache key replacement logic'.
			        	 */
		            	this.getSelectInstance(targetElement).selectByIndex(Integer.parseInt(this.cache.getCachedValue(testStep.getAttributeKey())));
					}
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case SELECT_TEXT_FROM_CACHE:
	        	try {
	            	try {
			        	targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
			        	/*
			        	 * BAG - 5/11/20 - We retrieve the original attributeKey and not the attributeValue, since the attributeValue
			        	 * may have been modified above in the 'cache key replacement logic'.
			        	 */
			        	this.getSelectInstance(targetElement).selectByVisibleText(this.cache.getCachedValue(testStep.getAttributeKey()));
	            		
	            	/*
	            	 * BAG - 8/4/20 - Handle StaleElementReferenceException that may occur due to element being refreshed between
	            	 * locating it and selecting it.
	            	 */
	            	} catch (StaleElementReferenceException sere) {
				        LOGGER.log(java.util.logging.Level.INFO, TEXT_HANDLING_SEE_EXCEPTION);
			        	targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
			        	/*
			        	 * BAG - 5/11/20 - We retrieve the original attributeKey and not the attributeValue, since the attributeValue
			        	 * may have been modified above in the 'cache key replacement logic'.
			        	 */
			        	this.getSelectInstance(targetElement).selectByVisibleText(this.cache.getCachedValue(testStep.getAttributeKey()));
					}
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case SELECT_VALUE_FROM_CACHE:
	        	try {
	            	try {
			        	targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
			        	/*
			        	 * BAG - 5/11/20 - We retrieve the original attributeKey and not the attributeValue, since the attributeValue
			        	 * may have been modified above in the 'cache key replacement logic'.
			        	 */
			        	this.getSelectInstance(targetElement).selectByValue(this.cache.getCachedValue(testStep.getAttributeKey()));
	            		
	            	/*
	            	 * BAG - 8/4/20 - Handle StaleElementReferenceException that may occur due to element being refreshed between
	            	 * locating it and selecting it.
	            	 */
	            	} catch (StaleElementReferenceException sere) {
				        LOGGER.log(java.util.logging.Level.INFO, TEXT_HANDLING_SEE_EXCEPTION);
			        	targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
			        	/*
			        	 * BAG - 5/11/20 - We retrieve the original attributeKey and not the attributeValue, since the attributeValue
			        	 * may have been modified above in the 'cache key replacement logic'.
			        	 */
			        	this.getSelectInstance(targetElement).selectByValue(this.cache.getCachedValue(testStep.getAttributeKey()));
					}
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case SETTEXT_FROM_CACHE:
	        	try {
		        	/*
		        	 * BAG - 5/11/20 - We retrieve the original attributeKey and not the attributeValue, since the attributeValue
		        	 * may have been modified above in the 'cache key replacement logic'.
		        	 */
	        		webElementFinder.setText(testStep, this.cache.getCachedValue(testStep.getAttributeKey()));
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case SETTEXT:
	        	try {
	        		webElementFinder.setText(testStep, testStep.getAttributeValue());
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case SKIP_TO_TESTCASE:
           		testStepResult = new TestStepResult(testStep, true);
	        	break;

	        case STORE_CURRENT_TIMESTAMP:

        		String currentTimestamp = DateUtil.getCurrentDate("yyyy-MM-dd HH:mm:ss");
    			/*
    			 * If a value was defined in the Attribute Key field, use it as the cache key.
    			 * Otherwise, use the default cache key.
    			 */
        		if (testStep.hasAttributeValue()) {
        			this.cache.save(testStep.getAttributeValue(), currentTimestamp);
        		} else {
        			this.cache.save(DEFAULT_CACHE_KEY, currentTimestamp);
        		}
        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	break;

	        case STORE_DATE_AS_KEY:
	        	try {
	        		if (!testStep.hasAttributeValue()) {
	        	    	throw new IllegalArgumentException("Attribute value missing for test step: " + testStep);
	        		}

	        		targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
	        		String elementValue = webElementFinder.getText(targetElement);
	        		
	        		Date elementTextToDate = DateUtil.convertToDate(elementValue);
	        		
	        		if (elementTextToDate == null) {
	        			throw new IllegalArgumentException("Element text is not in a valid date format: " + elementValue);
	        		}
	        		
	        		/*
	        		 * This will save the date as MM/DD/YYYY and YYYY-MM-DD formats, using the attributeValue as the cache key prefix:
	        		 * 
	        		 * <cache-key>_MMDDYYYY
	        		 * <cache-key>_YYYYMMDD
	        		 */
	    			CacheUtil.saveDateToCache(testStep.getAttributeValue().trim(), elementTextToDate, this.getCache());

	            	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case STORE_TIME:
	        	try {

	        		/*
	        		 * This assumes an attributeValue in the format [store_key][|[DAY or MONTH or YEAR|[unit number]|[date_format]]
	        		 */
	        		String[] values = StringUtils.split(testStep.getAttributeValue(), PIPE);
	        		LOGGER.log(java.util.logging.Level.INFO, "values: {0}", values);

 	        		String time = ExecutionUtil.getCalendarTime(values.length > 1 ? StringUtils.substringAfter(testStep.getAttributeValue(), PIPE) : null);

 	        		// Get key to store in
        		 	this.cache.save(values[0], time);

        		 	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

 	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case STORE_LEFT_CHARS:
	        	try {
	        		targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
	        		String storedValue = webElementFinder.getText(targetElement);

	        		attrTokens = testStep.getOperation().getParser().parse(testStep.getAttributeValue(), PIPE);

	        	    /*
	        	     * Initially assume that there's only one argument in the attribute field, which
	        	     * should be the number of leftmost characters to be saved.
	        	     */
	        	    int charLen = attrTokens.getPrimaryNumeric() < storedValue.length() ? attrTokens.getPrimaryNumeric() : storedValue.length();
	        	    String cacheKey = attrTokens.hasString() ? attrTokens.getString() : DEFAULT_CACHE_KEY;

	        		LOGGER.log(java.util.logging.Level.INFO, "storedValue|charLen|cacheKey: {0}|{1}|{2}", new Object[]{storedValue, charLen, cacheKey});

	             	storedValue = StringUtils.left(storedValue, charLen);
        			testStep.setAttributeValue(storedValue);
	        		this.cache.save(cacheKey, storedValue);

	            	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case STORE_RIGHT_CHARS:
	        	try {
	        		targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
	        		String storedValue = webElementFinder.getText(targetElement);

	        		attrTokens = testStep.getOperation().getParser().parse(testStep.getAttributeValue(), PIPE);

	        	    /*
	        	     * Initially assume that there's only one argument in the attribute field, which
	        	     * should be the number of leftmost characters to be saved.
	        	     */
	        	    int charLen = attrTokens.getPrimaryNumeric() < storedValue.length() ? attrTokens.getPrimaryNumeric() : storedValue.length();
	        	    String cacheKey = attrTokens.hasString() ? attrTokens.getString() : DEFAULT_CACHE_KEY;

	        		LOGGER.log(java.util.logging.Level.INFO, "storedValue|charLen|cacheKey: {0}|{1}|{2}", new Object[]{storedValue, charLen, cacheKey});

       				storedValue = StringUtils.right(storedValue, charLen);
	        		testStep.setAttributeValue(storedValue);
	        		this.cache.save(cacheKey, storedValue);

	            	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case STORE_SUBSTR:
	        	try {
	        		targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
	        		String storedValue = webElementFinder.getText(targetElement);

	        		attrTokens = testStep.getOperation().getParser().parse(testStep.getAttributeValue(), PIPE);

	        		/*
	        		 * Set the start index to zero if the value is not less than the length of the attribute value.
	        		 * Set the end index to the length of the attribute value if the value is less than the start index.
	        		 */
	        	    int startIdx = attrTokens.getPrimaryNumeric() < storedValue.length() ? attrTokens.getPrimaryNumeric() : 0;
	        	    int endIdx = attrTokens.getSecondaryNumeric() > startIdx ? attrTokens.getSecondaryNumeric() : storedValue.length();
	        	    String cacheKey = attrTokens.hasString() ? attrTokens.getString() : DEFAULT_CACHE_KEY;

	        		LOGGER.log(java.util.logging.Level.INFO, "storedValue|startIdx|endIdx|cacheKey: {0}|{1}|{2}|{3}", new Object[]{storedValue, startIdx, endIdx, cacheKey});
	      
       				storedValue = StringUtils.substring(storedValue, startIdx, endIdx);
	        		testStep.setAttributeValue(storedValue);
	        		this.cache.save(cacheKey, storedValue);

	            	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case STORE_VALUE:
	        	try {
	        		targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
	        		String theValue = webElementFinder.getText(targetElement);

	        		/*
	        		 * BAG - 2/10/20 - Pass true to enable persistence of all space-delimited tokens under unique cache keys.
	        		 */
	        		if (testStep.hasAttributeValue()) {
	        			this.cache.save(testStep.getAttributeValue(), theValue, true);
	        		} else {
		        		this.cache.save(DEFAULT_CACHE_KEY, theValue, true);
	        		}

	            	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case STORE_VALUE_AS_KEY:
	        	try {
	        		if (!testStep.hasAttributeValue()) {
	        	    	throw new IllegalArgumentException("Attribute value missing for test step: " + testStep);
	        		}

	        		StringTokenizer parameters = new StringTokenizer(testStep.getAttributeValue(), ExecutionConstants.PIPE);
	        		if (parameters.countTokens() < 2) {
	        	    	throw new IllegalArgumentException("Attribute value must contain 2 pipe-delimited tokens. Actual: " + parameters.countTokens());
	        		}

	        		String value = parameters.nextToken();
	        		String key = parameters.nextToken();

	        		/*
	        		 * BAG - 2/10/20 - Pass true to enable persistence of all space-delimited tokens under unique cache keys.
	        		 */
        			this.cache.save(key, value, true);

	            	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	            	
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case STORE_CHILD_VALUE:
	        	try {
	        		targetElement = webElementFinder.getElementForStep(testStep, targetElement);
	        		String theValue = webElementFinder.getText(targetElement);

	        		if (testStep.hasAttributeValue()) {
	        			this.cache.save(testStep.getAttributeValue(), theValue);
	        		} else {
		        		this.cache.save(DEFAULT_CACHE_KEY, theValue);
	        		}

	            	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case STORE_VALUE_BY_INDEX:
	        	try {
	        		targetElement = webElementFinder.findByIndex(testStep, this.overrideWaitTimeoutInSeconds);
	        		String storedValue = webElementFinder.getText(targetElement);
	        		testStep.setAttributeValue(storedValue);
	        		this.cache.save(DEFAULT_CACHE_KEY, storedValue);

	            	testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);

	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case SWITCH_TO_DEFAULT_CONTENT:
           		driver.switchTo().defaultContent();
        		testStepResult = new TestStepResult(testStep, true);
	            break;

	        case SWITCH_TO_FRAME:
           		testStepResult = new TestStepResult(testStep, webElementFinder.switchToFrame(testStep.getAttributeValue()));
	            break;

	        case VERIFY:
	        	try {
	        		webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);
	        	} catch (Exception e) {
	            	LOGGER.log(java.util.logging.Level.WARNING, e.getMessage(), e);
	        	}
        		testStepResult = new TestStepResult(testStep, true);
	            break;

	        case VERIFY_CHILD_ABSENT:
	        	try {
	        		success = webElementFinder.isAbsent(testStep, targetElement);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case VERIFY_CHILD_COUNT:
	        	try {
	        		success = webElementFinder.verifyCount(testStep, targetElement);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case VERIFY_CHILD_PRESENT:
	        	try {
	        		success = webElementFinder.isPresent(testStep, targetElement);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case VERIFY_CHILD_NOT_CONTAINS_VALUE:
	        	try {
	        		success = !webElementFinder.childContainsValue(testStep, targetElement);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case VERIFY_HELPTEXT:
	        	try {
	        		success = webElementFinder.verifyHelpText(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case VERIFY_INVALID_BY_CSS:
	        	try {
	        		success = webElementFinder.isInvalid(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case VERIFY_LOCATORS:
	            LocatorUtil.verifyLocators(testStep, uiLocators, driver, scenario);
    			testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	            break;

            case IF_PSEUDO_ELEMENT_CONTAINS:
            case VERIFY_PSEUDO_ELEMENT_CONTAINS:
                try {
                    // find the desired element
                    targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);

                    AttributeTokens tokens = testStep.getOperation().getParser().parse(testStep.getAttributeValue(), FlynetConstants.PIPE);

                    String pseudoContent = webElementFinder.getPseudoContent(targetElement, tokens.getPrimaryString());

                    LOGGER.log(Level.INFO, "Comparing value of pseudoContent [{0}] to attribute value passed in [{1}]", new Object[] {pseudoContent, tokens.getSecondaryString()} );
                    success = StringUtils.containsIgnoreCase(pseudoContent, tokens.getSecondaryString());

                    // create test step result
                    testStepResult = new TestStepResult(testStep, success);
                } catch (Exception e ) {
                    testStepResult = new TestStepResult(testStep, e);
                }
                break;

            case IF_PSEUDO_ELEMENT_EQUALS:
            case VERIFY_PSEUDO_ELEMENT_EQUALS:
                try {
                    // find the desired element
                    targetElement = webElementFinder.waitForElementForStep(testStep, this.overrideWaitTimeoutInSeconds);

                    AttributeTokens tokens = testStep.getOperation().getParser().parse(testStep.getAttributeValue(), FlynetConstants.PIPE);

                    String pseudoContent = webElementFinder.getPseudoContent(targetElement, tokens.getPrimaryString());

                    LOGGER.log(Level.INFO, "Comparing value of pseudoContent [{0}] to attribute value passed in [{1}]", new Object[] {pseudoContent, tokens.getSecondaryString()} );
                    success = StringUtils.equalsIgnoreCase(pseudoContent, tokens.getSecondaryString());

                    // create test step result
                    testStepResult = new TestStepResult(testStep, success);
                } catch (Exception e ) {
                    testStepResult = new TestStepResult(testStep, e);
                }
                break;

	        case VERIFY_OPTIONAL_BY_CSS:
	        	try {
	        		success = webElementFinder.isOptional(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case VERIFY_URL_CONTAINS:
        		LOGGER.log(java.util.logging.Level.INFO, "comparing attribute value [{0}] against current URL: {1}", new Object[]{testStep.getAttributeValue(), driver.getCurrentUrl()});
        		testStepResult = new TestStepResult(testStep, driver.getCurrentUrl().toLowerCase().contains(testStep.getAttributeValue().toLowerCase()));
	            break;

	        case VERIFY_VALID_BY_CSS:
	        	try {
	        		success = webElementFinder.isValid(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	        	break;

	        case WAIT:
	        	try {
	        		ExecutionUtil.sleep(Integer.parseInt(testStep.getAttributeValue()));
	    			testStepResult = new TestStepResult(testStep, true);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case WAIT_UNTIL_ABSENT:
	        	try {
	        		success = webElementFinder.waitUntilAbsent(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case WAIT_UNTIL_CONTAINS_VALUE:
	        	try {
	        		success = webElementFinder.waitUntilContainsValue(testStep, this.overrideWaitTimeoutInSeconds);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case WAIT_UNTIL_DISABLED:
	        	try {
	        		success = webElementFinder.waitUntilDisabled(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case WAIT_UNTIL_EMPTY:
	        	try {
	        		success = webElementFinder.waitUntilEmpty(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case WAIT_UNTIL_ENABLED:
	        	try {
	        		success = webElementFinder.waitUntilEnabled(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case WAIT_UNTIL_PRESENT:
	        	try {
	        		success = webElementFinder.waitUntilPresent(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case WAIT_FOR_DESKTOP_MODE:
	        	try {
	        		success = webElementFinder.waitForValue(testStep);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (UnhandledAlertException uae) {
	        		this.handleAlertIfPresent(testStep);
	        		testStepResult = new TestStepResult(testStep, Outcome.SUCCESS);
	        	} catch (Exception e) {
	        		LOGGER.log(java.util.logging.Level.SEVERE, e.getMessage(), e);
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case WAIT_FOR_VALUE:
	        	try {
	        		success = webElementFinder.waitForValue(testStep, 30);
	        		testStepResult = new TestStepResult(testStep, success);
	        	} catch (Exception e) {
	        		LOGGER.log(java.util.logging.Level.SEVERE, e.getMessage(), e);
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

    		case WARNING:
    			testStepResult = new TestStepResult(testStep, false);
	        	break;

	        case WARN_IF_PRESENT:
	        	try {
	        		/*
	        		 * Both WARN_IF_PRESENT and WARN_IF_ABSENT are handled differently, since it's really the combination of
	        		 * IF_PRESENT/IF_ABSENT and WARNING, meaning the 'warning' occurs if the condition is TRUE, not FALSE. See
	        		 * ExecutionUtil.determineOutcome() for more details.
	        		 */
	        		testStepResult = new TestStepResult(testStep, webElementFinder.isPresent(testStep));
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case WARN_IF_ABSENT:
	        	try {
	        		/*
	        		 * Both WARN_IF_PRESENT and WARN_IF_ABSENT are handled differently, since it's really the combination of
	        		 * IF_PRESENT/IF_ABSENT and WARNING, meaning the 'warning' occurs if the condition is TRUE, not FALSE. See
	        		 * ExecutionUtil.determineOutcome() for more details.
	        		 */
	        		testStepResult = new TestStepResult(testStep, webElementFinder.isAbsent(testStep));
	        	} catch (Exception e) {
	        		testStepResult = new TestStepResult(testStep, e);
	        	}
	            break;

	        case WINDOW_SWITCHING_DISABLED:
        		webElementFinder.setWindowSwitchingEnabled(false);
        		testStepResult = new TestStepResult(testStep, true);
	            break;

	        case WINDOW_SWITCHING_ENABLED:
        		webElementFinder.setWindowSwitchingEnabled(true);
        		testStepResult = new TestStepResult(testStep, true);
	            break;

	        default:

        	if (testStep.getOperation().isFlynetOperation()) {
        		testStepResult = this.getFlynetOperationHandler().process(testStep, driver);
        	}
        	else if (testStep.getOperation().isMouseOperation()) {
        		testStepResult = this.getMouseOperationHandler().process(testStep, driver);
        	}
        	else if (scenario.isRemoteBrowser()) {
            	testStepResult = new TestStepResult(testStep, new ScriptException("Operation [" + testStep.getOperation()
            		+ "] not allowed for BrowserChoice(s): " + scenario.getBrowserChoices()));
            }
            else  if (testStep.getOperation().isKeyboardOperation()) {
        		testStepResult = this.getKeyboardOperationHandler().process(testStep, driver);
        	}
        	else if (testStep.getOperation().isSAPOperation()) {
        		testStepResult = this.getSapOperationHandler().process(testStep, driver);
        	}
        	else if (testStep.getOperation().isLanFaxOperation()) {
        		testStepResult = this.getLanFaxOperationHandler().process(testStep, driver);
        	}
        	else if (testStep.getOperation().isJSMOperation()) {
        		testStepResult = this.getJsmOperationHandler().process(testStep);
        	}
        	else if (testStep.getOperation().isAndroidOperation()) {
        		testStepResult = this.getAndroidOperationHandler().process(testStep, driver);
        	}
        	else if (testStep.getOperation().isIOSOperation()) {
        		testStepResult = this.getIOSOperationHandler().process(testStep, driver);
        	}
            break;
        }

		/*
		 * Take a screenshot for warning 'successes'
		 */
		if (testStepResult != null && testStep.getOperation().isWarningOperation() && (testStepResult.isSuccess() || testStepResult.isWarning())) {
			this.takeScreenshot(testStepResult.getTestStep(), driver);
		}
		/*
		 * Take a screenshot for all 'non-successes' (except for INFO) or screenshot operations.
		 */
		else if (testStepResult != null && ((!testStepResult.isSuccess() && !testStepResult.isInfo()) || testStepResult.isScreenshot())) {
			this.handleThrowableAndTakeScreenshot(testStepResult);
		}
		else if (testStep.hasPostOperationCode() && testStep.getPostOperationCode().equals(PostOperationCode.S)) {
			this.takeScreenshot(testStepResult.getTestStep(), driver);
    	}
		else if (testStep.hasPostOperationCode() && testStep.getPostOperationCode().equals(PostOperationCode.H)) {
			this.haltExecution();
    	}
		/*
		 * BAG - 6/25/20 - Take a screenshot for all WAIT operations.
		 */
		else if (testStep.getOperation().isWaitOperation()) {
			this.takeScreenshot(testStepResult.getTestStep(), driver);
		}
		
		/*
		 * BAG - 6/11/20 - Save off the previous/current URLs to cache.
		 */
        if (!scenario.isMobile()) {
    		if (this.cache.contains(KEY_CURRENT_URL)) {
    			this.cache.save(KEY_PREVIOUS_URL, this.cache.get(KEY_CURRENT_URL));
            }
            try {
                this.cache.save(KEY_CURRENT_URL, driver.getCurrentUrl());
            } catch (NoSuchWindowException nswException) {
                LOGGER.log(java.util.logging.Level.WARNING, "NoSuchWindowException thrown when trying to save off the current URL. "
                	+ "This is most likely caused by a CLOSE_WINDOW or similar operation.");
            }
        }
		
    	/*
    	 * 'Turn off' the inclusion of hidden elements that was set only for this operation.
    	 */
		if (PreOperationCode.H.equals(testStep.getPreOperationCode())) {
			webElementFinder.setExcludeHiddenElements(true);
		}

		/*
		 * BAG - 4/1/19 - Halt execution after the first TestStep if requested to do so.
		 */
		if (scenario.isHaltExecutionAfterFirstOperation() && !executionHaltedAfterFirstOperation) {
			this.haltExecution();
			executionHaltedAfterFirstOperation = true;
    	}

    	return testStepResult;
    }

    private List<CachedDate> getCachedDatesForAddOperation(AttributeTokens attrTokens) {

    	List<CachedDate> list = new ArrayList<>();
    	
		String cacheKey = attrTokens.getPrimaryString();
		
		if (this.cache.contains(cacheKey)) {
			String cachedDateStr = this.cache.getCachedValue(cacheKey);
			Date cachedDate = DateUtil.convertToDate(cachedDateStr);
			String dateFormatStr = CacheUtil.getFormatFor(cachedDateStr);
			
			if (cachedDate == null) {
				throw new IllegalArgumentException(TEXT_CACHED_VALUE_INVALID_DATE + cachedDateStr);
			}
			
			list.add(new CachedDate(cachedDate, dateFormatStr, StringUtils.EMPTY));
		}

		cacheKey = attrTokens.getPrimaryString() + VALUE_IDENTIFIER_MMDDYYYY;
		
		if (this.cache.contains(cacheKey)) {
			String cachedDateStr = this.cache.getCachedValue(cacheKey);
			Date cachedDate = DateUtil.convertToDate(cachedDateStr);
			String dateFormatStr = CacheUtil.getFormatFor(cachedDateStr);
			
			if (cachedDate == null) {
				throw new IllegalArgumentException(TEXT_CACHED_VALUE_INVALID_DATE + cachedDateStr);
			}
			
			list.add(new CachedDate(cachedDate, dateFormatStr, VALUE_IDENTIFIER_MMDDYYYY));
		}

		cacheKey = attrTokens.getPrimaryString() + VALUE_IDENTIFIER_YYYYMMDD;
		
		if (this.cache.contains(cacheKey)) {
			String cachedDateStr = this.cache.getCachedValue(cacheKey);
			Date cachedDate = DateUtil.convertToDate(cachedDateStr);
			String dateFormatStr = CacheUtil.getFormatFor(cachedDateStr);
			
			if (cachedDate == null) {
				throw new IllegalArgumentException(TEXT_CACHED_VALUE_INVALID_DATE + cachedDateStr);
			}
			
			list.add(new CachedDate(cachedDate, dateFormatStr, VALUE_IDENTIFIER_YYYYMMDD));
		}

		if (list.isEmpty()) {
			throw new IllegalArgumentException("No dates for cache key prefix [" + attrTokens.getPrimaryString() + "] exist in cache");
		}

		return list;
	}

	private boolean performValueComparison(String attributeValue, Comparison comparison) {

		if (StringUtils.isEmpty(attributeValue)) { 
			throw new IllegalArgumentException("attribute value must contain 2 pipe-delimited tokens in the format [literal, cache key, data prop key]|[literal, cache key, data prop key]");
		}

		String[] values = StringUtils.split(attributeValue, ExecutionConstants.PIPE);

		if (values.length != 2) {
			throw new IllegalArgumentException("attribute value must contain 2 pipe-delimited tokens in the format [literal, cache key, data prop key]|[literal, cache key, data prop key]");
		}
		return this.performValueComparison(values[0].trim(), values[1].trim(), comparison);
	}

    private boolean performValueComparison(String value1, String value2, Comparison comparison) {

		if (StringUtils.isEmpty(value1)) { 
			throw new IllegalArgumentException("value1 cannot be blank");
		}

		if (StringUtils.isEmpty(value2)) { 
			throw new IllegalArgumentException("value2 cannot be blank");
		}

		LOGGER.log(java.util.logging.Level.INFO, "performing [{0}] comparison for value [{1}] and value [{2}]", 
			new Object[]{comparison, value1, value2});

		if (comparison.isContains()) {
			return value1.toLowerCase().contains(value2.toLowerCase());
		}
		if (comparison.isStartsWith()) {
			return value1.toLowerCase().startsWith(value2.toLowerCase());
		}
		return value1.equalsIgnoreCase(value2);
	}

	private boolean performStoredValueComparison(String attributeValue, Comparison comparison) {

		if (StringUtils.isEmpty(attributeValue)) { 
			throw new IllegalArgumentException("attribute value must contain 2 pipe-delimited tokens in the format [cache key]|[comparison value]");
		}

		String[] values = StringUtils.split(attributeValue, ExecutionConstants.PIPE);

		if (values.length != 2) {
			throw new IllegalArgumentException("attribute value must contain 2 pipe-delimited tokens in the format [cache key]|[comparison value]");
		}
		
		String cacheKey = values[0].trim();
		
		if (!this.cache.contains(cacheKey)) {
			throw new IllegalArgumentException(TEXT_PREFIX_KEY + cacheKey + TEXT_NOT_FOUND_IN_CACHE);
		}

		return this.performValueComparison(this.cache.getCachedValue(cacheKey).trim(), values[1].trim(), comparison);
	}

	private String waitForNewWindow(Set<String> origWindowHandles, TestStep testStep) throws WindowNotFoundException {

    	int attempts = 0;
    	int waitInSec = testStep.isAttributeValueNumericAndLessThan(WebElementFinder.MAX_WAIT_IN_SEC) ? 
    			testStep.getAttributeValueAsNumeric() : 30;

    	LOGGER.log(java.util.logging.Level.INFO, "waitInSec: {0}", waitInSec);

    	while (driver.getWindowHandles().size() == origWindowHandles.size()) {
    		ExecutionUtil.sleep(this.getTimeoutInMillis(1000));
    		attempts++;

    		if (attempts > waitInSec) {
    			throw new WindowNotFoundException("New window did not appear within the alloted time");
    		}
    	}

    	Set<String> newWindowHandles = driver.getWindowHandles();
    	LOGGER.log(java.util.logging.Level.INFO, "new window handles:    {0}", newWindowHandles);

    	newWindowHandles.removeAll(origWindowHandles);

    	/*
    	 * BAG - 12/27/18 - Save off both the original (ie. current) window handle, as well
    	 * as the window handle for the window that just opened. These handles will be used
    	 * by CLOSE_LAST_WINDOW to a) close the window that was just opened and b) switch
    	 * the driver back to the original window.
    	 */
    	String theWindowHandle = newWindowHandles.iterator().next();
    	LOGGER.log(java.util.logging.Level.INFO, "new window handle: {0}", theWindowHandle);

    	return theWindowHandle;
    }

    private void haltExecution() {

    	synchronized(this) {

			if (!halted) {
				haltedBy = this.getExecutionScenario().getExecutionId();
				halted = true;
			}
		}

		if (haltedBy.equals(this.getExecutionScenario().getExecutionId())) {
	       	LOGGER.log(java.util.logging.Level.INFO, "Execution was halted by Execution Scenario [{0}]. Showing Halt dialog.", haltedBy);
			this.displayHaltExecutionDialog();

        	LOGGER.log(java.util.logging.Level.INFO, "User has responded. Continuing test(s)");
			haltedBy = null;
			halted = false;

		} else {

        	while (halted) {
		       	LOGGER.log(java.util.logging.Level.INFO, "Execution Scenario [{0}] awaiting user response", this.getExecutionScenario().getExecutionId());
        		ExecutionUtil.sleep(this.getTimeoutInMillis(1000));
        	}
		}
	}

    protected void displayHaltExecutionDialog() {
    	JOptionPane.showMessageDialog(new UIFrame("Execution Halted"), "Click [Ok] to resume test(s).");
    }
    
	private void handleThrowableAndTakeScreenshot(TestStepResult testStepResult) {

       	LOGGER.log(java.util.logging.Level.INFO, "testStepResult: {0}", testStepResult);

		if (testStepResult.hasThrowable()) {
			LOGGER.log(java.util.logging.Level.SEVERE, testStepResult.getThrowable().getMessage(), testStepResult.getThrowable());
		}

    	this.takeScreenshot(testStepResult.getTestStep(), this.driver);
    }

    protected void takeScreenshot(TestStep testStep, WebDriver driver) {

    	LOGGER.log(java.util.logging.Level.INFO, "taking Selenium screenshot for testStep: {0}", testStep);

    	String absolutePath = null;

		try {

			if (testStep.getOperation() == Operation.SCREENSHOT_VIA_ROBOT) {
				if (!this.scenario.isRemoteBrowser()) {
					absolutePath = CaptureUtil.takeSnapShot(testStep, this.scenario.getOutputDirectory(), testStep.isScreenshotOperation());
				}
			} 
			else if (this.scenario.isSeleniumScreenshotsEnabled()) {
				absolutePath = CaptureUtil.takeSeleniumScreenshot(testStep, this.scenario.getOutputDirectory(), driver);
			} else {
				if (!this.scenario.isRemoteBrowser()) {
					absolutePath = CaptureUtil.takeSnapShot(testStep, this.scenario.getOutputDirectory(), testStep.isScreenshotOperation());
				}
			}

			if (absolutePath != null) {
				LOGGER.log(java.util.logging.Level.INFO, "screenshot saved to: {0}", absolutePath);
				testStep.setScreenshotAbsolutePath(absolutePath);
			}
		} catch (Exception e) {
			LOGGER.log(java.util.logging.Level.SEVERE, e.getMessage(), e);
		}
    }

    protected boolean handleAlertIfPresent(TestStep testStep) {

        LOGGER.log(Level.INFO, "testStep: {0}", testStep);

        try {
        	/*
        	 * Attempt to switch to the alert popup, which will throw a NoAlertPresentException if
        	 * it doesn't exist.
        	 */
    	    Alert alert = driver.switchTo().alert();

    	    LOGGER.log(java.util.logging.Level.INFO, "Alert text: {0}", alert.getText());
            String alertText = alert.getText();

    	    ExecutionUtil.sleep(this.getAlertCheckWaitInMillis(250));

    		AttributeTokens attrTokens = testStep.getOperation().getParser().parse(testStep.getAttributeValue(), PIPE);

    		LOGGER.log(java.util.logging.Level.INFO, "accept|textToCompare: {0}|{1}", new Object[]{attrTokens.getPrimaryBoolean(), attrTokens.getString()});

    	    if (attrTokens.getPrimaryBoolean()) {
                LOGGER.log(java.util.logging.Level.INFO, "ALERT IS PRESENT. Accepting it.");
        	    alert.accept();
    	    } else {
                LOGGER.log(java.util.logging.Level.INFO, "ALERT IS PRESENT. Cancelling it.");
        	    alert.dismiss();
    	    }

    	    /*
    	     * Compare against the popup text if necessary.
    	     */
    	    if (attrTokens.hasString()) {
                LOGGER.log(java.util.logging.Level.INFO, "checking Alert text for value: {0}", attrTokens.getString());
    	    	return (alertText.toUpperCase().contains(attrTokens.getString().toUpperCase()));
    	    }

    	    return true;
    	}
    	catch (NoAlertPresentException e) {
            LOGGER.log(java.util.logging.Level.INFO, "Alert is not displayed.");
    	    return false;
    	}
    }

	public String getOriginalWindowHandle() {
		return originalWindowHandle;
	}

	public String getLastOpenedWindowHandle() {
		return lastOpenedWindowHandle;
	}
	
	public int getId() {
		return id;
	}

	public ExecutionCache getCache() {
		return cache;
	}

	public ExecutionScenario getExecutionScenario() {
		return scenario;
	}

	public TestData getTestData() {
		return testData;
	}

	public WebDriver getDriver() {
		return driver;
	}
	
	public UILocators getUILocators() {
		return uiLocators;
	}

	public int getOverrideWaitTimeoutInSeconds() {
		return overrideWaitTimeoutInSeconds;
	}

	public void setOverrideWaitTimeoutInSeconds(int overrideWaitTimeoutInSeconds) {
		this.overrideWaitTimeoutInSeconds = overrideWaitTimeoutInSeconds;
	}

	// For jUnit testing purposes
	public void setWebElementFinder(WebElementFinder webElementFinder) {
		this.webElementFinder = webElementFinder;
	}

	public WebElementFinder getWebElementFinder() {
		return webElementFinder;
	}
	
	public SAPOperationHandler getSapOperationHandler() {
		return sapOperationHandler;
	}

	public KeyboardOperationHandler getKeyboardOperationHandler() {
		return keyboardOperationHandler;
	}

	public MouseOperationHandler getMouseOperationHandler() {
		return mouseOperationHandler;
	}

	public LanFaxOperationHandler getLanFaxOperationHandler() {
		return lanFaxOperationHandler;
	}

	public JSMOperationHandler getJsmOperationHandler() {
		return jsmOperationHandler;
	}
	
	public AndroidOperationHandler getAndroidOperationHandler() {
		return androidOperationHandler;
	}

	public IOSOperationHandler getIOSOperationHandler() {
		return iosOperationHandler;
	}

	public FlynetOperationHandler getFlynetOperationHandler() {
		return flynetOperationHandler;
	}

	/*
	 * Allows for overriding of timeout for jUnit testing.
	 */
	public int getTimeoutInMillis(int defaultTimeoutInMillis) {
		return defaultTimeoutInMillis;
	}

	/*
	 * Allows for overriding of alert check wait for jUnit testing.
	 */
	public int getAlertCheckWaitInMillis(int defaultWaitInMillis) {
		return defaultWaitInMillis;
	}
	
	/*
	 * For jUnit testing (we need to reset this boolean between tests)
	 */
	public static void setExecutionHaltedAfterFirstOperation(boolean executionHaltedAfterFirstOperation) {
		OperationHandler.executionHaltedAfterFirstOperation = executionHaltedAfterFirstOperation;
	}

	protected ISelect getSelectInstance(WebElement element) {
		return new Select(element);	
	}
	
	public String toString() {
		return "[OperationHandler:" + this.getId() + "]" + this.getWebElementFinder();
	}
}
class CachedDate {
	
	private Date date;
	private String format;
	private String keySuffix;
	
	protected CachedDate(Date date, String format, String keySuffix) {
		this.date = date;
		this.format = format;
		this.keySuffix = keySuffix;
	}

	public Date getDate() {
		return date;
	}

	public String getFormat() {
		return format;
	}

	public String getKeySuffix() {
		return keySuffix;
	}
}


