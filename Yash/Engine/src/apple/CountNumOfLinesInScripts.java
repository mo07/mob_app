package apple;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class CountNumOfLinesInScripts {
	public static void main(String[] args) {

		ArrayList<String> folderList = new ArrayList<String>();
		// folderList.add("R:\\ISPortal_PUBLIC\\Grant\\SeleniumEngine\\RD3910_Yash\\scripts");
		folderList.add("/Users/eqe/Desktop/Script Updates/MHTK_Mobile_Shared/Scripts/");


		for (int f = 0; f < folderList.size(); f++) {
			System.out.println("\n*******************     " + folderList.get(f) + "     ***********************\n");
			File folder = new File(folderList.get(f));
			File[] listOfFiles = folder.listFiles();

			int count = 0;
			int rowNum = 0;
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isDirectory())
					continue;
				//System.out.println("File  " + i + "   " + "of " + listOfFiles.length + "  " + listOfFiles[i].getName());
				
				count+= rowNum;
				
				Workbook workbook;


				try {
					workbook = WorkbookFactory
							.create(new FileInputStream(folderList.get(f)  + listOfFiles[i].getName()));
					// Get the first sheet.
					// Sheet sheet = workbook.getSheetAt(0);
					Sheet sheet = workbook.getSheet("TestSteps");
					rowNum = 0;
					for (Row row : sheet) {
						rowNum++;
						Cell c0 = row.getCell(0);
						if (c0 == null) {
							break;
						}
						//System.out.println("Row = " + rowNum);
					}
					System.out.println("File  " + i + "   " + "of " + listOfFiles.length + "  [ " + rowNum + " ]   " + listOfFiles[i].getName());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			System.out.println("Total num of lines = " + count);
		}
	}
}