package apple;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

 

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

 

public class FindScriptByOperaions {

 

    public static void main(String[] args) {
        int searchColumn = 4; // Operation Column
        ArrayList<String> folderList = new ArrayList<String>();
		// folderList.add("R:\\ISPortal_PUBLIC\\Grant\\SeleniumEngine\\RD3910_Yash\\scripts");
		//folderList.add("/Applications/SeleniumEngine/MHTK_Mobile_Shared/Scripts/");
		folderList.add("/Applications/Automation/mob_app/MHTK_Mobile_Shared/Scripts/");

 

        ArrayList<String> operaion = new ArrayList<String>();
        // operaion.add("CLICK|S");
        //operaion.add("OORDINAT");
        // operaion.add("");
        //operaion.add("CLOSE_WINDOW_BY_TITLE");
        //operaion.add("ANDROID_PRESS_ENTER");
        operaion.add("STORE_VALUE_AS_KEY ");

 

        for (int f = 0; f < folderList.size(); f++) {

 

            System.out.println("\n*******************     " + folderList.get(f) + "     ***********************\n");
            File folder = new File(folderList.get(f));
            File[] listOfFiles = folder.listFiles();

 

            int count = 0;
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isDirectory())
                    continue;
				if ( !(listOfFiles[i].getName().contains(".xls")) )
					continue;
                // System.out.println("File " + listOfFiles[i].getName());
                Workbook workbook;
                // int masterSheetColumnIndex = 0;
                try {
                    workbook = WorkbookFactory
                            .create(new FileInputStream(folderList.get(f) +  listOfFiles[i].getName()));
                    // Get the first sheet.
                    // Sheet sheet = workbook.getSheetAt(0);
                    Sheet sheet = workbook.getSheet("TestSteps");

 

                    int rowNum = 1;
                    boolean bSkipSheet = false;
                    int nullRowCount = 0;
                    for (Row row1 : sheet) {
                        if (rowNum == 1) {
                            rowNum++;
                            continue;
                        }
                        rowNum++;
                        Cell c0 = row1.getCell(0);
                        if (c0 == null) {
                            if (++nullRowCount > 30) {
                                break;
                            }

 

                            continue;
                        }
                        Cell c = row1.getCell(searchColumn);
                        if (c != null) {
                            for (int k = 0; k < operaion.size(); k++) {
                                if ((c.getCellType() == CellType.STRING)
                                        && c.getStringCellValue().contains(operaion.get(k).toUpperCase())) {
                                    System.out.println(++count + ") " + listOfFiles[i].getName() + "=" + rowNum + " == "
                                            + operaion.get(k));
                                    bSkipSheet = true;
                                }
                            }
                        }
                        if (bSkipSheet)
                            break;
                    }

 

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}