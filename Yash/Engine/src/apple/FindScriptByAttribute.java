package apple;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class FindScriptByAttribute {
	public static void main(String[] args) {
		boolean bFound = false;
		int searchColumn = 6; // Attribute Key
		ArrayList<String> folderList = new ArrayList<String>();
		// folderList.add("R:\\ISPortal_PUBLIC\\Grant\\SeleniumEngine\\RD3910_Yash\\scripts");
		//folderList.add("/Applications/SeleniumEngine/MHTK_Mobile_Shared/Scripts/");
		folderList.add("/Applications/Automation/mob_app/MHTK_Mobile_Shared/Scripts/");


		ArrayList<String> attribute_key = new ArrayList<String>();

		//attribute_key.add("WEBVIEW_");
		//attribute_key.add("hasPharmacy");
		//attribute_key.add("CHROMIUM");
		//attribute_key.add("webdriver.healthMemberRelationship");
		//attribute_key.add("webdriver.memberDOB.month");
		//attribute_key.add("webdriver.memberAddress");
		//attribute_key.add("webdriver.subID");
		attribute_key.add("isSupportAccess");


		for (int f = 0; f < folderList.size(); f++) {
			System.out.println("\n*******************     " + folderList.get(f) + "     ***********************\n");
			File folder = new File(folderList.get(f));
			File[] listOfFiles = folder.listFiles();

			int count = 0;
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isDirectory())
					continue;
				if ( !(listOfFiles[i].getName().contains(".xls")) )
					continue;
				// System.out.println("File " + listOfFiles[i].getName());
				Workbook workbook;
				// int masterSheetColumnIndex = 0;
				try {
					workbook = WorkbookFactory
							.create(new FileInputStream(folderList.get(f)  + listOfFiles[i].getName()));
					// Get the first sheet.
					// Sheet sheet = workbook.getSheetAt(0);
					Sheet sheet = workbook.getSheet("TestSteps");
					int rowNum = 1;
					for (Row row1 : sheet) {
						if (rowNum == 1) {
							rowNum++;
							continue;
						}
						rowNum++;
						Cell c0 = row1.getCell(0);
						if (c0 == null) {
							break;
						}
						Cell c = row1.getCell(searchColumn);
						if (c != null) {
							for (int k = 0; k < attribute_key.size(); k++) {
								if ((c.getCellType() == CellType.STRING)
										&& c.getStringCellValue().contains(attribute_key.get(k))) {
									System.out.println(++count + ") " + listOfFiles[i].getName() + "=" + rowNum + " == "
											+ c.getStringCellValue());
									bFound = true;
									// System.out.println(c);
								}
							}
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		if (!bFound) {
			System.out.println("No match found\n");
		}
	}
}