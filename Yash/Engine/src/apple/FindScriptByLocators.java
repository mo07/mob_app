package apple;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class FindScriptByLocators {
	public static void main(String[] args) {
		boolean bFound = false;
		int searchColumn = 5; // Locator
		ArrayList<String> folderList = new ArrayList<String>();
		// folderList.add("R:\\ISPortal_PUBLIC\\Grant\\SeleniumEngine\\RD3910_Yash\\scripts");
		//folderList.add("/Applications/SeleniumEngine/MHTK_Mobile_Shared/Scripts/");
		folderList.add("/Applications/Automation/mob_app/MHTK_Mobile_Shared/Scripts/");


		ArrayList<String> locators = new ArrayList<String>();

		//locators.add("locator.common.alert.allow");
		//locators.add("locator.common.findText.usingH2Value");
		//locators.add("locator.login.native.applogo");
		//locators.add("locator.fullSite.healthCoverage");
		//locators.add("locator.errorMsg.networkError");
		//locators.add("locator.homePage.idCardDownloaded.closeBtn");
		//locators.add("locator.common.progressBar");
		locators.add("locator.common.findLink.usingAValue");


		for (int f = 0; f < folderList.size(); f++) {
			System.out.println("\n*******************     " + folderList.get(f) + "     ***********************\n");
			File folder = new File(folderList.get(f));
			File[] listOfFiles = folder.listFiles();

			int count = 0;
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isDirectory())
					continue;
				if ( !(listOfFiles[i].getName().contains(".xls")) )
					continue;
				 //System.out.println("File " + listOfFiles[i].getName());
				Workbook workbook;
				// int masterSheetColumnIndex = 0;
				try {
					workbook = WorkbookFactory
							.create(new FileInputStream(folderList.get(f)  + listOfFiles[i].getName()));
					// Get the first sheet.
					// Sheet sheet = workbook.getSheetAt(0);
					Sheet sheet = workbook.getSheet("TestSteps");
					int rowNum = 1;
					for (Row row1 : sheet) {
						if (rowNum == 1) {
							rowNum++;
							continue;
						}
						rowNum++;
						Cell c0 = row1.getCell(0);
						if (c0 == null) {
							break;
						}
						Cell c = row1.getCell(searchColumn);
						if (c != null) {
							for (int k = 0; k < locators.size(); k++) {
								if ((c.getCellType() == CellType.STRING)
										&& c.getStringCellValue().contains(locators.get(k))) {
									System.out.println(++count + ") " + listOfFiles[i].getName() + "=" + rowNum + " == "
											+ locators.get(k));
									bFound = true;
									// System.out.println(c);
								}
							}
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		if (!bFound) {
			System.out.println("No match founf\n");
		}
	}
}