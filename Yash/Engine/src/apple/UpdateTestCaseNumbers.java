package apple;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;

public class UpdateTestCaseNumbers {
	public static void main(String[] args) {
		try {
			// excel files
			FileInputStream inputFile = new FileInputStream(new File(
					"/Applications/Automation/mob_app/MHTK_Mobile_Shared/Scripts/Financial_Accounts_Transaction_Substantiation copy.xls"));

			@SuppressWarnings("resource")
			HSSFWorkbook workbook = new HSSFWorkbook(inputFile);

			// Get first/desired sheetTestCases from the workbook
			HSSFSheet sheetTestCases = workbook.getSheet("TestCases");
			HSSFSheet sheetTestSteps = workbook.getSheet("TestSteps");
			HSSFSheet sheetUpdate = workbook.getSheet("Update");

			for (int j = sheetUpdate.getLastRowNum(); j > sheetUpdate.getFirstRowNum(); j--) {
				HSSFRow row = sheetUpdate.getRow(j);
				HSSFCell cell_0 = row.getCell(0);
				HSSFCell cell_1 = row.getCell(1);
				String old_testcase = cell_0.getStringCellValue();
				String new_testcase = cell_1.getStringCellValue();

				for (int m = sheetTestCases.getFirstRowNum(); m <= sheetTestCases.getLastRowNum(); m++) {
					HSSFRow rowTC = sheetTestCases.getRow(m);
					for (int k = rowTC.getFirstCellNum(); k < rowTC.getLastCellNum(); k++) {
						HSSFCell cell = rowTC.getCell(k);
						if (cell != null) {
							CellType type = cell.getCellType();
							if (type != CellType.NUMERIC) {
								String oldValue = cell.getStringCellValue();
								if (oldValue != null && oldValue.contains(old_testcase)) {
									String newValue = oldValue.replace(old_testcase, new_testcase);
									//System.out.println("TestCase : " + old_testcase + " to " + new_testcase);
									cell.setCellValue(newValue);
								}
							}
						}
					}
				}

				for (int n = sheetTestSteps.getFirstRowNum(); n <= sheetTestSteps.getLastRowNum(); n++) {
					HSSFRow rowTS = sheetTestSteps.getRow(n);
					for (int k = rowTS.getFirstCellNum(); k < rowTS.getLastCellNum(); k++) {
						HSSFCell cell = rowTS.getCell(k);
						if (cell != null) {
							CellType type = cell.getCellType();
							if (type != CellType.NUMERIC) {
								// if (type == CellType.STRING) {
								String oldValue = cell.getStringCellValue();
								if (oldValue != null && oldValue.contains(old_testcase)) {
									String newValue = oldValue.replace(old_testcase, new_testcase);
									// System.out.println("TestSteps: " + old_testcase + " to " + new_testcase);
									cell.setCellValue(newValue);
								}
							}
						}
					}
				}

			}

			inputFile.close();

			FileOutputStream outFile = new FileOutputStream(new File(
					"/Applications/Automation/mob_app/MHTK_Mobile_Shared/Scripts/Financial_Accounts_Transaction_Substantiation copy updated.xls"));
			workbook.write(outFile);
			outFile.close();

			System.out.println("Files were  updated successfulyy ");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}